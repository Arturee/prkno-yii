<?php
namespace app\consts;

/**
 * Class containing constants representing http methods names
 * @package app\consts
 */
class Http {
    const
        GET = 'GET',
        POST = 'POST';
}