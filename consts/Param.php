<?php
namespace app\consts;

/**
 * Class containing general parameters
 */
class Param
{
    const
        EMAIL_ROBOT = 'robot.email',
        PASSWORD_RECOVERY_TOKEN_EXPIRATION = 'password.recovery.token.expiration',
        LOGIN_EXPIRATION = 'login.expiration',

        YEAR_OF_LAST_PROJECT_STATE_UPDATE = 1996,
        PROJECT_LENGTH = '+ 9 month',
        PROJECT_DOCUMENTS_BEFORE_DEFENCE = '- 14 days',
        PROJECT_DEADLINE_FOR_ANALYSIS = '3 month',
        PROJECT_CONDITIONAL_DEFEND_DEADLINE = '3 month',

        DOMAIN_NAME = 'domainName',
        ANONYMIZATION_INACTIVITY_THRESHOLD_YEARS = '5',

        FILE_STORAGE_DIRECTORY = 'fileStorageDirectory',
        CAS_PARAMETERS = 'cas',

        CRON_TIME_PROPOSAL_DECISION_REMINDER = 'cronProposalDecision',
        CRON_TIME_DEFENCE_PARTICIPATION_REMINDER = 'cronDefenceParticipation',
        CRON_TIME_UPCOMING_DEFENCE_SETUP = 'cronUpcomingDefence',
        CRON_TIME_PROJECT_DEADLINE = 'cronProjectDeadline',

        AMOUNT_OF_HOURS_BEFORE_DEFENCE_REMINDER = 'hoursBeforeDefenceReminder',
        AMOUNT_OF_DAYS_BEFORE_DEFENCE_REMINDER = 'daysBeforeDefenceReminder',
        AMOUNT_OF_WEEKS_BEFORE_DEFENCE_REMINDER = 'weeksBeforeDefenceReminder',

        AMOUNT_OF_DAYS_BEFORE_PROJECT_DOCUMENTS_DEADLINE = '5 days',
        AMOUNT_OF_DAYS_FOR_ANALYSIS_OPEN_DEFENCE = '15 days',
        AMOUNT_OF_DAYS_FOR_IMPLEMENTATION_OPEN_DEFENCE = '30 days',

        //if you change this parameter here, you have to supply the appropriate translation in messages/app
        PAGE_NO_TITLE = '<untitled>',
        PAGE_NO_VERSION_SELECTED = 'Document is empty or no version has been selected.',

        PAGE_RESERVED_URLS = 'reservedPageURLs',

        //if you change this parameter here, you have to supply the appropriate translation in messages/app
        USER_DELETED_USER = '<deleted user>',
        MIN_USERS_ON_PROJECT = 4,

        NUMBER_OF_DAYS_TO_HIDE_GENERATED_NEWS_AFTER = '3 days',

        LOG_CATEGORY_AUDIT = 'audit';
}