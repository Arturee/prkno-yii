<?php
namespace app\consts;

/**
 * Class containing constants representing languages
 */
class Lang {
    const
        CS = 'cs',
        EN = 'en';
}