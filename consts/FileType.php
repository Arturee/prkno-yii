<?php
namespace app\consts;

/**
 * Class FileType containing constants representing file types
 */
class FileType
{
    const PDF = "application/pdf";
}