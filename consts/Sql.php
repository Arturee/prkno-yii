<?php
namespace app\consts;

use yii\db\Expression;

/**
 * Class containing sql commands and names of the mandatory database columns
 */
class Sql
{
    const
        NOW = 'NOW()',
        LIKE = 'LIKE',

        COL_ID = 'id',
        COL_DELETED = 'deleted',
        COL_CREATED_AT = 'created_at',
        COL_UPDATED_AT = 'updated_at',

        CONDITION_NOT_DELETED = [self::COL_DELETED => false];

    static function now() : Expression
    {
        return new Expression(self::NOW);
    }
}