<?php
namespace app\utils;

use app\consts\Param;
use app\exceptions\BugError;
use app\models\records\User;
use Yii;

/**
 * Class for anonymizing the users. Providing methods which
 * replace user sensitive information with default anonymized
 * data.
 */
class Anonymizer
{
    /**
     * @param array $usersToAnonymize
     * @return string
     */
    public static function createUserAnonymizationConfirmation(array $usersToAnonymize) : string
    {
        $result = "";

        if (count($usersToAnonymize) == 0) {
            return Yii::t('app', "no user") . "\n";
        }

        /** @var User $user */
        foreach ($usersToAnonymize as $user) {
            $result .= $user->getFullNameWithEmail() . " (" . Yii::t('app', User::roleToString($user->role)) . ") \n";
        }

        return $result;
    }

    /**
     * @param array $usersToAnonymize
     */
    public static function anonymizeInactiveAndDeletedUsers(array $usersToAnonymize = null) : void
    {
        if (!$usersToAnonymize) {
            $usersToAnonymize = self::getUsersWhoShouldBeAnonymized();
        }

        /** @var User $userToAnonymize */
        foreach ($usersToAnonymize as $userToAnonymize) {
            $userToAnonymize->anonymize();
        }
    }

    /**
     * @return array 
     */
    public static function getUsersWhoShouldBeAnonymized() : array
    {
        $result = [];
        $allUsers = User::find()->all();
        $now = new DateTime();
        $timeThreshold = $now->modifyClone('-' . Param::ANONYMIZATION_INACTIVITY_THRESHOLD_YEARS . ' years');
        if (!$timeThreshold) {
            throw new BugError("Anonymization inactivity threshold parameter incorrect! : " . Param::ANONYMIZATION_INACTIVITY_THRESHOLD_YEARS . ' years');
        }

        /** @var User $user */
        foreach ($allUsers as $user) {
            if ($user->isAnonymized()) {
                continue;
            }

            if ($user->deleted) {
                $result[] = $user;
            } else {
                $userLastLogin = DateTime::from($user->last_login);
                if ($userLastLogin->isBefore($timeThreshold)) {
                    $result[] = $user;
                }
            }
        }

        return $result;
    }
}