<?php
namespace app\utils;

use yii\helpers\Markdown;

/**
 * Class providing rendeing procecss of the markdown content
 */
class MarkdownRenderer
{
    /**
     * Generate markdown content and prevents injections of JavaScript
     * @param null|string $markdown     Markdown content
     * @return string HTML output of the markdown content
     */
    public static function render(? string $markdown) : string
    {
        if (!$markdown) {
            return '';
        }
        $markdown = preg_replace('@<script(.*?)>(.*?)</script>@', '', $markdown);
        return Markdown::process($markdown);
    }
}