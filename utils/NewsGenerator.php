<?php


namespace app\utils;

use app\consts\Param;
use app\models\records\DefenceDate;
use app\models\records\News;
use app\models\records\Page;

/** Handles the automatic news generation when something important happens (e.g. a new defence date is open or cancelled,
 * or when a page is updated.
 */
class NewsGenerator
{
    /** Function is called when a defence date is opened. */
    public static function defenceDateOpened(DefenceDate $defenceDate)
    {
        $titleEn = "New defence date";
        $titleCs = "Nové datum obhajoby";

        $contentEn =
            "A new defence was just opened for **$defenceDate->date**.\n\nProjects can now be assigned to this date.";
        $contentCs =
            "Nová obhajoba byla zahájena dne **$defenceDate->date**.\n\nProjekty už můžou být přihlášené k tomuto datu.";

        self::generateNews($titleEn, $titleCs, $contentEn, $contentCs);
    }

    /** Function is called when a defence date is cancelled. */
    public static function defenceDateCancelled(DefenceDate $defenceDate)
    {
        $titleEn = "Defence date cancelled";
        $titleCs = "Datum obhajoby zrušené";

        $contentEn =
            "The defence date (**$defenceDate->date**) was just **cancelled**.\n\nThe sign-up for projects is disabled. Already signed up projects are removed from the date.";
        $contentCs =
            "Datum obhajoby ($defenceDate->date) bylo **zrušené**.\n\nPřihlašování projektů je zakázáno. Přihlášené projekty jsou z obhajoby odhlášeny.";

        self::generateNews($titleEn, $titleCs, $contentEn, $contentCs);
    }

    /** This function gets called when a page is published or when for a published static page:
     * - a new page version is selected
     * - when the currently selected page version is updated
     * - the updated page is saved as a new version (the new page version automatically gets selected - so the first point
     * applies here
     */
    public static function pageUpdated(Page $page)
    {
        $titleEn = "Page updated";
        $titleCs = "Stránka aktualizovaná";

        $pageEnTitle = $page->getTitle($page->title_en, $page->title_cs);
        $pageCsTitle = $page->getTitle($page->title_cs, $page->title_en);

        $contentEn =
            "The page [$pageEnTitle](/pages/$page->url) was updated.";
        $contentCs =
            "Stránka [$pageCsTitle](/pages/$page->url) byla aktualizovaná.";

        self::generateNews($titleEn, $titleCs, $contentEn, $contentCs);
    }


    private static function generateNews(string $titleEn, string $titleCs, string $contentEn, string $contentCs)
    {
        $news = new News();

        $news->title_en = $titleEn;
        $news->title_cs = $titleCs;

        $news->content_en = $contentEn;
        $news->content_cs = $contentCs;

        $news->public = true;
        $news->type = News::TYPE_GENERATED;

        $hideAtDate = new DateTime();
        $hideAtDate->modify("+" . Param::NUMBER_OF_DAYS_TO_HIDE_GENERATED_NEWS_AFTER);
        $news->hide_at = $hideAtDate->format(DateTime::SQL_DATETIME);

        $news->save();
    }

}