<?php

namespace app\utils;

use Nette\Utils;

/**
 * Class extending Nette\Utils\DateTime
 * 
 * Example of usage:
 *
 * $now = new DateTime();
 * $now->modifyClone('+10 years, 1 month, 1 day');
 * $now->modifyClone('-10 hours');
 *
 * Notes: it is impossible to implement string to DateTimes conversion in model classes,
 * even __set() doesn't work because of how Yii is implemented. Therefore model classes
 * will always have dates as string in SQL_DATETIME format.
 */
class DateTime extends Utils\DateTime
{
    const
        DATETIME = 'd.m.Y H:i:s',
        DATETIME_MINS = 'd.m.Y H:i',
        DATE = 'd.m.Y',
        TIME_MINS = 'H:i',
        SQL_DATETIME = 'Y-m-d H:i:s',
        SQL_DATE = 'Y-m-d';

    const
        YII_DATETIME = 'dd.MM.yyyy H:i:s',
        YII_DATETIME_MINS = 'dd.MM.yyyy H:i',
        YII_DATE = 'dd.MM.yyyy';

    const FORMATS = [self::DATETIME, self::DATETIME_MINS, self::DATE, self::TIME_MINS];

    /**
     * Compare two string dates and determine
     * whether they equals
     * @param string $time1     Time 1    
     * @param string $time2     Time 2
     * @return bool True for equality, false otherwise
     */
    public static function equals(string $time1, string $time2) : bool
    {
        $dateTime1 = new DateTime($time1);
        $dateTime2 = new DateTime($time2);
        return $dateTime1 == $dateTime2;
    }

    /**
     * Compare two string dates and determine
     * whether they equals
     * @param string $time1     Time 1    
     * @param string $time2     Time 2
     * @return bool True for equality, false otherwise
     */
    public static function equalsDate(string $time1, string $time2) : bool
    {
        $date1 = (new DateTime($time1))->format(self::DATE);
        $date2 = (new DateTime($time2))->format(self::DATE);
        return $date1 == $date2;
    }

    /**
     * Create date time from parameter
     * @param $time string time in string format
     * @return DateTime Date time object
     */
    public static function from($time) : DateTime
    {
        $result = null;
        try {
            $result = parent::from($time);
        } catch (\Exception $e) {
            foreach (self::FORMATS as $format) {
                if (!$result) {
                    $result = DateTime::createFromFormat($format, $time);
                } else {
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Takes this date and apply time HH:MM from parameter
     * and return date
     * @param string $time  Time in HH:MM format
     * @return DateTime     Date time with original date and new hour and minute values
     */
    public function setHoursAndMinutes(string $time) : DateTime
    {
        return $time ? self::from($this->format(self::DATE) . " " . $time . ":00") : $this;
    }

    /**
     * @return bool true/false whether the date is in the future, or not
     */
    public function isInFuture() : bool
    {
        $now = new DateTime();
        return $now < $this;
    }

    /**
     * @return bool true/false whether the date is in the past, or not
     */
    public function isInPast() : bool
    {
        return !$this->isInFuture();
    }

    /**
     * @return string Date in string SQL date format
     */
    public function toYii() : string
    {
        return $this->format(self::SQL_DATETIME);
    }

    /**
     * @param DateTime $datetime     Reference date
     * @return bool true/false whether the date is after the date from parameter
     */
    public function isAfter(DateTime $datetime) : bool
    {
        return $this->diff($datetime)->invert === 1;
    }

    /**
     * @return bool true/false whether the date is before the date from parameter
     */
    public function isBefore(DateTime $datetime) : bool
    {
        return !$this->isAfter($datetime);
    }

    /**
     * @return string Return formatted date in Y-m-d format
     */
    public function toDate() : string
    {
        return $this->format('Y-m-d');
    }

    /**
     * @inheritdoc
     */
    public function modifyClone($modify = '') : DateTime
    {
        if ($modify === "0000-00-00 00:00:00") {
            return clone $this;
        }
        return parent::modifyClone($modify);
    }

    /**
     * @param DateTime $otherDate   Compared date time
     * @return bool true/false whether this datetime has the same day, month and year
     */
    public function isOnSameDayAs(? DateTime $otherDate) : bool
    {
        if (!$otherDate) {
            return false;
        }

        $thisDateDay = $this->toDate();
        $otherDateDay = $otherDate->toDate();

        return $thisDateDay == $otherDateDay;
    }

    /**
     * @return array Array with key 'dateFormat' and value self::YII_DATETIME
     */
    public static function getDatetimeOptions() : array
    {
        return ['dateFormat' => self::YII_DATETIME];
    }

    /**
     * @return array Array with key 'dateFormat' and value self::YII_DATE
     */
    public static function getDateOptions() : array
    {
        return ['dateFormat' => self::YII_DATE];
    }

    /**
     * @param string $date  Date time in string format
     * @return string Date from paramter in self::DATE format
     */
    public static function date(string $date) : string
    {
        return DateTime::from($date)->format(self::DATE);
    }

    /**
     * @param string $date  Date time in string format
     * @return string Date from paramter in self::TIME_MINS format
     */
    public static function time(string $date) : string
    {
        return DateTime::from($date)->format(self::TIME_MINS);
    }

    /**
     * @param string|null $date  Date time or null
     * @return string|null Null if parameter is null, otherwise date in self::DATETIME format
     */
    public static function dateTime(? string $date) : ? string
    {
        if (is_null($date)) {
            return null;
        }
        return DateTime::from($date)->format(self::DATETIME);
    }

    /**
     * @param string $dateString    Date time in string
     * @return string Given date time from parameter in DATE_ISO8601 format
     */
    public static function toJsonDateFormat(?string $dateString) : ?string
    {
        if (!$dateString) {
            return null;
        }
        $date = date_create_from_format(self::SQL_DATETIME, $dateString);
        return $date->format(DATE_ISO8601);
    }
}