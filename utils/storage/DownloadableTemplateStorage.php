<?php
namespace app\utils\storage;

use Yii;
use yii2tech\filestorage\local\Bucket;

/**
 * Class implementing FileStorage for downloading markdown templates
 * @Author: Gergely
 */
class DownloadableTemplateStorage extends FileStorage
{
    protected function getSpecificBucket() : Bucket
    {
        return Yii::$app->fileStorage->getBucket('templatesToDownload');
    }
}