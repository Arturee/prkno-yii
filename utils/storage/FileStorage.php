<?php
namespace app\utils\storage;

use Yii;
use yii2tech\filestorage\local\Bucket;

/**
 * Class implementing functionality for storing uploading and storing files
 * File storage extension is at link:
 * https://github.com/yii2tech/file-storage
 * @Author: Gergely
 */
class FileStorage
{
    /**
     * You can optionally specify the array which lists the files present in the bucket to speed up to process
     * (this saves the continuous fileExists call which has to look into the directory every time, while with the
     * array we only check the filenames in memory)
     * @param string $fullFileName  Full file name
     * @param Bucket $bucket        Bucket
     * @param array|NULL $filesInFolder     List of files in which the file name will be search
     * @return string fullFileName if not exists, or new file name if otherwise
     */
    public static function getNewFileNameIfNecessary(string $fullFileName, Bucket $bucket = null, array $filesInFolder = null) : string
    {
        if (!$bucket) {
            $bucket = static::getSpecificBucket();
        }

        if ($bucket->fileExists($fullFileName)) {
            $filesInFolder = $filesInFolder ? $filesInFolder : self::getFiles($bucket);
            return static::createNewFileNameForDuplicates($fullFileName, $filesInFolder);
        } else {
            return $fullFileName;
        }
    }

    /**
     * Saves content as new file.
     * @param string $fileName  New file name.
     * @param string $content   New file content.
     * @param bool $shouldOverwrite True for getting new file if necessary
     * @return bool success.
     */
    public static function saveFileContent(string $fileName, string $content, bool $shouldOverwrite = true) : bool
    {
        $bucket = static::getSpecificBucket();
        if ($shouldOverwrite) {
            $fileName = static::getNewFileNameIfNecessary($fileName, null);
        }
        return $bucket->saveFileContent($fileName, $content);
    }

    /**
     * Returns content of an existing file.
     * @param string $fileName  New file name.
     * @return string $content  File content.
     */
    public static function getFileContent(string $fileName) : string
    {
        $bucket = static::getSpecificBucket();
        return $bucket->getFileContent($fileName);
    }

    /**
     * Deletes an existing file.
     * @param string $fileName - new file name.
     * @return bool success.
     */
    public static function deleteFile(string $fileName) : bool
    {
        $bucket = static::getSpecificBucket();
        return $bucket->deleteFile($fileName);
    }

    /**
     * Checks if the file exists in the bucket.
     * @param string $fileName - searching file name.
     * @return bool file exists.
     */
    public static function fileExists(string $fileName) : bool
    {
        $bucket = static::getSpecificBucket();
        return $bucket->fileExists($fileName);
    }

    /**
     * Copies file from the OS file system into the bucket.
     * @param string $srcFileName   OS full file name.
     * @param string $fileName      New bucket file name.
     * @param bool $shouldOverwrite True for getting new file if necessary
     * @return bool success.
     */
    public static function copyFileIn(string $srcFileName, string $fileName, bool $shouldOverwrite = false) : bool
    {
        $bucket = static::getSpecificBucket();
        if (!$shouldOverwrite) {
            $fileName = static::getNewFileNameIfNecessary($fileName, null);
        }

        $uploadedFile = fopen($srcFileName, 'r'); //PHP style
        $bucketFile = $bucket->openFile($fileName, 'w');

        while (!feof($uploadedFile)) {
            $filePart = fread($uploadedFile, 512000);
            fwrite($bucketFile, $filePart);
        }

        fclose($uploadedFile);
        fclose($bucketFile);

        return true;

    }

    /**
     * Store source file
     * @param string $sourceFilePath    Source file path
     * @param string $fileName          Name of the stored file
     * @param string $shouldOverwrite   True for overwriting the original file
     * @return string file web URL
     * @throws \Exception if file cannot be stored (reasons unknown)
     */
    public static function store(string $sourceFilePath, string $fileName, bool $shouldOverwrite = false) : string
    {
        $ok = self::copyFileIn($sourceFilePath, $fileName, $shouldOverwrite);
        if (!$ok) {
            throw new \Exception('file cannot be stored');
        }
        Yii::$app->session->addFlash('success', Yii::t('app', 'File stored'));
        return self::getFileUrl($fileName);
    }

    /**
     * Copies file from the bucket into the OS file system.
     * @param string $fileName - bucket existing file name.
     * @param string $destFileName - new OS full file name.
     * @return bool success.
     */
    public static function copyFileOut(string $fileName, string $destFileName) : bool
    {
        $bucket = static::getSpecificBucket();
        return $bucket->copyFileOut($fileName, $destFileName);
    }

    /**
     * Gets web URL of the file.
     * @param string $fileName - self file name.
     * @return string file web URL.
     */
    public static function getFileUrl(string $fileName) : string
    {
        $bucket = static::getSpecificBucket();
        return $bucket->getFileUrl($fileName);
    }

    protected function getSpecificBucket() : Bucket
    {
        return Yii::$app->fileStorage->getBucket('otherFiles');
    }

    private static function getFiles(Bucket $bucket = null) : ? array
    {
        if (!$bucket) {
            $bucket = static::getSpecificBucket();
        }

        $fullDirectoryName = $bucket->getFullBasePath();

        if (file_exists($fullDirectoryName)) {
            $allFiles = scandir($fullDirectoryName);
            $filesWithoutDots = array_slice($allFiles, 2);
            return $filesWithoutDots;
        } else {
            return null;
        }
    }

    private function isFileNameAlreadyPresent(string $newFileName, array $filesInFolder) : bool
    {
        foreach ($filesInFolder as $file) {
            $existingFileName = pathinfo($file, PATHINFO_FILENAME);
            if ($existingFileName == $newFileName) {
                return true;
            }
        }

        return false;
    }

    // Finds the last string occurence in the string and replaces it with the new value.
    private function replaceFileName(string $fullPath, string $oldName, string $newName) : string
    {
        return substr_replace($fullPath, $newName, strrpos($fullPath, $oldName), strlen($oldName));
    }

    private function createNewFileNameForDuplicates(string $fullFileName, array $filesInFolder = null) : string
    {
        $fileName = pathinfo($fullFileName, PATHINFO_FILENAME);

        $fileCounter = 1;
        $newFileName = $fileName . "_$fileCounter";

        while (static::isFileNameAlreadyPresent($newFileName, $filesInFolder)) {
            $fileCounter++;
            $newFileName = $fileName . "_$fileCounter";
        }

        return static::replaceFileName($fullFileName, $fileName, $newFileName);
    }
}