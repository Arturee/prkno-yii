<?php
namespace app\utils\storage;

use app\models\records\ProjectDefence;
use Yii;
use yii2tech\filestorage\local\Bucket;

/**
 * @Author: Gergely
 * Use this class to upload the defence files - the opponents and supervisors review and the defence statement.
 */
class DefenceStorage
{
    const
        FILETYPE_OPPONENT_REVIEW = 'OPPONENT_REVIEW',
        FILETYPE_SUPERVISOR_REVIEW = 'SUPERVISOR_REVIEW',
        FILETYPE_DEFENCE_STATEMENT = 'DEFENCE_STATEMENT',
        ALLOWED_FILE_EXTENSION = "PDF";

    /**
     * Saves string content as file. Currently it has the functionality to submit the DEFENCE_PDF as markdown,
     * although that will overwrite the existing DEFENCE_PDF if present.
     * @param string $fileName  File name
     * @param string $content   Content
     * @param int $projectId    Project id of the defence 
     * @param string $projectFileType from class ProjectFileType constant
     * @param int|null $defenceNumber   Defence number (1 - analysis, 2 - First final defence, 3 - Second final defence)
     * @return bool     True if the file was successfully saved, false otherwise
     */
    public static function saveDefenceFileContent(
        int $projectId,
        int $defenceId,
        string $defenceFileType,
        string $content
    ) : bool {
        $filePath = self::composeFilePath($projectId, $defenceId, $defenceFileType);
        if (!$filePath) {
            return false;
        }

        return self::getSpecificBucket()->saveFileContent($filePath, $content);
    }

    /**
     * Get file content of the document uploaded for the defence of the
     * particular project
     * @param int $projectId    Project id the file belongs to
     * @param int $defenceId    Id of the defence file is uploaded for
     * @param string $defenceFileType   Type of the file listed in DefenceStorage consts
     * @return string|null Get file content
     */
    public static function getDefenceFileContent(int $projectId, int $defenceId, string $defenceFileType) : ? string
    {
        $filePath = self::composeFilePath($projectId, $defenceId, $defenceFileType);
        if (!$filePath) {
            return null;
        }
        return self::getSpecificBucket()->getFileContent($filePath);
    }

    /**
     * Deletes an existing file in the bucket
     * @param int $projectId    Project id the file belongs to
     * @param int $defenceId    Defence id
     * @param string $defenceFileType   Defence file type listed in DefenceStorage consts
     * @return bool ture for success, false otherwise
     */
    public static function deleteDefenceFile(int $projectId, int $defenceId, string $defenceFileType) : bool
    {
        $filePath = self::composeFilePath($projectId, $defenceId, $defenceFileType);
        if (!$filePath) {
            return false;
        }
        return self::getSpecificBucket()->deleteFile($filePath);
    }

    /**
     * Checks if the file exists in the bucket
     * @param int $projectId    Project id
     * @param int $defenceId    Defence id
     * @param string $defenceFileType   Defence file type listed in DefenceStorage consts
     * @return bool True if file exists.
     */
    public static function defenceFileExists(int $projectId, int $defenceId, string $defenceFileType) : bool
    {
        $filePath = self::composeFilePath($projectId, $defenceId, $defenceFileType);
        if (!$filePath) {
            return false;
        }
        return self::getSpecificBucket()->fileExists($filePath);
    }

    /**
     * Store file for project defence
     * @param string $sourceFilePath    Source file path
     * @param string $fileName          Name of the stored file
     * @param string $shouldOverwrite   True for overwriting the original file
     * @return string file web URL
     * @throws \Exception if file cannot be stored (reasons unknown)
     */
    public static function store(string $sourceFilePath, ProjectDefence $defence, string $defenceFileType) : string
    {
        $ok = self::copyDefenceFileIn($sourceFilePath, $defence->project_id, $defence->id, $defenceFileType);
        if (!$ok) {
            throw new \Exception('file cannot be stored');
        }
        return self::getDefenceFileUrl($defence->project_id, $defence->id, $defenceFileType);
    }

    /**
     * Copies file from the OS file system into the bucket.
     * @param string $srcFileName - OS full file name.
     * @param string $fileName - new bucket file name.
     * @return bool success.
     */
    public static function copyDefenceFileIn(string $srcFileName, int $projectId, int $defenceId, string $defenceFileType) : bool
    {
        /*if (!self::doesFileHaveValidExtension($srcFileName))
        {
            Yii::error("PDF file expected, the extension of the file is something else! Filename: $srcFileName", __METHOD__);
            return false;
        }*/

        $filePath = self::composeFilePath($projectId, $defenceId, $defenceFileType);
        if (!$filePath) {
            return false;
        }

        return self::getSpecificBucket()->copyFileIn($srcFileName, $filePath);
    }

    /**
     * Copies file from the bucket into the OS file system.
     * @param int $projectId    Project id
     * @param int $defenceId    Defence id
     * @param string $defenceFileType   File type (pdf, zip, other, ...)
     * @param string $destFileName - new OS full file name.
     * @return bool True for success, false otherwise
     */
    public static function copyDefenceFileOut(int $projectId, int $defenceId, string $defenceFileType, string $destFileName) : bool
    {
        if (!self::doesFileHaveValidExtension($destFileName)) {
            Yii::error("PDF file expected, the extension of the destination file is something else! Filename: $destFileName", __METHOD__);
            return false;
        }

        $filePath = self::composeFilePath($projectId, $defenceId, $defenceFileType);
        if (!$filePath) {
            return false;
        }
        return self::getSpecificBucket()->copyFileOut($filePath, $destFileName);
    }

    /**
     * Gets web URL of the file.
     * @param int $projectId    Project id
     * @param int $defenceId    Defence id
     * @param string $defenceFileType - self file name.
     * @return string|null file web URL.
     */
    public static function getDefenceFileUrl(int $projectId, int $defenceId, string $defenceFileType) : ? string
    {
        $filePath = self::composeFilePath($projectId, $defenceId, $defenceFileType);
        if (!$filePath) {
            return null;
        }
        return self::getSpecificBucket()->getFileUrl($filePath);
    }

    /**
     * @param int $projectId    Project id
     * @return array|null List of all files uploaded for given defence for the project
     */
    public static function getAllDefenceFilesForProject(int $projectId) : ? array
    {
        /** @var Bucket $bucket */
        $bucket = static::getSpecificBucket();

        $directoryName = $projectId . '/defence/';
        $fullDirectoryName = $bucket->getFullFileName($directoryName);

        if (!file_exists($fullDirectoryName)) {
            return null;
        }

        $allDefenceFiles = [];

        $allDefenceDirectories = scandir($fullDirectoryName);
        $directoriesWithoutDots = array_slice($allDefenceDirectories, 2);

        foreach ($directoriesWithoutDots as $directoryNumber) {
            $defenceFiles = self::getSpecificDefenceFiles($projectId, $directoryNumber);
            if ($defenceFiles) {
                $allDefenceFiles[$directoryNumber] = $defenceFiles;
            }
        }

        return $allDefenceFiles;
    }

    protected static function getSpecificBucket() : Bucket
    {
        return Yii::$app->fileStorage->getBucket('defenceUploads');
    }

    private static function getSpecificDefenceFiles(int $projectId, int $defenceId) : ? array
    {
        /** @var Bucket $bucket */
        $bucket = static::getSpecificBucket();

        $directoryName = $projectId . '/defence/' . $defenceId . '/';
        $fullDirectoryName = $bucket->getFullFileName($directoryName);

        if (file_exists($fullDirectoryName)) {
            $allFiles = scandir($fullDirectoryName);
            $filesWithoutDots = array_slice($allFiles, 2);
            return $filesWithoutDots;
        } else {
            return null;
        }
    }

    private static function getFileNameBasedOnType(string $defenceFileType) : string
    {
        return $defenceFileType . '.' . self::ALLOWED_FILE_EXTENSION;
    }

    private static function doesFileHaveValidExtension(string $fullFileName) : bool
    {
        $extension = pathinfo($fullFileName, PATHINFO_EXTENSION);

        if (!$extension) {
            return false;
        }

        $extension = strtoupper($extension);

        return $extension == self::ALLOWED_FILE_EXTENSION;
    }

    private static function composeFilePath(int $projectId, int $defenceId, string $defenceFileType) : ? string
    {
        if (!self::isValidDefenceFileType($defenceFileType)) {
            Yii::error("Defence file type $defenceFileType INVALID!", __METHOD__);
            return null;
        }

        $newFileName = self::getFileNameBasedOnType($defenceFileType);
        $newFileFullName = $projectId . '/defence/' . $defenceId . '/' . $newFileName;

        return $newFileFullName;
    }

    private static function isValidDefenceFileType(string $defenceFileType) : bool
    {
        return $defenceFileType == self::FILETYPE_OPPONENT_REVIEW
            || $defenceFileType == self::FILETYPE_SUPERVISOR_REVIEW
            || $defenceFileType == self::FILETYPE_DEFENCE_STATEMENT;
    }
}