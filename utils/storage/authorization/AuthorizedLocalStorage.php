<?php
namespace app\utils\storage\authorization;

use yii2tech\filestorage\local\Storage;

/**
 * Storage class for authorized actions with files as viewing and downloading
 */
class AuthorizedLocalStorage extends Storage
{
    /**
     * @inheritdoc
     */
    public $bucketClassName = 'app\utils\storage\authorization\AuthorizedLocalBucket';
}