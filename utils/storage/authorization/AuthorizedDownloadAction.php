<?php
namespace app\utils\storage\authorization;

use app\consts\Permission;
use app\models\records\ProjectDocument;
use app\models\records\User;
use yii\di\Instance;
use yii\web\NotFoundHttpException;
use yii2tech\filestorage\DownloadAction;
use app\models\records\Document;
use app\models\records\ProjectDefence;
use yii;

class AuthorizedDownloadAction extends DownloadAction
{
    /**
     * Runs the download action.
     * @param string $bucket name of the file source bucket
     * @param string $filename name of the file to be downloaded.
     * @return \yii\web\Response response.
     * @throws NotFoundHttpException if bucket or file does not exist.
     */
    public function run($bucket, $filename)
    {
        $this->fileStorage = Instance::ensure($this->fileStorage, 'yii2tech\filestorage\StorageInterface');
        if (!$this->fileStorage->hasBucket($bucket)) {
            throw new NotFoundHttpException(Yii::t('app', "Bucket '{0}' does not exist.", $bucket));
        }

        /** @var AuthorizedLocalBucket $authorizedBucket */
        $authorizedBucket = $this->fileStorage->getBucket($bucket);

        if (!$this->isAuthorizedToDownload($authorizedBucket, $filename))
        {
            throw new NotFoundHttpException(Yii::t('app', 'Access to file is denied.'));
        }

        return parent::run($bucket, $filename);
    }

    protected function isAuthorizedToDownload(AuthorizedLocalBucket $bucket, string $fileName): bool
    {
        $permissionsForBucket = $bucket->getPermissions();

        $fallback = Yii::$app->request->baseUrl."/files/download?bucket=" . urlencode($bucket->name) . "&filename=" . urlencode($fileName);

        $document = Document::findOne(['fallback_path' => $fallback, 'deleted' => false]);
        if (!$document)
        {
            Yii::error("Cannot find document with fallback: $fallback", __METHOD__);
            return false;
        }

        $userRole = $this->getRoleOfUserInSession();

        if ($this->isUserOnRoleWhiteList($bucket->getRoleWhiteList(), $userRole))
        {
            return true;
        }

        if ($this->isUserOnRoleBlackList($bucket->getRoleBlackList(), $userRole))
        {
            return false;
        }

        if (empty($permissionsForBucket))
        {
            return true;
        } else {
            return $this->isUserAuthorized($document, $permissionsForBucket);
        }
    }

    private function isUserAuthorized(Document $document, array $permissionsForBucket) : bool
    {
        $params = $this->populateRuleParameters($document);

        foreach ($permissionsForBucket as $permission)
        {
            $authorized = \Yii::$app->user->can($permission, $params);
            if (!$authorized)
            {
                return false;
            }
        }

        return true;
    }

    private function isUserOnRoleWhiteList(array $roleWhiteList, ?string $userRole) : bool
    {
        return in_array($userRole, $roleWhiteList);
    }

    private function isUserOnRoleBlackList(array $roleBlackList, ?string $userRole) : bool
    {
        return in_array($userRole, $roleBlackList);
    }

    private function getRoleOfUserInSession() : ?string
    {
        $user = User::findOne(['id' => \Yii::$app->user->id]);

        if (!$user)
        {
            return null;
        }

        return $user->role;
    }

    private function populateRuleParameters(Document $document) : array
    {
        $params = [Permission::PARAM_DOCUMENT => $document];

        $project = null;

        $projectDocument = ProjectDocument::findOne(['document_id' => $document->id]);
        if ($projectDocument)
        {
            $project = $projectDocument->project;
        } else {
            /** @var ProjectDefence $projectDefence */
            $projectDefence = ProjectDefence::find()
                ->orWhere(['defence_statement' => $document->id])
                ->orWhere(['opponent_review' => $document->id])
                ->orWhere(['supervisor_review' => $document->id])
                ->one();

            if ($projectDefence)
            {
                $project = $projectDefence->project;
            }
        }

        $params[Permission::PARAM_PROJECT] = $project;
        $params[Permission::PARAM_PROJECT_DOCUMENT] = $projectDocument;

        return $params;
    }



}