<?php
namespace app\utils\storage\authorization;

use yii2tech\filestorage\local\Bucket;

/**
 * Bucket for authorized access. Implements getting and setting
 * array storing allowed user roles, who are allowed for to view
 * and download files from this bucket.
 */
class AuthorizedLocalBucket extends Bucket
{
    /**
     * @var array of Permissions who are allowed to view the files and download from this
     * bucket.
     */
    private $permissions = [];

    /**
     * @var array of roles. If the user in session has the role specified, he has access to the bucket.
     */
    private $roleWhiteList = [];

    /**
     * @var array of roles. If the user in session has the role specified, his access is denied immediately.
     */
    private $roleBlackList = [];

    /**
     * @param allowedFor array of Permissions who are allowed to view the files and download from this bucket
     */
    public function setPermissions(array $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * @return array of Permissions who are allowed to view the files and download from this bucket
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param array $roleWhiteList the roles who have immediate access to the bucket
     */
    public function setRoleWhiteList(array $roleWhiteList)
    {
        $this->roleWhiteList = $roleWhiteList;
    }

    /**
     * @return array the roles who have immediate access to the bucket
     */
    public function getRoleWhiteList()
    {
        return $this->roleWhiteList;
    }

    /**
     * @param array $roleWhiteList the roles who have their access immediately denied
     */
    public function setRoleBlackList(array $roleBlackList)
    {
        $this->roleBlackList = $roleBlackList;
    }

    /**
     * @return array the roles who have their access immediately denied
     */
    public function getRoleBlackList()
    {
        return $this->roleBlackList;
    }

}