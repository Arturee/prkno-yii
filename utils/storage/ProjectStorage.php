<?php
namespace app\utils\storage;

use app\models\records\ProjectDocument;
use Yii;
use yii2tech\filestorage\local\Bucket;

/**
 * Use this class to upload various project files including the PDF and ZIP files that the team submits for the
 * defence
 * Class ProjectStorage
 * @Author: Gergely
 */
class ProjectStorage
{
    const
        FILETYPE_DEFENCE_PDF = 'DEFENCE_PDF_',
        FILETYPE_DEFENCE_ZIP = 'DEFENCE_ZIP_',
        FILETYPE_DOCUMENT = 'OTHER';

    /**
     * Saves string content as file. Currently it has the functionality to submit the DEFENCE_PDF as markdown,
     * although that will overwrite the existing DEFENCE_PDF if present.
     * @param int $projectId    Project id
     * @param string $projectFileType From class ProjectFileType constant
     * @param string $fileName  File name
     * @param string $content   Content
     * @param int|null $defenceNumber   Defence number (1 - 3)
     * @return bool     True for success, false otherwise
     */
    public static function saveProjectFileContent(
        int $projectId,
        string $projectFileType,
        string $fileName,
        string $content,
        ? int $defenceNumber = null
    ) : bool {
        if ($projectFileType == self::FILETYPE_DEFENCE_ZIP) {
            Yii::error("Uploading project file marked as type DEFENCE_ZIP can't be submitted from string! Filename: $fileName", __METHOD__);
            return false;
        }

        $filePath = self::composeFilePath($projectId, $fileName, $projectFileType, $defenceNumber);
        if (!$filePath) {
            return false;
        }

        $filePath = self::getNewNameIfNecessary($projectId, $projectFileType, $filePath);

        return self::getSpecificBucket()->saveFileContent($filePath, $content);
    }

    /**
     * Get file content of the file uploaded for the project
     * @param int $projectId            Project id
     * @param string $projectFileType   Project file type
     * @param string $fileName          File name
     * @param null|int $defenceNumber   Defence number or null
     * @return string|null  Content of the file
     */
    public static function getProjectFileContent(
        int $projectId,
        string $projectFileType,
        string $fileName,
        ? int $defenceNumber = null
    ) : ? string {
        if ($projectFileType == self::FILETYPE_DEFENCE_ZIP) {
            Yii::error("Project file marked as type DEFENCE_ZIP can't be downloaded as string! Filename: $fileName", __METHOD__);
            return null;
        }

        $filePath = self::composeFilePath($projectId, $fileName, $projectFileType, $defenceNumber);
        if (!$filePath) {
            return null;
        }
        return self::getSpecificBucket()->getFileContent($filePath);
    }

    /**
     * Deletes an existing file.
     * @param int $projectId            Project id
     * @param string $projectFileType   Project file type (pdf, zip, other)
     * @param string $fileName          New file name.     
     * @param null|int $defenceNumber   Defence number or null
     * @return bool True if success, false otherwise
     */
    public static function deleteProjectFile(
        int $projectId,
        string $projectFileType,
        string $fileName,
        ? int $defenceNumber = null
    ) : bool {
        $filePath = self::composeFilePath($projectId, $fileName, $projectFileType, $defenceNumber);
        if (!$filePath) {
            return false;
        }
        return self::getSpecificBucket()->deleteFile($filePath);
    }

    /**
     * Checks if the file exists in the bucket.
     * @param int $projectId            Project id
     * @param string $projectFileType   Project file type (pdf, zip, other)
     * @param string $fileName - searching file name.
     * @param null|int $defenceNumber   Defence number or null if project file is not associated with any project defence
     * @return bool True if the file exists, false if not
     */
    public static function projectFileExists(
        int $projectId,
        string $projectFileType,
        string $fileName,
        ? int $defenceNumber = null
    ) : bool {
        $filePath = self::composeFilePath($projectId, $fileName, $projectFileType, $defenceNumber);
        if (!$filePath) {
            return false;
        }
        return self::getSpecificBucket()->fileExists($filePath);
    }

    /**
     * Store file for particular project with document type (pdf, zip or other)
     * with filename. Document can be associated for particular defence with
     * given number.
     * @param string $sourceFilePath        Source file path
     * @param int $projectId                Project id
     * @param string $projectDocumentType   Type of the document
     * @param string $fileName              File name
     * @param int|null $defenceNumber       Defence number, or null if file is not associated with any project defence
     * @return string Url of the project file
     * @throws \Exception if file cannot be stored (reasons unknown)
     */
    public static function store(
        string $sourceFilePath,
        int $projectId,
        string $projectDocumentType,
        string $fileName,
        ? int $defenceNumber = null
    ) : string {
        $projectFileType = self::getProjectFileType($projectDocumentType);
        $ok = self::copyProjectFileIn($sourceFilePath, $projectId, $projectFileType, $fileName, $defenceNumber);
        if (!$ok) {
            throw new \Exception('file cannot be stored');
        }
        Yii::$app->session->addFlash('success', Yii::t('app', 'Documents stored'));
        return self::getProjectFileUrl($projectId, $projectFileType, $fileName, $defenceNumber);
    }

    /**
     * Copies file from the OS file system into the bucket.
     * @param string $srcFileName - OS full file name.
     * @param int $projectId            Project id
     * @param string $projectFileType   Project file type
     * @param string $fileName          New bucket file name.
     * @param null|int $defenceNumber   Defence number (1-3)
     * @return bool True for success, false otherwise
     */
    public static function copyProjectFileIn(
        string $srcFileName,
        int $projectId,
        string $projectFileType,
        string $fileName,
        ? int $defenceNumber = null
    ) : bool {
        /*if (!self::isFileTheRightExtension($srcFileName, $projectFileType))
        {
            Yii::error("PDF or ZIP file expected, the extension of the file is neither! Filename: $srcFileName", __METHOD__);
            return false;
        }*/

        $filePath = self::composeFilePath($projectId, $fileName, $projectFileType, $defenceNumber);
        if (!$filePath) {
            return false;
        }
        $filePath = self::getNewNameIfNecessary($projectId, $projectFileType, $filePath);
        return self::getSpecificBucket()->copyFileIn($srcFileName, $filePath);
    }

    /**
     * Copies file from the bucket into the OS file system.
     * @param int $projectId            Project id
     * @param string $projectFileType   Project file type from local consts
     * @param string $fileName          Bucket existing file name.
     * @param null|int $defenceNumber   Defence number or null
     * @param string $destFileName - new OS full file name.
     * @return bool success.
     */
    public static function copyProjectFileOut(
        int $projectId,
        string $projectFileType,
        string $fileName,
        ? int $defenceNumber = null,
        string $destFileName
    ) : bool {
        if (!self::isFileTheRightExtension($destFileName, $projectFileType)) {
            Yii::error("PDF or ZIP file expected, the extension of the file is neither! Filename: $destFileName", __METHOD__);
            return false;
        }

        $filePath = self::composeFilePath($projectId, $fileName, $projectFileType, $defenceNumber);
        if (!$filePath) {
            return false;
        }
        return self::getSpecificBucket()->copyFileOut($filePath, $destFileName);
    }

    /**
     * Gets web URL of the file.
     * @param int $projectId            Project id
     * @param string $projectFileType   Project file type
     * @param string $fileName          Self file name.
     * @param null|int $defenceNumber   Defence number
     * @return null|string File web URL.
     */
    public static function getProjectFileUrl(
        int $projectId,
        string $projectFileType,
        string $fileName,
        ? int $defenceNumber = null
    ) : ? string {
        $filePath = self::composeFilePath($projectId, $fileName, $projectFileType, $defenceNumber);
        if (!$filePath) {
            return null;
        }
        return self::getSpecificBucket()->getFileUrl($filePath);
    }

    /**
     * @param int $projectId    Project id
     * @return array|null   List of uploaded files for the project
     */
    public static function getProjectFiles(int $projectId) : ? array
    {
        /** @var Bucket $bucket */
        $bucket = static::getSpecificBucket();

        $directoryName = $projectId . '/uploads/';
        $fullDirectoryName = $bucket->getFullFileName($directoryName);

        if (file_exists($fullDirectoryName)) {
            $allFiles = scandir($fullDirectoryName);
            $filesWithoutDots = array_slice($allFiles, 2);
            return $filesWithoutDots;
        } else {
            return null;
        }
    }

    protected static function getSpecificBucket() : Bucket
    {
        return \Yii::$app->fileStorage->getBucket('projectUploads');
    }

    private static function modifyFileNameBasedOnType(string $fileName, string $projectFileType, ? int $defenceNumber = null) : string
    {
        if ($projectFileType == self::FILETYPE_DEFENCE_PDF) {
            return self::FILETYPE_DEFENCE_PDF . $defenceNumber . ".PDF";
        } else if ($projectFileType == self::FILETYPE_DEFENCE_ZIP) {
            return self::FILETYPE_DEFENCE_ZIP . $defenceNumber . ".ZIP";
        } else {
            return $fileName;
        }
    }

    private static function isFileTheRightExtension(string $fileName, string $projectFileType) : bool
    {
        if ($projectFileType == self::FILETYPE_DOCUMENT) {
            return true;
        }

        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        if (!$extension) {
            return false;
        }

        $extension = strtoupper($extension);

        if ($projectFileType == self::FILETYPE_DEFENCE_PDF && $extension == "PDF") {
            return true;
        }
        if ($projectFileType == self::FILETYPE_DEFENCE_ZIP && $extension == "ZIP") {
            return true;
        }

        return false;
    }

    private static function composeFilePath(int $projectId, string $fileName, string $projectFileType, ? int $defenceNumber = null) : ? string
    {
        if (!self::isValidProjectFileType($projectFileType)) {
            Yii::error("Project file type $projectFileType INVALID: $fileName", __METHOD__);
            return null;
        }

        if ($projectFileType == self::FILETYPE_DEFENCE_PDF || $projectFileType == self::FILETYPE_DEFENCE_ZIP) {
            if ($defenceNumber == null) {
                Yii::error("File marked as file for defence, but defence number is not submitted! Filename: $fileName", __METHOD__);
                return null;
            }
        }

        $newFileName = self::modifyFileNameBasedOnType($fileName, $projectFileType, $defenceNumber);
        $newFileFullName = $projectId . '/uploads/' . $newFileName;

        return $newFileFullName;
    }

    private static function isValidProjectFileType(string $projectFileType) : bool
    {
        return $projectFileType == self::FILETYPE_DEFENCE_PDF
            || $projectFileType == self::FILETYPE_DEFENCE_ZIP
            || $projectFileType == self::FILETYPE_DOCUMENT;
    }

    private static function getNewNameIfNecessary(int $projectId, string $projectFileType, $newFileFullName) : string
    {
        if ($projectFileType == self::FILETYPE_DOCUMENT) {
            $projectFiles = self::getProjectFiles($projectId);
            $newFileFullName = FileStorage::getNewFileNameIfNecessary($newFileFullName, self::getSpecificBucket(), $projectFiles);
        }

        return $newFileFullName;
    }

    private static function getProjectFileType(string $documentType) : string
    {
        $projectFileType = ProjectStorage::FILETYPE_DOCUMENT;
        if ($documentType == ProjectDocument::TYPE_FOR_DEFENSE_ZIP) {
            $projectFileType = ProjectStorage::FILETYPE_DEFENCE_ZIP;
        } elseif ($documentType == ProjectDocument::TYPE_FOR_DEFENSE_PDF) {
            $projectFileType = ProjectStorage::FILETYPE_DEFENCE_PDF;
        }
        return $projectFileType;
    }
}