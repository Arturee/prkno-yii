<?php
namespace app\utils\storage;

use Yii;
use yii2tech\filestorage\local\Bucket;

/**
 * Class for storing markdown templates
 * @Author: Gergely
 */
class MarkdownTemplateStorage extends FileStorage
{
    protected function getSpecificBucket() : Bucket
    {
        return Yii::$app->fileStorage->getBucket('templatesMarkdown');
    }
}