<?php
namespace app\utils\storage;

use Yii;
use yii2tech\filestorage\local\Bucket;

/**
 * Class for storing static pages
 * @Author: Gergely
 */
class PageStorage extends FileStorage
{
    protected function getSpecificBucket() : Bucket
    {
        return Yii::$app->fileStorage->getBucket('pages');
    }
}