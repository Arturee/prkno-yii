<?php
return [
    'USERS_MANAGE' => [
        'type' => 2,
        'description' => 'Manage users',
    ],
    'USERS_DELETE' => [
        'type' => 2,
        'description' => 'Delete user',
    ],
    'USERS_VIEW_NAME' => [
        'type' => 2,
        'description' => 'View the names / emails of other users',
    ],
    'USERS_UPDATE_OWN_PROFILE' => [
        'type' => 2,
        'description' => 'Update own profile',
        'ruleName' => 'isProfileOwner',
    ],
    'PROJECT_CREATE' => [
        'type' => 2,
        'description' => 'Create a project',
    ],
    'PROJECT_UPDATE' => [
        'type' => 2,
        'description' => 'Update project',
    ],
    'PROJECT_UPDATE_OWN_PROJECT' => [
        'type' => 2,
        'description' => 'Update own project',
        'ruleName' => 'canUpdateOwnProject',
    ],
    'PROJECT_SEND_DOCUMENTS' => [
        'type' => 2,
        'description' => 'Send documents to defence',
        'ruleName' => 'canDefenceDocumentsOwnProject',
    ],
    'PROJECT_UPDATE_STATUS' => [
        'type' => 2,
        'description' => 'Update project status',
    ],
    'PROJECT_UPDATE_CONTENT' => [
        'type' => 2,
        'description' => 'Update project content',
        'ruleName' => 'isProjectMember',
    ],
    'PROJECT_UPDATE_ADVICE' => [
        'type' => 2,
        'description' => 'Update project advice',
        'ruleName' => 'isProjectMember',
    ],
    'PROJECT_UPLOAD_FILE' => [
        'type' => 2,
        'description' => 'Upload project files',
        'ruleName' => 'isOnProjectUser',
    ],
    'PROJECT_CAN_DOWNLOAD_FILE' => [
        'type' => 2,
        'description' => 'Download project files',
        'ruleName' => 'canDownloadProjectFile',
    ],
    'PROJECT_IS_ASSIGNED' => [
        'type' => 2,
        'description' => 'Is user assigned to project',
        'ruleName' => 'isProjectAssigned',
    ],
    'PROJECT_ASSIGN_TO_DEFENCE' => [
        'type' => 2,
        'description' => 'Assign project to defence',
        'ruleName' => 'isOnProjectUser',
    ],
    'PROJECT_VIEW_DEFENCE_OF_MY_PROJECT' => [
        'type' => 2,
        'description' => 'View defence of my project',
        'ruleName' => 'isOnProjectUser',
    ],
    'PROJECT_DELETE' => [
        'type' => 2,
        'description' => 'Delete project',
    ],
    'PROJECT_IS_SUPERVISOR_OF_PROJECT' => [
        'type' => 2,
        'description' => 'Is supervisor of project',
        'ruleName' => 'isSupervisorOfProject',
    ],
    'PROPOSAL_LIST' => [
        'type' => 2,
        'description' => 'List proposals',
    ],
    'PROPOSAL_VIEW' => [
        'type' => 2,
        'description' => 'View proposal',
        'ruleName' => 'isProposalSent',
    ],
    'PROPOSAL_UPDATE' => [
        'type' => 2,
        'description' => 'Update proposal',
        'ruleName' => 'isProjectOwner',
    ],
    'PROPOSAL_VOTE' => [
        'type' => 2,
        'description' => 'Vote for proposal',
        'ruleName' => 'canProposalVote',
    ],
    'PROPOSALS_MANAGE' => [
        'type' => 2,
        'description' => 'Manage proposals',
    ],
    'PROPOSAL_UPDATE_OWN_COMMENT' => [
        'type' => 2,
        'description' => 'Update own proposal comment',
        'ruleName' => 'isProposalCommentOwner',
    ],
    'PROPOSAL_COMMENTS_VIEW' => [
        'type' => 2,
        'description' => 'View proposal comments',
    ],
    'DEFENCES_VOTE_ABOUT_DATE' => [
        'type' => 2,
        'description' => 'Vote for defence date',
    ],
    'DEFENCES_MANAGE' => [
        'type' => 2,
        'description' => 'Manage defences',
    ],
    'DEFENCES_VIEW' => [
        'type' => 2,
        'description' => 'View a defence',
    ],
    'DEFENCES_UPLOAD_OPPONENT_REVIEW' => [
        'type' => 2,
        'description' => 'Upload opponents review',
        'ruleName' => 'canUploadOpponentReview',
    ],
    'DEFENCES_UPLOAD_SUPERVISOR_REVIEW' => [
        'type' => 2,
        'description' => 'Upload supervisors review',
        'ruleName' => 'canUploadSupervisorReview',
    ],
    'DEFENCE_ADD_PROJECT' => [
        'type' => 2,
        'description' => 'Add a project to the defence date',
        'ruleName' => 'canAddProjectToDefence',
    ],
    'NEWS_MANAGE' => [
        'type' => 2,
        'description' => 'Manage news',
    ],
    'DOC_VIEW' => [
        'type' => 2,
        'description' => 'View documentation',
    ],
    'ADS_MANAGE' => [
        'type' => 2,
        'description' => 'Manage advertisements',
    ],
    'ADS_CREATE' => [
        'type' => 2,
        'description' => 'Create advertisements',
    ],
    'ADS_MANAGE_OWN_ADS' => [
        'type' => 2,
        'description' => 'Manage own advertisement',
        'ruleName' => 'isAdvertisementOwner',
    ],
    'ADS_MANAGE_COMMENTS' => [
        'type' => 2,
        'description' => 'Manage advertisement comments',
    ],
    'ADS_CREATE_COMMENT' => [
        'type' => 2,
        'description' => 'Create advertisement comments',
    ],
    'ADS_MANAGE_OWN_COMMENTS' => [
        'type' => 2,
        'description' => 'Manage own advertisement comment',
        'ruleName' => 'isAdCommentOwner',
    ],
    'PAGES_MANAGE' => [
        'type' => 2,
        'description' => 'Manage pages',
    ],
    'INBOX_LIST' => [
        'type' => 2,
        'description' => 'List inbox',
    ],
    'INBOX_PROJECTS' => [
        'type' => 2,
        'description' => 'Projects inbox',
    ],
    'guest' => [
        'type' => 1,
        'children' => [
            'PROJECT_CAN_DOWNLOAD_FILE',
        ],
    ],
    'common.for.students.and.academics' => [
        'type' => 1,
        'children' => [
            'guest',
            'PROJECT_UPLOAD_FILE',
            'PROJECT_ASSIGN_TO_DEFENCE',
            'PROJECT_VIEW_DEFENCE_OF_MY_PROJECT',
            'USERS_UPDATE_OWN_PROFILE',
            'ADS_CREATE',
            'ADS_MANAGE_OWN_COMMENTS',
            'ADS_CREATE_COMMENT',
            'ADS_MANAGE_OWN_ADS',
            'USERS_VIEW_NAME',
            'DEFENCE_ADD_PROJECT',
            'PROJECT_CAN_DOWNLOAD_FILE',
            'PROJECT_IS_ASSIGNED',
        ],
    ],
    'student' => [
        'type' => 1,
        'children' => [
            'common.for.students.and.academics',
            'PROJECT_UPDATE_CONTENT',
            'PROJECT_UPDATE_ADVICE',
        ],
    ],
    'academic' => [
        'type' => 1,
        'children' => [
            'common.for.students.and.academics',
            'PROJECT_CREATE',
            'PROJECT_UPDATE_OWN_PROJECT',
            'PROJECT_SEND_DOCUMENTS',
            'PROPOSAL_VIEW',
            'PROPOSAL_UPDATE',
            'DEFENCES_UPLOAD_SUPERVISOR_REVIEW',
            'PROJECT_IS_SUPERVISOR_OF_PROJECT',
        ],
    ],
    'committee_member' => [
        'type' => 1,
        'children' => [
            'academic',
            'PROPOSAL_LIST',
            'PROPOSAL_VOTE',
            'PROPOSAL_COMMENTS_VIEW',
            'PROPOSAL_UPDATE_OWN_COMMENT',
            'DEFENCES_VOTE_ABOUT_DATE',
            'DEFENCES_UPLOAD_OPPONENT_REVIEW',
            'DEFENCES_VIEW',
            'DOC_VIEW',
            'INBOX_LIST',
        ],
    ],
    'committee_chair' => [
        'type' => 1,
        'children' => [
            'committee_member',
            'USERS_MANAGE',
            'PROPOSALS_MANAGE',
            'DEFENCES_MANAGE',
            'ADS_MANAGE',
            'ADS_MANAGE_COMMENTS',
            'NEWS_MANAGE',
            'PAGES_MANAGE',
            'PROJECT_UPDATE',
            'PROJECT_UPDATE_STATUS',
            'PROJECT_DELETE',
            'INBOX_LIST',
            'INBOX_PROJECTS',
            'USERS_DELETE',
        ],
    ],
];
