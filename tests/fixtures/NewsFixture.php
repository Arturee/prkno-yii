<?php
namespace app\tests\fixtures;

use app\utils\DateTime;

class NewsFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\News';
    public $depends = [
        'app\tests\fixtures\UserFixture'
    ];

    protected function getData() {
        $now = new DateTime();
        $dateConverted = function($value) use(&$now) { return $now->modifyClone($value)->__toString(); };
        return $this->getTransformedData([
            "hide_at" => $dateConverted,
            "created_at" => $dateConverted,
            "updated_at" => $dateConverted
        ]);
    }
}