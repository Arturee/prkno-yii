<?php
namespace app\tests\fixtures;

class ProposalFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\Proposal';
    public $depends = [
        'app\tests\fixtures\ProjectFixture',
        'app\tests\fixtures\DocumentFixture'
	];
}