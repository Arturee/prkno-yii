<?php
namespace app\tests\fixtures;

use app\utils\DateTime;
use Yii;

class UserFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\User';

    protected function getData() {
        Yii::$app->security->passwordHashCost = 4;
        $now = new DateTime();
        $dateConverted = function($value) use(&$now) { return $now->modifyClone($value)->__toString(); };
        $passwordHashGenerator = function ($password) { return Yii::$app->security->generatePasswordHash($password); };
        return $this->getTransformedData([
            "password" => $passwordHashGenerator,
            "last_login" => $dateConverted,
            "last_CAS_refresh" => $dateConverted,
            "role_expiration" => $dateConverted,
            "password_recovery_token_expiration" => $dateConverted,
            "created_at" => $dateConverted,
            "updated_at" => $dateConverted
        ]);
    }
}