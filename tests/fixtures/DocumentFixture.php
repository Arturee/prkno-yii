<?php
namespace app\tests\fixtures;

class DocumentFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\Document';
}