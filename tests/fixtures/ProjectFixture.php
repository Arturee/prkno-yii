<?php
namespace app\tests\fixtures;

use app\utils\DateTime;

class ProjectFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\Project';
	public $depends = [
        'app\tests\fixtures\DocumentFixture'
    ];

	protected function getData() {
        $now = new DateTime();
        $dateConverted = function($value) use(&$now) { return $now->modifyClone($value)->__toString(); };
        return $this->getTransformedData([
        	"run_start_date" => $dateConverted,
        	"deadline" => $dateConverted,
            "created_at" => $dateConverted,
            "updated_at" => $dateConverted
        ]);
    }
}