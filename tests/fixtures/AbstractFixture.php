<?php
namespace app\tests\fixtures;

use app\utils\DateTime;
use yii\test\ActiveFixture;

class AbstractFixture extends ActiveFixture
{
    /**
     * Method for transforming values by function
     * @param array $converter  Array of key => function values
     * @return array converted data values
     */
    protected function getTransformedData(array $converter = []): array {
        $data = parent::getData();
        foreach ($converter as $key => $foo) {
            foreach ($data as &$row) {
                $row[$key] = $foo($row[$key]);
            }
        }
        return $data;
    }

    protected function getData() {
        $now = new DateTime();
        $dateConverted = function($value) use(&$now) { return $now->modifyClone($value)->__toString(); };
        return $this->getTransformedData([
            "created_at" => $dateConverted,
            "updated_at" => $dateConverted
        ]);
    }
}