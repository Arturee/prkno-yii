<?php
namespace app\tests\fixtures;

use app\utils\DateTime;

class ProjectStateHistoryFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\ProjectStateHistory';
	public $depends = [
        'app\tests\fixtures\ProjectFixture'
    ];

	protected function getData() {
        $now = new DateTime();
        $dateConverted = function($value) use(&$now) { return $now->modifyClone($value)->__toString(); };
        return $this->getTransformedData([
            "created_at" => $dateConverted,
            "updated_at" => $dateConverted
        ]);
    }
}