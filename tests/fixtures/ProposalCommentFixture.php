<?php
namespace app\tests\fixtures;

class ProposalCommentFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\ProposalComment';
    public $depends = [
        'app\tests\fixtures\UserFixture',
        'app\tests\fixtures\ProposalFixture'
    ];
}