<?php
namespace app\tests\fixtures;

class PageVersionFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\PageVersion';
}