<?php
namespace app\tests\fixtures;

class DefenceAttendanceFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\DefenceAttendance';
    public $depends = [
        'app\tests\fixtures\DefenceDateFixture',
        'app\tests\fixtures\UserFixture'
    ];
}