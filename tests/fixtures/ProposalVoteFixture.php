<?php
namespace app\tests\fixtures;

class ProposalVoteFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\ProposalVote';
    public $depends = [
        'app\tests\fixtures\UserFixture',
        'app\tests\fixtures\ProposalFixture'
	];
}