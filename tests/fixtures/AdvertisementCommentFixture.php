<?php
namespace app\tests\fixtures;

class AdvertisementCommentFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\AdvertisementComment';
    public $depends = [
        'app\tests\fixtures\UserFixture',
        'app\tests\fixtures\AdvertisementFixture'
	];
}