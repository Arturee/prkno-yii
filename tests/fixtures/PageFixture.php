<?php
namespace app\tests\fixtures;

class PageFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\Page';
}