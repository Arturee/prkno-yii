<?php
namespace app\tests\fixtures;

use app\utils\DateTime;

class ProjectDefenceFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\ProjectDefence';

    public $depends = [
        'app\tests\fixtures\UserFixture',
        'app\tests\fixtures\ProjectFixture',
        'app\tests\fixtures\DocumentFixture',
        'app\tests\fixtures\DefenceDateFixture',
    ];

    protected function getData() {
        $now = new DateTime();
        $dateConverted = function($value) use(&$now) { return $now->modifyClone($value)->__toString(); };
        return $this->getTransformedData([
        	"time" => $dateConverted,
            "created_at" => $dateConverted,
            "updated_at" => $dateConverted
        ]);
    }
}