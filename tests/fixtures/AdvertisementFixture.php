<?php
namespace app\tests\fixtures;

class AdvertisementFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\Advertisement';
    public $depends = [
        'app\tests\fixtures\UserFixture'
    ];
}