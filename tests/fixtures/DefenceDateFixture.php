<?php
namespace app\tests\fixtures;

use app\utils\DateTime;

class DefenceDateFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\DefenceDate';

	protected function getData() {
        $now = new DateTime();
        $dateConverted = function($value) use(&$now) { return $now->modifyClone($value)->__toString(); };
        return $this->getTransformedData([
        	"date" => $dateConverted,
        	"signup_deadline" => $dateConverted,
            "created_at" => $dateConverted,
            "updated_at" => $dateConverted
        ]);
    }
}