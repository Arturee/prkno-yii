<?php
namespace app\tests\fixtures;

class ProjectDocumentFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\ProjectDocument';
    public $depends = [
        'app\tests\fixtures\DocumentFixture',
        'app\tests\fixtures\ProjectFixture'
    ];
}