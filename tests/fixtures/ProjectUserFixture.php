<?php
namespace app\tests\fixtures;

class ProjectUserFixture extends AbstractFixture
{
    public $modelClass = 'app\models\records\ProjectUser';
    public $depends = [
        'app\tests\fixtures\UserFixture',
        'app\tests\fixtures\ProjectFixture'
    ];
}