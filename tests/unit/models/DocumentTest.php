<?php
namespace app\tests\unit\models;

use app\models\records\Document;
use app\tests\fixtures\DocumentFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for document model
 */
class DocumentsTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'document' => [
                'class' => DocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'document.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new Document();
    }

    /**
     * Test for name attribute validation
     */
    public function testNameValidation()
    {
        $record = new Document();
        $key = 'fallback_path';
        $this->tester->recordValidateTrue($record, $key, null);
        $this->tester->recordValidateFalse($record, $key, $this->tester->randomStr(1000));
        $this->tester->recordValidateTrue($record, $key, 'string');
    }
}
