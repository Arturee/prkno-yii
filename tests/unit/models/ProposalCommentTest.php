<?php
namespace app\tests\unit\models;

use app\models\records\Users;
use app\models\records\ProposalId;
use app\models\records\ProposalComment;
use app\tests\fixtures\ProposalCommentFixture;
use app\tests\fixtures\ProposalFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * 
 */
class ProposalCommentsTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures() {
        return [
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'proposal' => [
                'class' => ProposalFixture::className(),
                'dataFile' => codecept_data_dir() . 'proposal.php'
            ],
            'proposalComment' => [
                'class' => ProposalCommentFixture::className(),
                'dataFile' => codecept_data_dir() . 'proposalComment.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord() {
        return new ProposalComment();
    }

    /**
     * Test content attribute validation
     */
    public function testContentValidation()
    {
        $record = new ProposalComment();
        $this->tester->recordValidateFalse($record, "content", null);
        $this->tester->recordValidateFalse($record, "content", $this->tester->randomStr(5000));
        $this->tester->recordValidateTrue($record, "content", "some text");
    }

    /**
     * Test creator_id, proposal_id attributes
     */
    public function testIdValidation()
    {
        $record = new ProposalComment();
        $this->tester->recordValidateTrue($record, "creator_id", 2);
        $this->tester->recordValidateFalse($record, "creator_id", 12420);
        $this->tester->recordValidateTrue($record, "proposal_id", 1);
        $this->tester->recordValidateFalse($record, "proposal_id", 13422);
    }
}
