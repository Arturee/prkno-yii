<?php
namespace app\tests\unit\models;

use app\models\records\Proposal;
use app\models\records\ProposalVote;
use app\models\records\User;
use app\tests\fixtures\ProposalFixture;
use app\tests\fixtures\ProposalVoteFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for proposal votes model
 */
class ProposalVotesTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'proposalVote' => [
                'class' => ProposalVoteFixture::className(),
                'dataFile' => codecept_data_dir() . 'proposalVote.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new ProposalVote();
    }

    /**
     * Test vote attribute validation
     */
    public function testRole()
    {
        $record = new ProposalVote();
        $key = 'vote';
        $this->tester->recordValidateTrue($record, $key, ProposalVote::VOTE_NO);
        $this->tester->recordValidateTrue($record, $key, ProposalVote::VOTE_YES);
        $this->tester->recordValidateFalse($record, $key, 'string');
        $this->tester->recordValidateFalse($record, $key, 13425);
    }

    /**
     * Test user_id attribute validation
     */
    public function testIdValidation()
    {
        $record = new ProposalVote();
        $this->tester->recordValidateTrue($record, "user_id", 2);
        $this->tester->recordValidateFalse($record, "user_id", 12420);
    }
}
