<?php
namespace app\tests\unit\models;

use app\models\records\Project;
use app\models\records\ProjectStateHistory;
use app\tests\fixtures\ProjectFixture;
use app\tests\fixtures\ProjectStateHistoryFixture;
use app\tests\unit\RecordTest;

class ProjectStateHistoryTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'project' => [
                'class' => ProjectFixture::className(),
                'dataFile' => codecept_data_dir() . 'project.php'
            ],
            'projectStateHistory' => [
                'class' => ProjectStateHistoryFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectStateHistory.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new ProjectStateHistory();
    }

    /**
     * Test comment validation
     */
    public function testCommentValidation()
    {
        $record = new ProjectStateHistory();
        $this->tester->recordValidateTrue($record, "comment", null);
        $this->tester->recordValidateFalse($record, "comment", $this->tester->randomStr(2000));
        $this->tester->recordValidateTrue($record, "comment", "some text");
    }

    /**
     * Test project_id attribute validation
     */
    public function testIdValidation()
    {
        $record = new ProjectStateHistory();
        $this->tester->recordValidateTrue($record, "project_id", 2);
        $this->tester->recordValidateFalse($record, "project_id", 12420);
    }
}
