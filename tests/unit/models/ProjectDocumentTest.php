<?php
namespace app\tests\unit\models;

use app\models\records\Document;
use app\models\records\Project;
use app\models\records\ProjectDocument;
use app\tests\fixtures\DocumentFixture;
use app\tests\fixtures\ProjectFixture;
use app\tests\fixtures\ProjectDocumentFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for project document
 */
class ProjectDocumentTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures() {
        return [
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'projects' => [
                'class' => ProjectFixture::className(),
                'dataFile' => codecept_data_dir() . 'project.php'
            ],
            'documents' => [
                'class' => DocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'document.php'
            ],
            'projectDocuments' => [
                'class' => ProjectDocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectDocument.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord() {
        return new ProjectDocument();
    }

    /**
     * Test project document type validation
     */
    public function testProjectDocumentType()
    {
        $record = new ProjectDocument();
        $key = 'type';
        $this->tester->recordValidateTrue($record, $key, ProjectDocument::TYPE_FOR_DEFENSE_ZIP);
        $this->tester->recordValidateTrue($record, $key, ProjectDocument::TYPE_FOR_DEFENSE_PDF);
        $this->tester->recordValidateTrue($record, $key, ProjectDocument::TYPE_READ_ONLY);
        $this->tester->recordValidateTrue($record, $key, ProjectDocument::TYPE_ADVICE);    
        $this->tester->recordValidateFalse($record, $key, 'string');
        $this->tester->recordValidateFalse($record, $key, 13425);
    }


    /**
     * Test comment attribute validation
     */
    public function testCommentValidation()
    {
        $record = new ProjectDocument();
        $this->tester->recordValidateTrue($record, "comment", null);
        $this->tester->recordValidateFalse($record, "comment", $this->tester->randomStr(2000));
        $this->tester->recordValidateTrue($record, "comment", "some text");
    }

    /**
     * Test project_id and document_id validation
     */
    public function testIdValidation()
    {
        $record = new ProjectDocument();
        $this->tester->recordValidateTrue($record, "project_id", 2);
        $this->tester->recordValidateFalse($record, "project_id", 12420);
        $this->tester->recordValidateTrue($record, "document_id", 1);
        $this->tester->recordValidateFalse($record, "document_id", 13422);
    }
}
