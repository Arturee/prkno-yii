<?php
namespace app\tests\unit\models;

use app\models\records\User;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for users
 */
class UsersTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new User();
    }

    /**
     * Test email attrobute validation
     */
    public function testEmailValidation()
    {
        $record = new User();
        $key = 'email';
        $this->tester->recordValidateFalse($record, $key, null);
        $this->tester->recordValidateFalse($record, $key, 'nonvalidmail');
        $this->tester->recordValidateFalse($record, $key, 'nonvalidmailwith@');
        $this->tester->recordValidateFalse($record, $key, 'nonvalidmailwith@gmail');
        $this->tester->recordValidateTrue($record, $key, 'mail@gmail.com');
    }

    /**
     * Test name attribute validation
     */
    public function testNameValidation()
    {
        $record = new User();
        $key = 'name';
        $this->tester->recordValidateFalse($record, $key, null);
        $this->tester->recordValidateFalse($record, $key, 'a');
        $this->tester->recordValidateFalse($record, $key, $this->tester->randomStr(1000));
        $this->tester->recordValidateTrue($record, $key, 'name');
    }

    /**
     * Test role attribute validation
     */
    public function testRole()
    {
        $record = new User();
        $key = 'role';
        $this->tester->recordValidateTrue($record, $key, User::ROLE_GUEST);
        $this->tester->recordValidateTrue($record, $key, User::ROLE_STUDENT);
        $this->tester->recordValidateTrue($record, $key, User::ROLE_ACADEMIC);
        $this->tester->recordValidateTrue($record, $key, User::ROLE_COMMITTEE_MEMBER);
        $this->tester->recordValidateTrue($record, $key, User::ROLE_COMMITTEE_CHAIR);
        $this->tester->recordValidateFalse($record, $key, 'string');
        $this->tester->recordValidateFalse($record, $key, 13425);
    }

    /**
     * Test role_is_from_CAS and banned attribute validation
     */
    public function testBooleanAttributesValidation()
    {
        $record = new User();
        $keys = ['role_is_from_CAS', 'banned'];
        foreach ($keys as $key) {
            $this->tester->recordValidateFalse($record, $key, 2);
            $this->tester->recordValidateTrue($record, $key, null); // have set default value
            $this->tester->recordValidateTrue($record, $key, true);
            $this->tester->recordValidateTrue($record, $key, false);
        }
    }

    /**
     * Test of fixture usage
     */
    public function test()
    {
        $fixture = $this->tester->grabFixture('user', 'user1');
        $this->tester->seeRecord('app\models\records\User', ['email' => 'comitteechair@gmail.com']);
        $user = User::findByEmail('comitteechair@gmail.com');
        expect_that($user);
    }

    /**
     * Test of finding user by email address
     */
    public function testFindUserByEmail()
    {
        expect_that(User::findByEmail('comitteechair@gmail.com'));
        expect_not(User::findByEmail('not-admin'));
    }
}