<?php
namespace app\tests\unit\models;

use app\models\records\DefenceDate;
use app\tests\fixtures\DefenceDateFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for defence dates
 */
class DefenceDatesTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'defenceDate' => [
                'class' => DefenceDateFixture::className(),
                'dataFile' => codecept_data_dir() . 'defenceDate.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new DefenceDate();
    }

    /**
     * Test for room attribute validation
     */
    public function testRoomValidation()
    {
        $record = new DefenceDate();
        $key = 'room';
        $this->tester->recordValidateTrue($record, $key, null);
        $this->tester->recordValidateFalse($record, $key, $this->tester->randomStr(1000));
        $this->tester->recordValidateTrue($record, $key, 'S3');
    }
}
