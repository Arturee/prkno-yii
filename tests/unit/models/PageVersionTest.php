<?php
namespace app\tests\unit\models;

use app\models\records\User;
use app\models\records\Document;
use aoo\models\records\Page;
use app\models\records\PageVersion;
use app\tests\fixtures\UserFixture;
use app\tests\fixtures\DocumentFixture;
use app\tests\fixtures\PageFixture;
use app\tests\fixtures\PageVersionFixture;
use app\tests\unit\RecordTest;

/**
 * Unit test for page versions
 */
class PageVersionsTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'document' => [
                'class' => DocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'document.php'
            ],
            'pages' => [
                'class' => PageFixture::className(),
                'dataFile' => codecept_data_dir() . 'page.php'
            ],
            'pageVersion' => [
                'class' => PageVersionFixture::className(),
                'dataFile' => codecept_data_dir() . 'pageVersion.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new PageVersion();
    }

    /**
     * Test content_cs, content_en, user_id and page_id values
     */
    public function testIdValidation()
    {
        $record = new PageVersion();
        $this->tester->recordValidateTrue($record, "content_cs", 2);
        $this->tester->recordValidateFalse($record, "content_cs", 12420);
        $this->tester->recordValidateTrue($record, "content_en", 1);
        $this->tester->recordValidateFalse($record, "content_en", 13422);
        $this->tester->recordValidateTrue($record, "user_id", 1);
        $this->tester->recordValidateFalse($record, "user_id", 2131);
        $this->tester->recordValidateTrue($record, "page_id", 1);
        $this->tester->recordValidateFalse($record, "page_id", 3522);
    }
}
