<?php
namespace app\tests\unit\models;

use app\models\records\Page;
use app\tests\fixtures\PageFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for page model
 */
class PageTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'pages' => [
                'class' => PageFixture::className(),
                'dataFile' => codecept_data_dir() . 'page.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new Page();
    }

    /**
     * Test title_en, title_cs, and path_pdf_download attribute validators
     */
    public function testPathValidation()
    {
        $record = new Page();
        $this->tester->recordValidateTrue($record, "title_en", null);
        $this->tester->recordValidateTrue($record, "title_cs", null);
        $this->tester->recordValidateTrue($record, "path_pdf_download", null);
        $this->tester->recordValidateFalse($record, "title_en", $this->tester->randomStr(1000));
        $this->tester->recordValidateFalse($record, "title_cs", $this->tester->randomStr(1000));
        $this->tester->recordValidateFalse($record, "path_pdf_download", $this->tester->randomStr(1000));
        $this->tester->recordValidateTrue($record, "title_en", "some text");
        $this->tester->recordValidateTrue($record, "title_cs", "some text");
        $this->tester->recordValidateTrue($record, "path_pdf_download", "some text");
    }
}
