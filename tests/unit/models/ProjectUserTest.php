<?php
namespace app\tests\unit\models;

use app\models\records\Project;
use app\models\records\ProjectUser;
use app\tests\fixtures\ProjectFixture;
use app\tests\fixtures\ProjectUserFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for projectUser model
 */
class UsersOnProjectTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'project' => [
                'class' => ProjectFixture::className(),
                'dataFile' => codecept_data_dir() . 'project.php'
            ],
            'projectUser' => [
                'class' => ProjectUserFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectUser.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new ProjectUser();
    }

    /**
     * Test project role attribute validation
     */
    public function testProjectRole()
    {
        $record = new ProjectUser();
        $key = 'project_role';
        $this->tester->recordValidateTrue($record, $key, ProjectUser::ROLE_TEAM_MEMBER);
        $this->tester->recordValidateTrue($record, $key, ProjectUser::ROLE_SUPERVISOR);
        $this->tester->recordValidateTrue($record, $key, ProjectUser::ROLE_OPPONENT);
        $this->tester->recordValidateTrue($record, $key, ProjectUser::ROLE_CONSULTANT);
        $this->tester->recordValidateFalse($record, $key, 'string');
        $this->tester->recordValidateFalse($record, $key, 13425);
    }

    /**
     * Test comment attribute validation
     */
    public function testCommentValidation()
    {
        $record = new ProjectUser();
        $this->tester->recordValidateTrue($record, "comment", null);
        $this->tester->recordValidateFalse($record, "comment", $this->tester->randomStr(2000));
        $this->tester->recordValidateTrue($record, "comment", "some text");
    }

    /**
     * Test project_id and user_id validation
     */
    public function testIdValidation()
    {
        $record = new ProjectUser();
        $this->tester->recordValidateTrue($record, "project_id", 2);
        $this->tester->recordValidateFalse($record, "project_id", 12420);
        $this->tester->recordValidateTrue($record, "user_id", 1);
        $this->tester->recordValidateFalse($record, "user_id", 13422);
    }
}
