<?php
namespace app\tests\unit\models;

use app\models\records\User;
use app\models\records\Advertisement;
use app\tests\fixtures\AdvertisementFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for advertisements
 */
class AdvertisementsTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'advertisements' => [
                'class' => AdvertisementFixture::className(),
                'dataFile' => codecept_data_dir() . 'advertisement.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new Advertisement();
    }

    /**
     * Test for description attribute validation
     */
    public function testDescriptionValidation()
    {
        $record = new Advertisement();
        $key = 'description';
        $this->tester->recordValidateFalse($record, $key, null);
        $this->tester->recordValidateTrue($record, $key, 'a');
    }

    /**
     * Test for user_id value validation
     */
    public function testIdValidation()
    {
        $record = new Advertisement();
        $this->tester->recordValidateTrue($record, "user_id", 1);
        $this->tester->recordValidateFalse($record, "user_id", 13422);
    }

    /**
     * Test for role attribute validation
     */
    public function testRole()
    {
        $record = new Advertisement();
        $key = 'type';
        $this->tester->recordValidateTrue($record, $key, Advertisement::TYPE_PROJECT);
        $this->tester->recordValidateTrue($record, $key, Advertisement::TYPE_PERSON);
        $this->tester->recordValidateFalse($record, $key, 'string');
        $this->tester->recordValidateFalse($record, $key, 13425);
    }
}
