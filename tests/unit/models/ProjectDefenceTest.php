<?php
namespace app\tests\unit\models;

use app\models\records\DefenceDate;
use app\models\records\Document;
use app\models\records\ProjectDefence;
use app\models\records\User;
use app\tests\fixtures\DefenceDateFixture;
use app\tests\fixtures\DocumentFixture;
use app\tests\fixtures\ProjectDefenceFixture;
use app\tests\fixtures\ProjectFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for project defence model
 */
class ProjectDefenceTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'projects' => [
                'class' => ProjectFixture::className(),
                'dataFile' => codecept_data_dir() . 'project.php'
            ],
            'documents' => [
                'class' => DocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'document.php'
            ],
            'defenceDates' => [
                'class' => DefenceDateFixture::className(),
                'dataFile' => codecept_data_dir() . 'defenceDate.php'
            ],
            'projectDefences' => [
                'class' => ProjectDefenceFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectDefence.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new ProjectDefence();
    }

    /**
     * Test result attribute validation
     */
    public function testResult()
    {
        $record = new ProjectDefence();
        $key = 'result';
        $this->tester->recordValidateTrue($record, $key, ProjectDefence::RESULT_DEFENDED);
        $this->tester->recordValidateTrue($record, $key, ProjectDefence::RESULT_CONDITIONALLY_DEFENDED);
        $this->tester->recordValidateTrue($record, $key, ProjectDefence::RESULT_FAILED);
        $this->tester->recordValidateFalse($record, $key, 'string');
        $this->tester->recordValidateFalse($record, $key, 13425);
    }

    /**
     * Test all model attributes with foreign keys
     */
    public function testIdValidation()
    {
        $record = new ProjectDefence();
        $this->tester->recordValidateFalse($record, "defence_date_id", null);
        $this->tester->recordValidateTrue($record, "defence_date_id", 2);
        $this->tester->recordValidateFalse($record, "defence_date_id", 12420);
        $this->tester->recordValidateFalse($record, "project_id", null);
        $this->tester->recordValidateTrue($record, "project_id", 1);
        $this->tester->recordValidateFalse($record, "project_id", 13422);
        $this->tester->recordValidateTrue($record, "opponent_id", null);
        $this->tester->recordValidateTrue($record, "opponent_id", 1);
        $this->tester->recordValidateFalse($record, "opponent_id", 13422);
        $this->tester->recordValidateTrue($record, "statement_author_id", null);
        $this->tester->recordValidateTrue($record, "statement_author_id", 1);
        $this->tester->recordValidateFalse($record, "statement_author_id", 13422);
        $this->tester->recordValidateTrue($record, "supervisor_review", null);
        $this->tester->recordValidateTrue($record, "supervisor_review", 1);
        $this->tester->recordValidateFalse($record, "supervisor_review", 13422);
        $this->tester->recordValidateTrue($record, "opponent_review", null);
        $this->tester->recordValidateTrue($record, "opponent_review", 1);
        $this->tester->recordValidateFalse($record, "opponent_review", 13422);
        $this->tester->recordValidateTrue($record, "defence_statement", null);
        $this->tester->recordValidateTrue($record, "defence_statement", 1);
        $this->tester->recordValidateFalse($record, "defence_statement", 13422);
    }
}
