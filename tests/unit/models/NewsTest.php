<?php
namespace app\tests\unit\models;

use app\models\records\User;
use app\models\records\News;
use app\tests\fixtures\NewsFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for news
 */
class NewsTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'news' => [
                'class' => NewsFixture::className(),
                'dataFile' => codecept_data_dir() . 'news.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new News();
    }

    /**
     * Test validation of user id attribute.
     */
    public function testUserIdValidation()
    {
        $record = new News();
        $key = 'user_id';
        $this->tester->recordValidateFalse($record, $key, "invalidType");
        $this->tester->recordValidateFalse($record, $key, 24245);
        $this->tester->recordValidateTrue($record, $key, null);
        $this->tester->recordValidateTrue($record, $key, 1);
    }

    /**
     * Test validation of type attribute.
     * Type has to have enum type generated/normal/important
     */
    public function testTypeValidation()
    {
        $record = new News();
        $key = 'type';
        $this->tester->recordValidateFalse($record, $key, null);
        $this->tester->recordValidateFalse($record, $key, "invalidString");
        $this->tester->recordValidateTrue($record, $key, News::TYPE_GENERATED);
        $this->tester->recordValidateTrue($record, $key, News::TYPE_NORMAL);
        $this->tester->recordValidateTrue($record, $key, News::TYPE_IMPORTANT);
    }

    /**
     * Test english title. Title has to be non null,
     * have string type and have max 100 characters.
     * Test en and cs titles.
     */
    public function testTitles()
    {
        $record = new News();
        $keys = ['title_en', 'title_cs'];
        foreach ($keys as $key) {
            $this->tester->recordValidateFalse($record, $key, null);
            $this->tester->recordValidateFalse($record, $key, 42);
            $this->tester->recordValidateFalse($record, $key, $this->tester->randomStr(101));
            $this->tester->recordValidateTrue($record, $key, $this->tester->randomStr(100));
        }
    }

    /**
     * Test content variable. Content should be non null
     * and have string type. Test en and cs contents.
     */
    public function testContentsFormat()
    {
        $record = new News();
        $keys = ['content_en', 'content_cs'];
        foreach ($keys as $key) {
            $this->tester->recordValidateFalse($record, $key, null);
            $this->tester->recordValidateFalse($record, $key, 42);
            $this->tester->recordValidateTrue($record, $key, $this->tester->randomStr(10));
        }
    }

    /**
     * Test public value. Title has to be non null,
     * have string type and have max 100 characters.
     * Test en and cs titles.
     */
    public function testPublicFormat()
    {
        $record = new News();
        $key = 'public';
        $this->tester->recordValidateFalse($record, $key, null);
        $this->tester->recordValidateFalse($record, $key, 2);
        $this->tester->recordValidateTrue($record, $key, true);
        $this->tester->recordValidateTrue($record, $key, false);
    }

    /**
     * Test english title. If DeTitle has to be non null,
     * have string type and have max 100 characters.
     * Test en and cs titles.
     */
    public function testHideAtFormat()
    {
        $record = new News();
        $key = 'hide_at';
        $this->tester->recordValidateTrue($record, $key, null);
        $this->tester->recordValidateTrue($record, $key, "2018-02-10 00:00:00");
    }

    /**
     * Test english title. If DeTitle has to be non null,
     * have string type and have max 100 characters.
     * Test en and cs titles.
     */
    public function testUserId()
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $fixture = $this->tester->grabFixture('user');
        $className = $fixture->modelClass;
    }
}