<?php
namespace app\tests\unit\models;

use app\models\records\User;
use app\models\records\AdvertisementComment;
use app\tests\fixtures\AdvertisementFixture;
use app\tests\fixtures\AdvertisementCommentFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for ad comments
 */
class AdCommentsTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'advertisements' => [
                'class' => AdvertisementFixture::className(),
                'dataFile' => codecept_data_dir() . 'advertisement.php'
            ],
            'advertisementComments' => [
                'class' => AdvertisementCommentFixture::className(),
                'dataFile' => codecept_data_dir() . 'advertisementComment.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new AdvertisementComment();
    }

    /**
     * Test for content attribute validation
     */
    public function testContentValidation()
    {
        $record = new AdvertisementComment();
        $this->tester->recordValidateFalse($record, "content", null);
        $this->tester->recordValidateFalse($record, "content", $this->tester->randomStr(2000));
        $this->tester->recordValidateTrue($record, "content", "some text");
    }

    /**
     * Test for validating foreign keys
     */
    public function testIdValidation()
    {
        $record = new AdvertisementComment();
        $this->tester->recordValidateTrue($record, "ad_id", 2);
        $this->tester->recordValidateFalse($record, "ad_id", 12420);
        $this->tester->recordValidateTrue($record, "user_id", 1);
        $this->tester->recordValidateFalse($record, "user_id", 13422);
    }
}
