<?php
namespace app\tests\unit\models;

use app\models\records\User;
use app\models\records\RoleHistory;
use app\tests\fixtures\RoleHistoryFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for RoleHistory model
 */
class RoleHistoryTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'roleHistory' => [
                'class' => RoleHistoryFixture::className(),
                'dataFile' => codecept_data_dir() . 'roleHistory.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new RoleHistory();
    }

    /**
     * Test comment attribute validation
     */
    public function testCommentValidation()
    {
        $record = new RoleHistory();
        $key = 'comment';
        $this->tester->recordValidateTrue($record, $key, null);
        $this->tester->recordValidateTrue($record, $key, 'comment');
        $this->tester->recordValidateFalse($record, $key, $this->tester->randomStr(1000));
    }

    /**
     * Test user_id attribute validation
     */
    public function testUserIdValidation()
    {
        $record = new RoleHistory();
        $key = 'user_id';
        $this->tester->recordValidateTrue($record, $key, 2);
        $this->tester->recordValidateFalse($record, $key, 'string');
        $this->tester->recordValidateFalse($record, $key, 242255);
    }

    /**
     * Test role attribute validation
     */
    public function testRole()
    {
        $record = new RoleHistory();
        $key = 'role';
        $this->tester->recordValidateTrue($record, $key, User::ROLE_GUEST);
        $this->tester->recordValidateTrue($record, $key, User::ROLE_STUDENT);
        $this->tester->recordValidateTrue($record, $key, User::ROLE_ACADEMIC);
        $this->tester->recordValidateTrue($record, $key, User::ROLE_COMMITTEE_MEMBER);
        $this->tester->recordValidateTrue($record, $key, User::ROLE_COMMITTEE_CHAIR);
        $this->tester->recordValidateFalse($record, $key, 'string');
        $this->tester->recordValidateFalse($record, $key, 13425);
    }
}
