<?php
namespace app\tests\unit\models;

use app\models\records\DefenceAttendance;
use app\tests\fixtures\DefenceAttendanceFixture;
use app\tests\fixtures\DefenceDateFixture;
use app\tests\fixtures\UserFixture;
use app\tests\unit\RecordTest;

/**
 * Unit tests for defence attendance
 */
class DefenceAttendanceTest extends RecordTest
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'defenceDates' => [
                'class' => DefenceDateFixture::className(),
                'dataFile' => codecept_data_dir() . 'defenceDate.php'
            ],
            'defenceAttendance' => [
                'class' => DefenceAttendanceFixture::className(),
                'dataFile' => codecept_data_dir() . 'defenceAttendance.php'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createRecord()
    {
        return new DefenceAttendance();
    }

    /**
     * Test for comment attribute validation
     */
    public function testCommentValidation()
    {
        $record = new DefenceAttendance();
        $this->tester->recordValidateTrue($record, "comment", null);
        $this->tester->recordValidateFalse($record, "comment", $this->tester->randomStr(2000));
        $this->tester->recordValidateTrue($record, "comment", "some text");
    }

    /**
     * Test for vote attribute validation
     */
    public function testVote()
    {
        $record = new DefenceAttendance();
        $key = 'vote';
        $this->tester->recordValidateTrue($record, $key, DefenceAttendance::VOTE_YES);
        $this->tester->recordValidateTrue($record, $key, DefenceAttendance::VOTE_NO);
        $this->tester->recordValidateTrue($record, $key, DefenceAttendance::VOTE_UNKNOWN);
        $this->tester->recordValidateFalse($record, $key, 'string');
        $this->tester->recordValidateFalse($record, $key, 13425);
    }

    /**
     * Test for defence_date_id and user_id attributes validation
     */
    public function testIdValidation()
    {
        $record = new DefenceAttendance();
        $this->tester->recordValidateTrue($record, "defence_date_id", 2);
        $this->tester->recordValidateFalse($record, "defence_date_id", 12420);
        $this->tester->recordValidateTrue($record, "user_id", 1);
        $this->tester->recordValidateFalse($record, "user_id", 13422);
    }
}
