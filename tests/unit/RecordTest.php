<?php
namespace app\tests\unit;

use Codeception\Test\Unit;

/**
 * Abstract class for unit testing of ORM records
 */
abstract class RecordTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Method for creating instance of ORM record
     * @return OrmRecord    Empty ORM record
     */
    public abstract function createRecord();

    /**
     * Test deleted attribute.
     */
    public function testDeletedFormat() {
        $record = $this->createRecord();
        $key = 'deleted';
        $this->tester->recordValidateTrue($record, $key, true);
        $this->tester->recordValidateTrue($record, $key, false);
        $this->tester->recordValidateFalse($record, $key, 2);
    }
}