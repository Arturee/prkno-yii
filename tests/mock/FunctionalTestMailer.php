<?php
namespace app\tests\mock;

use app\consts\Param;
use Codeception\Lib\Connector\Yii2\TestMailer;
use Yii;
use yii\mail\MessageInterface;

/**
 * Custom test mailer
 */
class FunctionalTestMailer extends TestMailer {
    /**
     * 
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    // \yii\swiftmailer\Mailer

    /**
     * Templates are located in /app/mail and their filenames have
     * -html.php -text.php suffixes.
     *
     * @param string $filename filename without the suffix (represents both the html and text file)
     * @param array $data custom data passed to the templates
     */
    public function fromTemplates(string $filename, array $data): MessageInterface
    {
        $result = $this->compose(
            [
                'html' => $filename . '-html',
                'text' => $filename . '-text'
            ],
            $data
        );
        $result->setFrom([ Yii::$app->params[Param::EMAIL_ROBOT] => Yii::$app->name . ' robot']);
        return $result;
    }
}