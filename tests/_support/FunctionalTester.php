<?php
use app\utils\DateTime;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends AbstractTester
{
    use _generated\FunctionalTesterActions;

    /**
     * User should not have access rights to this page
     * @param array $page   Parameter for amOnPage method
     */
    function notAllowedPage($page): void {
        try {
            $this->amOnPage($page);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            if ($message != "You are not allowed to access this page") {
                throw new \Exception("Page is accessible");
            }
        }
    }

    /**
     * Test whether the current url is path from parameter.
     * Example: $I->seeCurrentUrl(['users/view', 'id' => 3])
     * @param array $url   URL
     */
    function seeCurrentUrl(array $path): void {
        $url1 = $url2 = "/index-test.php?";
        foreach ($path as $key => $value) {
            $value = str_replace("/", "%2F", $value);
            if (is_int($key)) {
                $url1 .= "r=".$value;
                $url2 .= "r=".$value;
            } else {
                $url1 .= "%3F".$key."%3D".$value;
                $url2 .= "&".$key."=".$value;
            }
        }
        // codeception bug - sometimes different url formats, at least one has to match
        try {
            $this->seeCurrentUrlEquals($url1);
        } catch (Exception $ex) {
            $this->seeCurrentUrlEquals($url2);
        }
    }

    /**
     * Check equality of date times from parameter and selector
     * @param string $date      Date
     * @param string $selector  CSS or XPath selector
     */
    function seeDateTime(string $date, $selector): void {
        $selectorDate = $this->grabTextFrom($selector);
        expect_that(DateTime::equals($date, $selectorDate));
    }

    /**
     * Check equality of dates from parameter and selector
     * @param string $date      Date
     * @param string $selector  CSS or XPath selector
     */
    function seeDate(string $date, $selector): void {
        $selectorDate = $this->grabTextFrom($selector);
        expect_that(DateTime::equalsDate($date, $selectorDate));
    }

    /**
     * Same as see, but allows to have a null value
     * @param string $item  item
     */
    function seeOrNull(?string $item): void {
        if ($item) {
            $this->see($item);
        }
    }

    /**
     * Same as seeDateTime, but allows to have a $date as null value
     * @param string $date      Date
     * @param string $selector  CSS or XPath selector
     */
    function seeDateTimeOrNull(?string $date, $selector): void {
        if ($date) {
            $this->seeDateTime($date, $selector);
        }
    }

    /**
     * See whether there is 'Yes' or 'No' value based on
     * parameter
     * @param bool $value       Value
     * @param array $selector   CSS or XPath selector
     */
    function seeYesOrNo(bool $value, $selector): void {
        $this->see($value ? "Yes" : "No", $selector);
    }

    /**
     * Check whether the values equals. If do not,
     * throw exception
     * @param $val1     Value 1
     * @param $val2     Value 2
     */
    function assertEquals($val1, $val2): void {
        if ($val1 != $val2) {
            throw new \Exception("Values '".$val1."' and '".$val2."' are not equal");
        }
    }

    /**
     * Check whether the year, month and days equals. If do not,
     * throw exception
     * @param $val1     Date 1
     * @param $val2     Date 2
     */
    function assertEqualDates($date1, $date2): void {
        $date1 = new DateTime($date1);
        $date2 = new DateTime($date2);
        $formatedDate1 = $date1->format('Ymd');
        $formatedDate2 = $date2->format('Ymd');
        $this->assertEquals($formatedDate1, $formatedDate2);
    }

    /**
     * Check whether the parameter is true
     * @param bool $condition   Condition
     */
    function assertTrue(bool $condition): void {
        if (!$condition) {
            throw new \Exception("Values is not true");
        }
    }

    /**
     * Check whether the values not equals. If do,
     * throw exception
     * @param $val1     Value 1
     * @param $val2     Value 2
     */
    function assertNotEquals($val1, $val2): void {
        if ($val1 == $val2) {
            throw new \Exception("Values '".$val1."' and '".$val2."' are equal");
        }
    }

    /**
     * Grab value from attribute and run assertEqual
     * with the value from parameter
     * @param $value    Value
     * @param $selector Selector
     */
    function assertEqualsGrabbedValue(?string $value, $selector): void {
        $grabbedValue = $this->grabValueFrom($selector);
        if ($value == null && $grabbedValue == "") return;
        $this->assertEquals($value, $grabbedValue);
    }

    /**
     * Grab value from checked option by selector
     * @param $selector     Selector
     * @return string Value or null if does not exists
     */
    function grabCheckedValueFrom($selector): ?string {
        $values = $this->grabValueFrom($selector.'[checked]');
        return sizeof($values) > 0 ? $values[0] : null;
    }

    /**
     * Check whether the checked value from input
     * equals to selector
     * @param $value        Value
     * @param $selector     Selector
     */
    function assertEqualsGrabbedCheckedValue($value, $selector): void {
        $checkedValue = $this->grabCheckedValueFrom($selector);
        $this->assertEquals($value, $checkedValue);
    }

    /**
     * Check whether the input with the given selector has checked
     * radio button with given value
     * @param $value    Value
     * @param $selector CSS selector
     */
    function asserEqualsCheckedOption($value, string $selector): void {
        $values = $this->grabValueFrom($selector.'[checked]');
        $val = implode($values);
        $this->assertEquals($value, $val);
    }

    /**
     * Check equality of dates (Y:M:D) from parameter and selector
     * to the input value
     * @param string $date      Date
     * @param string $selector  CSS or XPath selector
     */
    function seeInputDate(string $date, $selector): void {
        $selectorDate = $this->grabValueFrom($selector);
        $this->assertEqualDates($date, $selectorDate);
    }

    /**
     * Pass only if callback with throw expect with given type
     * @param class $exceptionClass     Exception type
     * @param callable Callback
     */
    function expectException($exceptionClass, Callable $a): void {
        try {
            $a();
        } catch (Exception $e) {
            if ($e instanceof $exceptionClass) {
                return;
            } else {
                throw $e;
            }
        }
        throw new Exception("Expected exception: ".$exceptionClass);
    }
}
