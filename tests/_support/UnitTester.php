<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class UnitTester extends AbstractTester
{
    use _generated\UnitTesterActions;

    /**
     * Validate record with validation function as correct value
     * Return assert exception if validation fail
     * @param OrmRecord $record Orm record with function validate
     * @param string $attr      String value
     * @param Any $value        Value
     */
    public function recordValidateTrue($record, string $attr, $value): void {
        $record->$attr = $value;
        $this->assertTrue($record->validate([$attr]));
    }

    /**
     * Validate record with validation function as incorrect value
     * Return assert exception if validation fail
     * @param OrmRecord $record Orm record with function validate
     * @param string $attr      String value
     * @param Any $value        Value
     */
    public function recordValidateFalse($record, string $attr, $value): void {
        $record->$attr = $value;
        $this->assertFalse($record->validate([$attr]));
    }
}
