<?php

use Codeception\Lib\Console\Output;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
abstract class AbstractTester extends \Codeception\Actor
{
    private $output = null;

    /**
     * Print message into test output
     * @param string $message   Message
     */
    public function print(string $message): void
    {
        if (!$this->output) {
            $this->output = new Output([]);
        }
        $this->output->writeln("\n".$message);
    }

    /**
     * Generate a random string, using a cryptographically secure 
     * pseudorandom number generator (random_int)
     * @param int $length      How many characters do we want
     * @param string $keyspace A string of all possible characters to select from
     * @return string random string with given length
     */
    public function randomStr(int $length): string
    {
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }
}
