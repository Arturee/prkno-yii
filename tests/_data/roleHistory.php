<?php
use app\models\records\User;
use app\models\records\RoleHistory;

return [
	"roleHistory1" => [
		"id" => 1,
		"user_id" => 1,
		"role" => User::ROLE_STUDENT,
		"comment" => "MFF student working on the project",
		"created_at" => "2018-01-15 00:00:00",
		"updated_at" => "2018-01-15 00:00:00"
	],
	"roleHistory2" => [
		"id" => 2,
		"user_id" => 5,
		"role" => User::ROLE_STUDENT,
		"comment" => NULL,
		"created_at" => "2017-12-27 00:00:00",
		"updated_at" => "2017-12-28 00:00:00"
    ],
    "roleHistory3" => [
    	"id" => 3,
    	"user_id" => 7,
    	"role" => User::ROLE_STUDENT,
    	"comment" => "Opponent assigned because of project properties",
    	"created_at" => "2018-01-14 00:00:00",
    	"updated_at" => "2018-01-14 00:00:00"
    ],
    "roleHistory4" => [
    	"id" => 4,
    	"user_id" => 10,
    	"role" => User::ROLE_STUDENT,
    	"comment" => NULL,
    	"created_at" => "2018-01-16 00:00:00",
    	"updated_at" => "2018-01-29 00:00:00"
	],
    "roleHistory5" => [
    	"id" => 5,
    	"user_id" => 5,
    	"role" => User::ROLE_STUDENT,
    	"comment" => NULL,
    	"created_at" => "2017-12-27 00:00:00",
    	"updated_at" => "2017-12-28 00:00:00",
    	"deleted" => true
    ],
    "roleHistory7" => [
	    "id" => 6,
	    "user_id" => 5,
	    "role" => User::ROLE_STUDENT,
	    "comment" => NULL,
	    "created_at" => "2017-12-27 00:00:00",
	    "updated_at" => "2017-12-28 00:00:00"
	],
	"roleHistory8" => [
	    "id" => 7,
	    "user_id" => 5,
	    "role" => User::ROLE_STUDENT,
	    "comment" => NULL,
	    "created_at" => "2017-12-27 00:00:00",
	    "updated_at" => "2017-12-28 00:00:00"
	],
	"roleHistory9" => [
    	"id" => 8,
    	"user_id" => 5,
    	"role" => User::ROLE_STUDENT,
    	"comment" => NULL,
    	"created_at" => "2017-12-27 00:00:00",
    	"updated_at" => "2017-12-28 00:00:00"
	]
];