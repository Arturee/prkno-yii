<?php
use app\models\records\ProposalVote;

return [
	"proposalVote1" => [
		"id" => 1,
		"user_id" => 8,
		"proposal_id" => 1,
		"vote" => ProposalVote::VOTE_YES,
		"created_at" => "2018-02-01 00:00:00",
		"updated_at" => "2018-02-01 00:00:00"
	],
	"proposalVote2" => [
		"id" => 2,
		"user_id" => 5,
		"proposal_id" => 1,
		"vote" => ProposalVote::VOTE_NO,
		"created_at" => "2018-02-01 00:00:00",
		"updated_at" => "2018-02-02 00:00:00"
	],
	"proposalVote3" => [
    	"id" => 3,
    	"user_id" => 4,
    	"proposal_id" => 1,
    	"vote" => ProposalVote::VOTE_YES,
    	"created_at" => "2018-01-18 00:00:00",
    	"updated_at" => "2018-01-18 00:00:00"
    ],
	"proposalVote4" => [
    	"id" => 4,
    	"user_id" => 7,
    	"proposal_id" => 15,
    	"vote" => ProposalVote::VOTE_YES,
    	"created_at" => "2018-01-18 00:00:00",
    	"updated_at" => "2018-01-18 00:00:00"
    ],
    "proposalVote5" => [
    	"id" => 5,
    	"user_id" => 6,
    	"proposal_id" => 15,
    	"vote" => ProposalVote::VOTE_NO,
    	"created_at" => "2018-01-18 00:00:00",
    	"updated_at" => "2018-01-18 00:00:00"
    ],
    "proposalVote6" => [
    	"id" => 6,
    	"user_id" => 7,
    	"proposal_id" => 16,
    	"vote" => ProposalVote::VOTE_NO,
    	"created_at" => "2018-01-18 00:00:00",
    	"updated_at" => "2018-01-18 00:00:00"
    ],
    "proposalVote7" => [
    	"id" => 7,
    	"user_id" => 10,
    	"proposal_id" => 2,
    	"vote" => ProposalVote::VOTE_NO,
    	"created_at" => "2018-01-18 00:00:00",
    	"updated_at" => "2018-01-18 00:00:00"
    ]
];