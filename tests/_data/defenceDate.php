<?php
use app\models\records\DefenceDate;

return [
    "defenceDate1" => [
        "id" => 1,
        "date" => "+25 days",
        "signup_deadline" => "+24 days",
        "active_voting" => true,
        "created_at" => "2018-02-18 00:00:00",
        "updated_at" => "2018-02-19 10:38:55"
    ],
    "defenceDate2" => [
        "id" => 2,
        "date" => "+45 days",
        "signup_deadline" => "+24 days",
        "room" => "S3",
        "active_voting" => false,
        "created_at" => "2018-01-21 00:12:25",
        "updated_at" => "2018-01-30 10:38:55"
    ],
    "defenceDate3" => [
        "id" => 3,
        "date" => "+12 days",
        "signup_deadline" => "+41 days",
        "active_voting" => true,
        "created_at" => "2018-01-30 11:12:24",
        "updated_at" => "2018-02-27 10:38:55"
    ],
    "defenceDate4" => [
        "id" => 4,
        "date" => "+3 months 1 day",
        "signup_deadline" => "3 days",
        "active_voting" => true,
        "created_at" => "2018-02-24 11:12:24",
        "updated_at" => "2018-02-24 11:12:24"
    ],
    "defenceDate5" => [
        "id" => 5,
        "date" => "-3 months 1 day",
        "signup_deadline" => "+3 months",
        "active_voting" => true,
        "created_at" => "2018-01-20 11:12:24",
        "updated_at" => "2018-02-20 14:39:25"
    ],
    "defenceDate6" => [
        "id" => 6,
        "date" => "-2 days",
        "signup_deadline" => "-3 months 2 days",
        "room" => "S5",
        "active_voting" => false,
        "created_at" => "2018-02-21 00:12:24",
        "updated_at" => "2018-02-21 00:12:24"
    ],
    "defenceDate7" => [
        "id" => 7,
        "date" => "+1 week",
        "signup_deadline" => "-1 day",
        "active_voting" => true,
        "created_at" => "2018-02-24 00:12:24",
        "updated_at" => "2018-02-29 00:12:24"
    ],
    "defenceDate8" => [
        "id" => 8,
        "date" => "+3 weeks",
        "signup_deadline" => "-1 day",
        "room" => "S4",
        "active_voting" => false,
        "created_at" => "2018-02-25 00:12:24",
        "updated_at" => "2018-02-25 00:12:24"
    ],
    "defenceDate9" => [
        "id" => 9,
        "date" => "+1 week",
        "signup_deadline" => "+3 days",
        "active_voting" => true,
        "created_at" => "2018-02-04 00:11:24",
        "updated_at" => "2018-02-04 00:11:24"
    ],
    "defenceDate10" => [
        "id" => 10,
        "date" => "+2 days",
        "signup_deadline" => "+1 day",
        "active_voting" => true,
        "created_at" => "2018-02-21 00:12:24",
        "updated_at" => "2018-02-21 00:12:24"
    ]
];