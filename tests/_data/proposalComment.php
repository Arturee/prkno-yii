<?php
use app\models\records\ProposalComment;

return [
	"proposalComment1" => [
		"id" => 1,
		"creator_id" => 5,
		"proposal_id" => 3,
		"content" => "Seems good",
		"committee_only" => 0,
		"created_at" => "2018-02-01 00:00:00",
		"updated_at" => "2018-02-01 00:00:00"
	],
	"proposalComment2" => [
		"id" => 2,
		"creator_id" => 7,
		"proposal_id" => 1,
		"content" => "I think this is great.",
		"committee_only" => 1,
		"created_at" => "2018-02-03 00:00:00",
		"updated_at" => "2018-02-04 00:00:00",
		"deleted" => true
	],
	"proposalComment3" => [
    	"id" => 3,
    	"creator_id" => 10,
    	"proposal_id" => 4,
    	"content" => "Awful.",
    	"committee_only" => 1,
    	"created_at" => "2017-11-11 00:00:00",
    	"updated_at" => "2017-11-13 00:00:00"
    ],
    "proposalComment4" => [
    	"id" => 4,
    	"creator_id" => 5,
    	"proposal_id" => 2,
    	"content" => "MFF worthy stuff",
    	"committee_only" => 0,
    	"created_at" => "2018-01-19 00:00:00",
    	"updated_at" => "2018-01-19 00:00:00",
    	"deleted" => true
    ]
];