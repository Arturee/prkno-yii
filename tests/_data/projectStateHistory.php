<?php
use app\models\records\Project;
use app\models\records\ProjectStateHistory;

return [
	"projectStateHistory1" => [
		"id" =>1,
		"project_id" => 4,
		"state" => Project::STATE_ANALYSIS,
		"comment" => "The proposal was accepted, so the project entered the analysis stage.",
		"created_at" => "2018-01-22 00:00:00",
		"updated_at" => "2018-01-22 00:00:00"
	],
	"projectStateHistory2" => [
    	"id" => 2,
    	"project_id" => 8,
    	"state" => Project::STATE_ANALYSIS_HANDED_IN,
    	"comment" => NULL,
    	"created_at" => "2017-12-20 00:00:00",
    	"updated_at" => "2017-12-20 00:00:00"
    ],
    "projectStateHistory3" => [
    	"id" => 3,
    	"project_id" => 2,
    	"state" => Project::STATE_PROPOSAL,
    	"comment" => NULL,
    	"created_at" => "2017-12-20 00:00:00",
    	"updated_at" => "2017-12-20 00:00:00"
	],
	"projectStateHistory4" => [
		"id" => 4,
		"project_id" => 1,
		"state" => Project::STATE_PROPOSAL,
		"comment" => NULL,
		"created_at" => "2018-01-15 00:00:00",
		"updated_at" => "2018-01-15 00:00:00"
	],
    "projectStateHistory5" => [
    	"id" => 5,
    	"project_id" => 3,
    	"state" => Project::STATE_PROPOSAL,
    	"comment" => NULL,
    	"created_at" => "2018-01-14 00:00:00",
    	"updated_at" => "2018-01-14 00:00:00"
	],
	"projectStateHistory6" => [
		"id" => 6,
		"project_id" => 3,
		"state" => Project::STATE_ACCEPTED,
		"comment" => "Interesting project.",
		"created_at" => "2018-01-19 00:00:00",
		"updated_at" => "2018-01-19 00:00:00"
	],
	"projectStateHistory7" => [
	    "id" => 7,
	    "project_id" => 4,
	    "state" => Project::STATE_PROPOSAL,
	    "comment" => NULL,
	    "created_at" => "2017-11-21 00:00:00",
	    "updated_at" => "2017-11-21 00:00:00"
	],
	"projectStateHistory8" => [
	    "id" => 8,
	    "project_id" => 4,
	    "state" => Project::STATE_ACCEPTED,
	    "comment" => "Looks promising",
	    "created_at" => "2018-01-09 00:00:00",
	    "updated_at" => "2018-01-09 00:00:00",
	    "deleted" => true
	],
	"projectStateHistory9" => [
	    "id" => 9,
	    "project_id" => 5,
	    "state" => Project::STATE_PROPOSAL,
	    "comment" => NULL,
	    "created_at" => "2017-08-30 00:00:00",
	    "updated_at" => "2017-08-30 00:00:00"
	],
	"projectStateHistory10" => [
	    "id" => 10,
	    "project_id" => 5,
	    "state" => Project::STATE_ACCEPTED,
	    "comment" => NULL,
	    "created_at" => "2017-09-01 00:00:00",
	    "updated_at" => "2017-09-01 00:00:00"
	],
	"projectStateHistory11" => [
	    "id" => 11,
	    "project_id" => 5,
	    "state" => Project::STATE_ANALYSIS,
	    "comment" => NULL,
	    "created_at" => "2017-09-11 00:00:00",
	    "updated_at" => "2017-09-11 00:00:00"
    ],
    "projectStateHistory12" => [
    	"id" => 12,
    	"project_id" => 5,
	    "state" => Project::STATE_ANALYSIS_HANDED_IN,
	    "comment" => NULL,
	    "created_at" => "2017-11-01 00:00:00",
	    "updated_at" => "2017-11-01 00:00:00"
	],
	"projectStateHistory13" => [
    	"id" => 13,
    	"project_id" => 6,
    	"state" => Project::STATE_PROPOSAL,
    	"comment" => NULL,
    	"created_at" => "2017-11-16 00:00:00",
    	"updated_at" => "2017-11-16 00:00:00"
    ],
    "projectStateHistory14" => [
    	"id" => 14,
    	"project_id" => 6,
    	"state" => Project::STATE_ACCEPTED,
    	"comment" => "Useful",
    	"created_at" => "2017-11-17 00:00:00",
    	"updated_at" => "2017-11-17 00:00:00"
	],
	"projectStateHistory15" => [
    	"id" => 15,
    	"project_id" => 6,
    	"state" => Project::STATE_ANALYSIS,
    	"comment" => NULL,
    	"created_at" => "2017-11-30 00:00:00",
    	"updated_at" => "2017-11-30 00:00:00",
    	"deleted" => true
	],
    "projectStateHistory16" => [
    	"id" => 16,
    	"project_id" => 6,
    	"state" => Project::STATE_ANALYSIS_HANDED_IN,
    	"comment" => NULL,
    	"created_at" => "2017-12-30 00:00:00",
    	"updated_at" => "2017-12-30 00:00:00"
    ],
    "projectStateHistory17" => [
    	"id" => 17,
    	"project_id" => 6,
    	"state" => Project::STATE_IMPLEMENTATION,
    	"comment" => "Professor Jekyll had trouble at this stage",
    	"created_at" => "2018-02-25 00:00:00",
    	"updated_at" => "2018-02-25 00:00:00"
    ],
    "projectStateHistory18" => [
    	"id" => 18,
    	"project_id" => 8,
    	"state" => Project::STATE_PROPOSAL,
    	"comment" => NULL,
    	"created_at" => "2017-08-16 00:00:00",
    	"updated_at" => "2017-08-16 00:00:00"
	],
	"projectStateHistory19" => [
    	"id" => 19,
    	"project_id" => 8,
    	"state" => Project::STATE_ACCEPTED,
    	"comment" => NULL,
    	"created_at" => "2017-08-18 00:00:00",
    	"updated_at" => "2017-08-18 00:00:00"
	],
	"projectStateHistory20" => [
		"id" => 20,
		"project_id" => 8,
		"state" => Project::STATE_ANALYSIS,
		"comment" => NULL,
		"created_at" => "2017-09-16 00:00:00",
		"updated_at" => "2017-09-16 00:00:00"
	],
	"projectStateHistory21" => [
    	"id" => 21,
    	"project_id" => 8,
    	"state" => Project::STATE_IMPLEMENTATION,
    	"comment" => "The analysis was \Exceptional",
    	"created_at" => "2017-11-18 00:00:00",
    	"updated_at" => "2017-11-18 00:00:00"
    ],
    "projectStateHistory22" => [
    	"id" => 22,
    	"project_id" => 8,
    	"state" => Project::STATE_IMPLEMENTATION_HANDED_IN,
    	"comment" => NULL,
    	"created_at" => "2017-12-18 00:00:00",
    	"updated_at" => "2017-12-18 00:00:00",
    	"deleted" => true
    ],
    "projectStateHistory23" => [
    	"id" => 23,
    	"project_id" => 9,
    	"state" => Project::STATE_PROPOSAL,
    	"comment" => NULL,
    	"created_at" => "2017-11-01 00:00:00",
    	"updated_at" => "2017-11-01 00:00:00"
    ],
    "projectStateHistory24" => [
	    "id" => 24,
	    "project_id" => 9,
	    "state" => Project::STATE_ACCEPTED,
	    "comment" => NULL,
	    "created_at" => "2017-11-03 00:00:00",
	    "updated_at" => "2017-11-03 00:00:00"
	],
	"projectStateHistory25" => [
		"id" => 25,
		"project_id" => 9,
		"state" => Project::STATE_ANALYSIS,
		"comment" => NULL,
		"created_at" => "2017-11-09 00:00:00",
		"updated_at" => "2017-11-09 00:00:00"
	],
	"projectStateHistory26" => [
		"id" => 26,
		"project_id" => 26,
		"state" => 9,
		"comment" => Project::STATE_ANALYSIS_HANDED_IN,
		"created_at" => "2017-12-31 00:00:00",
		"updated_at" => "2017-12-31 00:00:00"
    ],
    "projectStateHistory27" => [
	    "id" => 27,
	    "project_id" => 9,
	    "state" => Project::STATE_IMPLEMENTATION,
	    "comment" => NULL,
	    "created_at" => "2018-01-02 00:00:00",
	    "updated_at" => "2018-01-02 00:00:00"
    ],
    "projectStateHistory28" => [
	    "id" => 28,
	    "project_id" => 9,
	    "state" => Project::STATE_IMPLEMENTATION_HANDED_IN,
	    "comment" => NULL,
	    "created_at" => "2018-03-02 00:00:00",
	    "updated_at" => "2018-03-02 00:00:00"
	],
	"projectStateHistory29" => [
	    "id" => 29,
	    "project_id" => 9,
	    "state" => Project::STATE_CONDITIONALLY_DEFENDED,
	    "comment" => "Some problems with documentation",
	    "created_at" => "2018-04-02 00:00:00",
	    "updated_at" => "2018-04-02 00:00:00"
	],
	"projectStateHistory30" => [
	    "id" => 30,
	    "project_id" => 10,
	    "state" => Project::STATE_PROPOSAL,
	    "comment" => NULL,
	    "created_at" => "2017-05-24 00:00:00",
	    "updated_at" => "2017-05-24 00:00:00"
    ],
    "projectStateHistory31" => [
    	"id" => 31,
    	"project_id" => 10,
    	"state" => Project::STATE_ACCEPTED,
    	"comment" => "I think this will be successful",
    	"created_at" => "2017-05-27 00:00:00",
    	"updated_at" => "2017-05-27 00:00:00"
	],
	"projectStateHistory32" => [
	    "id" => 32,
	    "project_id" => 10,
	    "state" => Project::STATE_ANALYSIS,
	    "comment" => NULL,
	    "created_at" => "2017-06-24 00:00:00",
	    "updated_at" => "2017-06-24 00:00:00"
	],
	"projectStateHistory33" => [
	    "id" => 33,
	    "project_id" => 10,
	    "state" => Project::STATE_ANALYSIS_HANDED_IN,
	    "comment" => NULL,
	    "created_at" => "2017-07-27 00:00:00",
	    "updated_at" => "2017-07-27 00:00:00"
	],
	"projectStateHistory34" => [
	    "id" => 34,
	    "project_id" => 10,
	    "state" => Project::STATE_IMPLEMENTATION,
	    "comment" => NULL,
	    "created_at" => "2017-08-29 00:00:00",
	    "updated_at" => "2017-08-29 00:00:00"
	],
	"projectStateHistory35" => [
	    "id" => 35,
	    "project_id" => 10,
	    "state" => Project::STATE_IMPLEMENTATION_HANDED_IN,
	    "comment" => NULL,
	    "created_at" => "2017-11-29 00:00:00",
	    "updated_at" => "2017-11-29 00:00:00"
	],
	"projectStateHistory36" => [
		"id" => 36,
	    "project_id" => 11,
	    "state" => Project::STATE_PROPOSAL,
	    "comment" => NULL,
	    "created_at" => "2017-09-05 07:56:12",
	    "updated_at" => "2017-09-05 07:56:12"
    ],
    "projectStateHistory37" => [
    	"id" => 37,
    	"project_id" => 11,
    	"state" => Project::STATE_ACCEPTED,
    	"comment" => NULL,
    	"created_at" => "2017-09-11 07:56:12",
    	"updated_at" => "2017-09-11 07:56:12"
	],
	"projectStateHistory38" => [
	    "id" => 38,
	    "project_id" => 12,
	    "state" => Project::STATE_PROPOSAL,
	    "comment" => NULL,
	    "created_at" => "2017-10-16 00:00:00",
	    "updated_at" => "2017-10-16 00:00:00"
	],
	"projectStateHistory39" => [
		"id" => 39,
		"project_id" => 12,
		"state" => Project::STATE_CANCELED,
		"comment" => NULL,
		"created_at" => "2017-10-17 00:00:00",
		"updated_at" => "2017-10-17 00:00:00"
	],
	"projectStateHistory40" => [
	    "id" => 40,
	    "project_id" => 2,
	    "state" => Project::STATE_ACCEPTED,
	    "comment" => NULL,
	    "created_at" => "2018-01-15 00:00:00",
	    "updated_at" => "2018-01-15 00:00:00"
	],
	"projectStateHistory41" => [
	    "id" => 41,
	    "project_id" => 7,
	    "state" => Project::STATE_PROPOSAL,
	    "comment" => NULL,
	    "created_at" => "2018-01-30 17:37:35",
	    "updated_at" => "2018-01-30 17:37:35"
	],
	"projectStateHistory42" => [
	    "id" => 42,
	    "project_id" => 13,
	    "state" => Project::STATE_PROPOSAL,
	    "comment" => NULL,
	    "created_at" => "2018-01-30 17:37:35",
	    "updated_at" => "2018-01-30 17:37:35"
	]
];