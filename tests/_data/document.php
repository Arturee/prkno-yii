<?php
use app\models\records\Document;

return [
	"document1" => [
		"id" => 1,
		"content_md" => "this is the content md of the first first first first first first first first  ID document",
		"fallback_path" => "fallback_path/doc",
		"created_at" => "2018-01-28 00:00:00",
		"updated_at" => "2018-01-30 17:41:17"
	],
    "document2" => [
        "id" => 2,
        "content_md" => NULL,
        "fallback_path" => NULL,
        "created_at" => "2018-01-09 00:00:00",
        "updated_at" => "2018-01-17 00:00:00"
    ],
    "document3" => [
        "id" => 3,
        "content_md" => "this is the content md of the third third third third third third third third third third third third  document",
        "fallback_path" => NULL,
        "created_at" => "2018-01-30 17:42:27",
        "updated_at" => "2018-01-30 17:42:27"
    ],
    "document4" => [
        "id" => 4,
        "content_md" => NULL,
        "fallback_path" => "random_fallback_path",
        "created_at" => "2018-01-07 00:00:00",
        "updated_at" => "2018-01-15 00:00:00",
        "deleted" => 1
    ],
    "document5" => [
        "id" => 5,
        "content_md" => "Performance analysis of Facebook. We will explore all the public data.",
        "fallback_path" => NULL,
        "created_at" => "2017-12-11 00:00:00",
        "updated_at" => "2018-01-21 00:00:00"
    ],
    "document6" => [
        "id" => 6,
        "content_md" => NULL,
        "fallback_path" => "fallback_ex/folder",
        "created_at" => "2018-01-21 00:00:00",
        "updated_at" => "2018-01-21 00:00:00",
        "deleted" => true
    ],
    "document7" => [
        "id" => 7,
        "content_md" => "Musím opustit projekt.",
        "fallback_path" => NULL,
        "created_at" => "2018-01-28 00:00:00",
        "updated_at" => "2018-01-28 00:00:00"
    ],
    "document8" => [
        "id" => 8,
        "content_md" => "Posudek první",
        "fallback_path" => NULL,
        "created_at" => "2018-01-23 00:00:00",
        "updated_at" => "2018-01-23 00:00:00"
    ],
    "document9" => [
        "id" => 9,
        "content_md" => "Posudek oponenta",
        "fallback_path" => NULL,
        "created_at" => "2018-01-20 00:00:00",
        "updated_at" => "2018-01-20 00:00:00"
    ],
    "document10" => [
        "id" => 10,
        "content_md" => "Defense review",
        "fallback_path" => NULL,
        "created_at" => "2018-02-12 00:00:00",
        "updated_at" => "2018-02-12 00:00:00"
    ],
    "document11" => [
        "id" => 11,
        "content_md" => NULL,
        "fallback_path" => NULL,
        "created_at" => "2018-01-31 00:00:00",
        "updated_at" => "2018-02-01 00:00:00"
    ],
    "document12" => [
        "id" => 12,
        "content_md" => NULL,
        "fallback_path" => NULL,
        "created_at" => "2017-10-23 00:00:00",
        "updated_at" => "2017-10-31 00:00:00",
        "deleted" => true
    ],
    "document13" => [
        "id" => 13,
        "content_md" => "Third proposal.",
        "fallback_path" => NULL,
        "created_at" => "2018-01-30 00:00:00",
        "updated_at" => "2018-01-30 00:00:00"
    ],
    "document14" => [
        "id" => 14,
        "content_md" => "Supervisor review for project 8",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document15" => [
        "id" => 15,
        "content_md" => "Proposal for project 3",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document16" => [
        "id" => 16,
        "content_md" => "Proposal for project 4",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document17" => [
        "id" => 17,
        "content_md" => "Proposal for project 5",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document18" => [
        "id" => 18,
        "content_md" => "Proposal for project 6",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document19" => [
        "id" => 19,
        "content_md" => "Proposal for project 7",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document20" => [
        "id" => 20,
        "content_md" => "Proposal for project 8",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document21" => [
        "id" => 21,
        "content_md" => "Proposal for project 9",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document22" => [
        "id" => 22,
        "content_md" => "Proposal for project 10",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document23" => [
        "id" => 23,
        "content_md" => "Proposal for project 11",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document24" => [
        "id" => 24,
        "content_md" => "Proposal for project 12",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document25" => [
        "id" => 25,
        "content_md" => "The analysis is finished, but there aren't many diagrams.",
        "fallback_path" => NULL,
        "created_at" => "2017-12-11 19:30:00",
        "updated_at" => "2017-12-11 19:30:00"
    ],
    "document26" => [
        "id" => 26,
        "content_md" => "I would decline due to the lack of diagrams, and let the team work a bit harder on them.",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document27" => [
        "id" => 27,
        "content_md" => "# Final statement for NPREL #\n Due to the lack of nice diagrams, the project <span style='color: orange;' title=\"That's harsh, I know\">failed to defend.</span><script>alert('haha ur hacked')</script>",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document28" => [
        "id" => 28,
        "content_md" => NULL,
        "fallback_path" => 'docs/sources.zip',
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document29" => [
        "id" => 29,
        "content_md" => NULL,
        "fallback_path" => 'docs/sources2.zip',
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document30" => [
        "id" => 30,
        "content_md" => NULL,
        "fallback_path" => 'docs/text1.zip',
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document31" => [
        "id" => 31,
        "content_md" => NULL,
        "fallback_path" => 'docs/text2.zip',
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document32" => [
        "id" => 32,
        "content_md" => NULL,
        "fallback_path" => 'docs/src-num2.zip',
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document33" => [
        "id" => 33,
        "content_md" => NULL,
        "fallback_path" => 'docs/text-num2.pdf',
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document34" => [
        "id" => 34,
        "content_md" => NULL,
        "fallback_path" => 'docs/sources-num3.zip',
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document35" => [
        "id" => 35,
        "content_md" => NULL,
        "fallback_path" => 'docs/textpdf-num3.pdf',
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document36" => [
        "id" => 36,
        "content_md" => "Diagrams done",
        "fallback_path" => NULL,
        "created_at" => "2017-12-11 19:30:00",
        "updated_at" => "2017-12-11 19:30:00"
    ],
    "document37" => [
        "id" => 37,
        "content_md" => "I vote the let them pass.\n",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document38" => [
        "id" => 38,
        "content_md" => "Diagrams are sufficient, the project passes. \n",
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ],
    "document39" => [
        "id" => 39,
        "content_md" => NULL,
        "fallback_path" => NULL,
        "created_at" => "2017-12-26 00:00:00",
        "updated_at" => "2017-12-26 00:00:00"
    ]
];