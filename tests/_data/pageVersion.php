<?php
use app\models\records\PageVersion;

return [
    "pageVersion1" => [
        "id" => 1,
        "page_id" => 2,
        "user_id" => 8,
        "content_en" => 6,
        "content_cs" => 3,
        "published" => 1,
        "created_at" => "2017-12-17 00:00:00",
        "updated_at" => "2018-01-22 00:00:00",
        "deleted" => 0
    ],
    "pageVersion2" => [
        "id" => 2,
        "page_id" => 2,
        "user_id" => 10,
        "content_en" => 4,
        "content_cs" => 6,
        "published" => 0,
        "created_at" => "2018-01-23 00:00:00",
        "updated_at" => "2018-01-30 00:00:00",
        "deleted" => 1
    ],
    "pageVersion3" => [
        "id" => 3,
        "page_id" => 3,
        "user_id" => 5,
        "content_en" => 5,
        "content_cs" => 1,
        "published" => 1,
        "created_at" => "2018-01-21 00:00:00",
        "updated_at" => "2018-01-21 00:00:00",
        "deleted" => 1
    ],
    "pageVersion4" => [
        "id" => 4,
        "page_id" => 1,
        "user_id" => 6,
        "content_en" => 5,
        "content_cs" => 2,
        "published" => 0,
        "created_at" => "2018-01-23 00:00:00",
        "updated_at" => "2018-01-23 00:00:00",
        "deleted" => 0
    ]
];