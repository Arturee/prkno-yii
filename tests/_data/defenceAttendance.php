<?php
use app\models\records\defenceAttendance;

return [
    "defenceAttendance1" => [
        "id" => 1,
        "defence_date_id" => 1,
        "user_id" => 8,
        "vote" => defenceAttendance::VOTE_YES,
        "comment" => "I'll definitely be there.",
        "must_come" => true,
        "created_at" => "2018-01-22 00:00:00",
        "updated_at" => "2018-01-23 00:00:00"
    ],
    "defenceAttendance2" => [
        "id" => 2,
        "defence_date_id" => 1,
        "user_id" => 10,
        "vote" => defenceAttendance::VOTE_NO,
        "comment" => "I have a dentist appointment.",
        "must_come" => true,
        "created_at" => "2018-01-22 00:00:00",
        "updated_at" => "2018-01-22 00:00:00",
        "deleted" => true
    ],
    "defenceAttendance3" => [
        "id" => 3,
        "defence_date_id" => 4,
        "user_id" => 6,
        "vote" => defenceAttendance::VOTE_UNKNOWN,
        "comment" => "I'll definitely be there.",
        "must_come" => true,
        "created_at" => "2018-01-07 00:00:00",
        "updated_at" => "2018-01-07 00:00:00"
    ],
    "defenceAttendance4" => [
        "id" => 4,
        "defence_date_id" => 3,
        "user_id" => 7,
        "vote" => defenceAttendance::VOTE_YES,
        "comment" => NULL,
        "must_come" => false,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance5" => [
        "id" => 5,
        "defence_date_id" => 3,
        "user_id" => 2,
        "vote" => defenceAttendance::VOTE_NO,
        "comment" => "Jsem na státnicích.",
        "must_come" => false,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance6" => [
        "id" => 6,
        "defence_date_id" => 3,
        "user_id" => 3,
        "vote" => defenceAttendance::VOTE_YES,
        "comment" => "Kdykoliv po 17:00, předtím přednáším.",
        "must_come" => false,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance7" => [
        "id" => 7,
        "defence_date_id" => 3,
        "user_id" => 5,
        "vote" => defenceAttendance::VOTE_UNKNOWN,
        "comment" => NULL,
        "must_come" => false,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance8" => [
        "id" => 8,
        "defence_date_id" => 3,
        "user_id" => 6,
        "vote" => defenceAttendance::VOTE_UNKNOWN,
        "comment" => NULL,
        "must_come" => false,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance9" => [
        "id" => 9,
        "defence_date_id" => 3,
        "user_id" => 7,
        "vote" => defenceAttendance::VOTE_UNKNOWN,
        "comment" => NULL,
        "must_come" => false,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance10" => [
        "id" => 10,
        "defence_date_id" => 3,
        "user_id" => 8,
        "vote" => defenceAttendance::VOTE_UNKNOWN,
        "comment" => NULL,
        "must_come" => false,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance11" => [
        "id" => 11,
        "defence_date_id" => 5,
        "user_id" => 6,
        "vote" => defenceAttendance::VOTE_YES,
        "comment" => NULL,
        "must_come" => true,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance12" => [
        "id" => 12,
        "defence_date_id" => 5,
        "user_id" => 7,
        "vote" => defenceAttendance::VOTE_YES,
        "comment" => NULL,
        "must_come" => true,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance13" => [
        "id" => 13,
        "defence_date_id" => 5,
        "user_id" => 9,
        "vote" => defenceAttendance::VOTE_YES,
        "comment" => NULL,
        "must_come" => true,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
    ],
    "defenceAttendance14" => [
        "id" => 14,
        "defence_date_id" => 5,
        "user_id" => 10,
        "vote" => defenceAttendance::VOTE_YES,
        "comment" => NULL,
        "must_come" => true,
        "created_at" => "2018-02-05 14:00:00",
        "updated_at" => "2018-02-05 14:00:00"
	]
];