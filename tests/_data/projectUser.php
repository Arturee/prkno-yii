<?php
use app\models\records\ProjectUser;

return [
    "projectUser1" => [
        "id" => 1,
        "user_id" => 10,
        "project_id" => 1,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser2" => [
        "id" => 2,
        "user_id" => 10,
        "project_id" => 2,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser3" => [
        "id" => 3,
        "user_id" => 10,
        "project_id" => 3,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser4" => [
        "id" => 4,
        "user_id" => 8,
        "project_id" => 4,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser5" => [
        "id" => 5,
        "user_id" => 8,
        "project_id" => 5,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser6" => [
        "id" => 6,
        "user_id" => 8,
        "project_id" => 6,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser7" => [
        "id" => 7,
        "user_id" => 1,
        "project_id" => 7,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser8" => [
        "id" => 8,
        "user_id" => 2,
        "project_id" => 8,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser9" => [
        "id" => 9,
        "user_id" => 3,
        "project_id" => 9,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser10" => [
        "id" => 10,
        "user_id" => 4,
        "project_id" => 10,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser9" => [
        "id" => 9,
        "user_id" => 5,
        "project_id" => 9,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser10" => [
        "id" => 10,
        "user_id" => 6,
        "project_id" => 10,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser11" => [
        "id" => 11,
        "user_id" => 7,
        "project_id" => 9,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser12" => [
        "id" => 12,
        "user_id" => 8,
        "project_id" => 9,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser13" => [
        "id" => 13,
        "user_id" => 3,
        "project_id" => 2,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser14" => [
        "id" => 14,
        "user_id" => 8,
        "project_id" => 10,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser15" => [
        "id" => 15,
        "user_id" => 6,
        "project_id" => 6,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser16" => [
        "id" => 16,
        "user_id" => 8,
        "project_id" => 9,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser17" => [
        "id" => 17,
        "user_id" => 10,
        "project_id" => 4,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser18" => [
        "id" => 18,
        "user_id" => 7,
        "project_id" => 2,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser19" => [
        "id" => 19,
        "user_id" => 4,
        "project_id" => 2,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ],
    "projectUser20" => [
        "id" => 20,
        "user_id" => 2,
        "project_id" => 4,
        "project_role" => ProjectUser::ROLE_SUPERVISOR,
        "comment" => NULL,
        "created_at" => "2018-03-02 00:00:00",
        "updated_at" => "2018-03-02 00:00:00"
    ]
];