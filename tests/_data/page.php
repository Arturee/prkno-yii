<?php
use app\models\records\Page;

return [
    'page1' => [
        "id" => 1,
        "title_en" => "Rules of software project",
        "title_cs" => "Pravidlá softwarových projektu",
        "path_pdf_download" => "pages/rules.pdf",
        "url" => "projectrules",
        "order_in_menu" => 2,
        "published" => 0,
        "created_at" => "2018-01-14 00:00:00",
        "updated_at" => "2018-01-29 00:00:00",
        "deleted" => 0
    ],
    'page2' => [
        "id" => 2,
        "title_en" => NULL,
        "title_cs" => "Prubeh práce na projektu",
        "path_pdf_download" => NULL,
        "url" => "projectflow",
        "order_in_menu" => 3,
        "published" => 1,
        "created_at" => "2018-02-01 00:00:00",
        "updated_at" => "2018-02-01 00:00:00",
        "deleted" => 1
    ],
    'page3' => [
        "id" => 3,
        "title_en" => "Tutorial",
        "title_cs" => NULL,
        "path_pdf_download" => "pages/tutorial.pdf",
        "url" => "tutorial",
        "order_in_menu" => 1,
        "published" => 0,
        "created_at" => "2018-01-14 00:00:00",
        "updated_at" => "2018-01-14 00:00:00",
        "deleted" => 0
    ],
    'page4' => [
        "id" => 4,
        "title_en" => "Committee members",
        "title_cs" => NULL,
        "path_pdf_download" => NULL,
        "url" => "committee",
        "order_in_menu" => 4,
        "published" => 1,
        "created_at" => "2017-11-13 00:00:00",
        "updated_at" => "2018-01-28 00:00:00",
        "deleted" => 0
    ]
];