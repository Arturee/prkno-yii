<?php
use app\models\records\Advertisement;

return [
    'advertisement1' => [
        "id" => 1,
        "user_id" => 7,
        "title" => "Facebook performance analysis",
        "description" => "Content of the performance analysis of Facebook.",
        "type" => Advertisement::TYPE_PROJECT,
        "keywords" => "facebook,performance",
        "subscribed_via_email" => false,
        "published" => true,
        "created_at" => "2018-01-16 03:15:11",
        "updated_at" => "2018-01-29 01:22:25"
    ],
    'advertisement2' => [
        "id" => 2,
        "user_id" => 7,
        "title" => "Student looking for C# projects",
        "description" => "Hi. I am student looking for some project written in C#.",
        "type" => Advertisement::TYPE_PERSON,
        "keywords" => NULL,
        "subscribed_via_email" => true,
        "published" => false,
        "created_at" => "2018-01-21 00:00:00",
        "updated_at" => "2018-01-21 00:00:00",
    ],
    'advertisement3' => [
        "id" => 3,
        "user_id" => 8,
        "title" => "Project advertisement",
        "description" => "Content of a project advertisement",
        "type" => Advertisement::TYPE_PROJECT,
        "keywords" => NULL,
        "subscribed_via_email" => true,
        "published" => true,
        "created_at" => "2018-01-15 00:00:00",
        "updated_at" => "2018-02-01 00:00:00"
    ],
    'advertisement4' => [
        "id" => 4,
        "user_id" => 3,
        "title" => "Non published unsubscribed ad",
        "description" => "Content of non published unsubscribed ad",
        "type" => Advertisement::TYPE_PERSON,
        "keywords" => "advertisement",
        "subscribed_via_email" => false,
        "published" => false,
        "created_at" => "2018-01-21 00:00:00",
        "updated_at" => "2018-01-21 00:00:00"
    ],
    'advertisement5' => [
        "id" => 5,
        "user_id" => 3,
        "title" => "Published advertisement with markdown content",
        "description" => "# H1\n## H2\n### H3 Markdown content of subscribed published advertisement",
        "type" => Advertisement::TYPE_PERSON,
        "keywords" => "keyword1,keyword2",
        "subscribed_via_email" => true,
        "published" => true,
        "created_at" => "2018-01-21 00:00:00",
        "updated_at" => "2018-01-21 00:00:00",
    ],
    'advertisement6' => [
        "id" => 6,
        "user_id" => 3,
        "title" => "Title of the deleted advertisement",
        "description" => "Content of the deleted advertisement",
        "type" => Advertisement::TYPE_PERSON,
        "keywords" => "keyword1,keyword2",
        "subscribed_via_email" => true,
        "published" => true,
        "created_at" => "2018-01-21 00:00:00",
        "updated_at" => "2018-01-21 00:00:00",
        "deleted" => true
    ]
];