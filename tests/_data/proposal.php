<?php
use app\models\records\Proposal;

return [
	"proposal1" => [
		"id" => 1,
		"project_id" => 1,
		"document_id" => 11,
		"state" => Proposal::STATE_SENT,
		"comment" => NULL,
		"created_at" => "2018-01-31 00:00:00",
		"updated_at" => "2018-02-01 00:00:00"
	],
	"proposal2" => [
	    "id" => 2,
	    "project_id" => 2,
	    "document_id" => 25,
	    "state" => Proposal::STATE_ACCEPTED,
	    "comment" => "Good",
	    "created_at" => "2018-01-17 00:00:00",
	    "updated_at" => "2018-01-17 00:00:00"
	],
	"proposal3" => [
		"id" => 3,
		"project_id" => 1,
		"document_id" => 13,
		"state" => Proposal::STATE_DRAFT,
		"comment" => NULL,
		"created_at" => "2018-01-30 19:00:00",
		"updated_at" => "2018-01-30 19:00:00",
		"deleted" => true
	],
	"proposal4" => [
    	"id" => 4,
    	"project_id" => 12,
    	"document_id" => 12,
    	"state" => Proposal::STATE_DECLINED,
    	"comment" => "Serious issues with this proposal",
    	"created_at" => "2017-10-25 00:00:00",
    	"updated_at" => "2017-11-07 00:00:00",
    	"deleted" => true
    ],
    "proposal5" => [
	    "id" => 5,
	    "project_id" => 3,
	    "document_id" => 15,
	    "state" => Proposal::STATE_ACCEPTED,
	    "comment" => NULL,
	    "created_at" => "2017-10-25 00:00:00",
	    "updated_at" => "2017-10-25 00:00:00"
    ],
    "proposal6" => [
    	"id" => 6,
    	"project_id" => 4,
    	"document_id" => 16,
    	"state" => Proposal::STATE_ACCEPTED,
    	"comment" => "Nice",
    	"created_at" => "2017-10-25 00:00:00",
    	"updated_at" => "2017-10-25 00:00:00"
	],
	"proposal7" => [
		"id" => 7,
    	"project_id" => 5,
    	"document_id" => 17,
    	"state" => Proposal::STATE_ACCEPTED,
    	"comment" => NULL,
    	"created_at" => "2017-10-25 00:00:00",
    	"updated_at" => "2017-10-25 00:00:00"
	],
	"proposal8" => [
		"id" => 8,
	    "project_id" => 6,
	    "document_id" => 18,
	    "state" => Proposal::STATE_ACCEPTED,
	    "comment" => "Cool",
	    "created_at" => "2017-10-25 00:00:00",
	    "updated_at" => "2017-10-25 00:00:00"
	],
	"proposal9" => [
    	"id" => 9,
    	"project_id" => 8,
    	"document_id" => 20,
    	"state" => Proposal::STATE_ACCEPTED,
    	"comment" => NULL,
    	"created_at" => "2017-10-25 00:00:00",
    	"updated_at" => "2017-10-25 00:00:00"
	],
	"proposal10" => [
		"id" => 10,
		"project_id" => 9,
		"document_id" => 21,
		"state" => Proposal::STATE_ACCEPTED,
		"comment" => NULL,
		"created_at" => "2017-10-25 00:00:00",
		"updated_at" => "2017-10-25 00:00:00"
    ],
    "proposal11" => [
    	"id" => 11,
    	"project_id" => 10,
    	"document_id" => 22,
    	"state" => Proposal::STATE_ACCEPTED,
    	"comment" => NULL,
    	"created_at" => "2017-10-25 00:00:00",
    	"updated_at" => "2017-10-25 00:00:00"
	],
	"proposal12" => [
		"id" => 12,
		"project_id" => 11,
		"document_id" => 23,
		"state" => Proposal::STATE_ACCEPTED,
		"comment" => NULL,
		"created_at" => "2017-10-25 00:00:00",
		"updated_at" => "2017-10-25 00:00:00"
	],
	"proposal13" => [
		"id" => 13,
		"project_id" => 12,
		"document_id" => 24,
		"state" => Proposal::STATE_ACCEPTED,
		"comment" => NULL,
		"created_at" => "2017-10-25 00:00:00",
		"updated_at" => "2017-10-25 00:00:00"
	],
    "proposal14" => [
    	"id" => 14,
    	"project_id" => 7,
    	"document_id" => 19,
    	"state" => Proposal::STATE_DRAFT,
    	"comment" => NULL,
    	"created_at" => "2018-01-30 19:00:00",
    	"updated_at" => "2018-01-30 19:00:00"
    ]
];