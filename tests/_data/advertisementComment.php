<?php
use app\models\records\AdvertisementComment;

return [
    'advertisementComment1' => [
        "id" => 1,
        "ad_id" => 5,
        "user_id" => 1,
        "content" => "Some comment 1",
        "created_at" => "2018-01-16 00:00:00",
        "updated_at" => "2018-01-27 00:00:00"
    ],
    'advertisementComment2' => [
        "id" => 2,
        "ad_id" => 5,
        "user_id" => 2,
        "content" => "Some comment 2",
        "created_at" => "2018-01-20 00:00:00",
        "updated_at" => "2018-01-23 00:00:00"
    ],
    'advertisementComment3' => [
        "id" => 3,
        "ad_id" => 4,
        "user_id" => 3,
        "content" => "Some comment 3",
        "created_at" => "2018-01-25 00:00:00",
        "updated_at" => "2018-01-25 00:00:00"
    ],
    'advertisementComment4' => [
        "id" => 4,
        "ad_id" => 4,
        "user_id" => 3,
        "content" => "Some comment 3",
        "created_at" => "2018-01-25 00:00:00",
        "updated_at" => "2018-01-25 00:00:00",
        "deleted" => 1
    ]
];