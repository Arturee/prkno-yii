<?php
use app\models\records\ProjectDocument;

return [
	"projectDocument1" => [
    	"id" => 1,
    	"project_id" => 3,
    	"document_id" => 3,
    	"comment" => "First draft of analysis.",
    	"team_private" => 1,
    	"defence_number" => 2,
    	"type" => ProjectDocument::TYPE_READ_ONLY,
    	"created_at" => "2018-01-21 00:00:00",
    	"updated_at" => "2018-01-30 00:00:00"
    ],
    "projectDocument2" => [
    	"id" => 2,
    	"project_id" => 9,
    	"document_id" => 2,
    	"comment" => NULL,
    	"team_private" => 1,
    	"defence_number" => NULL,
    	"type" => ProjectDocument::TYPE_FOR_DEFENSE_ZIP,
    	"created_at" => "2018-02-03 00:00:00",
    	"updated_at" => "2018-02-04 00:00:00"
    ],
    "projectDocument3" => [
    	"id" => 3,
    	"project_id" => 11,
    	"document_id" => 1,
    	"comment" => "Supervisor advice.",
    	"team_private" => 0,
    	"defence_number" => NULL,
    	"type" => ProjectDocument::TYPE_ADVICE,
    	"created_at" => "2018-01-14 00:00:00",
    	"updated_at" => "2018-01-14 00:00:00",
    	"deleted" => true
    ],
    "projectDocument4" => [
    	"id" => 4,
    	"project_id" => 5,
    	"document_id" => 4,
    	"comment" => NULL,
    	"team_private" => 1,
    	"defence_number" => NULL,
    	"type" => ProjectDocument::TYPE_READ_ONLY,
    	"created_at" => "2018-01-07 00:00:00",
    	"updated_at" => "2018-01-11 00:00:00"
    ],
    "projectDocument5" => [
    	"id" => 5,
    	"project_id" => 5,
    	"document_id" => 28,
    	"comment" => NULL,
    	"team_private" => 0,
    	"defence_number" => 1,
    	"type" => ProjectDocument::TYPE_FOR_DEFENSE_ZIP,
    	"created_at" => "2018-01-07 00:00:00",
    	"updated_at" => "2018-01-11 00:00:00"
    ],
    "projectDocument6" => [
    	"id" => 6,
    	"project_id" => 5,
    	"document_id" => 29,
    	"comment" => NULL,
    	"team_private" => 0,
    	"defence_number" => 1,
    	"type" => ProjectDocument::TYPE_FOR_DEFENSE_ZIP,
    	"created_at" => "2018-01-08 00:00:00",
    	"updated_at" => "2018-01-11 00:00:00"
    ],
    "projectDocument7" => [
    	"id" => 7,
    	"project_id" => 5,
    	"document_id" => 30,
    	"comment" => NULL,
    	"team_private" => 0,
    	"defence_number" => 1,
    	"type" => ProjectDocument::TYPE_FOR_DEFENSE_PDF,
    	"created_at" => "2018-01-07 00:00:00",
    	"updated_at" => "2018-01-11 00:00:00"
    ],
    "projectDocument8" => [
    	"id" => 8,
    	"project_id" => 5,
    	"document_id" => 31,
    	"comment" => NULL,
    	"team_private" => 0,
    	"defence_number" => 1,
    	"type" => ProjectDocument::TYPE_FOR_DEFENSE_PDF,
    	"created_at" => "2018-01-08 00:00:00",
    	"updated_at" => "2018-01-11 00:00:00"
    ],
    "projectDocument9" => [
    	"id" => 9,
    	"project_id" => 5,
    	"document_id" => 32,
    	"comment" => NULL,
    	"team_private" => 0,
    	"defence_number" => 2,
    	"type" => ProjectDocument::TYPE_FOR_DEFENSE_ZIP,
    	"created_at" => "2018-03-08 00:00:00",
    	"updated_at" => "2018-01-11 00:00:00"
    ],
    "projectDocument10" => [
    	"id" => 10,
    	"project_id" => 5,
    	"document_id" => 33,
    	"comment" => NULL,
    	"team_private" => 0,
    	"defence_number" => 2,
    	"type" => ProjectDocument::TYPE_FOR_DEFENSE_PDF,
    	"created_at" => "2018-03-08 00:00:00",
    	"updated_at" => "2018-01-11 00:00:00"
    ],
    "projectDocument11" => [
    	"id" => 11,
    	"project_id" => 5,
    	"document_id" => 34,
    	"comment" => NULL,
    	"team_private" => 0,
    	"defence_number" => 3,
    	"type" => ProjectDocument::TYPE_FOR_DEFENSE_ZIP,
    	"created_at" => "2018-07-08 00:00:00",
    	"updated_at" => "2018-01-11 00:00:00"
    ],
    "projectDocument12" => [
    	"id" => 12,
    	"project_id" => 5,
    	"document_id" => 35,
    	"comment" => NULL,
    	"team_private" => 0,
    	"defence_number" => 3,
    	"type" => ProjectDocument::TYPE_FOR_DEFENSE_PDF,
    	"created_at" => "2018-07-08 00:00:00",
    	"updated_at" => "2018-01-11 00:00:00"
    ]
];