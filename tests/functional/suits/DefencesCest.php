<?php
namespace app\tests\functional\suits;

use app\models\records\Project;
use app\models\records\ProjectUser;
use app\models\records\User;
use app\models\records\DefenceDate;
use app\tests\fixtures\DefenceAttendanceFixture;
use app\tests\fixtures\DefenceDateFixture;
use app\tests\fixtures\DocumentFixture;
use app\tests\fixtures\ProjectFixture;
use app\tests\fixtures\ProjectUserFixture;
use app\tests\fixtures\ProjectStateHistoryFixture;
use app\tests\fixtures\ProjectDefenceFixture;
use app\tests\fixtures\ProjectDocumentFixture;
use app\tests\fixtures\ProposalFixture;
use app\tests\fixtures\UserFixture;
use app\utils\DateTime;
use Yii;

/**
 * Functional tests for defence dates
 */
class DefencesCest
{
    /**
     * Mocking mailer to the custom class
     */
    public function _before()
    {
        Yii::$app->set('mailer', 'app\tests\mock\FunctionalTestMailer');
    }

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'project' => [
                'class' => ProjectFixture::className(),
                'dataFile' => codecept_data_dir() . 'project.php'
            ],
            'projectUser' => [
                'class' => ProjectUserFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectUser.php'
            ],
            'projectStateHistory' => [
                'class' => ProjectStateHistoryFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectStateHistory.php'
            ],
            'documents' => [
                'class' => DocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'document.php'
            ],
            'projectDocuments' => [
                'class' => ProjectDocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectDocument.php'
            ],
            'proposals' => [
                'class' => ProposalFixture::className(),
                'dataFile' => codecept_data_dir() . 'proposal.php'
            ],
            'defenceDates' => [
                'class' => DefenceDateFixture::className(),
                'dataFile' => codecept_data_dir() . 'defenceDate.php'
            ],
            'projectDefences' => [
                'class' => ProjectDefenceFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectDefence.php'
            ],
            'defenceAttendance' => [
                'class' => DefenceAttendanceFixture::className(),
                'dataFile' => codecept_data_dir() . 'defenceAttendance.php'
            ]
        ];
    }

    /**
     * Test whether the committee chair see headings of tables in
     * main defence date page
     */
    public function testViewHeadings(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['defence-dates']);
        $I->see('Voting');
        $I->see('Future defences');
        $I->see('Past defences');
    }

    /**
     * Test commitee chair whether have access to see list of defence-dates
     */
    public function testSeeList(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['defence-dates']);
    }

    /**
     * See defence dates in the right categories
     */
    public function testDefenceList(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['defence-dates']);
        $all = DefenceDate::find()->all();
        foreach ($all as $defenceDate) {
            if (!$defenceDate->deleted) {
                $confirmed = !$defenceDate->active_voting;
                $dateTime = new DateTime($defenceDate->date);
                $future = $dateTime->isInFuture();
                $id = $defenceDate->id;
                $dateString = $dateTime->format(DateTime::DATE);
                $I->see($dateString, 'tr[data-key="' . $id . '"] td');
                if (!$confirmed && $future) {
                    $I->see($dateString, "#table-voting");
                }
                if ($confirmed && $future) {
                    $I->see($dateString, "#table-future-defences");
                }
                if ($confirmed && !$future) {
                    $I->see($dateString, "#table-past-defences");
                }
            }
        }
    }

    /**
     * Test whether commitee chair see room on past defence dates
     */
    public function testDefenceSeeRoom(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['defence-dates']);
        $all = DefenceDate::find()->all();
        foreach ($all as $defenceDate) {
            if (!$defenceDate->deleted) {
                $confirmed = !$defenceDate->active_voting;
                $id = $defenceDate->id;
                if ($confirmed && $defenceDate->room) {
                    $I->see($defenceDate->room, 'tr[data-key="' . $id . '"] td');
                }
            }
        }
    }

    /**
     * Test commitee chair to see project names in past defences
     */
    public function testDefenceSeeProjectnames(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['defence-dates']);
        $all = DefenceDate::find()->all();
        foreach ($all as $defenceDate) {
            $dateTime = new DateTime($defenceDate->date);
            $confirmed = !$defenceDate->active_voting;
            if ($confirmed && !$defenceDate->deleted && $dateTime->isInPast()) {
                $id = $defenceDate->id;
                $projectDefences = $defenceDate->getProjectDefences()->all();
                foreach ($projectDefences as $projectDefence) {
                    $projectName = $projectDefence->project->name;
                    $I->see($projectName, 'tr[data-key="' . $id . '"] td');
                }
            }
        }
    }

    /**
     * See defences as guest
     */
    public function testSeeListAsGuest(\FunctionalTester $I)
    {
        $I->amOnPage(['defence-dates']);
        $I->dontSeeElement("#table-voting");
        $I->seeElement("#table-future-defences");
        $I->seeElement("#table-past-defences");
    }

    /**
     * See defences as student
     */
    public function testSeeListAsStudent(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_STUDENT);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['defence-dates']);
        $I->dontSeeElement("#table-voting");
        $I->seeElement("#table-future-defences");
        $I->seeElement("#table-past-defences");
    }

    /**
     * See defence as academic
     */
    public function testSeeListAsAcademic(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_ACADEMIC);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['defence-dates']);
        $I->dontSeeElement("#table-voting");
        $I->seeElement("#table-future-defences");
        $I->seeElement("#table-past-defences");
    }

    /**
     * Test update unconfirmed defence date
     */
    public function testUpdateUnconfirmedDefenceDate(\FunctionalTester $I)
    {
        $defenceDateId = 1;
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['defence-dates/update', 'id' => $defenceDateId]);
        $I->see("Confirm Date");
        $I->see("Delete");
        $I->dontSee("Room");
        $I->dontSee("Add projects");
    }

    /**
     * Test to see elements on confirmed defence date page
     */
    public function testUpdateConfirmedDefenceDate(\FunctionalTester $I)
    {
        $defenceDateId = 2;
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['defence-dates/update', 'id' => $defenceDateId]);
        $I->dontSee("Confirm Date");
        $I->see("Delete");
        $I->see("Room");
        $I->see("Add projects");
    }
}