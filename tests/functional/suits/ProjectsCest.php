<?php
namespace app\tests\functional\suits;

use app\models\records\Project;
use app\models\records\ProjectUser;
use app\models\records\User;
use app\tests\fixtures\DocumentFixture;
use app\tests\fixtures\ProjectFixture;
use app\tests\fixtures\ProjectUserFixture;
use app\tests\fixtures\ProjectStateHistoryFixture;
use app\tests\fixtures\ProjectDocumentFixture;
use app\tests\fixtures\ProposalFixture;
use app\tests\fixtures\UserFixture;
use Yii;

/**
 * Functional tests for projects
 */
class ProjectsCest
{
    /**
     * Mocking mailer to the custom class
     */
    public function _before()
    {
        Yii::$app->set('mailer', 'app\tests\mock\FunctionalTestMailer');
    }

    /**
     * Fixtures
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'project' => [
                'class' => ProjectFixture::className(),
                'dataFile' => codecept_data_dir() . 'project.php'
            ],
            'projectUser' => [
                'class' => ProjectUserFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectUser.php'
            ],
            'projectStateHistory' => [
                'class' => ProjectStateHistoryFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectStateHistory.php'
            ],
            'documents' => [
                'class' => DocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'document.php'
            ],
            'projectDocuments' => [
                'class' => ProjectDocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectDocument.php'
            ],
            'proposals' => [
                'class' => ProposalFixture::className(),
                'dataFile' => codecept_data_dir() . 'proposal.php'
            ]
        ];
    }

    /**
     * See heading
     */
    public function testViewHeading(\FunctionalTester $I)
    {
        $user = User::findById(1);
        $I->amLoggedInAs($user);
        $I->amOnPage(['projects']);
        $I->see('Projects', 'h1');
    }

    /**
     * Don't see Create Project and My projects links as guest
     */
    public function testProjectDontSeeLinksAsGuest(\FunctionalTester $I)
    {
        $I->amOnPage(['projects']);
        $I->dontSee("Create Project", ['css' => 'a']);
        $I->dontSee("My Projects", ['css' => 'a']);
    }

    /**
     * See only link to My projects links as committee member
     */
    public function testProjectSeeLinksAsCommitteeMember(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_MEMBER);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['projects']);
        $I->dontSee("Create Project", ['css' => 'a']);
        $I->see("My Projects", ['css' => 'a']);
    }

    /**
     * See link Create Project and My projects links as committee chair
     */
    public function testProjectSeeLinksAsCommitteeChair(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['projects']);
        $I->see("Create Project", ['css' => 'a']);
        $I->see("My Projects", ['css' => 'a']);
    }

    /**
     * See all users and right data with buttons for view, update and ban
     */
    public function testProjectList(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['projects']);
        $all = Project::find()->all();
        foreach ($all as $project) {
            if ($project->deleted || $project->state == Project::STATE_PROPOSAL) continue;
            $id = $project->id;
            $supervisor = $project->getSupervisors()->one();
            $I->see($project->name, ['css' => 'a']);
            $I->see($project->short_name, ['css' => 'td']);
            if ($supervisor) {
                $I->see($supervisor->name, ['css' => 'td']);
            }
            $I->see($project->state, ['css' => 'td']);
        }
    }

    /**
     * List of projects displayed for guests
     */
    public function testProjectListForGuest(\FunctionalTester $I)
    {
        $I->amOnPage(['projects']);
        $all = Project::find()->all();
        foreach ($all as $project) {
            if ($project->deleted || $project->state == Project::STATE_PROPOSAL) continue;
            $id = $project->id;
            $supervisor = $project->getSupervisors()->one();
            $I->see($project->name, ['css' => 'a']);
            $I->see($project->short_name, ['css' => 'td']);
            if ($supervisor) {
                $I->see($supervisor->name, ['css' => 'td']);
            }
            $I->see($project->state, ['css' => 'td']);
        }
    }

    /**
     * Test to see view button as commitee chair
     */
    public function testProjectViewButton(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['projects']);
        $id = 11;
        $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td a']);
        $I->click(['css' => 'tr[data-key="' . $id . '"] td a']);
        $I->seeCurrentUrl(['projects/view', 'id' => $id]);

    }

    /**
     * See projects as commitee chair
     */
    public function testProjectViewAsCommitteeChair(\FunctionalTester $I)
    {
        $project = Project::findById(4);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['projects/view', 'id' => $project->id]);

        $I->see('State from', ['css' => 'table tr th']);
        $I->seeDateTime($project->lastState->created_at, ['css' => 'table tr:nth-child(1) td']);

        $I->see('State', ['css' => 'table tr th']);
        $I->see($project->lastState->state, ['css' => 'table tr:nth-child(2) td']);

        $I->see('Start Date', ['css' => 'tr th']);
        $I->seeDate($project->run_start_date, ['css' => 'table tr:nth-child(3) td']);

        $I->see('Deadline', ['css' => 'tr th']);
        $I->seeDate($project->deadline, ['css' => 'table tr:nth-child(4) td']);

        $I->see('Supervisors', ['css' => 'tr th']);
        $I->see($project->supervisors[0]->name);

        $I->see("Description");
        $I->see($project->description);
    }

    /**
     * Login as committee chair and see all project states from the past
     */
    public function testProjectViewStateHistoryAsCommitteeChair(\FunctionalTester $I)
    {
        $project = Project::findById(4);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['projects/view', 'id' => $project->id]);
        $I->see('State from', ['css' => 'table tr th']);
        $I->seeDateTime($project->lastState->created_at, ['css' => 'table tr:nth-child(1) td']);
        $projectStateHistory = $project->projectStateHistories;
        foreach ($projectStateHistory as $projectStateHistory) {
            $I->see($projectStateHistory->state);
            if ($projectStateHistory->comment) {
                $I->see($projectStateHistory->comment);
            }
        }
    }

    /**
     * View list of project defences in project view
     * as committee chair
     */
    public function testProjectViewDefencesAsCommitteeChair(\FunctionalTester $I)
    {
        $project = Project::findById(4);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['projects/view', 'id' => $project->id]);
        foreach ($project->projectDefences as $projectDefence) {
            if ($projectDefence->state) {
                $I->see($projectDefence->state);
            }
        }
    }

    /**
     * See list of project defences as guest
     */
    public function testProjectViewDefencesAsGuest(\FunctionalTester $I)
    {
        $project = Project::findById(4);
        $I->amOnPage(['projects/view', 'id' => $project->id]);
        foreach ($project->projectDefences as $projectDefence) {
            if ($projectDefence->state) {
                $I->see($projectDefence->state);
            }
        }
    }

    /**
     * See all my project as user
     */
    public function testProjectMy(\FunctionalTester $I)
    {
        $loggedUser = User::findById(2);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['projects/my']);
        $all = Project::find()->all();
        $projectUsers = $loggedUser->getUsersOnProjects()->all();
        $projectsIds = [];
        foreach ($projectUsers as $projectUser) {
            $projectIds[$projectUser->project_id] = true;
        }
        foreach ($all as $project) {
            if (!isset($projectIds[$project->id]) || ($project->deleted || $project->state == Project::STATE_PROPOSAL)) {
                $I->dontSee($project->name);
            } else {
                $id = $project->id;
                $I->see($project->short_name, ['css' => 'td']);
                $I->see($project->state, ['css' => 'td']);
            }
        }
    }

    /**
     * Committee member should not be allowed to access project/create page
     */
    public function testProjectNotAllowedForCommitteeMember(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_MEMBER);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['projects/create', 'id' => $user->id]);
    }

    /**
     * Test creating project
     */
    public function testCreateProject(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['projects/create']);
        $name = 'Web projektove komise';
        $shortName = 'PRKNO';
        $supervisors = 1;
        $consultants = "3";
        $members = "2";
        $description = "Some description";
        $formValues = [
            'Project[name]' => $name,
            'Project[short_name]' => $shortName,
            'Project[supervisorIds]' => $supervisors,
            'Project[consultantIds]' => $consultants,
            'Project[memberIds]' => $members,
            'Project[description]' => $description
        ];
    }

    /**
     * Test create project with wrong name
     */
    public function testCreateProjectWrongName(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['projects/create']);
        $key = "name";
        $this->wrongInputTest($I, $key, "");
        $this->wrongInputTest($I, $key, null);
        $this->wrongInputTest($I, $key, $I->randomStr(1000));
    }

    /**
     * Test create project with wrong short name
     */
    public function testCreateProjectWrongShortName(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['projects/create']);
        $key = "short_name";
        $this->wrongInputTest($I, $key, "");
        $this->wrongInputTest($I, $key, null);
        $this->wrongInputTest($I, $key, $I->randomStr(1000));
    }

    /**
     * Test creating project with wrong description
     */
    public function testCreateProjectWrongDescription(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['projects/create']);
        $key = "description";
        $this->wrongInputTest($I, $key, null);
        $this->wrongInputTest($I, $key, $I->randomStr(3000));
    }

    /**
     * Update project name
     */
    public function testUpdateProjectName(\FunctionalTester $I)
    {
        $key = "name";
        $this->updateProjectAttrTest($I, 1, $key, null, false);
        $this->updateProjectAttrTest($I, 1, $key, $I->randomStr(3000), false);
    }

    /**
     * Update project short name
     */
    public function testUpdateProjectShortName(\FunctionalTester $I)
    {
        $key = "short_name";
        $this->updateProjectAttrTest($I, 1, $key, null, false);
        $this->updateProjectAttrTest($I, 1, $key, $I->randomStr(100), false);
    }

    /**
     * Update project desciption
     */
    public function testUpdateProjectDescription(\FunctionalTester $I)
    {
        $key = "description";
        $this->updateProjectAttrTest($I, 1, $key, null, false);
        $this->updateProjectAttrTest($I, 1, $key, $I->randomStr(5000), false);
    }

    private function updateProjectAttrTest(\FunctionalTester $I, int $projectId, $attrName, $value, bool $success)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $project = Project::findById($projectId);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['projects/update', 'id' => $project->id]);
        $I->submitForm('form', ['Project[' . $attrName . ']' => $value]);
        $newProject = Project::findById($projectId);
        $testValue = $success ? $value : $project->$attrName;
        $I->assertEquals($testValue, $newProject->$attrName);
    }

    private function wrongInputTest(\FunctionalTester $I, string $attr, $value)
    {
        $name = 'Web projektove komise';
        $shortName = 'PRKNO';
        $supervisors = 1;
        $consultants = "3";
        $members = "2";
        $description = "Some description";
        $formValues = [
            'Project[name]' => $name,
            'Project[short_name]' => $shortName,
            'Project[supervisorIds]' => $supervisors,
            'Project[consultantIds]' => $consultants,
            'Project[memberIds]' => $members,
            'Project[description]' => $description
        ];
        $formValues['Project[' . $attr . ']'] = $value;
        $oldCount = $this->numberOfProjects();
        $I->amOnPage(['projects/create']);
        $I->submitForm('form', $formValues);
        $newCount = $this->numberOfProjects();
        $I->assertEquals($oldCount, $newCount);
        $I->seeCurrentUrl(['projects/create']);
    }

    private function numberOfProjects() : int
    {
        $all = Project::find()->all();
        return sizeof($all);
    }
}