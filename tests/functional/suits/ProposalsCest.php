<?php
namespace app\tests\functional\suits;

use app\models\records\Project;
use app\models\records\ProjectUser;
use app\models\records\Proposal;
use app\models\records\ProposalComment;
use app\models\records\User;
use app\tests\fixtures\DocumentFixture;
use app\tests\fixtures\ProjectFixture;
use app\tests\fixtures\ProjectUserFixture;
use app\tests\fixtures\ProjectStateHistoryFixture;
use app\tests\fixtures\ProjectDocumentFixture;
use app\tests\fixtures\ProposalFixture;
use app\tests\fixtures\ProposalCommentFixture;
use app\tests\fixtures\UserFixture;
use app\utils\DateTime;
use Yii;

/**
 * Functional tests for proposals
 */
class ProposalsCest
{
    /**
     * Mocking mailer to the custom class
     */
    public function _before()
    {
        Yii::$app->set('mailer', 'app\tests\mock\FunctionalTestMailer');
    }

    /**
     * Fixtures
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'project' => [
                'class' => ProjectFixture::className(),
                'dataFile' => codecept_data_dir() . 'project.php'
            ],
            'projectUser' => [
                'class' => ProjectUserFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectUser.php'
            ],
            'projectStateHistory' => [
                'class' => ProjectStateHistoryFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectStateHistory.php'
            ],
            'documents' => [
                'class' => DocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'document.php'
            ],
            'projectDocuments' => [
                'class' => ProjectDocumentFixture::className(),
                'dataFile' => codecept_data_dir() . 'projectDocument.php'
            ],
            'proposals' => [
                'class' => ProposalFixture::className(),
                'dataFile' => codecept_data_dir() . 'proposal.php'
            ],
            'proposalComments' => [
                'class' => ProposalFixture::className(),
                'dataFile' => codecept_data_dir() . 'proposalComment.php'
            ]
        ];
    }

    /**
     * Test to see heading in proposals
     */
    public function testSeeHeadingsProposals(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['proposals']);
        $I->see('Proposals', 'h1');
    }

    /**
     * Test student to be not allowed to see proposals/index and proposals/view pages
     */
    public function testProposalNotAllowedForStudent(\FunctionalTester $I)
    {
        $proposalId = 1;
        $user = User::findByRole(User::ROLE_STUDENT);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['proposals/index']);
        $I->notAllowedPage(['proposals/view', 'id' => $proposalId]);
    }

    /**
     * Test academic to be not allowed to see proposals/index and proposals/view pages
     */
    public function testProjectNotAllowedForAcademic(\FunctionalTester $I)
    {
        $proposalId = 1;
        $user = User::findByRole(User::ROLE_ACADEMIC);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['proposals/index']);
        $I->notAllowedPage(['proposals/view', 'id' => $proposalId]);
    }

    /**
     * See proposal list as committee chair
     */
    public function testSeeProposalList(\FunctionalTester $I) {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['proposals/index']);
        $all = Proposal::find()->all();
        $projectNames = array();
        foreach ($all as $index => $proposal) {
            $project = $proposal->project;
            $displayed = $proposal->deleted == 0 && $project->state == Project::STATE_PROPOSAL && $proposal->state == Proposal::STATE_SENT;
            if ($displayed) {
                $projectNames[$project->short_name] = true;
            }
        }
        foreach ($all as $index => $proposal) {
            $project = $proposal->project;
            if (isset($projectNames[$project->short_name])) {
                $I->see($project->name);
                $I->see($project->short_name);
                $I->see($project->supervisor[0]->name);
            } else {
                $I->dontSee($project->name);
                $I->dontSee($project->short_name);
            }
        }
    }

    /**
     * Test to send proposal comment
     */
    public function testSendProposalComment(\FunctionalTester $I) {
        $proposal = Proposal::findOne(['state' => Proposal::STATE_SENT, 'deleted' => false]);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['proposals/view', 'id' => $proposal->id]);
        $comment = "comment_abc";
        $formValues = [
            'ProposalComment[content]' => $comment
        ];
        $oldCount = sizeof(ProposalComment::find()->all());
        $I->submitForm('#proposal-comments-form form', $formValues);
        $newCount = sizeof(ProposalComment::find()->all());
        $I->assertEquals($oldCount + 1, $newCount);
        $proposalComment = ProposalComment::findOne(['content' => $comment, 'deleted' => false]);
        $I->assertEquals($proposalComment->proposal->id, $proposal->id);
    }

    private function numberOfProposalComments(): int {
        $all = ProposalComment::find()->all();
        return sizeof($all);
    }
}