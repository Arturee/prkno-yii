<?php
namespace app\tests\functional\suits;

use app\models\records\News;
use app\models\records\User;
use app\tests\fixtures\NewsFixture;
use app\tests\fixtures\UserFixture;
use app\utils\DateTime;
use Yii;

/**
 * Functional tests for news
 */
class NewsCest
{
    /**
     * Mocking mailer to the custom class
     */
    public function _before()
    {
        Yii::$app->set('mailer', 'app\tests\mock\FunctionalTestMailer');
    }

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'news' => [
                'class' => NewsFixture::className(),
                'dataFile' => codecept_data_dir() . 'news.php'
            ]
        ];
    }

    /**
     * Login as committee chair to see button
     * for creating news
     */
    public function testSeeCreateNewsButton(\FunctionalTester $I)
    {
        $user = User::findIdentity(1);
        $I->amLoggedInAs($user);
        $I->amOnPage(['/']);
        $I->wantTo('see create button for committee chair');
        $I->see('News', 'h1');
        $I->see('Create News');
    }

    /**
     * Don't see create button as nonauthorized person
     * for creating news
     */
    public function testNotSeeCreateNewsButtonCommitteeMember(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_MEMBER);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['/']);
        $I->wantTo('Not see create button for student');
        $I->see('News', 'h1');
        $I->dontSee('Create News');
    }

    /**
     * Don't see create button as nonauthorized person
     * for creating news
     */
    public function testNotSeeCreateNewsButtonAcademic(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_ACADEMIC);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['/']);
        $I->wantTo('Not see create button for academic');
        $I->see('News', 'h1');
        $I->dontSee('Create News');
    }

    /**
     * See all users and right data with buttons for view, update and ban
     */
    public function testSeeNewsList(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['/']);
        $all = News::find()->all();
        foreach ($all as $index => $news) {
            $title = $news->getLocalTitle();
            $content = $news->getLocalContent();
            $deleted = $news->deleted;
            $displayed = $news->deleted == 0 && ($news->hide_at == null || (new DateTime($news->hide_at))->isInFuture());
            $updateDate = DateTime::from($news->updated_at)->format(DateTime::DATE);
            if ($displayed) {
                $I->see($title);
                $I->see($content);
                $I->see($updateDate);
            } else {
                $I->dontSee($title);
                $I->dontSee($content);
            }
        }
    }

    /**
     * See news ordered according to last update descending
     */
    public function testSeeOrderedByDate(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['/']);
        $all = News::find()->all();
        $all = array_filter($all, function ($news) {
            return $news->deleted == 0 && ($news->hide_at == null || (new DateTime($news->hide_at))->isInFuture());
        });
        $i = 1;
        $lastDate = new DateTime();
        foreach ($all as $index => $news) {
            $dateText = $I->grabTextFrom('(//span[@class="news-date"])[' . $i . ']');
            $date = new DateTime($dateText);
            $I->assertTrue($date <= $lastDate);
            $lastDate = $date;
            $i++;
        }
    }

    /**
     * Test visibility of public/private news
     */
    public function testSeeNewsLoggedOut(\FunctionalTester $I)
    {
        $I->amOnPage(['/']);
        $all = News::find()->all();
        foreach ($all as $index => $news) {
            $title = $news->getLocalTitle();
            $content = $news->getLocalContent();
            $deleted = $news->deleted;
            $displayed = $news->deleted == 0 && $news->public && ($news->hide_at == null || (new DateTime($news->hide_at))->isInFuture());
            $updateDate = DateTime::from($news->updated_at)->format(DateTime::DATE);
            if ($displayed) {
                $I->see($title);
                $I->see($content);
                $I->see($updateDate);
            } else {
                $I->dontSee($title);
                $I->dontSee($content);
            }
        }
    }

    /**
     * See important news
     */
    public function testSeeImportant(\FunctionalTester $I)
    {
        News::updateAll(['type' => News::TYPE_IMPORTANT]);
        $I->amOnPage(['/']);
        $I->see('(important)');
    }

    /**
     * Dont see important news after setting type to normal
     */
    public function testDontSeeImportant(\FunctionalTester $I)
    {
        News::updateAll(['type' => News::TYPE_NORMAL]);
        $I->amOnPage(['/']);
        $I->dontSee('(important)');
        News::updateAll(['type' => News::TYPE_GENERATED]);
        $I->amOnPage(['/']);
        $I->dontSee('(important)');
    }

    /**
     * See update and delete buttons as committee chair
     */
    public function testSeeButtonsAsCommitteeChair(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['/']);
        $I->seeElement(['css' => 'span[class="glyphicon glyphicon-pencil"]']);
        $I->seeElement(['css' => 'span[class="glyphicon glyphicon-trash"]']);
    }

    /**
     * Dont see update and delete buttons as committee member
     */
    public function testDontSeeButtonsAsCommitteeMember(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_MEMBER);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['/']);
        $I->dontSeeElement(['css' => 'span[class="glyphicon glyphicon-pencil"]']);
        $I->dontSeeElement(['css' => 'span[class="glyphicon glyphicon-trash"]']);
    }

    /**
     * Update button url
     */
    public function testUpdateButtonUrl(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['/']);
        $id = 2;
        $I->click(['css' => 'div[class="news-active-record news-normal"] a:nth-child(1)']);
        $I->seeCurrentUrl(['news/update', 'id' => $id]);
    }

    /**
     * Delete button url
     */
    public function testDeleteButtonUrl(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['/']);
        $id = 2;
        $I->click(['css' => 'div[class="news-active-record news-normal"] a:nth-child(2)']);
        $I->seeCurrentUrl(['news/delete', 'id' => $id]);
    }

    /**
     * Create button url
     */
    public function testCreateButtonUrl(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['/']);
        $I->click(['css' => 'div[class="news-index"] a']);
        $I->seeCurrentUrl(['news/create']);
    }

    /**
     * Student should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testManageNotAllowedForStudent(\FunctionalTester $I)
    {
        $id = 1;
        $user = User::findByRole(User::ROLE_STUDENT);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['news/create']);
        $I->notAllowedPage(['news/update', 'id' => $id]);
        $I->notAllowedPage(['users/delete', 'id' => $id]);
    }

    /**
     * Academic should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testManageNotAllowedForAcademic(\FunctionalTester $I)
    {
        $id = 1;
        $user = User::findByRole(User::ROLE_ACADEMIC);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['news/create']);
        $I->notAllowedPage(['news/update', 'id' => $id]);
        $I->notAllowedPage(['users/delete', 'id' => $id]);
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testManageNotAllowedForCommitteeMember(\FunctionalTester $I)
    {
        $id = 1;
        $user = User::findByRole(User::ROLE_COMMITTEE_MEMBER);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['news/create']);
        $I->notAllowedPage(['news/update', 'id' => $id]);
        $I->notAllowedPage(['users/delete', 'id' => $id]);
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testCreateNews(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $titleEn = 'New title en';
        $titleCs = 'New title cs';
        $contentEn = 'New content en';
        $contentCs = 'New content cs';
        $oldCount = $this->numberOfNews();
        $I->submitForm('form', [
            'News[title_en]' => $titleEn,
            'News[title_cs]' => $titleCs,
            'News[content_en]' => $contentEn,
            'News[content_cs]' => $contentCs
        ]);
        $newCount = $this->numberOfNews();
        $I->assertEquals($oldCount + 1, $newCount);
    }

    /**
     * News should not be created with missing value
     */
    public function testCreateNewsWrongTitle(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $keys = ["title_en", "title_cs"];
        foreach ($keys as $key) {
            $this->wrongInputTest($I, $key, "");
            $this->wrongInputTest($I, $key, null);
            $this->wrongInputTest($I, $key, $I->randomStr(200));
        }
    }

    /**
     * News should not be created with missing value
     */
    public function testCreateNewsWrongContent(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $keys = ["content_en", "content_cs"];
        foreach ($keys as $key) {
            $this->wrongInputTest($I, $key, "");
            $this->wrongInputTest($I, $key, null);
        }
    }

    /**
     * Create news and see the proper values
     */
    public function testCreateNewsSeeCorrectValues(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $titleEn = 'New title en';
        $titleCs = 'New title cs';
        $contentEn = 'New content en';
        $contentCs = 'New content cs';
        $hideAt = '14.05.2019';
        $oldCount = $this->numberOfNews();
        $public = true;
        $type = News::TYPE_NORMAL;
        $id = 42952;
        $I->submitForm('form', [
            'News[id]' => $id,
            'News[type]' => $type,
            'News[public]' => $public,
            'News[hide_at]' => $hideAt,
            'News[title_en]' => $titleEn,
            'News[title_cs]' => $titleCs,
            'News[content_en]' => $contentEn,
            'News[content_cs]' => $contentCs
        ]);
        $newCount = $this->numberOfNews();
        $news = News::findById($id);
        $I->assertEquals($news->title_en, $titleEn);
        $I->assertEquals($news->title_cs, $titleCs);
        $I->assertEquals($news->content_en, $contentEn);
        $I->assertEquals($news->content_cs, $contentCs);
        $I->assertEqualDates($news->hide_at, $hideAt);
        $I->assertEquals($news->public, $public);
        $I->assertEquals($news->type, $type);
    }

    /**
     * Update title
     */
    public function testUpdateNewsTitle(\FunctionalTester $I)
    {
        $keys = ["title_en", "title_cs"];
        foreach ($keys as $key) {
            $this->updateNewsAttrTest($I, 1, $key, null, false);
            $this->updateNewsAttrTest($I, 1, $key, "", false);
            $this->updateNewsAttrTest($I, 1, $key, $I->randomStr(200), false);
            $this->updateNewsAttrTest($I, 1, $key, "title", true);
        }
    }

    /**
     * Update content
     */
    public function testUpdateNewsContent(\FunctionalTester $I)
    {
        $keys = ["content_en", "content_cs"];
        foreach ($keys as $key) {
            $this->updateNewsAttrTest($I, 1, $key, null, false);
            $this->updateNewsAttrTest($I, 1, $key, "", false);
            $this->updateNewsAttrTest($I, 1, $key, "content", true);
        }
    }

    /**
     * Update type attribute
     */
    public function testUpdateNewsType(\FunctionalTester $I)
    {
        $key = "type";
        $this->updateNewsAttrTest($I, 1, $key, "normal", true);
        $this->updateNewsAttrTest($I, 1, $key, "important", true);
    }

    /**
     * Update visibility attribute
     */
    public function testUpdateNewsVisibility(\FunctionalTester $I)
    {
        $key = "public";
        $this->updateNewsAttrTest($I, 1, $key, 1, true);
        $this->updateNewsAttrTest($I, 1, $key, 0, true);
    }

    /**
     * Set null date of hide
     */
    public function testUpdateNewsHideAtDate(\FunctionalTester $I)
    {
        $newsId = 1;
        $news = News::findById($newsId);
        $value = '11.01.2018';
        $I->amLoggedInAs($news);
        $I->amOnPage(['news/update', 'id' => $news->id]);
        $I->submitForm('form', ['News[hide_at]' => $value]);
        $newNews = News::findById($newsId);
        $I->assertEqualDates($value, $newNews->hide_at);
    }

    /**
     * Set null date of hide
     */
    public function testUpdateNewsHideAtNull(\FunctionalTester $I)
    {
        $newsId = 1;
        $news = News::findById($newsId);
        $I->amLoggedInAs($news);
        $I->amOnPage(['news/update', 'id' => $news->id]);
        $I->submitForm('form', ['News[hide_at]' => ""]);
        $newNews = News::findById($newsId);
        $I->assertEquals(null, $newNews->hide_at);
    }

    /**
     * Helper method for testint update individual attributes
     * @param int $userId
     * @param string $attrName
     * @param $value
     * @param bool $success
     */
    private function updateNewsAttrTest(\FunctionalTester $I, int $newsId, $attrName, $value, bool $success)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $news = News::findById($newsId);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['news/update', 'id' => $news->id]);
        $I->submitForm('form', ['News[' . $attrName . ']' => $value]);
        $newNews = News::findById($newsId);
        $testValue = $success ? $value : $news->$attrName;
        $I->assertEquals($testValue, $newNews->$attrName);
    }

    private function wrongInputTest(\FunctionalTester $I, string $attr, $value)
    {
        $titleEn = 'New title en';
        $titleCs = 'New title cs';
        $contentEn = 'Content en';
        $contentCs = 'Content cs';
        $formValues = [
            'News[title_en]' => $titleEn,
            'News[title_cs]' => $titleCs,
            'News[content_en]' => $contentEn,
            'News[content_cs]' => $contentCs
        ];
        $formValues['News[' . $attr . ']'] = $value;
        $oldCount = $this->numberOfNews();
        $I->amOnPage(['news/create']);
        $I->submitForm('form', $formValues);
        $newCount = $this->numberOfNews();
        $I->assertEquals($oldCount, $newCount);
        $I->seeCurrentUrl(['news/create']);
    }

    private function numberOfNews() : int
    {
        $all = News::find()->all();
        return sizeof($all);
    }
}