<?php
namespace app\tests\functional\suits;

use app\models\records\User;
use app\tests\fixtures\UserFixture;

/**
 * Functional tests for login
 */
class LoginFormCest
{
    /**
     * @inheritdoc
     */
    public function _before(\FunctionalTester $I)
    {
        $I->amOnRoute('site/login');
    }

    /**
     * @inheritdoc
     */
    public function _fixtures() {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ];
    }

    /**
     * Test seeing login form
     */
    public function openLoginPage(\FunctionalTester $I)
    {
        $I->see('Login', 'h1');
    }

    /**
     * Test method amLoggedInAs by user id
     */
    public function testCommitteeChairLoggedIn(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['/']);
        $I->see($user->name, 'a');
    }

    /**
     * Test submitting empty form and see validation errors
     */
    public function loginWithEmptyCredentials(\FunctionalTester $I)
    {
        $I->submitForm('#login-form', []);
        $I->expectTo('see validations errors');
        $I->see('Email cannot be blank.');
        $I->see('Password cannot be blank');
    }

    /**
     * Test submitting form with wrong email
     * and see validation errors
     */
    public function loginWithWrongEmail(\FunctionalTester $I)
    {
        $I->submitForm('#login-form', [
            'LoginForm[email]' => 'wrondemail',
            'LoginForm[password]' => 'wrongPassword'
        ]);
        $I->expectTo('see validations errors');
        $I->see('Incorrect email or password');
    }

    /**
     * Test submitting form with wrong password
     */
    public function testLoginWithWrongPassword(\FunctionalTester $I)
    {
        $I->submitForm('#login-form', [
            'LoginForm[email]' => 'comitteechair@gmail.com',
            'LoginForm[password]' => 'wrongPassword'
        ]);
        $I->expectTo('see validations errors');
        $I->see('Incorrect email or password');
    }

    /**
     * Test submitting form with wrong password
     */
    public function testLoginAsBannedUserWithWrongPassword(\FunctionalTester $I)
    {
        $user = User::findById(8);
        $I->submitForm('#login-form', [
            'LoginForm[email]' => $user->email,
            'LoginForm[password]' => 'wrongPassword'
        ]);
        $I->expectTo('see validations errors');
        $I->see('Incorrect email or password');
    }

    /**
     * Test submitting form with wrong password
     */
    public function testLoginAsBannedUserWithCorrectPassword(\FunctionalTester $I)
    {
        $user = User::findById(8);
        $I->submitForm('#login-form', [
            'LoginForm[email]' => $user->email,
            'LoginForm[password]' => 'Test1234'
        ]);
        $I->expectTo('see validations errors');
        $I->see('You are banned. Contact the committee for an explanation.');
    }

    /**
     * Test submitting form with wrong password
     */
    public function testLoginAsDeletedUser(\FunctionalTester $I)
    {
        $user = User::findById(7);
        $I->submitForm('#login-form', [
            'LoginForm[email]' => $user->email,
            'LoginForm[password]' => 'Test1234'
        ]);
        $I->expectTo('see validations errors');
        $I->see('Incorrect email or password');
    }

    /**
     * Test submitting form with wrong password
     */
    public function testLoginAsCasUser(\FunctionalTester $I)
    {
        $user = User::findById(3);
        $I->submitForm('#login-form', [
            'LoginForm[email]' => $user->email,
            'LoginForm[password]' => 'Test1234'
        ]);
        $I->see('Logout', 'a');
    }

    /**
     * Test login as a user without role
     */
    public function testLoginWithoutRole(\FunctionalTester $I)
    {
        $user = User::findById(10);
        $I->submitForm('#login-form', [
            'LoginForm[email]' => $user->email,
            'LoginForm[password]' => 'Test1234'
        ]);
        $I->expectTo('see validations errors');
        $I->see('You do not have sufficient privileges to log in.');
    }

    /**
     * Test login as a user with expired role
     */
    public function testLoginWithExpiredRole(\FunctionalTester $I)
    {
        $user = User::findById(9);
        $I->submitForm('#login-form', [
            'LoginForm[email]' => $user->email,
            'LoginForm[password]' => 'Test1234'
        ]);
        $I->expectTo('see validations errors');
        $I->see('Your account has expired. Ask the committee to refresh it.');
    }

    /**
     * Test of successfull login
     */
    public function testLoginSuccessfully(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->submitForm('#login-form', [
            'LoginForm[email]' => $user->email,
            'LoginForm[password]' => 'Test1234',
        ]);
        $I->see('Logout', 'a');
    }
}