<?php
namespace app\tests\functional\suits;

use app\models\records\Advertisement;
use app\models\records\AdvertisementComment;
use app\models\records\User;
use app\tests\fixtures\AdvertisementCommentFixture;
use app\tests\fixtures\AdvertisementFixture;
use app\tests\fixtures\UserFixture;
use Yii;
use yii\helpers\Url;

/**
 * Functional tests for advertisements
 */
class AdvertisementsCest
{
    /**
     * Mocking mailer to the custom class
     */
    public function _before()
    {
        Yii::$app->set('mailer', 'app\tests\mock\FunctionalTestMailer');
    }

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'advertisement' => [
                'class' => AdvertisementFixture::className(),
                'dataFile' => codecept_data_dir() . 'advertisement.php'
            ],
            'advertisementComment' => [
                'class' => AdvertisementCommentFixture::className(),
                'dataFile' => codecept_data_dir() . 'advertisementComment.php'
            ]
        ];
    }

    /**
     * Test see heading for advertisements
     */
    public function testAdvertisementsHeading(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements']);
        $I->wantTo('see list of users');
        $I->see('Advertisements', 'h1');
    }

    /**
     * Test see list of advertisements
     */
    public function testAdListAsCommitteeChair(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements']);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            $id = $ads->id;
            if (!$ads->published || $ads->deleted) {
                $I->dontSee($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
            } else {
                $I->see($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
                if ($ads->user->deleted) {
                    $I->dontSee($ads->user->name, ['css' => 'tr[data-key="' . $id . '"] td']);
                } else {
                    $I->see($ads->user->name, ['css' => 'tr[data-key="' . $id . '"] td']);
                }
                $I->seeDate($ads->updated_at, ['css' => 'tr[data-key="' . $id . '"] span[class="ads-date"]']);
                $I->see($ads->type, ['css' => 'tr[data-key="' . $id . '"] td']);
                $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Update"]']);
                $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Delete"]']);
                foreach ($ads->keywordsList as $keyword) {
                    if ($keyword) {
                        $I->see("#" . $keyword, 'a[href="' . Url::to(['advertisements/index', 'AdvertisementSearch[keywords]' => $keyword]) . '"]');
                    }
                }
            }
        }
    }

    /**
     * Test see list of advertisements as student
     * for creating news. See update and restore/delete
     * buttons if the ad was written by logged user
     */
    public function testAdListAsStudent(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_STUDENT);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements']);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            $id = $ads->id;
            if ($ads->deleted || !$ads->published) {
                $I->dontSee($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
            } else {
                $I->see($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
                if ($ads->user->deleted) {
                    $I->dontSee($ads->user->name, ['css' => 'tr[data-key="' . $id . '"] td']);
                } else {
                    $I->see($ads->user->name, ['css' => 'tr[data-key="' . $id . '"] td']);
                }
                $I->seeDate($ads->updated_at, ['css' => 'tr[data-key="' . $id . '"] span[class="ads-date"]']);
                $I->see($ads->type, ['css' => 'tr[data-key="' . $id . '"] td']);
                $I->see("Send", "button");
                if ($ads->user_id == $loggedUser->id) {
                    $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Update"]']);
                    $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Delete"]']);
                } else {
                    $I->dontSeeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Update"]']);
                    $I->dontSeeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Delete"]']);
                }
                foreach ($ads->keywordsList as $keyword) {
                    if ($keyword) {
                        $I->see("#" . $keyword, 'a[href="' . Url::to(['advertisements/index', 'AdvertisementSearch[keywords]' => $keyword]) . '"]');
                    }
                }
            }
        }
    }

    /**
     * See ads as guest - do not display any names
     */
    public function testAdListOffline(\FunctionalTester $I)
    {
        $I->amOnPage(['advertisements']);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            if ($ads->deleted || !$ads->published) {
                $I->dontSee($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
            } else {
                $id = $ads->id;
                $I->see($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
                $I->dontSee($ads->user->name, ['css' => 'tr[data-key="' . $id . '"] td']);
                $I->seeDate($ads->updated_at, ['css' => 'tr[data-key="' . $id . '"] span[class="ads-date"]']);
                $I->see($ads->type, ['css' => 'tr[data-key="' . $id . '"] td']);
                $I->dontSeeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Update"]']);
                $I->dontSeeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Delete"]']);
                $I->dontSee("Send", "button");
                foreach ($ads->keywordsList as $keyword) {
                    if ($keyword) {
                        $I->see("#" . $keyword, 'a[href="' . Url::to(['advertisements/index', 'AdvertisementSearch[keywords]' => $keyword]) . '"]');
                    }
                }
            }
        }
    }

    /**
     * See comments with content, user name and Delete/Restore icons
     */
    public function testSeeAdComments(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements']);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            $id = $ads->id;
            if ($ads->deleted || !$ads->published) {
                $I->dontSee($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
            } else {
                $comments = $ads->adComments;
                foreach ($comments as $comment) {
                    if ($comment->deleted) {
                        $I->dontSee($comment->content, ['css' => 'tr[data-key="' . $id . '"] tr[data-key="' . $comment->id . '"] p']);
                    } else {
                        $I->see($comment->content, ['css' => 'tr[data-key="' . $id . '"] tr[data-key="' . $comment->id . '"] p']);
                        $I->seeElement(['css' => 'tr[data-key="' . $id . '"] tr[data-key="' . $comment->id . '"] a[title="Delete"]']);
                        $I->see($comment->user->name, ['css' => 'tr[data-key="' . $id . '"] tr[data-key="' . $comment->id . '"] a']);
                    }
                }
            }
        }
    }

    /**
     * Dont see button for ad comments managing as student
     * If the comment is written by logged student, buttons are visible
     */
    public function testDontSeeCommentsButtonAsStudent(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_STUDENT);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements']);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            $id = $ads->id;
            if ($ads->deleted || !$ads->published) {
                $I->dontSee($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
            } else {
                $comments = $ads->adComments;
                foreach ($comments as $comment) {
                    if (!$comment->deleted && $comment->user_id == $loggedUser->id) {
                        $I->seeElement(['css' => 'tr[data-key="' . $id . '"] tr[data-key="' . $comment->id . '"] a[title="Delete"]']);
                    } else {
                        $I->dontSeeElement(['css' => 'tr[data-key="' . $id . '"] tr[data-key="' . $comment->id . '"] a[title="Delete"]']);
                    }
                }
            }
        }
    }

    /**
     * Test searching by past date - show every ad
     */
    public function testSearchByPastDate(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements']);
        $all = Advertisement::find()->all();
        $I->submitForm('form[method="get"]', [
            'AdvertisementSearch[updated_at]' => "2017-01-01"
        ]);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            $id = $ads->id;
            if ($ads->deleted || !$ads->published) {
                $I->dontSee($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
            } else {
                $I->see($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
            }
        }
    }

    /**
     * Test searching by future date - do not show any ad
     */
    public function testSearchByFutureDate(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements']);
        $all = Advertisement::find()->all();
        $I->submitForm('form[method="get"]', [
            'AdvertisementSearch[updated_at]' => "2030-01-01"
        ]);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            $I->dontSee($ads->title, ['css' => 'tr[data-key="' . $ads->id . '"] td']);
        }
    }

    /**
     * Test searching by type
     */
    public function testSearchByType(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements']);
        $all = Advertisement::find()->all();
        $type = "project";
        $I->submitForm('form[method="get"]', [
            'AdvertisementSearch[type]' => $type
        ]);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            if ($ads->published && $ads->type == $type) {
                $I->see($ads->title, ['css' => 'tr[data-key="' . $ads->id . '"] td']);
            } else {
                $I->dontSee($ads->title, ['css' => 'tr[data-key="' . $ads->id . '"] td']);
            }
        }
    }

    /**
     * Test adding comment
     */
    public function testSendComment(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements']);
        $comment = "New comment message";
        $formId = $I->grabAttributeFrom(['css' => 'form[method="post"]'], "id");
        $adId = $I->grabAttributeFrom(['css' => 'form[id="' . $formId . '"] input[name="AdvertisementComment[ad_id]"]'], "value");
        $oldCount = sizeof(AdvertisementComment::find()->all());
        $I->submitForm('form[id="' . $formId . '"]', [
            'AdvertisementComment[content]' => $comment
        ]);
        $newCount = sizeof(AdvertisementComment::find()->all());
        $I->assertEquals($oldCount + 1, $newCount);
        $addedComment = AdvertisementComment::find()->where(['content' => $comment])->one();
        $I->assertEquals($addedComment->user_id, $loggedUser->id);
        $I->assertEquals($addedComment->ad_id, $adId);
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testViewAdAsCommitteeChair(\FunctionalTester $I)
    {
        $adId = 1;
        $ad = Advertisement::findById($adId);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements/view', 'id' => $adId]);
        $I->see($ad->title);
        $I->see($ad->type);
        $I->see('Created At');
        $I->seeDate($ad->created_at, 'span[class="create-date"]');
        $I->see('Updated At');
        $I->seeDate($ad->updated_at, 'span[class="update-date"]');
        if ($ad->user->deleted) {
            $I->dontSee($ad->user->name, 'a');
        } else {
            $I->see($ad->user->name, 'a');
        }
        $I->see('Delete', "div[class='manage-buttons'] a");
        $I->see('Update', "div[class='manage-buttons'] a");
        foreach ($ad->keywordsList as $keyword) {
            if ($keyword) {
                $I->see("#" . $keyword, 'a[href="' . Url::to(['advertisements/index', 'AdvertisementSearch[keywords]' => $keyword]) . '"]');
            }
        }
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testViewAsOwner(\FunctionalTester $I)
    {
        $adId = 3;
        $ad = Advertisement::findById($adId);
        $loggedUser = $ad->user_id;
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements/view', 'id' => $adId]);
        $I->see('Delete', "div[class='manage-buttons'] a");
        $I->see('Update', "div[class='manage-buttons'] a");
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testViewAsNonOwner(\FunctionalTester $I)
    {
        $adId = 3;
        $ad = Advertisement::findById($adId);
        $loggedUser = 2;
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements/view', 'id' => $adId]);
        $I->dontSee('Delete', "div[class='manage-buttons'] a");
        $I->dontSee('Update', "div[class='manage-buttons'] a");
    }

    /**
     * See comments with content, user name and Delete/Restore icons
     */
    public function testViewSeeAdComments(\FunctionalTester $I)
    {
        $adId = 5;
        $ad = Advertisement::findById($adId);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements/view', 'id' => $adId]);
        $comments = $ad->adComments;
        foreach ($comments as $comment) {
            $I->see($comment->content, ['css' => 'tr[data-key="' . $comment->id . '"] p']);
            $I->seeElement(['css' => 'tr[data-key="' . $comment->id . '"] a[title="Delete"]']);
            if ($comment->user->deleted) {
                $I->dontSee($comment->user->name, ['css' => 'tr[data-key="' . $comment->id . '"] a']);
            } else {
                $I->see($comment->user->name, ['css' => 'tr[data-key="' . $comment->id . '"] a']);
            }
        }
    }

    /**
     * Test adding comment from the ad view
     */
    public function testViewSendComment(\FunctionalTester $I)
    {
        $adId = 5;
        $ad = Advertisement::findById($adId);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements/view', 'id' => $adId]);
        $comment = "New comment message";
        $oldCount = sizeof(AdvertisementComment::find()->all());
        $I->submitForm('form', [
            'AdvertisementComment[content]' => $comment
        ]);
        $newCount = sizeof(AdvertisementComment::find()->all());
        $I->assertEquals($oldCount + 1, $newCount);
        $addedComment = AdvertisementComment::find()->where(['content' => $comment])->one();
        $I->assertEquals($addedComment->user_id, $loggedUser->id);
        $I->assertEquals($addedComment->ad_id, $adId);
    }

    /**
     * Test update link
     */
    public function testViewUpdateLink(\FunctionalTester $I)
    {
        $adId = 5;
        $ad = Advertisement::findById($adId);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements/view', 'id' => $adId]);
        $I->click('Update', "div[class='manage-buttons'] a");
        $I->seeCurrentUrl(['advertisements/update', 'id' => $adId]);
    }

    /**
     * See ads as guest - do not display any names
     */
    public function testMyAdsSee(\FunctionalTester $I)
    {
        $loggedUserId = 3; // student
        $I->amLoggedInAs($loggedUserId);
        $I->amOnPage(['advertisements/my']);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            if ($ads->user_id != $loggedUserId || $ads->deleted) {
                $I->dontSee($ads->title);
            } else {
                $id = $ads->id;
                $I->see($ads->title, ['css' => 'tr[data-key="' . $id . '"] td']);
                if ($ads->user->deleted) {
                    $I->dontSee($ads->user->name, ['css' => 'tr[data-key="' . $id . '"] td']);
                } else {
                    $I->see($ads->user->name, ['css' => 'tr[data-key="' . $id . '"] td']);
                }
                $I->seeDate($ads->updated_at, ['css' => 'tr[data-key="' . $id . '"] span[class="ads-date"]']);
                $I->see($ads->type, ['css' => 'tr[data-key="' . $id . '"] td']);
                $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Update"]']);
                $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td[data-col-seq="3"] a[title="Delete"]']);
                $I->see("Send", "button");
                foreach ($ads->keywordsList as $keyword) {
                    if ($keyword) {
                        $I->see("#" . $keyword, 'a[href="' . Url::to(['advertisements/index', 'AdvertisementSearch[keywords]' => $keyword]) . '"]');
                    }
                }
            }
        }
    }

    /**
     * See comments with content, user name and Delete/Restore icons
     */
    public function testMyAdsSeeComments(\FunctionalTester $I)
    {
        $loggedUserId = 3;
        $I->amLoggedInAs($loggedUserId);
        $I->amOnPage(['advertisements/my']);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            if ($ads->user_id != $loggedUserId || $ads->deleted) {
                $I->dontSee($ads->title);
            } else {
                $comments = $ads->adComments;
                foreach ($comments as $comment) {
                    $I->see($comment->content, ['css' => 'tr[data-key="' . $comment->id . '"] p']);
                }
            }
        }
    }

    /**
     * See comments with content, user name and Delete/Restore icons
     */
    public function testMyAdsSeeCommentsAsCommitteeChair(\FunctionalTester $I)
    {
        $loggedUser = User::findById(7);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements/my']);
        $all = Advertisement::find()->all();
        foreach ($all as $ads) {
            if ($ads->user_id != $loggedUser->id) {
                $I->dontSee($ads->title);
            } else {
                $comments = $ads->adComments;
                foreach ($comments as $comment) {
                    $I->see($comment->content, ['css' => 'tr[data-key="' . $comment->id . '"] p']);
                    $I->seeElement(['css' => 'tr[data-key="' . $comment->id . '"] a[title="Delete"]']);
                    $I->see($comment->user->name, ['css' => 'tr[data-key="' . $comment->id . '"] a']);
                }
            }
        }
    }

    /**
     * Test whether after submitting form without changes
     * data keeps the same
     */
    public function testUpdateNoChange(\FunctionalTester $I)
    {
        $adId = 4;
        $user = User::findById(3);
        $ad1 = Advertisement::findById($adId);
        $I->amLoggedInAs($user);
        $I->amOnPage(['advertisements/update', 'id' => $adId]);
        $I->submitForm('form', []);
        $ad2 = Advertisement::findById($adId);
        $I->assertEquals($ad1->user_id, $ad2->user_id);
        $I->assertEquals($ad1->title, $ad2->title);
        $I->assertEquals($ad1->description, $ad2->description);
        $I->assertEquals($ad1->type, $ad2->type);
        $I->assertEquals($ad1->keywords, $ad2->keywords);
        $I->assertEquals($ad1->subscribed_via_email, $ad2->subscribed_via_email);
        $I->assertEquals($ad1->published, $ad2->published);
    }

    /**
     * Test updating title
     */
    public function testUpdateTitle(\FunctionalTester $I)
    {
        $this->updateAddAttrTest($I, 4, "title", "Test title", true);
        $this->updateAddAttrTest($I, 4, "title", "", false);
        $this->updateAddAttrTest($I, 4, "title", $I->randomStr(400), false);
    }

    /**
     * Test updating content
     */
    public function testUpdateContent(\FunctionalTester $I)
    {
        $this->updateAddAttrTest($I, 4, "description", "Custom content", true);
        $this->updateAddAttrTest($I, 4, "description", "", false);
        $this->updateAddAttrTest($I, 4, "description", $I->randomStr(400), true);
    }

    /**
     * Test updating type
     */
    public function testUpdateType(\FunctionalTester $I)
    {
        $this->updateAddAttrTest($I, 4, "type", "project", true);
        $this->updateAddAttrTest($I, 4, "type", "person", true);
    }

    /**
     * Test updating keywords
     */
    public function testUpdateKeywords(\FunctionalTester $I)
    {
        $this->updateAddAttrTest($I, 4, "keywords", "keywords1,keywords2", true);
    }

    /**
     * Test updating subscribed_via_email value
     */
    public function testUpdateSubscribtion(\FunctionalTester $I)
    {
        $this->updateAddAttrTest($I, 4, "subscribed_via_email", 0, true);
        $this->updateAddAttrTest($I, 4, "subscribed_via_email", 1, true);
    }

    /**
     * Test updating published value
     */
    public function testUpdatePublished(\FunctionalTester $I)
    {
        $this->updateAddAttrTest($I, 4, "published", 0, true);
        $this->updateAddAttrTest($I, 4, "published", 1, true);
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testCreateAd(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['advertisements/create']);
        $title = 'Ads title';
        $description = 'New content';
        $type = 'person';
        $keywords = "keyword1,keyword2";
        $subscribed = 1;
        $published = 1;
        $formValues = [
            'Advertisement[title]' => $title,
            'Advertisement[description]' => $description,
            'Advertisement[type]' => $type,
            'Advertisement[keywords]' => $keywords,
            'Advertisement[subscribed_via_email]' => $subscribed,
            'Advertisement[published]' => $published
        ];
        $oldCount = sizeof(Advertisement::find()->all());
        $I->submitForm('form', $formValues);
        $newCount = sizeof(Advertisement::find()->all());
        $I->assertEquals($oldCount + 1, $newCount);
    }

    /**
     * Test creating ad with empty title
     */
    public function testCreateAdWithWrongTitle(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['advertisements/create']);
        $this->wrongInputTest($I, "title", "");
    }

    /**
     * Test Creating ad with empty content
     */
    public function testCreateAdWithWrongContent(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['advertisements/create']);
        $this->wrongInputTest($I, "description", "");
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testPageNotAllowedForStudent(\FunctionalTester $I)
    {
        $adId = 7;
        $user = User::findByRole(User::ROLE_STUDENT);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['advertisements/create']);
        $I->notAllowedPage(['advertisements/update', 'id' => $adId]);
        $I->notAllowedPage(['advertisements/delete', 'id' => $adId]);
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testPageAllowedForStudentCreator(\FunctionalTester $I)
    {
        $adId = 4;
        $user = User::findById(3);
        $I->amLoggedInAs($user);
        $I->amOnPage(['advertisements/update', 'id' => $adId]);
        $I->amOnPage(['advertisements/delete', 'id' => $adId]);
    }

    /**
     * Helper method for testint update individual attributes
     * @param int $userId
     * @param string $attrName
     * @param $value
     * @param bool $success
     */
    private function updateAddAttrTest(\FunctionalTester $I, $adId, $attrName, $value, bool $success)
    {
        $ad1 = Advertisement::findById($adId);
        $loggedUser = User::findById($ad1->user_id);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['advertisements/update', 'id' => $adId]);
        $I->submitForm('form', ['Advertisement[' . $attrName . ']' => $value]);
        $ad2 = Advertisement::findById($adId);
        $testValue = $success ? $value : $ad1->$attrName;
        $I->assertEquals($testValue, $ad2->$attrName);
    }

    private function wrongInputTest(\FunctionalTester $I, string $attr, $value)
    {
        $title = 'Ads title';
        $content = 'New content';
        $type = 'person';
        $keywords = "keyword1,keyword2";
        $subscribed = 1;
        $published = 1;
        $formValues = [
            'Advertisement[title]' => $title,
            'Advertisement[content]' => $content,
            'Advertisement[type]' => $type,
            'Advertisement[keywords]' => $keywords,
            'Advertisement[subscribed_via_email]' => $subscribed,
            'Advertisement[published]' => $published
        ];
        $formValues['Advertisement[' . $attr . ']'] = $value;
        $oldCount = sizeof(Advertisement::find()->all());
        $I->amOnPage(['advertisements/create']);
        $I->submitForm('form', $formValues);
        $newCount = sizeof(Advertisement::find()->all());
        $I->assertEquals($oldCount, $newCount);
        $I->seeCurrentUrl(['advertisements/create']);
    }
}