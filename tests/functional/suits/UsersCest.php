<?php
namespace app\tests\functional\suits;

use app\models\records\User;
use app\tests\fixtures\UserFixture;
use Yii;

/**
 * Functional tests for users
 */
class UsersCest
{
    /**
     * Mocking mailer to the custom class
     */
    public function _before()
    {
        Yii::$app->set('mailer', 'app\tests\mock\FunctionalTestMailer');
    }

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ];
    }

    /**
     * Login as committee chair to see button
     * for creating news
     */
    public function testViewHeading(\FunctionalTester $I)
    {
        $user = User::findById(1);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users']);
        $I->wantTo('see list of users');
        $I->see('Users', 'h1');
    }

    /**
     * Login as committee chair to see button
     * for creating news
     */
    public function testViewAsCommitteeChair(\FunctionalTester $I)
    {
        $user = User::findById(1);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['users/view', 'id' => $user->id]);
        $I->wantTo('see attribute with correct values');

        $I->see('Created At', ['css' => 'table tr:nth-child(1) th']);
        $I->seeDateTime($user->created_at, 'table tr:nth-child(1) td');

        $I->see('Updated At', ['css' => 'table tr:nth-child(2) th']);
        $I->seeDateTime($user->updated_at, 'table tr:nth-child(2) td');

        $I->see('Name', ['css' => 'table tr:nth-child(3) th']);
        $I->see($user->name, ['css' => 'table tr:nth-child(3) td']);

        $I->see('Degree Prefix', ['css' => 'table tr:nth-child(4) th']);
        $I->seeOrNull($user->degree_prefix, ['css' => 'table tr:nth-child(4) td']);

        $I->see('Degree Suffix', ['css' => 'table tr:nth-child(5) th']);
        $I->seeOrNull($user->degree_suffix, ['css' => 'table tr:nth-child(5) td']);

        $I->see('Email', ['css' => 'table tr:nth-child(6) th']);
        $I->see($user->email, ['css' => 'table tr:nth-child(6) td']);

        $I->see('News Subscription', ['css' => 'table tr:nth-child(7) th']);
        $I->seeYesOrNo($user->news_subscription, ['css' => 'table tr:nth-child(7) td']);

        $I->see('Last Login', ['css' => 'table tr:nth-child(8) th']);
        $I->seeDateTimeOrNull($user->last_login, 'table tr:nth-child(8) td');

        $I->see('Custom Url', ['css' => 'table tr:nth-child(9) th']);
        $I->see($user->custom_url, ['css' => 'table tr:nth-child(9) td']);

        $I->see('Last Cas Refresh', ['css' => 'table tr:nth-child(10) th']);
        $I->seeDateTimeOrNull($user->last_CAS_refresh, 'table tr:nth-child(10) td');

        $I->see('Role', ['css' => 'table tr:nth-child(11) th']);
        $I->see(User::roleToString($user->role), ['css' => 'table tr:nth-child(11) td']);

        $I->see('Role expiration', ['css' => 'table tr:nth-child(12) th']);
        $I->seeDateTimeOrNull($user->role_expiration, 'table tr:nth-child(12) td');

        $I->see('Role Is From Cas', ['css' => 'table tr:nth-child(13) th']);
        $I->seeYesOrNo($user->role_is_from_CAS, ['css' => 'table tr:nth-child(13) td']);

        $I->see('Login Type', ['css' => 'table tr:nth-child(14) th']);
        $I->see($user->login_type, ['css' => 'table tr:nth-child(14) td']);

        $I->see('Banned', ['css' => 'table tr:nth-child(15) th']);
        $I->seeYesOrNo($user->banned, ['css' => 'table tr:nth-child(15) td']);
    }

    /**
     * Login as committee chair to see button
     * for creating news
     */
    public function testSeeAllAttributesOtherUserViewAsCommitteeChair(\FunctionalTester $I)
    {
        $user = User::findById(2);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['users/view', 'id' => $user->id]);
        $I->wantTo('see all attributes of other user as committee chair');
        $I->see('Created At', ['css' => 'table tr  th']);
        $I->see('Updated At', ['css' => 'table tr  th']);
        $I->see('Name', ['css' => 'table tr  th']);
        $I->see('Degree Prefix', ['css' => 'table tr th']);
        $I->see('Degree Suffix', ['css' => 'table tr th']);
        $I->see('Email', ['css' => 'table tr th']);
        $I->see('News Subscription', ['css' => 'table tr th']);
        $I->see('Last Login', ['css' => 'table tr th']);
        $I->see('Custom Url', ['css' => 'table tr th']);
        $I->see('Last Cas Refresh', ['css' => 'table tr th']);
        $I->see('Role', ['css' => 'table tr th']);
        $I->see('Role expiration', ['css' => 'table tr th']);
        $I->see('Role Is From Cas', ['css' => 'table tr th']);
        $I->see('Login Type', ['css' => 'table tr th']);
        $I->see('Banned', ['css' => 'table th']);
    }

    /**
     * Student should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testViewNotAllowedForStudent(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_STUDENT);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['users/view', 'id' => $user->id]);
        $I->notAllowedPage(['users/update', 'id' => $user->id]);
        $I->notAllowedPage(['users/delete', 'id' => $user->id]);
    }

    /**
     * Academic should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testViewNotAllowedForAcademic(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_ACADEMIC);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['users/view', 'id' => $user->id]);
        $I->notAllowedPage(['users/update', 'id' => $user->id]);
        $I->notAllowedPage(['users/delete', 'id' => $user->id]);
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testViewNotAllowedForCommitteeMember(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_MEMBER);
        $I->amLoggedInAs($user);
        $I->notAllowedPage(['users/view', 'id' => $user->id]);
        $I->notAllowedPage(['users/update', 'id' => $user->id]);
        $I->notAllowedPage(['users/delete', 'id' => $user->id]);
    }

    /**
     * Test whether the update button link to the right address
     */
    public function testViewButtonUpdate(\FunctionalTester $I)
    {
        $id = 1;
        $user = User::findById($id);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/view', 'id' => $id]);
        $I->seeLink('Update');
        $I->click('Update');
        $I->seeCurrentUrl(['users/update', 'id' => $id]);
    }

    /**
     * Test whether the committee chair see delete user button
     */
    public function testViewSeeButtonDeleteAsCommitteeChair(\FunctionalTester $I)
    {
        $id = 1;
        $user = User::findById($id);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/view', 'id' => $id]);
        $I->seeLink('Delete');
    }

    /**
     * Login as committee chair to see button
     * for creating news
     */
    public function testProfileHeading(\FunctionalTester $I)
    {
        $user = User::findById(1);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/profile', 'id' => $user->id]);
        $I->wantTo('see name as heading');
        $I->see($user->name, 'h1');
    }

    /**
     * Login as committee chair to see button
     * for creating news
     */
    public function testProfileSeeOwn(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/profile', 'id' => $user->id]);
        $I->wantTo('see name as heading');
        $I->see($user->name, 'h1');

        $I->see('Name', ['css' => 'div[class="user-profile"] label']);
        $I->see($user->getFullNameWithDegrees(), ['css' => 'div[class="user-profile"] p']);

        $I->see('Role', ['css' => 'div[class="user-profile"] label']);
        $I->see(User::roleToString($user->role), ['css' => 'div[class="user-profile"] p']);

        $I->see('Email', ['css' => 'div[class="user-profile"] label']);
        $I->see($user->email, ['css' => 'div[class="user-profile"] p']);

        $I->see('Custom Url', ['css' => 'div[class="user-profile"] label']);
        $I->see($user->custom_url, ['css' => 'div[class="user-profile"] p']);

        $I->see('News Subscription', ['css' => 'div[class="user-profile"] label']);
        $I->seeYesOrNo($user->news_subscription, ['css' => 'div[class="user-profile"] p']);

        $I->see('Last login', ['css' => 'div[class="user-profile"] label']);
        $I->seeDateTime($user->last_login, ['css' => 'div[class="user-profile"] span[class="last-login"]']);

        $I->see('Role Expiration', ['css' => 'div[class="user-profile"] label']);
        $I->seeDateTime($user->role_expiration, ['css' => 'div[class="user-profile"] span[class="role-expiration"]']);

        $I->see('Role Is From Cas', ['css' => 'div[class="user-profile"] label']);
        $I->seeYesOrNo($user->role_is_from_CAS, ['css' => 'div[class="user-profile"] p']);

        $I->see('Login Type', ['css' => 'div[class="user-profile"] label']);
        $I->see($user->login_type, ['css' => 'div[class="user-profile"] p']);

        $I->see('Banned', ['css' => 'div[class="user-profile"] label']);
        $I->seeYesOrNo($user->banned, ['css' => 'div[class="user-profile"] p']);
    }

    /**
     * Login as committee chair to see button
     * for creating news
     */
    public function testProfileSeeOthers(\FunctionalTester $I)
    {
        $user = User::findById(2);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['users/profile', 'id' => $user->id]);
        $I->wantTo('see other profile as committee chair');
        $I->see($user->name, 'h1');
        $I->see('Name', ['css' => 'div[class="user-profile"] label']);
        $I->see('Role', ['css' => 'div[class="user-profile"] label']);
        $I->see('Email', ['css' => 'div[class="user-profile"] label']);
        $I->see('Custom Url', ['css' => 'div[class="user-profile"] label']);
        $I->dontSee('News Subscription', ['css' => 'div[class="user-profile"] label']);
        $I->dontSee('Last login', ['css' => 'div[class="user-profile"] label']);
        $I->dontSee('Role Expiration', ['css' => 'div[class="user-profile"] label']);
        $I->dontSee('Role Is From Cas', ['css' => 'div[class="user-profile"] label']);
        $I->dontSee('Login Type', ['css' => 'div[class="user-profile"] label']);
        $I->dontSee('Banned', ['css' => 'div[class="user-profile"] label']);
    }

    /**
     * Test access to updating profile of other user
     */
    public function testUpdateProfileOtherAccess(\FunctionalTester $I)
    {
        $user = User::findById(1);
        $loggedUser = User::findById(2);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['users/profile', 'id' => $loggedUser->id]);
        $I->notAllowedPage(['users/update-profile', 'id' => $user->id]);
    }

    /**
     * Test whether default form values are correct
     */
    public function testProfileFormValues(\FunctionalTester $I)
    {
        $user = User::findById(1);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/update-profile', 'id' => $user->id]);
        $I->assertEqualsGrabbedValue($user->degree_prefix, 'form input[name="User[degree_prefix]"]');
        $I->assertEqualsGrabbedValue($user->degree_suffix, 'form input[name="User[degree_suffix]"]');
        $I->assertEqualsGrabbedValue($user->custom_url, 'form input[name="User[custom_url]"]');
        $I->assertEqualsGrabbedValue($user->email, 'form input[name="User[email]"]');
        $I->assertEqualsGrabbedValue($user->password, 'form input[name="User[password]"]');
    }

    /**
     * Test whether after submitting form without changes
     * data keeps the same
     */
    public function testProfileNoUpdate(\FunctionalTester $I)
    {
        $user = User::findById(1);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/update-profile', 'id' => $user->id]);
        $I->submitForm('form', []);
        $newUser = User::findById(1);
        $I->assertEquals($user->name, $newUser->name);
        $I->assertEquals($user->degree_prefix, $newUser->degree_prefix);
        $I->assertEquals($user->degree_suffix, $newUser->degree_suffix);
        $I->assertEquals($user->email, $newUser->email);
        $I->assertEquals($user->custom_url, $newUser->custom_url);
        $I->assertEquals($user->news_subscription, $newUser->news_subscription);
        $I->assertEquals($user->password, $newUser->password);
    }

    /**
     * Test update name in profile
     */
    public function testUpdateProfileName(\FunctionalTester $I)
    {
        $this->updateProfileAttrTest($I, 1, "name", $I->randomStr(1), false);
        $this->updateProfileAttrTest($I, 1, "name", $I->randomStr(200), false);
        $this->updateProfileAttrTest($I, 1, "name", "testname", true);
    }

    /**
     * Test update degree prefix in profile
     */
    public function testUpdateProfileDegreePrefix(\FunctionalTester $I)
    {
        $this->updateProfileAttrTest($I, 1, "degree_prefix", $I->randomStr(100), false);
        $this->updateProfileAttrTest($I, 1, "degree_prefix", "", true);
        $this->updateProfileAttrTest($I, 1, "degree_prefix", "Mgr.", true);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdateProfileDegreeSuffix(\FunctionalTester $I)
    {
        $this->updateProfileAttrTest($I, 1, "degree_suffix", $I->randomStr(100), false);
        $this->updateProfileAttrTest($I, 1, "degree_suffix", "", true);
        $this->updateProfileAttrTest($I, 1, "degree_suffix", "PhD.", true);
    }

    /**
     * Test update email in profile
     */
    public function testUpdateProfileEmail(\FunctionalTester $I)
    {
        $this->updateProfileAttrTest($I, 1, "email", $I->randomStr(300), false);
        $this->updateProfileAttrTest($I, 1, "email", 'nonvalidmail', false);
        $this->updateProfileAttrTest($I, 1, "email", 'nonvalidmailwith@', false);
        $this->updateProfileAttrTest($I, 1, "email", 'nonvalidmailwith@gmail', false);
        $this->updateProfileAttrTest($I, 1, "email", 'jiri.novak@gmail.com', false);
        $this->updateProfileAttrTest($I, 1, "email", "correctUnique@email.com", true);
    }

    /**
     * Test update password in profile
     */
    public function testUpdateProfilePassword(\FunctionalTester $I)
    {
        $newPassword = "newpassword";
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/update-profile', 'id' => $user->id]);
        $I->submitForm('form', ['User[password]' => $newPassword]);
        $newUser = User::findById($user->id);
        Yii::$app->getSecurity()->validatePassword($newPassword, $newUser->password);
    }

    /**
     * Test update news_subscription in profile
     */
    public function testUpdateProfileNewsSubscriptionLang(\FunctionalTester $I)
    {
        $this->updateProfileAttrTest($I, 1, "news_subscription", 0, true);
        $this->updateProfileAttrTest($I, 1, "news_subscription", 1, true);
        $this->updateProfileAttrTest($I, 1, "news_subscription", 25, false);
    }

    /**
     * Test whether default form values are correct
     */
    public function testUpdateFormValues(\FunctionalTester $I)
    {
        $user = User::findById(1);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/update', 'id' => $user->id]);
        $I->assertEqualsGrabbedValue($user->name, 'form input[name="User[name]"]');
        $I->assertEqualsGrabbedValue($user->degree_prefix, 'form input[name="User[degree_prefix]"]');
        $I->assertEqualsGrabbedValue($user->degree_suffix, 'form input[name="User[degree_suffix]"]');
        $I->assertEqualsGrabbedValue($user->email, 'form input[name="User[email]"]');
        $I->assertEqualsGrabbedValue($user->password, 'form input[name="User[password]"]');
        $I->seeOptionIsSelected('form select[name="User[role]"]', User::roleToString($user->role));
        $I->assertEqualsGrabbedValue($user->role, 'form select[name="User[role]"]');
        $I->assertEqualsGrabbedValue($user->password, 'form input[name="User[password]"]');
        $I->assertEqualsGrabbedValue($user->custom_url, 'form input[name="User[custom_url]"]');
        $I->asserEqualsCheckedOption($user->news_subscription, 'form input[name="User[news_subscription]"]');
        $I->seeInputDate($user->role_expiration, 'form input[name="User[role_expiration]"]');
        $I->asserEqualsCheckedOption($user->login_type, 'form input[name="User[login_type]"]');
        $I->asserEqualsCheckedOption($user->role_is_from_CAS, 'form input[name="User[role_is_from_CAS]"]');
        $I->asserEqualsCheckedOption($user->banned, 'form input[name="User[banned]"]');
    }

    /**
     * Dont see deleted users in update, view, profile and profile-update
     * views.
     */
    public function dontSeeDeletedUsers(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $deletedUser = User::findById(7);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/update', 'id' => $deletedUser->id]);
        $I->seePageNotFound();
        $I->amOnPage(['users/view', 'id' => $deletedUser->id]);
        $I->seePageNotFound();
        $I->amOnPage(['users/profile', 'id' => $deletedUser->id]);
        $I->seePageNotFound();
        $I->amOnPage(['users/profile-update', 'id' => $deletedUser->id]);
        $I->seePageNotFound();
    }

    /**
     * Test whether after submitting form without changes
     * data remains the same
     */
    public function testUpdateEmptyForm(\FunctionalTester $I)
    {
        $user = User::findById(1);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/update', 'id' => $user->id]);
        $I->submitForm('form', []);
        $newUser = User::findById(1);
        $I->assertEquals($user->name, $newUser->name);
        $I->assertEquals($user->degree_prefix, $newUser->degree_prefix);
        $I->assertEquals($user->degree_suffix, $newUser->degree_suffix);
        $I->assertEquals($user->email, $newUser->email);
        $I->assertEquals($user->custom_url, $newUser->custom_url);
        $I->assertEquals($user->news_subscription, $newUser->news_subscription);
        $I->assertEquals($user->password, $newUser->password);
        $I->assertEquals($user->login_type, $newUser->login_type);
        $I->assertEquals($user->role_is_from_CAS, $newUser->role_is_from_CAS);
        $I->assertEquals($user->banned, $newUser->banned);
        $I->assertEquals($user->deleted, $newUser->deleted);
    }

    /**
     * Test update name in profile
     */
    public function testUpdateName(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "name", $I->randomStr(1), false);
        $this->updateUserAttrTest($I, 1, "name", $I->randomStr(200), false);
        $this->updateUserAttrTest($I, 1, "name", "testname", true);
    }

    /**
     * Test update degree prefix in profile
     */
    public function testUpdateDegreePrefix(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "degree_prefix", $I->randomStr(100), false);
        $this->updateUserAttrTest($I, 1, "degree_prefix", "", true);
        $this->updateUserAttrTest($I, 1, "degree_prefix", "Mgr.", true);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdateDegreeSuffix(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "degree_suffix", $I->randomStr(100), false);
        $this->updateUserAttrTest($I, 1, "degree_suffix", "", true);
        $this->updateUserAttrTest($I, 1, "degree_suffix", "PhD.", true);
    }

    /**
     * Test update email in profile
     */
    public function testUpdateEmail(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "email", $I->randomStr(300), false);
        $this->updateUserAttrTest($I, 1, "email", 'nonvalidmail', false);
        $this->updateUserAttrTest($I, 1, "email", 'nonvalidmailwith@', false);
        $this->updateUserAttrTest($I, 1, "email", 'nonvalidmailwith@gmail', false);
        $this->updateUserAttrTest($I, 1, "email", 'jiri.novak@gmail.com', false);
        $this->updateUserAttrTest($I, 1, "email", "correctUnique@email.com", true);
    }

    /**
     * Test whether updating wrong password won't change password
     */
    public function testUpdateWrongPassword(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "password", $I->randomStr(4), false);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdatePassword(\FunctionalTester $I)
    {
        $newPassword = "newpassword";
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/update', 'id' => $user->id]);
        $I->submitForm('form', ['User[password]' => $newPassword]);
        $newUser = User::findById($user->id);
        Yii::$app->getSecurity()->validatePassword($newPassword, $newUser->password);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdateRole(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 2, "role", "invalidRole", false);
        $this->updateUserAttrTest($I, 2, "role", User::ROLE_COMMITTEE_MEMBER, true);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdateCustomUrl(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "custom_url", $I->randomStr(1000), false);
        $this->updateUserAttrTest($I, 1, "custom_url", $I->randomStr(100), true);
        $this->updateUserAttrTest($I, 1, "custom_url", "", true);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdateNewsSubscriptionLang(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "news_subscription", 0, true);
        $this->updateUserAttrTest($I, 1, "news_subscription", 1, true);
        $this->updateUserAttrTest($I, 1, "news_subscription", 25, false);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdateLoginType(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "login_type", "cas", true);
        $this->updateUserAttrTest($I, 1, "login_type", "manual", true);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdateRoleIsFromCas(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "role_is_from_CAS", 0, true);
        $this->updateUserAttrTest($I, 1, "role_is_from_CAS", 1, true);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdateBanned(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 1, "banned", 0, true);
        $this->updateUserAttrTest($I, 1, "banned", 1, true);
    }

    /**
     * Test update degree suffix in profile
     */
    public function testUpdateDeleted(\FunctionalTester $I)
    {
        $this->updateUserAttrTest($I, 2, "deleted", 1, true);
    }

    /**
     * Committee member should not be allowed to access users/view, users/update
     * and users/delete pages
     */
    public function testCreateUser(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $name = 'Name';
        $prefix = 'prefix';
        $postfix = 'postfix';
        $email = 'email@gmail.com';
        $password = "Test1234";
        $formValues = [
            'User[name]' => $name,
            'User[prefix]' => $prefix,
            'User[postfix]' => $postfix,
            'User[email]' => $email,
            'User[password]' => $password,
            'User[role]' => User::ROLE_STUDENT,
            'User[role_expiration]' => '30.11.2019'
        ];
        $oldCount = $this->numberOfUsers();
        $I->amOnPage(['users/create']);
        $I->submitForm('form', $formValues);
        $newCount = $this->numberOfUsers();
        $I->assertEquals($oldCount + 1, $newCount);
        $createUser = User::findByEmail($email);
        $I->seeCurrentUrl(['users/view', 'id' => $createUser->id]);
    }

    /**
     * Test create user with wrong email address
     */
    public function testCreateUserWithWrongEmail(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $this->wrongInputTest($I, "email", "");
        $this->wrongInputTest($I, "email", "email");
        $this->wrongInputTest($I, "email", "email@test");
    }

    /**
     * Test create user with wrong prefix suffix
     */
    public function testCreateUserWithWrongPrefixSuffix(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $keys = ["degree_prefix", "degree_suffix"];
        foreach ($keys as $key) {
            $this->wrongInputTest($I, $key, $I->randomStr(100));
        }
    }

    /**
     * Test create user with wrong name
     */
    public function testCreateUserWithWrongName(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $this->wrongInputTest($I, "name", "");
        $this->wrongInputTest($I, "name", $I->randomStr(200));
    }

    /**
     * Test create user with wrong password
     */
    public function testCreateUserWithWrongPassword(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $this->wrongInputTest($I, "password", "abc");
        $this->wrongInputTest($I, "password", $I->randomStr(400));
    }

    /**
     * Test create user with wrong role
     */
    public function testCreateUserWithWrongRole(\FunctionalTester $I)
    {
        $user = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($user);
        $I->amOnPage(['news/create']);
        $this->wrongInputTest($I, "role", "abc");
    }

    /**
     * See all users and right data with buttons for view, update and ban
     */
    public function testUserList(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['users']);
        $all = User::find()->all();
        foreach ($all as $user) {
            if ($user->deleted) continue;
            $id = $user->id;
            $I->see($user->name, ['css' => 'tr[data-key="' . $id . '"] td']);
            $I->see(User::roleToString($user->role), ['css' => 'tr[data-key="' . $id . '"] td']);
            $I->see($user->email, ['css' => 'tr[data-key="' . $id . '"] td']);
            $I->see($user->banned, ['css' => 'tr[data-key="' . $id . '"] td:nth-child(5)']);
            $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td a[title="View"]']);
            $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td a[title="Update"]']);
            $I->seeElement(['css' => 'tr[data-key="' . $id . '"] td a[title="Ban"]']);
        }
    }

    /**
     * Test view button
     */
    public function testUserViewButton(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['users']);
        $id = 2;
        $I->click(['css' => 'tr[data-key="' . $id . '"] td a[title="View"]']);
        $I->seeCurrentUrl(['users/view', 'id' => $id]);
    }

    /**
     * Test update button
     */
    public function testUserUpdateButton(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['users']);
        $id = 2;
        $I->click(['css' => 'tr[data-key="' . $id . '"] td a[title="Update"]']);
        $I->seeCurrentUrl(['users/update', 'id' => $id]);
    }

    /**
     * Test ban button
     */
    public function testUserBanButton(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['users']);
        $id = 2;
        $I->click(['css' => 'tr[data-key="' . $id . '"] td a[title="Ban"]']);
        $I->see(1, ['css' => 'tr[data-key="' . $id . '"] td:nth-child(5)']);
        $I->click(['css' => 'tr[data-key="' . $id . '"] td a[title="Ban"]']);
        $I->see(0, ['css' => 'tr[data-key="' . $id . '"] td:nth-child(5)']);
    }

    /**
     * See all users and right data
     */
    public function testUserAccessAsStudent(\FunctionalTester $I)
    {
        $loggedUser = User::findByRole(User::ROLE_STUDENT);
        $I->amLoggedInAs($loggedUser);
        $I->notAllowedPage(['users']);
    }

    /**
     * Helper method for testint update individual attributes
     * @param int $userId
     * @param string $attrName
     * @param $value
     * @param bool $success
     */
    private function updateProfileAttrTest(\FunctionalTester $I, int $userId, $attrName, $value, bool $success)
    {
        $user = User::findById($userId);
        $I->amLoggedInAs($user);
        $I->amOnPage(['users/update-profile', 'id' => $user->id]);
        $I->submitForm('form', ['User[' . $attrName . ']' => $value]);
        $newUser = User::findById($userId);
        $testValue = $success ? $value : $user->$attrName;
        $I->assertEquals($testValue, $newUser->$attrName);
    }

    /**
     * Helper method for testint update individual attributes
     * @param int $userId
     * @param string $attrName
     * @param $value
     * @param bool $success
     */
    private function updateUserAttrTest(\FunctionalTester $I, int $userId, $attrName, $value, bool $success)
    {
        $user = User::findById($userId);
        $loggedUser = User::findByRole(User::ROLE_COMMITTEE_CHAIR);
        $I->amLoggedInAs($loggedUser);
        $I->amOnPage(['users/update', 'id' => $user->id]);
        $I->submitForm('form', ['User[' . $attrName . ']' => $value]);
        $newUser = User::findById($userId);
        $testValue = $success ? $value : $user->$attrName;
        $I->assertEquals($testValue, $newUser->$attrName);
    }

    private function wrongInputTest(\FunctionalTester $I, string $attr, $value)
    {
        $name = 'Name';
        $prefix = 'prefix';
        $postfix = 'postfix';
        $email = 'email@gmail.com';
        $password = "Test1234";
        $formValues = [
            'User[name]' => $name,
            'User[prefix]' => $prefix,
            'User[postfix]' => $postfix,
            'User[email]' => $email,
            'User[password]' => $password,
            'User[role]' => User::ROLE_STUDENT,
            'User[role_expiration]' => '30-11-2019'
        ];
        $formValues['User[' . $attr . ']'] = $value;
        $oldCount = $this->numberOfUsers();
        $I->amOnPage(['users/create']);
        $I->submitForm('form', $formValues);
        $newCount = $this->numberOfUsers();
        $I->assertEquals($oldCount, $newCount);
        $I->seeCurrentUrl(['users/create']);
    }

    /**
     * Number of users
     * @return integer Number of users
     */
    private function numberOfUsers() : int
    {
        $all = User::find()->all();
        return sizeof($all);
    }
}