<?php
namespace app\rules;

use app\consts\Permission;
use app\models\records\ProjectDefence;
use yii\rbac\Rule;

/**
 * Checks if user is the opponent of this project and is allowed to upload the opponent's review
 */
class DefenceUploadOpponentReviewRule extends Rule
{
    /** @var string Rule name */
    public $name = 'canUploadOpponentReview';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if(!isset($params[ Permission::PARAM_DEFENCE ])){
            return false;
        }
        /** @var ProjectDefence $defence */
        $defence = $params[ Permission::PARAM_DEFENCE ];
        $opponent = $defence->getOpponent();
        $isDone = $defence->getDefenceStatement() !== null;
        return $opponent && $opponent->id === $user && !$isDone;
    }
}
