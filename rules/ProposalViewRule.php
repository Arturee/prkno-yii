<?php
namespace app\rules;

use app\models\records\Proposal;
use yii\rbac\Rule;

/**
 * Checks if proposal is viewable
 */
class ProposalViewRule extends Rule
{
    /** @var string Rule name */
    public $name = 'isProposalSent';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
    	return isset($params['proposal'])
            && !$params['proposal']->deleted
            && $params['proposal']->state != Proposal::STATE_DRAFT;
    }
}
