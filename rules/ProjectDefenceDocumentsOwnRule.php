<?php
namespace app\rules;

use app\models\records\Project;
use yii\rbac\Rule;

/**
 * Checks if the user can send, update, delete defence documents on his own project
 */
class ProjectDefenceDocumentsOwnRule extends Rule
{
    /** @var string Rule name */
    public $name = 'canDefenceDocumentsOwnProject';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!isset($params['project'])) {
            return false;
        }
        /** @var Project $project */
        $project = $params['project'];
        if (
            $project->deleted
            || !in_array($user, array_merge(
                $params['project']->memberIds,
                $params['project']->supervisorIds ))
            || !Project::canAssignToDefence($project->state)
        ) {
            return false;
        }
        return true;
    }
}
