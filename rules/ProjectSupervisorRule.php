<?php
namespace app\rules;

use app\consts\Permission;
use app\exceptions\BugError;
use yii\rbac\Rule;

/**
 * Checks if the user is the owner (the supervisor) of the project
 */
class ProjectSupervisorRule extends Rule
{
    /** @var string Rule name */
    public $name = 'isSupervisorOfProject';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!isset($params[ Permission::PARAM_PROJECT ])) {
            throw new BugError('project missing');
        }
        /** @var \app\models\records\Project $project */
        $project = $params[ Permission::PARAM_PROJECT ];
        if (!$project) {
            return false;
        }
        $supervisor = $project->getSupervisors()->one();
        if (!$supervisor) {
            return false;
        }
        $result = $supervisor->id === $user;
        return $result;
    }
}
