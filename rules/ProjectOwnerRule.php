<?php
namespace app\rules;

use app\models\records\Project;
use yii\rbac\Rule;

/**
 * Checks if the user is the owner (the supervisor) of the project
 */
class ProjectOwnerRule extends Rule
{
    /** @var string Rule name */
    public $name = 'isProjectOwner';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['project'])
            && !$params['project']->deleted
            && $params['project']->state == Project::STATE_PROPOSAL
            && in_array($user, $params['project']->supervisorIds);
    }
}
