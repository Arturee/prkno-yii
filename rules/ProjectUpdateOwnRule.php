<?php
namespace app\rules;

use app\models\records\Project;
use yii\rbac\Rule;

/**
 * Checks if the user can update his own project
 */
class ProjectUpdateOwnRule extends Rule
{
    /** @var string Rule name */
    public $name = 'canUpdateOwnProject';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!isset($params['project'])) {
            return false;
        }
        /** @var Project $project */
        $project = $params['project'];
        if (
            $project->deleted
            || !in_array($user, $project->supervisorIds)
            || !in_array($project->state, [ Project::STATE_PROPOSAL, Project::STATE_ACCEPTED ])
        ) {
            return false;
        }
        return true;
    }
}
