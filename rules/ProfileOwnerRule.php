<?php
namespace app\rules;

use yii\rbac\Rule;

/**
 * Checks if the user is the owner of this profile
 */
class ProfileOwnerRule extends Rule
{
    /** @var string Rule name */
    public $name = 'isProfileOwner';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['user'])
            && $params['user']->id == $user;
    }
}
