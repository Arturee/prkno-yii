<?php
namespace app\rules;

use app\models\records\Proposal;
use yii\rbac\Rule;

/**
 * Checks if the user is allowed to edit this project
 */
class ProjectEditRule extends Rule
{
    /** @var string Rule name */
    public $name = 'isProjectAccepted';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['project'])
            && in_array($user, $params['project']->supervisorIds)
            && !$params['project']->deleted
            && $params['project']->lastState->state != Proposal::STATE_SENT;
    }
}
