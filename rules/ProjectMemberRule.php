<?php
namespace app\rules;

use app\models\records\Project;
use yii\rbac\Rule;

/**
 * Checks if the user is a team member (a student) working of the project
 */
class ProjectMemberRule extends Rule
{
    /** @var string Rule name */
    public $name = 'isProjectMember';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['project'])
            && in_array($user, $params['project']->memberIds)
            && $params['project']->state != Project::STATE_CANCELED;
    }
}