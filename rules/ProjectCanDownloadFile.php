<?php

namespace app\rules;

use app\consts\Permission;
use app\models\records\ProjectDocument;
use yii\rbac\Rule;

class ProjectCanDownloadFile extends Rule
{
    /** @var string Rule name */
    public $name = 'canDownloadProjectFile';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!isset($params[Permission::PARAM_PROJECT_DOCUMENT])) {
            return false;
        }

        /** @var ProjectDocument $projectDocument */
        $projectDocument = $params[Permission::PARAM_PROJECT_DOCUMENT];
        if ($projectDocument->deleted || $projectDocument->project->deleted)
        {
            return false;
        }

        if ($projectDocument->team_private)
        {
            if (in_array($user, $projectDocument->project->memberIds)
                || in_array($user, $projectDocument->project->supervisorIds)
            ) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }
}