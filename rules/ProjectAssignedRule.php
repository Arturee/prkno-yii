<?php
namespace app\rules;

use app\consts\Permission;
use yii\rbac\Rule;

/**
 * Checks if the user is related to this project in any way
 */
class ProjectAssignedRule extends Rule
{
    /** @var string Rule name */
    public $name = 'isProjectAssigned';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params[Permission::PARAM_PROJECT])
            && in_array($user, array_merge(
                $params[Permission::PARAM_PROJECT]->consultantIds,
                $params[Permission::PARAM_PROJECT]->supervisorIds,
                $params[Permission::PARAM_PROJECT]->memberIds,
                $params[Permission::PARAM_PROJECT]->opponentIds
            ));
    }
}
