<?php
namespace app\rules;

use yii\rbac\Rule;

/**
 * Checks if the user can assign the project to a defence date
 */
class DefenceDateAddProjectRule extends Rule
{
    /** @var string Rule name */
    public $name = 'canAddProjectToDefence';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['project'])
            && in_array($user, array_merge(
                $params['project']->supervisorIds,
                $params['project']->memberIds
            ));
    }
}
