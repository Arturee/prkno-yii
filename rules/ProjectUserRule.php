<?php
namespace app\rules;

use app\models\records\Project;
use yii\rbac\Rule;

/**
 * Checks if the user is related to this project in any way
 */
class ProjectUserRule extends Rule
{
    /** @var string Rule name */
    public $name = 'isOnProjectUser';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['project'])
            && in_array($user, array_merge(
                $params['project']->consultantIds,
                $params['project']->memberIds,
                $params['project']->supervisorIds
            )) && $params['project']->state != Project::STATE_CANCELED;
    }
}