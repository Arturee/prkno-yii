<?php
namespace app\rules;

use app\models\records\Proposal;
use yii\rbac\Rule;

/**
 * Checks if the user is the author of a proposal comment
 */
class ProposalCommentOwnerRule extends Rule
{
    /** @var string Rule name */
    public $name = 'isProposalCommentOwner';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['comment'])
            && !$params['comment']->proposal->deleted
            && !$params['comment']->deleted
            && $params['comment']->proposal->state == Proposal::STATE_SENT
            && $params['comment']->creator_id == $user;
    }
}
