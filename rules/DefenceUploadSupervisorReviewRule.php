<?php
namespace app\rules;

use app\consts\Permission;
use app\models\records\ProjectDefence;
use yii\rbac\Rule;

/**
 * Checks if user is the supervisor of a project and can upload the supervisor's review
 */
class DefenceUploadSupervisorReviewRule extends Rule
{
    /** @var string Rule name */
    public $name = 'canUploadSupervisorReview';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if(!isset($params[ Permission::PARAM_DEFENCE ])){
            return false;
        }
        /** @var ProjectDefence $defence */
        $defence = $params[ Permission::PARAM_DEFENCE ];
        $supervisor = $defence->project->getSupervisors()->one();
        $isDone = $defence->getDefenceStatement() !== null;
        return $supervisor && $supervisor->id === $user && !$isDone;
    }
}
