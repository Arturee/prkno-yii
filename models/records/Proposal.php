<?php
namespace app\models\records;

use app\consts\Sql;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Markdown;
use yii\helpers\Html;

/**
 * This is the model class for table "proposal".
 * Proposal is a document describing project idea
 * before the project is approved. Project can
 * have several proposals and after approving
 * the last one, project state is changed from 'proposal' to 'accepted'.
 * If the proposal is declined, new proposal can be created and
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $project_id        Project id
 * @property int $document_id       Document id storing the proposal description
 * @property string $state          State of the proposal. Allowed values: 'draft', 'sent', 'accepted', 'declined'.
 * @property string $comment        Optional comment. Can be used for describing the reason behind the final decision.
 *
 * @property ProposalComment[] $proposalComments        Getter generated from getProposalComments method
 * @property ProposalComment[] $publicProposalComments  Getter generated from getPublicProposalComments method
 * @property ProposalVote[] $proposalVotes              Getter generated from getProposalVotes method
 * @property Document $document                         Getter generated from getDocument method
 * @property Project $project                           Getter generated from getProject method
 */
class Proposal extends OrmRecord
{
    const
        STATE_DRAFT = 'draft',
        STATE_SENT = 'sent',
        STATE_ACCEPTED = 'accepted',
        STATE_DECLINED = 'declined';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proposal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'state'], 'required'],
            [['created_at', 'updated_at', 'documentMarkdown', 'commentMarkdown'], 'safe'],
            [['project_id', 'document_id'], 'integer'],
            [['deleted'], 'boolean'],
            [['state', 'comment'], 'string'],
            [['state'], 'default', 'value' => self::STATE_DRAFT],
            ['state', 'in', 'range' => [self::STATE_DRAFT, self::STATE_SENT, self::STATE_ACCEPTED, self::STATE_DECLINED]],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['document_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'project_id' => Yii::t('app', 'Project ID'),
            'document_id' => Yii::t('app', 'Document ID'),
            'state' => Yii::t('app', 'State'),
            'comment' => Yii::t('app', 'Comment'),
            'documentMarkdown' => Yii::t('app', 'Markdown'),
        ];
    }

    /**
     * @return ActiveQuery  Get List of proposal comments for this proposal
     * in ascending order by creation date
     */
    public function getProposalComments() : ActiveQuery
    {
        return $this->hasMany(ProposalComment::class, ['proposal_id' => 'id'])
            ->andOnCondition(Sql::CONDITION_NOT_DELETED)
            ->orderBy(['created_at' => SORT_ASC])
            ->inverseOf('proposal');
    }

    /**
     * @return ActiveQuery  Get List of proposal comments for this proposal
     * in ascending order by creation date, which are visible only for
     * committee members
     */
    public function getPublicProposalComments() : ActiveQuery
    {
        return $this->hasMany(ProposalComment::class, ['proposal_id' => 'id'])
            ->andOnCondition(Sql::CONDITION_NOT_DELETED)
            ->andOnCondition(['committee_only' => 0])
            ->orderBy(['created_at' => SORT_ASC])
            ->inverseOf('proposal');
    }

    /**
     * @param bool $committee_only   True for retrieving comments visible only
     * for committee members
     * @return ProposalComment[]  List of all proposal comments or only comments visible
     * for commitee members based on parameter
     */
    public function getProposalViewComments(bool $committee_only)
    {
        if ($committee_only) {
            return $this->proposalComments;
        } else {
            return $this->publicProposalComments;
        }
    }

    /**
     * @return ActiveQuery  List of all ProposalVote records for this proposal
     */
    public function getProposalVotes() : ActiveQuery
    {
        return $this->hasMany(ProposalVote::class, ['proposal_id' => 'id'])
            ->andOnCondition(Sql::CONDITION_NOT_DELETED);
    }

    /**
     * @return ProposalVote|null    Proposal vote for this instance of the user
     * with given user id
     */
    public function getProposalVoteByUserId(int $userId) : ? ProposalVote
    {
        return ProposalVote::findOne(['proposal_id' => $this->id, 'user_id' => $userId, 'deleted' => false]);
    }

    /**
     * @return ActiveQuery  All Document records uploaded for this proposal
     */
    public function getDocument() : ActiveQuery
    {
        return $this->hasOne(Document::class, ['id' => 'document_id'])
            ->andOnCondition(Sql::CONDITION_NOT_DELETED);
    }

    /**
     * @return ActiveQuery  Project which this proposal belongs to
     */
    public function getProject() : ActiveQuery
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * Set content in markdown format for this proposal
     * @param string $content   Proposal description
     */
    public function setDocumentMarkdown(string $content) : void
    {
        $this->documentMarkdown = $content;
    }

    /**
     * Get markdown content from the document containing description
     * @return string Content markdown of the proposal document
     */
    public function getDocumentMarkdown()
    {
        $documentMarkdown = '';

        if ($this->isNewRecord) {
            return !empty($this->documentMarkdown) ? $this->documentMarkdown : $documentMarkdown;
        }

        if (isset($this->document->content_md)) {
            $documentMarkdown = $this->document->content_md;
        }

        return $documentMarkdown;
    }

    /**
     * @return string   Markdown document content as processed HTML
     */
    public function getDocumentMarkdownProcessed()
    {
        return Markdown::process(Html::encode($this->getDocumentMarkdown()));
    }

    /**
     * @return string   Markdown comment content as processed HTML
     */
    public function getCommentMarkdownProcessed()
    {
        return Markdown::process(Html::encode($this->comment));
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $document = $this->document;
        if (!isset($this->document)) {
            $document = new Document();
        }
        $document->content_md = $this->documentMarkdown;

        if (!$document->save()) {
            $transaction->rollBack();
            return false;
        }

        $this->document_id = $document->id;

        if (!parent::save(false, $attributeNames)) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    /**
     * @param string $state     Project state
     * @param string $language  Language code
     * @return string Label for state description
     */
    public static function getStateToString(string $state, string $language = null) : string
    {
        switch ($state) {
            case Proposal::STATE_DRAFT:
                return Yii::t('app', 'Draft Proposal', [], $language);
            case Proposal::STATE_SENT:
                return Yii::t('app', 'Sent Proposal', [], $language);
            case Proposal::STATE_ACCEPTED:
                return Yii::t('app', 'Accepted Proposal', [], $language);
            case Proposal::STATE_DECLINED:
                return Yii::t('app', 'Declined Proposal', [], $language);
        }
        return "";
    }
}
