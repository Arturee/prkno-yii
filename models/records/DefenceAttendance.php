<?php
namespace app\models\records;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "defence_attendance".
 * When record in defence_date table is created, each
 * committee member can vote whether he or she can attend
 * that event or not. Defence attendance table stores
 * these votes with some additional information.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $defence_date_id   Id of the defence date record
 * @property int $user_id           User id who vote whether he or she can or can not attend that defence
 * @property string $vote           Enum value 'yes', 'no', 'unkonwn' as a result of the voting
 * @property string $comment        Optional attribute storing comment. User can write here some additional information, for example, reason why he can not come.
 * @property int $must_come         Boolean describing whether the user has to come or not.
 *
 * @property DefenceDate $defenceDate   Getter generated from getDefenceDate method
 * @property User $user                 Getter generated from getUser method
 */
class DefenceAttendance extends OrmRecord
{
    const
        VOTE_YES = 'yes',
        VOTE_NO = 'no',
        VOTE_UNKNOWN = 'unknown',
        COL_VOTE = 'vote',
        SCENARIO_CREATE = 'scenario.create';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'defence_attendance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['defence_date_id', 'user_id', 'vote', 'must_come'], 'required'],
            [['id', 'defence_date_id', 'user_id'], 'integer'],
            [['must_come', 'deleted'], 'boolean'],
            ['must_come', 'default', 'value' => false],
            ['vote', 'in', 'range' => [self::VOTE_YES, self::VOTE_NO, self::VOTE_UNKNOWN]],
            [['created_at', 'updated_at'], 'safe'],
            [['comment'], 'string', 'max' => 500],
            [['defence_date_id'], 'exist', 'skipOnError' => true, 'targetClass' => DefenceDate::class, 'targetAttribute' => ['defence_date_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'defence_date_id' => Yii::t('app', 'Defence Date ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'vote' => Yii::t('app', 'Can come'),
            'comment' => Yii::t('app', 'Comment'),
            'must_come' => Yii::t('app', 'Must Come'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['defence_date_id', 'user_id', 'vote'];
        return $scenarios;
    }

    /**
     * Get translated label based on selected language
     * for vote enum value
     * @param string $vote     Vote enum value
     * @return string Label text
     */
    public static function getVoteLabel(string $vote) : string
    {
        $map = [
            self::VOTE_YES => Yii::t('app', 'Yes'),
            self::VOTE_NO => Yii::t('app', 'No'),
            self::VOTE_UNKNOWN => Yii::t('app', 'Unknown')
        ];
        return $map[$vote];
    }

    /**
     * Get DefenceDate
     * @return ActiveQuery      DefenceDate of the defence attendance
     */
    public function getDefenceDate() : ActiveQuery
    {
        return $this->hasOne(DefenceDate::class, ['id' => 'defence_date_id']);
    }

    /**
     * Get User
     * @return ActiveQuery      User
     */
    public function getUser() : ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Generate DefenceAttendance objects for given defence date
     * for all committee members.
     * @param int $defenceDateId    DefenceDate id
     */
    public static function generateForCommitteMembers(int $defenceDateId) : void
    {
        $committeeMembers = User::findCommitteeMembers();
        foreach ($committeeMembers as $member) {
            $attendance = new DefenceAttendance();
            $attendance->scenario = DefenceAttendance::SCENARIO_CREATE;
            $attendance->defence_date_id = $defenceDateId;
            $attendance->user_id = $member->id;
            $attendance->must_come = false;
            $attendance->vote = DefenceAttendance::VOTE_UNKNOWN;
            $attendance->save();
        }
    }
}
