<?php
namespace app\models\records;

use app\behaviors\TimestampBehavior;
use app\utils\DateTime;
use Yii;
use yii\db\ActiveRecord;
use app\consts\Param;

/**
 * General record used for abstraction. Represent one database record.
 * 
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 */
class OrmRecord extends ActiveRecord {
    const
        COL_CREATED_AT = 'created_at',
        SCENARIO_CREATE = 'scenarion.create';

    /** @var string[] date attributes  */
    public static $DATE_ATTRIBUTES = [];

    /** @var string[] date time attributes */
    public static $DATETIME_ATTRIBUTES = [ 'created_at', 'updated_at' ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [ 'class' => TimestampBehavior::class ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        foreach (static::$DATE_ATTRIBUTES as $attr) {
            if (isset($this->{$attr})) {
                $value = $this->{$attr};
                if (!$value || !is_string($value)) {
                    continue;
                }
                $value = DateTime::from($value);
                $this[$attr] = (new DateTime($value))->format(DateTime::SQL_DATE);
            }
        }
        foreach (static::$DATETIME_ATTRIBUTES as $attr) {
            if (isset($this->{$attr})){
                $value = $this->{$attr};
                if (!$value || !is_string($value)) {
                    continue;
                }
                $value = DateTime::from($value);
                $this[$attr] = (new DateTime($value))->format(DateTime::SQL_DATETIME);
            }
        }
        return parent::beforeValidate();
    }

    /**
     * Load data from post variables
     * @return true if user send some data with POST method
     */
    public function loadFromPost(...$args): bool {
        return $this->load(Yii::$app->request->post(), ...$args);
    }

    /**
     * Find record by id
     * @param int $id   ID of the record
     * @return OrmRecord    OrmRecord with given id
     */
    public static function findById(int $id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Delete record by setting deleted attribute to true
     */
    public function cancel(): void
    {
        $this->deleted = true;
        $this->save();
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $result = parent::save($runValidation, $attributeNames);
        if ($result)
        {
            Yii::info($this->serialize(), Param::LOG_CATEGORY_AUDIT);
        }
        return $result;
    }

    private function serialize() : string
    {
        $serialized = "[" . $this::tableName() . "]";

        $serialized .= "[";

        $attributes = $this->getAttributes();
        foreach ($attributes as $attributeName => $attributeValue) {
            $serialized .= "$attributeName=>'$attributeValue'|";
        }
        $serialized = substr_replace($serialized, "", -1);

        $serialized .= "]";

        return $serialized;
    }
}