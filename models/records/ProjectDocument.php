<?php
namespace app\models\records;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "project_document".
 * project_document is a table storing additional document information related
 * to project or defence. Example:
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $project_id        Id of the project the document is associated with
 * @property int $document_id       Document id
 * @property string $comment        Optional comment
 * @property int $team_private      True whether the document is visible only for team members
 * @property int $defence_number    Number of the project defence document is for. 1 - analysis, 2 - 1. final defence, 3 - 2. final defence or null if the document is not for defence.
 * @property string $type           Type of the project document. Allowed values: 'for_defense_pdf', 'for_defense_zip', 'read_only', 'advice'.
 *
 * @property Document $document     Getter generated from getDocument method
 * @property Project $project       Getter generated from getProject method
 */
class ProjectDocument extends OrmRecord
{
    const
        COL_DEFENCE_NUMBER = 'defence_number',
        COL_TYPE = 'type',
        COL_TEAM_PRIVATE = 'team_private',

        TYPE_FOR_DEFENSE_PDF = 'for_defense_pdf',
        TYPE_FOR_DEFENSE_ZIP ='for_defense_zip',
        TYPE_READ_ONLY = 'read_only',
        TYPE_ADVICE = 'advice';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'document_id', 'team_private'], 'required'],
            [['created_at', 'updated_at', 'date_submitted'], 'safe'],
            [['project_id', 'document_id', 'team_private', 'defence_number'], 'integer'],
            [['deleted'], 'boolean'],
            [['comment'], 'string', 'max' => 500],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['document_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            ['type', 'in', 'range' => [self::TYPE_FOR_DEFENSE_ZIP, self::TYPE_FOR_DEFENSE_PDF, self::TYPE_READ_ONLY, self::TYPE_ADVICE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'project_id' => Yii::t('app', 'Project ID'),
            'document_id' => Yii::t('app', 'Document ID'),
            'comment' => Yii::t('app', 'Comment'),
            'team_private' => Yii::t('app', 'Team Private'),
            'defence_number' => Yii::t('app', 'Defence Number'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return ActiveQuery Get document record for this project document
     */
    public function getDocument(): ActiveQuery
    {
        return $this->hasOne(Document::class, ['id' => 'document_id']);
    }

    /**
     * @return ActiveQuery Get project record for this project document
     */
    public function getProject(): ActiveQuery
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }
}
