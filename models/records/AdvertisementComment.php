<?php
namespace app\models\records;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "advertisement_comment".
 * User can post comment to particular advertisements. Table
 * advertisement_comment stores these comments.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $ad_id             Id of the advertisements
 * @property int $user_id           Author id
 * @property string $content        Content of the comment in text form
 *
 * @property Advertisement $ad      Getter generated from getAd method
 * @property User $user             Getter generated from getUser method
 */
class AdvertisementComment extends OrmRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertisement_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ad_id', 'content'], 'required'],
            [['ad_id', 'user_id'], 'integer'],
            [['deleted'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['content'], 'string', 'max' => 1000],
            [['ad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Advertisement::class, 'targetAttribute' => ['ad_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ad_id' => Yii::t('app', 'Ad ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'content' => Yii::t('app', 'Content'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return Advertisement Advertisement object
     */
    public function getAd() : ActiveQuery
    {
        return $this->hasOne(Advertisement::class, ['id' => 'ad_id']);
    }

    /**
     * @return User Get author of the advertisement comment
     */
    public function getUser() : ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

}
