<?php
namespace app\models\records;

use app\consts\Sql;
use app\utils\DateTime;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "project_defence".
 * project_defence is a table storing defences for individual projects.
 * Project can have 3 types of defence: analysis,
 * 1. main defence, 2. main defence. Project defence
 * can be assigned for a defence date. Project can't be
 * assigned for more defence dates so there can be for
 * every project only one record of project defence with
 * future date.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $project_id        Project id
 * @property string $project_state  Result from the project defence
 * @property int $defence_date_id   Defence date id
 * @property int $number            Number of the defence
 * @property int $opponent_id       Opponent id or null if no opponent has been assigned yet
 * @property int $statement_author_id   Id of the author of the final project defence statement - resolution whether the project was defended or not.
 * @property string $time           Day time as string in HH:MM format.
 * @property int $defence_statement Project document id for the defence statement
 * @property int $opponent_review   Project document id for the opponent review
 * @property int $supervisor_review Project document id for the supervisor review
 * @property string $result         Enum value representing result. Values can be: 'defended', 'conditionally_defended', 'failed'.
 *
 * @property DefenceDate $defenceDate       Getter generated from getDefenceDate method
 * @property Document $defenceStatement     Getter generated from getDefenceStatement method
 * @property User $opponent                 Getter generated from getOpponent method
 * @property Document $opponentReview       Getter generated from getOpponentReview method
 * @property Project $project               Getter generated from getProject method
 * @property User $statementAuthor          Getter generated from getStatementAuthor method
 * @property Document $supervisorReview     Getter generated from getSupervisorReview method
 */
class ProjectDefence extends OrmRecord
{
    const
        RESULT_DEFENDED = 'defended',
        RESULT_CONDITIONALLY_DEFENDED = 'conditionally_defended',
        RESULT_FAILED = 'failed',
        SCENARIO_SET_TIME = 'scenario.set.time',
        SCENARIO_ACCEPT = 'scenario.accept';

    public static $DATETIME_ATTRIBUTES = ['created_at', 'updated_at', 'time'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_defence';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['project_id', 'defence_date_id', 'number'];
        $scenarios[self::SCENARIO_SET_TIME] = ['project_id', 'defence_date_id', 'time'];
        $scenarios[self::SCENARIO_ACCEPT] = ['project_id', 'opponent_id', 'statement_author_id', 'defence_statement', 'opponent_review', 'supervisor_review', 'result'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'defence_date_id'], 'required'],
            [['created_at', 'updated_at', 'time'], 'safe'],
            [['project_id', 'defence_date_id', 'opponent_id', 'statement_author_id', 'defence_statement', 'opponent_review', 'supervisor_review', 'number'], 'integer'],
            [['deleted'], 'boolean'],
            [['project_state'], 'string'],
            [['result'], 'in', 'range' => [self::RESULT_DEFENDED, self::RESULT_CONDITIONALLY_DEFENDED, self::RESULT_FAILED]],
            [['defence_date_id'], 'exist', 'skipOnError' => true, 'targetClass' => DefenceDate::class, 'targetAttribute' => ['defence_date_id' => 'id']],
            [['defence_statement'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['defence_statement' => 'id']],
            [['opponent_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['opponent_id' => 'id']],
            [['opponent_review'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['opponent_review' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            [['statement_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['statement_author_id' => 'id']],
            [['supervisor_review'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['supervisor_review' => 'id']],
            [['project_id', 'opponent_id', 'statement_author_id', 'defence_statement', 'opponent_review', 'supervisor_review', 'result'], 'required', 'on' => self::SCENARIO_ACCEPT]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'project_id' => Yii::t('app', 'Project ID'),
            'defence_date_id' => Yii::t('app', 'Defence Date ID'),
            'number' => Yii::t('app', 'Ordinal number of defence'),
            'opponent_id' => Yii::t('app', 'Opponent ID'),
            'statement_author_id' => Yii::t('app', 'Statement Author ID'),
            'time' => Yii::t('app', 'Time'),
            'defence_statement' => Yii::t('app', 'Defence Statement'),
            'opponent_review' => Yii::t('app', 'Opponent Review'),
            'supervisor_review' => Yii::t('app', 'Supervisor Review'),
            'result' => Yii::t('app', 'Result'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return
            [
            'id',
            'time' => function ($model) {
                return DateTime::toJsonDateFormat($model->time);
            },
            'projectID' => 'project_id',
            'projectName' => function ($model) {
                return $model->project->name;
            },
            'projectShortName' => function ($model) {
                return $model->project->short_name;
            },
            'projectDescription' => function ($model) {
                return $model->project->description;
            },
            'projectKeywords' => function ($model) {
                return $model->project->keywords;
            },
            'projectRunStartDate' => function ($model) {
                return DateTime::toJsonDateFormat($model->project->run_start_date);
            },
            'defenceResult' => function ($model) {
                return $model->result;
            }
        ];
    }

    /**
     * @return ActiveQuery  Defence date record of this project defence
     */
    public function getDefenceDate() : ActiveQuery
    {
        return $this->hasOne(DefenceDate::class, ['id' => 'defence_date_id']);
    }

    /**
     * @return ActiveQuery Project record of this the project defence
     */
    public function getProject() : ActiveQuery
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * Get result of this defence or project state, if the value is not set
     * @return string Project defence state or project state if not defined yet
     */
    public function getProjectState() : string
    {
        $state = $this->project_state;
        if (!$state) {
            $state = $this->project->state;
        }
        return $state;
    }

    /**
     * @return User|null Get author of the project defence statement
     * or null, if statement was not declared yet
     */
    public function getStatementAuthor() : ? User
    {
        /** @var User $result */
        $result = $this->hasOne(User::class, ['id' => 'statement_author_id'])
            ->where(Sql::CONDITION_NOT_DELETED)
            ->one();
        return $result;
    }

    /**
     * @return User|null Get opponent of the project defence
     */
    public function getOpponent() : ? User
    {
        /** @var User $result */
        $result = $this->hasOne(User::class, ['id' => 'opponent_id'])
            ->where(Sql::CONDITION_NOT_DELETED)
            ->one();
        return $result;
    }

    /**
     * @return Document|null Get defence statement, or null if
     * final statement was not uploaded yet
     */
    public function getDefenceStatement() : ? Document
    {
        $result = $this->hasOne(Document::class, ['id' => 'defence_statement'])
            ->where(Sql::CONDITION_NOT_DELETED)
            ->one();
        return $result;
    }

    /**
     * @return Document|null Get supervisor review, or null if
     * review was not uploaded yet
     */
    public function getSupervisorReview() : ? Document
    {
        $result = $this->hasOne(Document::class, ['id' => 'supervisor_review'])
            ->where(Sql::CONDITION_NOT_DELETED)
            ->one();
        return $result;
    }

    /**
     * @return Document|null Get opponent review, or null if
     * review was not uploaded yet
     */
    public function getOpponentReview() : ? Document
    {
        $result = $this->hasOne(Document::class, ['id' => 'opponent_review'])
            ->where(Sql::CONDITION_NOT_DELETED)
            ->one();
        return $result;
    }

    /**
     * Get key => value array with final project states as keys
     * and translated labes for those states as values
     * @param string $result    Project state
     * @return string Translated label for the project state
     */
    public static function mapResult(string $result) : string
    {
        $map = [
            self::RESULT_DEFENDED => Yii::t('app', 'Defended'),
            self::RESULT_CONDITIONALLY_DEFENDED => Yii::t('app', 'Defended on condition'),
            self::RESULT_FAILED => Yii::t('app', 'Failed'),
        ];
        return $map[$result];
    }
}
