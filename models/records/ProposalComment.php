<?php
namespace app\models\records;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "proposal_comment".
 * Represents comment for the proposal. Can be used for
 * discussion about particular proposals.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $creator_id        Id of the author
 * @property int $proposal_id       Proposal id
 * @property string $content        Comment text
 * @property int $committee_only    True/false whether the comment is visible only for committee member.
 *
 * @property User $creator          Getter generated from getUser method
 * @property Proposal $proposal     Getter generated from getProposal method
 */
class ProposalComment extends OrmRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proposal_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_id', 'proposal_id', 'content'], 'required'],
            [['id', 'creator_id', 'proposal_id'], 'integer'],
            [['deleted', 'committee_only'], 'boolean'],
            [['committee_only'], 'default', 'value' => true],
            [['created_at', 'updated_at'], 'safe'],
            [['content'], 'string', 'max' => 2000],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['proposal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proposal::class, 'targetAttribute' => ['proposal_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'creator_id' => Yii::t('app', 'Creator ID'),
            'proposal_id' => Yii::t('app', 'Proposal ID'),
            'content' => Yii::t('app', 'Content'),
            'committee_only' => Yii::t('app', 'Committee Only'),
        ];
    }

    /**
     * @return ActiveQuery  List with User record of the proposal comment creator
     */
    public function getCreator() : ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'creator_id']);
    }

    /**
     * @return ActiveQuery  List with Proposal record which this comment belongs to
     */
    public function getProposal() : ActiveQuery
    {
        return $this->hasOne(Proposal::class, ['id' => 'proposal_id']);
    }
}
