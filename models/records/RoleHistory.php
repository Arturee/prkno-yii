<?php
namespace app\models\records;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "role_history".
 * Table stores user role values for particular time.
 * Current user role is the one with the greatest
 * creation time for given user.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $user_id           User id
 * @property string $role           User role. Allowed values: 'student', 'academic', 'committee_member', 'committee_chair'.
 * @property string $comment        Optional comment
 *
 * @property User $id0              Getter generated from getId0 method
 */
class RoleHistory extends OrmRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['deleted'], 'boolean'],
            [['role'], 'in', 'range' => User::ROLES ],
            [['comment'], 'string', 'max' => 200],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'user_id' => Yii::t('app', 'User ID'),
            'role' => Yii::t('app', 'Role'),
            'comment' => Yii::t('app', 'Comment')
        ];
    }

    /**
     * @return ActiveQuery  Get User record
     */
    public function getId0() : ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'id']);
    }
}
