<?php
namespace app\models\records;

use app\cas\CasInfo;
use app\consts\Lang;
use app\consts\Param;
use app\consts\Sql;
use app\exceptions\NotImplementedError;
use app\utils\DateTime;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 * Table stores all users.
 *
 * @property int $id                    Record id
 * @property string $created_at         Datetime of the record creation
 * @property string $updated_at         Datetime of the last record update
 * @property int $deleted               True if the record is deleted, false otherwise
 * @property string $name               First name and the last name
 * @property string $degree_prefix      Degree before the name
 * @property string $degree_suffix      Degree after the name
 * @property string $email              E-mail address
 * @property resource $password         Password hash
 * @property string $news_subscription  True/false whether the user turn on e-mail notification for news. If the value is set to true and new news is created, user receives a notification to e-mail about it.
 * @property string $last_login         Datetime of the last login
 * @property string $custom_url         Url for personal web page of the person, or null (optional).
 * @property string $last_CAS_refresh   Datetime when the info of the user was actualized by CAS last time, or null, if never.
 * @property string $role               User role. Allowed values: 'student', 'academic', 'committee_member', 'committee_chair'.
 * @property string $role_expiration    Time when user role stops to be valid and the user will loose the option for login into the system.
 * @property int $role_is_from_CAS      True whether the user role was extracted from CAS server (for debugging purposes).
 * @property string $login_type         Type of the login. User can log into the system by CAS or with credentials. Allowed values: 'cas', 'manual'.
 * @property string $password_recovery_token    If the user forgot the password, password recovery token will be generated if he ask for it. This token is used for the user for the password reset.
 * @property string $password_recovery_token_expiration     Date until which the recovery token is valid and can be used for password reset.
 * @property int $banned                True whether the user is banned or not. If so, log in is not allowed.
 * @property string $auth_key           Value used for user authentication by cookie.
 *
 * @property AdvertisementComment[] $adComments         Getter generated from getAdComments method
 * @property Advertisement[] $advertisements            Getter generated from getAdvertisements method
 * @property DefenceAttendance[] $defenceAttendances    Getter generated from getDefenceAttendances method
 * @property News[] $news                               Getter generated from getNews method
 * @property PageVersion[] $pageVersions                Getter generated from getPageVersions method
 * @property ProjectDefence[] $projectDefences          Getter generated from getProjectDefences method
 * @property ProjectDefence[] $projectDefences0         Getter generated from getProjectDefnces0 method
 * @property ProposalComment[] $proposalComments        Getter generated from getProposalComments method
 * @property ProposalComment[] $proposalComments0       Getter generated from getProposalComments0 method
 * @property ProposalVote[] $proposalVotes              Getter generated from getProposalVotes method
 * @property RoleHistory $roleHistory                   Getter generated from getRoleHistory method
 * @property ProjectUser[] $usersOnProjects             Getter generated from getUsersOnProjects method
 */
class User extends OrmRecord implements IdentityInterface
{
    const
        ROLE_GUEST = null,
        ROLE_STUDENT = 'student',
        ROLE_ACADEMIC = 'academic',
        ROLE_COMMITTEE_MEMBER = 'committee_member',
        ROLE_COMMITTEE_CHAIR = 'committee_chair',

        LOGIN_TYPE_CAS = 'cas',
        LOGIN_TYPE_MANUAL = 'manual';

    /** @return array Roles in committee */
    const COMMITTEE_ROLES = [self::ROLE_COMMITTEE_MEMBER, self::ROLE_COMMITTEE_CHAIR];

    /** @return array All roles */
    const ROLES = [self::ROLE_GUEST, self::ROLE_STUDENT, self::ROLE_ACADEMIC, self::ROLE_COMMITTEE_MEMBER, self::ROLE_COMMITTEE_CHAIR];

    /** @var array Attibures with date format */
    const DATETIME_ATTRIBUTES = ['created_at', 'updated_at', 'last_login', 'last_CAS_refresh', 'password_recovery_token_expiration', 'role_expiration'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name'], 'required'],
            [['created_at', 'updated_at', 'last_login', 'last_CAS_refresh', 'role_expiration', 'password_recovery_token_expiration'], 'safe'],
            [['deleted', 'role_is_from_CAS', 'banned'], 'boolean'],
            [['deleted', 'role_is_from_CAS', 'banned'], 'default', 'value' => false],
            [['role', 'login_type'], 'string'],
            [['news_subscription'], 'boolean'],
            [['name', 'password_recovery_token'], 'string', 'min' => 3, 'max' => 100],
            [['degree_prefix', 'degree_suffix'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 200],
            [['password'], 'string', 'min' => 8, 'max' => 200],
            [['custom_url'], 'string', 'max' => 500],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['email'], 'email'],
            ['role', 'in', 'range' => self::ROLES],
            ['login_type', 'in', 'range' => [self::LOGIN_TYPE_CAS, self::LOGIN_TYPE_MANUAL]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'name' => Yii::t('app', 'Name'),
            'degree_prefix' => Yii::t('app', 'Degree Prefix'),
            'degree_suffix' => Yii::t('app', 'Degree Suffix'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'news_subscription' => Yii::t('app', 'News Subscription'),
            'last_login' => Yii::t('app', 'Last Login'),
            'custom_url' => Yii::t('app', 'Custom Url'),
            'last_CAS_refresh' => Yii::t('app', 'Last Cas Refresh'),
            'role' => Yii::t('app', 'Role'),
            'role_expiration' => Yii::t('app', 'Role Expiration'),
            'role_is_from_CAS' => Yii::t('app', 'Role Is From Cas'),
            'login_type' => Yii::t('app', 'Login Type'),
            'password_recovery_token' => Yii::t('app', 'Password Recovery Token'),
            'password_recovery_token_expiration' => Yii::t('app', 'Password Recovery Token Expiration'),
            'banned' => Yii::t('app', 'Banned'),
        ];
    }

    /**
     * @return ActiveQuery  List of Advertisements comments of the user
     */
    public function getAdComments() : ActiveQuery
    {
        return $this->hasMany(AdvertisementComment::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of Advertisements the user is author
     */
    public function getAdvertisements() : ActiveQuery
    {
        return $this->hasMany(Advertisement::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of DefenceAttendance records for this user
     */
    public function getDefenceAttendances() : ActiveQuery
    {
        return $this->hasMany(DefenceAttendance::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of new this user is author
     */
    public function getNews() : ActiveQuery
    {
        return $this->hasMany(News::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of PageVersion records user is author
     */
    public function getPageVersions() : ActiveQuery
    {
        return $this->hasMany(PageVersion::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of ProjectDefence records the user is opponent
     */
    public function getDefences() : ActiveQuery
    {
        return $this->hasMany(ProjectDefence::class, ['opponent_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of ProjectDefence records the user is author of the final statement
     */
    public function getDefences0() : ActiveQuery
    {
        return $this->hasMany(ProjectDefence::class, ['statement_author_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of ProposalComment records the user is author
     */
    public function getProposalComments() : ActiveQuery
    {
        return $this->hasMany(ProposalComment::class, ['creator_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of ProposalComment records the user is creator
     */
    public function getProposalComments0() : ActiveQuery
    {
        return $this->hasMany(ProposalComment::class, ['creator_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of ProposalVote records of the user
     */
    public function getProposalVotes() : ActiveQuery
    {
        return $this->hasMany(ProposalVote::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of RoleHistory records of the user
     */
    public function getRoleHistory() : ActiveQuery
    {
        return $this->hasOne(RoleHistory::class, ['id' => 'id']);
    }

    /**
     * @return ActiveQuery  List of ProjectUser records of the user
     */
    public function getUsersOnProjects() : ActiveQuery
    {
        return $this->hasMany(ProjectUser::class, ['user_id' => 'id']);
    }

    /**
     * @return User|null  Get one user with given role, or null if not found
     */
    public static function findByRole(string $role) : ? User
    {
        return static::findOne(['role' => $role, 'deleted' => false]);
    }

    /**
     * @param int|string $id    User id
     * @return User|null Non deleted user with given Id or null if user does not exists
     */
    public static function findIdentity($id) : ? User
    {
        return static::findOne(['id' => $id, 'deleted' => false]);
    }

    /**
     * @param string $email     Email
     * @return User|null Non deleted user with given email or null if user does not exists
     */
    public static function findByEmail(string $email) : ? User
    {
        return static::findOne(['email' => $email, 'deleted' => false]);
    }

    /**
     * @param string $token Password recovery token
     * @return User|null    Get user with password recovery token or null if user does not exists
     */
    public static function findByPasswordRecoveryToken(string $token) : ? User
    {
        if (!static::isPasswordRecoveryTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_recovery_token' => $token,
            'deleted' => false,
            'banned' => false,
        ]);
    }

    /**
     * @param string $token     Password recovery token
     * @return bool             True if the password recovery token has valid format
     */
    public static function isPasswordRecoveryTokenValid(? string $token) : bool
    {
        if ($token == null || empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params[Param::PASSWORD_RECOVERY_TOKEN_EXPIRATION];

        return $timestamp + $expire >= time();
    }

    /**
     * @throws NotImplementedError
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotImplementedError();
    }

    /**
     * @inheritdoc
     */
    public function getId() : int
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() : string
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) : bool
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) : bool
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password
     * @throws Exception on bad password parameter or cost parameter.
     */
    public function setPassword(string $password) : void
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates new password recovery token
     * @throws Exception on failure.
     */
    public function generatePasswordRecoveryToken() : void
    {
        $this->password_recovery_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password recovery token
     */
    public function removePasswordRecoveryToken() : void
    {
        $this->password_recovery_token = null;
    }

    /**
     * Set password before saving the data
     * @param bool $insert  whether this method called while inserting a record.
     * If `false`, it means the method is called while updating a record.
     * @return bool Value returned from parent call
     * @throws Exception on bad password parameter or cost parameter.
     */
    public function beforeSave($insert) : bool
    {
        if (isset($this->password)) {
            $wasPasswordChanged = sizeof($this->getDirtyAttributes(['password']));
            if ($wasPasswordChanged) {
                $this->setPassword($this->password);
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return string   Full name with prefix and postfix academic degrees
     */
    public function getFullNameWithDegrees() : string
    {
        $result = $this->name;
        if ($this->degree_prefix) {
            $result = $this->degree_prefix . ' ' . $result;
        }
        if ($this->degree_suffix) {
            $result = $result . ', ' . $this->degree_suffix;
        }
        return trim($result);
    }

    public function getFullNameWithEmail() : string
    {
        $result = $this->getFullNameWithDegrees();
        $result .= " ($this->email)";
        return $result;
    }

    public function getNameWithEmail() : string
    {
        $result = $this->name;
        $result .= " ($this->email)";
        return $result;
    }

    /**
     * Generate default role expiration for the user. Role is expired
     * at least 9 months from now rounded up to the and of the
     * academic year, which is on 30th of September.
     * @return DateTime     Generate default date for role expiration
     */
    public static function defaultRoleExpiration() : DateTime
    {
        $date = new DateTime();
        $year = $date->format('Y');
        $month = $date->format('m');
        if ($month > 9) {
            $year++;
        }
        $endOfAcademicYear = '-09-30';
        $result = new DateTime($year . $endOfAcademicYear);
        return $result;
    }

    /**
     * @param null|string $role     Role enum value
     * @return string Role name
     */
    public static function roleToString(? string $role) : string
    {
        switch ($role) {
            case User::ROLE_STUDENT:
                return 'Student';
            case User::ROLE_ACADEMIC:
                return 'Academic';
            case User::ROLE_COMMITTEE_MEMBER:
                return 'Committee member';
            case User::ROLE_COMMITTEE_CHAIR:
                return 'Committee chair';
            case null:
                return 'No role';
        }
        return "";
    }

    /**
     * Get array with enum role => label pairs
     * @return array Roles
     */
    public static function getRoleMap() : array
    {
        return [
            null => Yii::t('app', 'No role'),
            User::ROLE_STUDENT => Yii::t('app', User::roleToString(User::ROLE_STUDENT)),
            User::ROLE_ACADEMIC => Yii::t('app', User::roleToString(User::ROLE_ACADEMIC)),
            User::ROLE_COMMITTEE_MEMBER => Yii::t('app', User::roleToString(User::ROLE_COMMITTEE_MEMBER)),
            User::ROLE_COMMITTEE_CHAIR => Yii::t('app', User::roleToString(User::ROLE_COMMITTEE_CHAIR))
        ];
    }

    /**
     * Map {0, 1} to translated values {'No', 'Yes'} in
     * users language.
     * @param int $value    0 for No, 1 for Yes
     * @return string Mapped value of 0,1 in string
     */
    public static function mapBool(int $value) : string
    {
        return self::getBoolMap()[$value];
    }

    /**
     * @return array Map of mapped values {0, 1} to translated values {'No', 'Yes'}.
     */
    public static function getBoolMap() : array
    {
        return [
            0 => Yii::t('app', 'No'),
            1 => Yii::t('app', 'Yes')
        ];
    }
    
    /**
     * Load info from CAS about the user
     * @param CasInfo $info     CAS data loaded for downloading user data
     */
    public function loadCasInfo(CasInfo $info) : void
    {
        $this->email = $info->getEmail();
        $this->name = $info->getFullName();
        $language = $info->getLanguage();
        if (!$language) {
            $language = Lang::CS;
        }
        $this->news_subscription = 0;
        $this->login_type = User::LOGIN_TYPE_CAS;
        $this->loadRole($info);
    }

    /**
     * Load role from cas and store it in this User instance.
     * @param CasInfo $info     CAS data loaded for downloading user data
     */
    public function loadRole(CasInfo $info) : void
    {
        $this->role = $info->getRole();
        $this->role_is_from_CAS = true;
        $this->role_expiration = self::defaultRoleExpiration();
        $this->last_CAS_refresh = new DateTime();
    }

    /**
     * @return User[]   List of all User records, which are subscribed for news
     */
    public static function findNewsSubscribedUsers() : array
    {
        return self::find()
            ->where(Sql::CONDITION_NOT_DELETED)
            ->andWhere(["news_subscription" => 1])
            ->all();
    }

    /**
     * @return Users[]  List of all User records, which are in committee
     */
    public static function findCommitteeMembers() : array
    {
        return self::find()
            ->where(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['in', 'role', self::COMMITTEE_ROLES])
            ->orderBy('name')
            ->all();
    }

    /**
     * @return Users[]  List of all User records, which can be supervisors or consultants
     */
    public static function findPossibleSupervisorConsultant() : array
    {
        return self::find()
            ->where(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['in', 'role', [User::ROLE_ACADEMIC, User::ROLE_COMMITTEE_CHAIR, User::ROLE_COMMITTEE_MEMBER]])
            ->orderBy('name')
            ->all();
    }

    /**
     * @return array Users[]    List of all User records, which have committee chair role
     */
    public static function findCommitteeChair() : array
    {
        return self::find()
            ->where(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['role' => self::ROLE_COMMITTEE_CHAIR])
            ->all();
    }

    /**
     * Anonymize user. All personal user data will be replaces
     * with default data.
     */
    public function anonymize() : void
    {
        $this->name = "Anonymus";
        $this->degree_prefix = null;
        $this->degree_suffix = null;

        $this->email = 'anonymus-' . $this->id . "@anonymus.anonymus";

        $this->password = null;
        $this->password_recovery_token = null;

        $this->custom_url = null;

        $this->last_login = null;

        $this->save();
    }

    /**
     * @return bool True whether the user is anonymized, false if not
     */
    public function isAnonymized() : bool
    {
        return $this->last_login == null;
    }
}
