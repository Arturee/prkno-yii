<?php
namespace app\models\records;

use app\utils\DateTime;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "news".
 * Table news stores records about the news. News represents
 * important information about upcomming events, rule
 * changes, upcomming deadlines, etc. All news has to
 * have both English and Czech versions.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $user_id           Id of the author
 * @property string $title_en       Title in English language
 * @property string $title_cs       Title in Czech language
 * @property int $content_en        Content in English language in markdown format
 * @property int $content_cs        Conetnt in Czech language in markdown format
 * @property int $public            True/false whether the news is published or it is in draft state and visible only for the creator.
 * @property string $type           Enum value for news type. Can have 3 values: 'generated', 'normal', 'important'.
 * @property string $hide_at        Datetime after which the news is not displayed anymore.
 * 
 * @property Document $contentCs    Getter generated from getContentCs method
 * @property Document $contentEn    Getter generated from getContentEn method
 * @property User $user             Getter generated from getUser method
 */
class News extends OrmRecord
{
    const
        TYPE_GENERATED = 'generated',
        TYPE_NORMAL = 'normal',
        TYPE_IMPORTANT = 'important';

    /** @var array Database columns with date type */
    public static $DATETIME_ATTRIBUTES = ['created_at', 'updated_at', 'hide_at'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['public', 'type', 'content_en', 'content_cs', 'title_en', 'title_cs'], 'required'],
            [['id', 'user_id'], 'integer'],
            [['created_at', 'updated_at', 'hide_at'], 'safe'],
            [['title_en', 'title_cs'], 'string', 'max' => 100],
            [['content_en', 'content_cs'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['public', 'deleted'], 'boolean'],
            ['type', 'in', 'range' => [self::TYPE_GENERATED, self::TYPE_NORMAL, self::TYPE_IMPORTANT]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'title_en' => Yii::t('app', 'Title En'),
            'title_cs' => Yii::t('app', 'Title Cs'),
            'content_en' => Yii::t('app', 'Content En'),
            'content_cs' => Yii::t('app', 'Content Cs'),
            'public' => Yii::t('app', 'Visibility'),
            'type' => Yii::t('app', 'Type'),
            'hide_at' => Yii::t('app', 'Hide At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return
            [
            'id',
            'titleEN' => 'title_en',
            'titleCS' => 'title_cs',
            'contentEN' => function ($model) {
                $content = $model->contentEn;
                return $content ? $content->content_md : null;
            },
            'contentCS' => function ($model) {
                $content = $model->contentCs;
                return $content ? $content->content_md : null;
            },
            'createdAt' => function ($model) {
                return DateTime::toJsonDateFormat($model->created_at);
            },
            'updatedAt' => function ($model) {
                return DateTime::toJsonDateFormat($model->updated_at);
            }
        ];
    }

    /**
     * @param string $ln    Language - en, cs, or if null, than app language is used
     * @return string       Local title in language from parameter
     */
    public function getLocalTitle(string $ln = null) : string
    {
        $lang = $ln ? $ln : substr(Yii::$app->language, 0, 2);
        $value = null;
        if ($lang == 'cs') {
            $value = $this->title_cs;
        }
        if ($lang == 'en') {
            $value = $this->title_en;
        }
        return $value ? $value : '(' . Yii::t('app', 'Missing title') . ')';
    }

    /**
     * @param string $ln    Language - en, cs, or if null, than app language is used
     * @return string       Local content
     */
    public function getLocalContent(string $ln = null) : string
    {
        $lang = $ln ? $ln : substr(Yii::$app->language, 0, 2);
        $value = null;
        if ($lang == 'cs') {
            $value = $this->content_cs;
        }
        if ($lang == 'en') {
            $value = $this->content_en;
        }
        return $value ? $value : '(' . Yii::t('app', 'Missing content') . ')';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity(int $id) : ? News
    {
        return static::findOne(['id' => $id, 'deleted' => false]);
    }

    /**
     * @return string Local content
     */
    public function getContentCs() : ActiveQuery
    {
        return $this->hasOne(Document::class, ['id' => 'content_cs']);
    }

    /**
     * @return string English content
     */
    public function getContentEn() : ActiveQuery
    {
        return $this->hasOne(Document::class, ['id' => 'content_en']);
    }

    /**
     * @return User Author of the news
     */
    public function getUser() : ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
