<?php
namespace app\models\records;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "project_user".
 * Project_user is a relational table associating users and project.
 * Records represents users on project like project members, opponent, supervisor or consultant. Each project user can have specific project role.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $user_id           User id
 * @property int $project_id        Project id
 * @property string $project_role   Role of the user on the project. Allowed values: 'team_member', 'supervisor', 'opponent', 'consultant'.
 * @property string $comment        Optional comment
 *
 * @property Project $project       Getter generated from getProject method
 * @property User $user             Getter generated from getUser method
 */
class ProjectUser extends OrmRecord
{
    const
        ROLE_TEAM_MEMBER = 'team_member',
        ROLE_SUPERVISOR = 'supervisor',
        ROLE_OPPONENT = 'opponent',
        ROLE_CONSULTANT = 'consultant';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'project_id', 'project_role'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'project_id'], 'integer'],
            [['deleted'], 'boolean'],
            [['project_role'], 'in', 'range' => self::allProjectRoles() ],
            [['comment'], 'string', 'max' => 500],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'user_id' => Yii::t('app', 'User ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'project_role' => Yii::t('app', 'Project Role'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    /**
     * @return string[] Get array of all project roles
     */
    public static function allProjectRoles(): array
    {
        return [
            self::ROLE_TEAM_MEMBER,
            self::ROLE_SUPERVISOR,
            self::ROLE_OPPONENT,
            self::ROLE_CONSULTANT
        ];
    }

    /**
     * @return Project Get project
     */
    public function getProject(): ActiveQuery
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return User Get user
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return ProjectUser[] Array of with all ProjectUser instance which have a supervisor role
     */
    public static function getAllSupervisors(): array
    {
		return self::find()
		    ->joinWith('user')
		    ->where(['project_role' => self::ROLE_SUPERVISOR])
		    ->all();
    }
}
