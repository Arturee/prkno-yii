<?php
namespace app\models\records;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "page_version".
 * Table page_version stores different version of a particular page.
 * One example of the page can be project rules. Those
 * can be changed over time, but different versions can
 * be valid for different projects base on time when
 * those rules were valid. Therefore several versions
 * of a page have to be stored.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $page_id           Page id
 * @property int $user_id           Author id
 * @property int $content_en        Content in English in markdown format
 * @property int $content_cs        Content in Czech in markdown format
 * @property int $published         True/false whether the page version is visible for all users, or if it is in draw state visible only for the creator.
 *
 * @property Document $contentCs    Getter generated from getContentCs method
 * @property Document $contentEn    Getter generated from getContentEn method
 * @property Page $page             Getter generated from getPage method
 * @property User $user             Getter generated from getUser method
 */
class PageVersion extends OrmRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_version';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'user_id', 'published'], 'required'],
            [['id', 'page_id', 'user_id', 'content_en', 'content_cs'], 'integer'],
            [['published', 'deleted'], 'boolean'],
            [['created_at', 'updated_at', 'id'], 'safe'],
            [['content_cs'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['content_cs' => 'id']],
            [['content_en'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['content_en' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::class, 'targetAttribute' => ['page_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_id' => Yii::t('app', 'Page ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'content_en' => Yii::t('app', 'Content En'),
            'content_cs' => Yii::t('app', 'Content Cs'),
            'published' => Yii::t('app', 'Published'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return ActiveQuery  Get content cs as activequery
     */
    public function getContentCs() : ActiveQuery
    {
        return $this->hasOne(Document::class, ['id' => 'content_cs']);
    }

    /**
     * @return ActiveQuery  Get content en as activequery
     */
    public function getContentEn() : ActiveQuery
    {
        return $this->hasOne(Document::class, ['id' => 'content_en']);
    }

    /**
     * @return ActiveQuery  Get content cs as activequery
     */
    public function getPage() : ActiveQuery
    {
        return $this->hasOne(Page::class, ['id' => 'page_id']);
    }

    /**
     * @return ActiveQuery  Get creator of the page version
     */
    public function getUser() : ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return bool     True whether this page version is the last from page
     *                  versions of the page it belongs
     */
    public function isLastCreatedPageVersion() : bool
    {
        $lastPageVersion = $this->page->getLastPageVersion();
        return $lastPageVersion->id == $this->id;
    }

    /**
     * @return Document|null     Document of the page version in users language
     */
    public function getDocumentBasedOnSiteLanguage() : ? Document
    {
        $isSiteLanguageCzech = Yii::$app->language == 'cs-CZ';

        if ($isSiteLanguageCzech) {
            return $this->getCorrectDocumentWithBackup($this->contentCs, $this->contentEn);
        } else {
            return $this->getCorrectDocumentWithBackup($this->contentEn, $this->contentCs);
        }
    }

    private function getContentWithBackup(Document $contentDocument, ? Document $backupContentDocument)
    {
        $content = $contentDocument->content_md;
        return trim($content) === "" ? $backupContentDocument : $contentDocument;
    }

    private function getCorrectDocumentWithBackup(? Document $mainDocument, ? Document $backupDocument)
    {
        if ($mainDocument) {
            return $this->getContentWithBackup($mainDocument, $backupDocument);
        } else if ($backupDocument) {
            return $this->getContentWithBackup($backupDocument, null);
        } else {
            return null;
        }
    }
}
