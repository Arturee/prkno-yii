<?php
namespace app\models\records;

use app\consts\Sql;
use app\utils\DateTime;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Markdown;
use yii\helpers\Html;

/**
 * This is the model class for table "advertisement".
 * Table advertisements represents set of records providing
 * information about students looking for a new projects
 * or about some project ideas looking for a students.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $user_id           Id of the creator of the advertisement or the user who created the advertisement
 * @property string $title          Title of the advertisement
 * @property int $description       Description of what is student looking for or a brief description of the project concept in case of project type
 * @property string $type           Type of advertisement. Is represented by enum and can have 'project' or 'person' values.
 * @property string $keywords       Keywords used for search separated by comma
 * @property int $subscribed_via_email  Boolean representing whether the author has to be notified to his e-mail in case of response from another user.
 * @property int $published         True if the advertisement is visible for other users, or false in case of draft (visibility only for the author).
 *
 * @property AdvertisementComment[] $adComments     Getter generated from getAdComments method
 * @property Document $document                     Getter generated from getDocument method
 * @property User $user                             Getter generated from getUser method
 */
class Advertisement extends OrmRecord
{
    const
        TYPE_PERSON = 'person',
        TYPE_PROJECT = 'project';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertisement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'subscribed_via_email', 'published', 'description', 'type'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'subscribed_via_email', 'published'], 'integer'],
            [['subscribed_via_email', 'published', 'deleted'], 'boolean'],
            [['type', 'description'], 'string'],
            [['title'], 'string', 'max' => 200],
            [['keywords'], 'string', 'max' => 500],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['type', 'in', 'range' => [self::TYPE_PROJECT, self::TYPE_PERSON]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'user_id' => Yii::t('app', 'User ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Content'),
            'type' => Yii::t('app', 'Type'),
            'keywords' => Yii::t('app', 'Keywords'),
            'subscribed_via_email' => Yii::t('app', 'Subscribed Responses Via Email'),
            'published' => Yii::t('app', 'Publish'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        //user_id is required so we don't have to check for nulls
        return
            [
            'id',
            'title',
            'authorName' => function ($model) {
                return $model->user->getFullNameWithDegrees();
            },
            'authorEmail' => function ($model) {
                return $model->user->email;
            },
            'type',
            'keywordsCSV' => function ($model) {
                return $this->convertToCSV($model->keywords);
            },
            'description' => function ($model) {
                return $model->description;
            },
            'createdAt' => function ($model) {
                return DateTime::toJsonDateFormat($model->created_at);
            },
            'updatedAt' => function ($model) {
                return DateTime::toJsonDateFormat($model->updated_at);
            }
        ];
    }

    /**
     * Get all non deleted advertisement comments from the database
     * sorted in ascending order
     * @return ActiveQuery  List of AdvertisementComment
     */
    public function getAdComments() : ActiveQuery
    {
        return $this->hasMany(AdvertisementComment::class, ['ad_id' => 'id'])
            ->orderBy(['created_at' => SORT_ASC])
            ->andFilterWhere(Sql::CONDITION_NOT_DELETED);
    }

    /**
     * Return array of keywords from the advertisement
     * @return string[] List of keywords
     */
    public function getKeywordsList() : array
    {
        return explode(',', $this->keywords);
    }

    /**
     * Get author of the ad
     * @return User Author
     */
    public function getUser() : ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Get HTML content of the description written in markdown
     * @return string Get HTML of the markdown content
     */
    public function getContentMarkdownProcessed() : string
    {
        return Markdown::process(Html::encode($this->description));
    }

    /**
     * @inheritDoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $this->adjustKeywords();
        $result = parent::save($runValidation, $attributeNames);
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return "";
    }

    // Remove duplicities and trim each keyword
    private function adjustKeywords()
    {
        $keywordsList = explode(',', $this->keywords);
        $keywordsList = array_unique($keywordsList);
        $keywordsList = array_map('trim', $keywordsList);
        $this->keywords = implode(',', $keywordsList);
    }

    private function convertToCSV(string $textWithCommas) : string
    {
        $textArray = explode(",", $textWithCommas);
        $trimmedArray = array_map('trim', $textArray);
        return implode(';', $trimmedArray);
    }
}
