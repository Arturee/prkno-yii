<?php
namespace app\models\records;

use app\utils\DateTime;
use app\utils\MarkdownRenderer;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "document".
 * Document table stores document records. A document can have
 * 2 forms: markdown format, or as an uploaded file.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property string $content_md     Content in markdown format. If the fallback\_path attribute is defined, this attribute has a null value
 * @property string $fallback_path  Path to the uploaded file. If content\_md attribute is defined, this attribute has a null value
 * 
 * @property Advertisement[] $advertisements    Getter generated from getAdvertisements method
 * @property News[] $news                       Getter generated from getNews method
 * @property News[] $news0                      Getter generated from getNews0 method
 * @property PageVersion[] $pageVersions        Getter generated from getPageVersions method
 * @property PageVersion[] $pageVersions0       Getter generated from getPageVersions0 method
 * @property ProjectDefence[] $projectDefences  Getter generated from getProjectDefences method
 * @property ProjectDefence[] $projectDefences0 Getter generated from getProjectDefences0 method
 * @property ProjectDefence[] $projectDefences1 Getter generated from getProjectDefences1 method
 * @property ProjectDocument $projectDocument   Getter generated from getProjectDocument method
 * @property Project[] $projects                Getter generated from getProjects method
 * @property Proposal[] $proposals              Getter generated from getProposals method
 */
class Document extends OrmRecord
{
    const
        COL_FALLBACK_PATH = 'fallback_path';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['deleted'], 'boolean'],
            [['content_md'], 'string'],
            [['fallback_path'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'content_md' => Yii::t('app', 'Content Md'),
            'fallback_path' => Yii::t('app', 'Fallback Path'),
        ];
    }

    /**
     * @return ActiveQuery  Advertisement with this document as content
     */
    public function getAdvertisements() : ActiveQuery
    {
        return $this->hasMany(Advertisement::class, ['content' => 'id']);
    }

    /**
     * @return ActiveQuery  News with this documents as local content
     */
    public function getNews() : ActiveQuery
    {
        return $this->hasMany(News::class, ['content_cs' => 'id']);
    }

    /**
     * @return ActiveQuery  News with this documents as eng content
     */
    public function getNews0() : ActiveQuery
    {
        return $this->hasMany(News::class, ['content_en' => 'id']);
    }

    /**
     * @return ActiveQuery  Page version with this document as local content
     */
    public function getPageVersions() : ActiveQuery
    {
        return $this->hasMany(PageVersion::class, ['content_cs' => 'id']);
    }

    /**
     * @return ActiveQuery  Page version with this document as eng content
     */
    public function getPageVersions0() : ActiveQuery
    {
        return $this->hasMany(PageVersion::class, ['content_en' => 'id']);
    }

    /**
     * @return ActiveQuery  ProjectDefence with defence statement as this document
     */
    public function getDefences() : ActiveQuery
    {
        return $this->hasMany(ProjectDefence::class, ['defence_statement' => 'id']);
    }

    /**
     * @return ActiveQuery  ProjectDefence with opponent review as this document
     */
    public function getDefences0() : ActiveQuery
    {
        return $this->hasMany(ProjectDefence::class, ['opponent_review' => 'id']);
    }

    /**
     * @return ActiveQuery  ProjectDefence with supervisor review as this document
     */
    public function getDefences1() : ActiveQuery
    {
        return $this->hasMany(ProjectDefence::class, ['supervisor_review' => 'id']);
    }

    /**
     * @return ProjectDocument  ProjectDocument associated with this document
     */
    public function getProjectDocument() : ProjectDocument
    {
        return $this->hasOne(ProjectDocument::class, ['document_id' => 'id'])->limit(1)->one();
    }

    /**
     * @return ActiveQuery  Project with content as this document
     */
    public function getProjects() : ActiveQuery
    {
        return $this->hasMany(Project::class, ['content' => 'id']);
    }

    /**
     * @return ActiveQuery  Proposal associated with this document
     */
    public function getProposals() : ActiveQuery
    {
        return $this->hasMany(Proposal::class, ['document_id' => 'id']);
    }

    /**
     * @return string   HTML rendered document
     */
    public function getContentMarkdownProcessed() : string
    {
        return MarkdownRenderer::render(Html::encode($this->content_md));
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $this->updated_at = new DateTime();
        return parent::save($runValidation, $attributeNames);
    }
}
