<?php
namespace app\models\records;

use app\consts\Sql;
use app\utils\DateTime;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "defence_date".
 * Table stores record representing defence event.
 * Event will happend in one particular date. One or
 * more projects can be assigned for the defence date.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property string $date           Date (day, month, year). There can not be more than one defence date record for the same date.
 * @property string $signup_deadline Deadline for signing up the projects.
 * @property int $active_voting     Boolean saying whether the defence date is confirmed or not. If the defence date is not confirmed, it is still in a decision process, is not visible for other users except committee members, no project can be assigned for it yet and no room can be selected.
 *
 * @property DefenceAttendance[] $defenceAttendances    Getter generated from getDefenceAttendances method
 * @property ProjectDefence[] $projectDefences          Getter generated from getProjectDefences method
 */
class DefenceDate extends OrmRecord
{
    const
        COL_DATE = 'date',
        COL_SIGNUP_DEADLINE = 'signup_deadline',
        COL_ACTIVE_VOTING = 'active_voting',
        SCENARIO_CREATE = 'scenario.create';

    /** @var array Database columns with date type */
    public static $DATE_ATTRIBUTES = [self::COL_DATE, self::COL_SIGNUP_DEADLINE];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'defence_date';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[self::COL_DATE, self::COL_SIGNUP_DEADLINE], 'required'],
            [['created_at', 'updated_at', self::COL_DATE, self::COL_SIGNUP_DEADLINE], 'safe'],
            ['room', 'string', 'max' => 200],
            [self::$DATE_ATTRIBUTES, 'date', 'format' => 'php:Y-m-d'],
            [self::$DATE_ATTRIBUTES, 'validateDateIsInTheFuture', 'on' => self::SCENARIO_CREATE],
            [[self::COL_DATE], 'validateDateNoTaken', 'on' => self::SCENARIO_CREATE],
            [[self::COL_SIGNUP_DEADLINE], 'validateSignupDeadlineIsBeforeDate', 'on' => self::SCENARIO_CREATE],
            [['deleted', 'active_voting'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            self::COL_DATE => Yii::t('app', 'Date'),
            self::COL_SIGNUP_DEADLINE => Yii::t('app', 'Signup Deadline'),
            'active_voting' => Yii::t('app', 'Voting turned on'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = [self::COL_DATE, self::COL_SIGNUP_DEADLINE];
        return $scenarios;
    }

    /**
     * Check whether the attribute with date type is in future, is it not, add error.
     * @param string $attributeName     Attribute name of the defence date with date type
     */
    public function validateDateIsInTheFuture(string $attributeName) : void
    {
        $date = new DateTime($this->{$attributeName});
        $now = new DateTime();

        if ($date->isBefore($now)) {
            $this->addError($attributeName, Yii::t('app', 'Date cannot be in the past'));
        }
    }

    /**
     * Check whether the signup deadline is before date
     * of the defence, and if it is not, add error.
     */
    public function validateSignupDeadlineIsBeforeDate() : void
    {
        $date = new DateTime($this->date);
        $signupDeadline = new DateTime($this->signup_deadline);
        if ($date->isBefore($signupDeadline)) {
            $this->addError(
                self::COL_SIGNUP_DEADLINE,
                Yii::t('app', 'The deadline must be before the date.')
            );
        }
    }

    /**
     * Check whether the date already exists, if so, add error.
     */
    public function validateDateNoTaken() : void
    {
        $date = DateTime::from($this->date)->format(DateTime::SQL_DATE);
        $countSameDates = self::find()->where([Sql::LIKE, self::COL_DATE, $date])->andWhere(["deleted" => false])->count();
        if ($countSameDates > 0) {
            $this->addError(
                self::COL_DATE,
                Yii::t('app', 'Date already exists.')
            );
        }
    }

    /**
     * @return ActiveQuery      List of defence attendance objects for the defence date
     */
    public function getDefenceAttendances() : ActiveQuery
    {
        return $this->hasMany(DefenceAttendance::class, ['defence_date_id' => 'id'])
            ->andOnCondition(Sql::CONDITION_NOT_DELETED);
    }

    /**
     * @return ActiveQuery  List of project defences assigned for this defence date
     */
    public function getProjectDefences() : ActiveQuery
    {
        return $this->hasMany(ProjectDefence::class, ['defence_date_id' => 'id'])
            ->where(Sql::CONDITION_NOT_DELETED);
    }

    /**
     * @return int  Number of votes from defence attendances
     */
    public function getCountVotes(string $vote) : int
    {
        return $this->getDefenceAttendances()
            ->andFilterWhere([DefenceAttendance::COL_VOTE => $vote])
            ->count();
    }

    /**
     * @return DefenceAttendance    Defence attendance object by user id from this defence date
     */
    public function getDefenceAttendanceByUserId(int $userId) : DefenceAttendance
    {
        $attendance = DefenceAttendance::findOne([
            'defence_date_id' => $this->id,
            'user_id' => $userId,
            'deleted' => false
        ]);
        if (!$attendance) {
            $attendance = new DefenceAttendance();
            $attendance->vote = DefenceAttendance::VOTE_UNKNOWN;
            $attendance->must_come = false;
        }
        return $attendance;
    }

    /**
     * @return ActiveQuery  Get all defence dates, which was not confirmed yet
     */
    public static function getVotingDates() : ActiveQuery
    {
        return DefenceDate::find()
            ->filterWhere(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['>=', 'date', Sql::now()])
            ->andWhere(['active_voting' => 1])
            ->orderBy(DefenceDate::COL_DATE);
    }

    /**
     * @return ActiveQuery  Get all defence dates, which was confirmed but
     *                      not happened yet
     */
    public static function getFutureDates() : ActiveQuery
    {
        return DefenceDate::find()
            ->filterWhere(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['>=', 'date', Sql::now()])
            ->andWhere(['active_voting' => 0])
            ->orderBy([DefenceDate::COL_DATE => SORT_DESC]);
    }

    /**
     * @return ActiveQuery  Get all confirmed defence dates with signup_deadline in a future
     *                      in descending order by date
     */
    public static function getSignUpDates() : ActiveQuery
    {
        return DefenceDate::find()
            ->filterWhere(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['>=', 'date', Sql::now()])
            ->andWhere(['>=', 'signup_deadline', Sql::now()])
            ->andWhere(['active_voting' => 0])
            ->orderBy([DefenceDate::COL_DATE => SORT_DESC]);
    }

    /**
     * @return ActiveQuery  Get all non deleted defence date from pass
     *                      in descending order by date
     */
    public static function getPastDates() : ActiveQuery
    {
        return DefenceDate::find()
            ->filterWhere(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['<', 'date', Sql::now()])
            ->orderBy([DefenceDate::COL_DATE => SORT_DESC]);
    }

    /**
     * @return User[]   Get all committee members from defence attendances associated with defence date
     */
    public function getCommittee() : array
    {
        $attendances = $this->defenceAttendances;
        $committee = [];
        foreach ($attendances as $attendance) {
            if ($attendance->must_come && $attendance->user) {
                $committee[] = $attendance->user;
            }
        }
        return $committee;
    }

    /**
     * Remove defence dates, assigned project defences and defence attendances
     */
    public function cancel() : void
    {
        $defences = $this->projectDefences;
        foreach ($defences as $defence) {
            $defence->cancel();
        }
        $attendances = $this->defenceAttendances;
        foreach ($attendances as $attendance) {
            $attendance->cancel();
        }

        parent::cancel();
    }
}
