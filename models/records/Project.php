<?php
namespace app\models\records;

use app\behaviors\TimestampBehavior;
use app\consts\Param;
use app\consts\Sql;
use app\models\DefenceDocument;
use app\utils\DateTime;
use app\utils\storage\MarkdownTemplateStorage;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\Markdown;
use yii2tech\ar\linkmany\LinkManyBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "project".
 * Table project store data about individual software projects.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property string $state          Enum arrtibute storing project state. There can have values: 'proposal', 'accepted', 'analysis', 'analysis_handed_in', 'implementation', 'implementation_handed_in', 'conditionally_defended', 'conditional_documents_handed_in', 'defended', 'failed', 'canceled'.
 * @property string $run_start_date Date when the project was started
 * @property string $deadline       Deadline for the project
 * @property string $name           Name of the project
 * @property string $short_name     Short name (acronym)
 * @property string $description    Short description
 * @property int $content           Long description
 * @property int $waiting_for_proposal_evaluation   True if the project is in state 'proposal', project proposal was uploaded sent for evaluation and is waiting for approval or denial from committee members.
 * @property int $requested_to_run  True if the project was requested to run and waiting for confirmation bt committee admin.
 * @property int $extra_credits     Number of bonus credits after the project was defended.
 *
 * @property ProjectDefence[] $projectDefences          Getter generated from getProjectDefence method
 * @property ProjectDocument[] $projectDocuments        Getter generated from getProjectDocument method
 * @property ProjectDocument[] $privateDocuments        Getter generated from getPrivateDocuments method
 * @property ProjectDocument[] $publicDocuments         Getter generated from getPublicDocuments method
 * @property ProjectDocument $adviceDocument            Getter generated from getAdviceDocument method
 * @property ProjectStateHistory[] $projectStateHistories   Getter generated from getProjectStateHistories method
 * @property ProjectStateHistory $lastState             Getter generated from getLastState method
 * @property Document $document                         Getter generated from getDocument method
 * @property Proposal[] $proposals                      Getter generated from getProposals method
 * @property ProjectUser[] $usersOnProjects             Getter generated from getUsersOnProjects method
 * @property User[]|array consultants                   Getter generated from getConsultants method
 * @property User[]|array supervisor                    Getter generated from getSupervisor method
 * @property User[]|array members                       Getter generated from getMembers method
 * @property User[]|array opponents                     Getter generated from getOpponents method
 */
class Project extends OrmRecord
{
    const
        STATE_PROPOSAL = 'proposal',
        STATE_ACCEPTED = 'accepted',
        STATE_ANALYSIS = 'analysis',
        STATE_ANALYSIS_HANDED_IN = 'analysis_handed_in',
        STATE_IMPLEMENTATION = 'implementation',
        STATE_IMPLEMENTATION_HANDED_IN = 'implementation_handed_in',
        STATE_CONDITIONALLY_DEFENDED = 'conditionally_defended',
        STATE_CONDITIONAL_DOCUMENTS_HANDED_IN = 'conditional_documents_handed_in',
        STATE_DEFENDED = 'defended',
        STATE_FAILED = 'failed',
        STATE_CANCELED = 'canceled',

        SCENARIO_CREATE = 'create',
        SCENARIO_DRAFT = 'draft',
        SCENARIO_SEND = 'send',
        SCENARIO_STATUS_UPDATE = 'statusupdate',

        COL_DEADLINE = 'deadline';

    /** @var array attribute names with date format */
    public static $DATETIME_ATTRIBUTES = ['created_at', 'updated_at', 'run_start_date', 'deadline'];

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['name', 'short_name', 'description', 'contentMarkdown', 'consultantIds', 'supervisorIds', 'memberIds'];
        $scenarios[self::SCENARIO_DRAFT] = ['name', 'short_name', 'description', 'contentMarkdown', 'consultantIds', 'supervisorIds', 'memberIds', 'start_run_date', 'deadline'];
        $scenarios[self::SCENARIO_SEND] = ['name', 'short_name', 'description', 'contentMarkdown', 'consultantIds', 'supervisorIds', 'memberIds'];
        $scenarios[self::SCENARIO_STATUS_UPDATE] = ['state'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::class],
            'linkConsultantsBehavior' => [
                'class' => LinkManyBehavior::class,
                'relation' => 'consultants',
                'relationReferenceAttribute' => 'consultantIds',
                'extraColumns' => [
                    'created_at' => new Expression('NOW()'),
                    'updated_at' => new Expression('NOW()'),
                    'project_role' => ProjectUser::ROLE_CONSULTANT,
                ],
                'deleteOnUnlink' => true,
            ],
            'linkSupervisorBehavior' => [
                'class' => LinkManyBehavior::class,
                'relation' => 'supervisor',
                'relationReferenceAttribute' => 'supervisorIds',
                'extraColumns' => [
                    'created_at' => new Expression('NOW()'),
                    'updated_at' => new Expression('NOW()'),
                    'project_role' => ProjectUser::ROLE_SUPERVISOR,
                ],
                'deleteOnUnlink' => true,
            ],
            'linkMembersBehavior' => [
                'class' => LinkManyBehavior::class,
                'relation' => 'members',
                'relationReferenceAttribute' => 'memberIds',
                'extraColumns' => [
                    'created_at' => new Expression('NOW()'),
                    'updated_at' => new Expression('NOW()'),
                    'project_role' => ProjectUser::ROLE_TEAM_MEMBER,
                ],
                'deleteOnUnlink' => true,
            ],
            'linkOpponentsBehavior' => [
                'class' => LinkManyBehavior::class,
                'relation' => 'opponents',
                'relationReferenceAttribute' => 'opponentIds',
                'extraColumns' => [
                    'created_at' => new Expression('NOW()'),
                    'updated_at' => new Expression('NOW()'),
                    'project_role' => ProjectUser::ROLE_OPPONENT,
                ],
                'deleteOnUnlink' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_name'], 'required'],
            [['updated_at', 'created_at', 'run_start_date', 'deadline', 'consultants', 'consultantIds', 'supervisor', 'supervisorIds', 'members', 'memberIds', 'opponents', 'opponentIds', 'contentMarkdown', 'adviceMarkdown'], 'safe'],
            [['content', 'extra_credits'], 'integer'],
            [['deleted', 'waiting_for_proposal_evaluation', 'requested_to_run', 'team_members_visible'], 'boolean'],
            [['state'], 'in', 'range' => self::allStates()],
            [['name', 'keywords'], 'string', 'max' => 500],
            [['short_name'], 'string', 'max' => 45],
            [['short_name'], 'unique'],
            [['description'], 'string', 'max' => 2000],
            [['content'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['content' => 'id']],
            [['name', 'short_name', 'description', 'supervisorIds'], 'required', 'on' => self::SCENARIO_CREATE],
            [['name', 'short_name', 'description', 'supervisorIds'], 'required', 'on' => self::SCENARIO_DRAFT],
            [['name', 'short_name', 'description', 'supervisorIds'], 'required', 'on' => self::SCENARIO_SEND],
            [['state'], 'required', 'on' => self::SCENARIO_STATUS_UPDATE],
            [['run_start_date', 'deadline'], 'validateDeadline'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'state' => Yii::t('app', 'State'),
            'run_start_date' => Yii::t('app', 'Start Date'),
            'deadline' => Yii::t('app', 'Deadline'),
            'name' => Yii::t('app', 'Project name'),
            'short_name' => Yii::t('app', 'Short Name'),
            'description' => Yii::t('app', 'Description'),
            'content' => Yii::t('app', 'Content'),
            'contentMarkdown' => Yii::t('app', 'Markdown'),
            'adviceMarkdown' => Yii::t('app', 'Markdown'),
            'waiting_for_proposal_evaluation' => Yii::t('app', 'Waiting For Proposal Evaluation'),
            'requested_to_run' => Yii::t('app', 'Requested To Run'),
            'extra_credits' => Yii::t('app', 'Extra Credits'),
            'team_members_visible' => Yii::t('app', 'Team Members Visible'),
            'keywords' => Yii::t('app', 'Keyword'),
            'supervisorIds' => Yii::t('app', 'Supervisor'),
            'consultantIds' => Yii::t('app', 'Consultants'),
            'memberIds' => Yii::t('app', 'Members'),
            'opponentIds' => Yii::t('app', 'Opponents'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return
            [
            'id',
            'name',
            'shortName' => 'short_name',
            'description',
            'keywords',
            'team' => function ($model) {
                if (!$model->team_members_visible) {
                    return "HIDDEN";
                }
                $result = $this->concatenateParticipantNames($model->members);
                return $result ? $result : "";
            },
            'supervisor' => function (Project $model) {
                /**
                 * @var $supervisor User
                 */
                $supervisor = $model->supervisors;
                if (sizeof($supervisor) > 0) {
                    return $supervisor[0]->getFullNameWithDegrees();
                } else {
                    return '';
                }
            },
            'state',
            'runStartDate' => function ($model) {
                return DateTime::toJsonDateFormat($model->run_start_date);
            },
            'deadline' => function ($model) {
                return DateTime::toJsonDateFormat($model->deadline);
            }
        ];
    }

    /**
     * @return array All project states
     */
    public static function allStates() : array
    {
        return [
            self::STATE_PROPOSAL,
            self::STATE_ACCEPTED,
            self::STATE_ANALYSIS,
            self::STATE_ANALYSIS_HANDED_IN,
            self::STATE_IMPLEMENTATION,
            self::STATE_IMPLEMENTATION_HANDED_IN,
            self::STATE_CONDITIONALLY_DEFENDED,
            self::STATE_CONDITIONAL_DOCUMENTS_HANDED_IN,
            self::STATE_DEFENDED,
            self::STATE_FAILED,
            self::STATE_CANCELED
        ];
    }

    /**
     * @return ActiveQuery List of project ids, which are assigned to defence and do not have the result
     */
    public static function getAssignedProjectIds() : ActiveQuery
    {
        return ProjectDefence::find()
            ->where(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['result' => null])
            ->select('project_id');
    }

    /**
     * @return Project[]    List of projects which are waiting for defence
     */
    public static function getDefendableProjects() : array
    {
        $pendingDefences = ProjectDefence::find()
            ->where([
                'deleted' => false,
                'result' => null
            ])
            ->select('project_id')
            ->distinct()
            ->all();
        $disallowedIds = [];
        foreach ($pendingDefences as $defence) {
            $disallowedIds[] = (int)$defence->project_id;
        }

        $projects = Project::find()
            ->where(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['not', ['id' => $disallowedIds]])
            ->all();
        return $projects;
    }

    /**
     * @return int  Get next defence number (number of defences of the project + 1)
     */
    public function getNextDefenceNumber() : int
    {
        $defences = $this->getProjectDefences()->all();
        $max = 0;
        foreach ($defences as $defence) {
            if ($defence->number > $max) {
                $max = $defence->number;
            }
        }
        return $max + 1;
    }

    /**
     * @return int  Get defence number of the upcoming defence
     *              or next defence number if project is not waiting for the
     *              defence yet.
     */
    public function getUpcomingDefenceNumber() : int
    {
        $upcomingDefence = $this->getUpcomingDefence();
        if ($upcomingDefence != null) {
            return $upcomingDefence->number;
        }
        return $this->getNextDefenceNumber();
    }

    /**
     * @return User[]   Get array of all project members
     */
    public function getTeamAndSupervisor() : array
    {
        return array_merge($this->supervisors, $this->members);
    }

    /**
     * @return ActiveQuery  Get all Project documents submitted for the project
     */
    public function getProjectDocuments() : ActiveQuery
    {
        return $this->hasMany(ProjectDocument::class, ['project_id' => 'id'])
            ->andWhere(Sql::CONDITION_NOT_DELETED)
            ->orderBy(['updated_at' => SORT_DESC]);
    }

    /**
     * @return ActiveQuery  Get all non deleted project defences ordered by 
     */
    public function getProjectDefences() : ActiveQuery
    {
        return $this->hasMany(ProjectDefence::class, ['project_id' => 'id'])
            ->andWhere(Sql::CONDITION_NOT_DELETED)
            ->orderBy(['updated_at' => SORT_DESC]);
    }

    /**
     * @return ActiveQuery  Get all non deleted project state history records
     *                      for the project in descending order by creation date
     */
    public function getProjectStateHistories() : ActiveQuery
    {
        return $this->hasMany(ProjectStateHistory::class, ['project_id' => 'id'])
            ->andWhere(Sql::CONDITION_NOT_DELETED)
            ->orderBy(['created_at' => SORT_DESC])
            ->inverseOf('project');
    }

    /**
     * @return ActiveQuery  Get all non deleted document records associated
     *                      with this project
     */
    public function getDocument() : ActiveQuery
    {
        return $this->hasOne(Document::class, ['id' => 'content'])
            ->andOnCondition([Sql::COL_DELETED => false]);
    }

    /**
     * @return ActiveQuery  Get all proposals
     */
    public function getProposals() : ActiveQuery
    {
        return $this->hasMany(Proposal::class, ['project_id' => 'id'])
            ->andOnCondition([Sql::COL_DELETED => false]);
    }

    /**
     * @return ActiveQuery  Get all ProjectUser records assigned to the project
     */
    public function getUsersOnProjects() : ActiveQuery
    {
        return $this->hasMany(ProjectUser::class, ['project_id' => 'id'])
            ->andOnCondition([Sql::COL_DELETED => false]);
    }

    /**
     * Set markdown content to the project.
     * @param string $content  Markdown content
     */
    public function setContentMarkdown(string $content)
    {
        $this->contentMarkdown = $content;
    }

    /**
     * Get markdown content from the document
     * @return string Content markdown of the project document
     */
    public function getContentMarkdown() : string
    {
        $contentMarkdown = '';

        if ($this->isNewRecord) {
            return !empty($this->contentMarkdown) ? $this->contentMarkdown : $contentMarkdown;
        }

        if (isset($this->document->content_md)) {
            $contentMarkdown = $this->document->content_md;
        }

        return $contentMarkdown;
    }

    /**
     * Set advice in markdown format. Value represents advice for
     * other students for this course, experiences from the project and so on.
     * @param string $advice    Project advice
     */
    public function setAdviceMarkdown(string $advice) : void
    {
        $this->adviceMarkdown = $advice;
    }

    /**
     * Get text of the project advice. Advice is in markdown format
     * and represent advice for other students for this course,
     * experiences from the project and so on.
     * @return string   Set markdown content of the advice
     */
    public function getAdviceMarkdown() : string
    {
        $adviceMarkdown = '';

        if ($this->isNewRecord) {
            return !empty($this->adviceMarkdown) ? $this->adviceMarkdown : $adviceMarkdown;
        }

        if (isset($this->adviceDocument->document->content_md)) {
            $adviceMarkdown = $this->adviceDocument->document->content_md;
        }

        return $adviceMarkdown;
    }

    /**
     * @return ProjectDefence|null   Project defence record which the project is
     * assigned to and not have a result yet
     */
    public function getUpcomingDefence() : ? ProjectDefence
    {
        /** @var ProjectDefence $result */
        $result = $this->getProjectDefences()
            ->where(['result' => null])
            ->andWhere(Sql::CONDITION_NOT_DELETED)
            ->one();
        return $result;
    }

    /**
     * @return string Get date in DateTime::Date format of the upcoming defence
     */
    public function getUpcomingDefenceDocDeadline() : string
    {
        /** @var ProjectDefence $result */
        $defence = $this->getUpcomingDefence();
        $date = new DateTime($defence->defenceDate->date);
        $result = ($date->modify(Param::PROJECT_DOCUMENTS_BEFORE_DEFENCE))->format(DateTime::DATE);
        return $result;
    }

    /**
     * @param int $number       Defence number
     * @return DefenceDocument|null  Get documents for specific project defence with given defence number
     */
    public function getDocumentsForDefence(int $number) : ? DefenceDocument
    {
        $zip = $this->getDocumentForDefence(ProjectDocument::TYPE_FOR_DEFENSE_ZIP, $number);
        $pdf = $this->getDocumentForDefence(ProjectDocument::TYPE_FOR_DEFENSE_PDF, $number);
        $result = new DefenceDocument();
        $result->zip = $zip;
        $result->pdf = $pdf;
        return $result;
    }

    /**
     * Create new DefenceDocument and insert into it all uploaded files
     * which can be in zip and pdf format, for the upcoming project defence
     * @return DefenceDocument  Create new DefenceDocument with all submitted project documents
     */
    public function getDefenceDocuments() : ? DefenceDocument
    {
        $zip = $this->getDocumentByType(ProjectDocument::TYPE_FOR_DEFENSE_ZIP);
        $pdf = $this->getDocumentByType(ProjectDocument::TYPE_FOR_DEFENSE_PDF);
        $result = new DefenceDocument();
        $result->zip = $zip;
        $result->pdf = $pdf;
        return $result;
    }

    /**
     * Check if the documents were saved and not sent for another defence
     * so the project can be assigned to defence date
     * @return bool     True if documents were uploaded so the project is prepared
     *                  for assigning to a defence date
     */
    public function canSendForDefence() : bool
    {
        /** @var DefenceDocument $documentPair */
        $documentPair = $this->getDefenceDocuments();
        if ($documentPair->pdf != null) {
            return $documentPair->pdf->projectDocument->team_private == 1;
        }
        if ($documentPair->zip != null) {
            return $documentPair->zip->projectDocument->team_private == 1;
        }
        return false;
    }

    /**
     * List of non deleted ProjectDocument records for this project.
     * Private documents are visible only for people, who have specific
     * priviledges (team members).
     * @return ActiveQuery  Get list of private ProjectDocument records assigned to the project
     */
    public function getPrivateDocuments() : ActiveQuery
    {
        return $this->getProjectDocuments()
            ->where([ProjectDocument::COL_TEAM_PRIVATE => true])
            ->andWhere([ProjectDocument::COL_TYPE => ProjectDocument::TYPE_READ_ONLY])
            ->andWhere(Sql::CONDITION_NOT_DELETED);
    }

    /**
     * List of non deleted ProjectDocument records for this project.
     * Private documents are visible only for people, who have specific
     * priviledges.
     * @return ActiveQuery  Get list of private ProjectDocument records assigned to the project
     */
    public function getPublicDocuments() : ActiveQuery
    {
        return $this->getProjectDocuments()
            ->where([ProjectDocument::COL_TEAM_PRIVATE => false])
            ->andWhere([ProjectDocument::COL_TYPE => ProjectDocument::TYPE_READ_ONLY])
            ->andWhere(Sql::CONDITION_NOT_DELETED);
    }

    /**
     * @return ProjectDocument  Get one ProjectDocument associated with project as advice (advice type)
     *                          or null if not uploaded
     */
    public function getAdviceDocument() : ? ProjectDocument
    {
        /** @var ProjectDocument $result */
        $result = $this->getProjectDocuments()
            ->where([ProjectDocument::COL_TYPE => ProjectDocument::TYPE_ADVICE])
            ->andWhere(Sql::CONDITION_NOT_DELETED)
            ->limit(1)
            ->one();
        return $result;
    }

    /**
     * @return string   Markdown description as processed HTML
     */
    public function getDescriptionMarkdownProcessed() : string
    {
        return Markdown::process(Html::encode($this->description));
    }

    /**
     * @return string   Markdown description as processed HTML
     */
    public function getContentMarkdownProcessed() : string
    {
        return Markdown::process(Html::encode($this->getContentMarkdown()));
    }

    /**
     * @return string   Markdown description as processed HTML
     */
    public function getAdviceMarkdownProcessed() : string
    {
        return Markdown::process(Html::encode($this->getAdviceMarkdown()));
    }

    /**
     * @return ActiveQuery  List of User records representing users
     *                      assigned to the project (have ProjectUser
     *                      records for this project)
     */
    public function getActiveUsers() : ActiveQuery
    {
        $projectUserTableName = ProjectUser::tableName();
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(
                $projectUserTableName,
                ['project_id' => 'id'],
                function ($query) use (&$projectUserTableName) {
                    $query->onCondition([
                        $projectUserTableName . '.deleted' => false
                    ]);
                }
            );
    }

    /**
     * Return supervisor. Method is declared due to backward compatibility.
     * Prefered method to use: getSupervisors
     * @return ActiveQuery  Query from databse
     */
    public function getSupervisor() : ActiveQuery
    {
        return $this->getSupervisors();
    }

    /**
     * Return list of supervisor on this project. Return users has record
     * ProjectUser with project_role supervisor in the database.
     * @return ActiveQuery  Query from databse as list of User records representing
     *                      supervisors
     */
    public function getSupervisors() : ActiveQuery
    {
        $projectUserTableName = ProjectUser::tableName();
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(
                $projectUserTableName,
                ['project_id' => 'id'],
                function (ActiveQuery $query) use (&$projectUserTableName) {
                    $query->onCondition([
                        $projectUserTableName . '.project_role' => ProjectUser::ROLE_SUPERVISOR,
                        $projectUserTableName . '.deleted' => false
                    ]);
                }
            );
    }

    /**
     * Return all users having record in ProjectUser table associated
     * with this project and with any role
     * @return ActiveQuery  List of User records as project members
     */
    public function getMembers() : ActiveQuery
    {
        $projectUserTableName = ProjectUser::tableName();
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(
                $projectUserTableName,
                ['project_id' => 'id'],
                function ($query) use (&$projectUserTableName) {
                    $query->onCondition([
                        $projectUserTableName . '.project_role' => ProjectUser::ROLE_TEAM_MEMBER,
                        $projectUserTableName . '.deleted' => false
                    ]);
                }
            );
    }

    /**
     * Return all users having record in ProjectUser table associated
     * with this project and with project role as consultant
     * @return ActiveQuery  List of all User records as project consultants
     */
    public function getConsultants() : ActiveQuery
    {
        $projectUserTableName = ProjectUser::tableName();
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(
                $projectUserTableName,
                ['project_id' => 'id'],
                function ($query) use (&$projectUserTableName) {
                    $query->onCondition([
                        $projectUserTableName . '.project_role' => ProjectUser::ROLE_CONSULTANT,
                        $projectUserTableName . '.deleted' => false
                    ]);
                }
            );
    }

    /**
     * Return all users having record in ProjectUser table associated
     * with this project and with project role as opponent
     * @return ActiveQuery  List of all User records as project opponents
     */
    public function getOpponents() : ActiveQuery
    {
        $projectUserTableName = ProjectUser::tableName();
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(
                $projectUserTableName,
                ['project_id' => 'id'],
                function ($query) use (&$projectUserTableName) {
                    $query->onCondition([
                        $projectUserTableName . '.project_role' => ProjectUser::ROLE_OPPONENT,
                        $projectUserTableName . '.deleted' => false
                    ]);
                }
            );
    }

    /**
     * Get last project state as ProjectStateRecord for this project
     * with the the latest creation date
     * @return ProjectStateHistory  Latest project state history records for this project
     *                              or null, if no records was found
     */
    public function getLastState() : ? ProjectStateHistory
    {
        /** @var ProjectStateHistory $result */
        $result = $this->hasOne(ProjectStateHistory::class, ['project_id' => 'id'])
            ->andOnCondition(Sql::CONDITION_NOT_DELETED)
            ->orderBy([Sql::COL_CREATED_AT => SORT_DESC])
            ->inverseOf('project')
            ->one();
        return $result;
    }

    /**
     * Get Proposal associated for this project with latest creation date
     * @return ActiveQuery  The latest proposal
     */
    public function getLastProposal() : ActiveQuery
    {
        return $this->hasOne(Proposal::class, ['project_id' => 'id'])
            ->andOnCondition(Sql::CONDITION_NOT_DELETED)
            ->orderBy(['created_at' => SORT_DESC])
            ->inverseOf('project');
    }

    /**
     * @return array    Array of project states with key => value as
     *                  state key and translated label
     */
    public static function getOptionsStates() : array
    {
        return [
            self::STATE_PROPOSAL => Yii::t('app', 'proposal'),
            self::STATE_ACCEPTED => Yii::t('app', 'accepted'),
            self::STATE_ANALYSIS => Yii::t('app', 'analysis'),
            self::STATE_ANALYSIS_HANDED_IN => Yii::t('app', 'analysis_handed_in'),
            self::STATE_IMPLEMENTATION => Yii::t('app', 'implementation'),
            self::STATE_IMPLEMENTATION_HANDED_IN => Yii::t('app', 'implementation_handed_in'),
            self::STATE_CONDITIONALLY_DEFENDED => Yii::t('app', 'conditionally_defended'),
            self::STATE_CONDITIONAL_DOCUMENTS_HANDED_IN => Yii::t('app', 'conditional_documents_handed_in'),
            self::STATE_DEFENDED => Yii::t('app', 'defended'),
            self::STATE_FAILED => Yii::t('app', 'failed'),
            self::STATE_CANCELED => Yii::t('app', 'canceled'),
        ];
    }

    /**
     * Calculate and return percentual progress based on project state
     * @param string $state     Project state
     * @return float Actual percentual project progress
     */
    public static function getProgress(string $state) : float
    {
        $percent = 100 / 9;
        switch ($state) {
            case self::STATE_PROPOSAL:
                return 1 * $percent;
            case self::STATE_ACCEPTED:
                return 2 * $percent;
            case self::STATE_ANALYSIS:
                return 3 * $percent;
            case self::STATE_ANALYSIS_HANDED_IN:
                return 4 * $percent;
            case self::STATE_IMPLEMENTATION:
                return 5 * $percent;
            case self::STATE_IMPLEMENTATION_HANDED_IN:
                return 6 * $percent;
            case self::STATE_CONDITIONALLY_DEFENDED:
                return 7 * $percent;
            case self::STATE_CONDITIONAL_DOCUMENTS_HANDED_IN:
                return 8 * $percent;
            case self::STATE_DEFENDED:
                return 9 * $percent;
            case self::STATE_FAILED:
                return 0;
            case self::STATE_CANCELED:
                return 0;
            default:
                return 0;
        }
    }

    /**
     * Get array of states project can be moved into from given state
     * @param string $state     Current project state
     * @return array List of project states, which can be moved into
     */
    public static function getNextStates(string $state) : array
    {
        $nextStates = [
            self::STATE_PROPOSAL => [self::STATE_ACCEPTED, self::STATE_CANCELED, self::STATE_FAILED],
            self::STATE_ACCEPTED => [self::STATE_ANALYSIS, self::STATE_FAILED],
            self::STATE_ANALYSIS => [self::STATE_ANALYSIS_HANDED_IN, self::STATE_FAILED],
            self::STATE_ANALYSIS_HANDED_IN => [self::STATE_ANALYSIS, self::STATE_IMPLEMENTATION, self::STATE_FAILED],
            self::STATE_IMPLEMENTATION => [self::STATE_IMPLEMENTATION_HANDED_IN, self::STATE_FAILED],
            self::STATE_IMPLEMENTATION_HANDED_IN => [self::STATE_IMPLEMENTATION_HANDED_IN, self::STATE_CONDITIONALLY_DEFENDED, self::STATE_DEFENDED, self::STATE_FAILED],
            self::STATE_CONDITIONALLY_DEFENDED => [self::STATE_IMPLEMENTATION_HANDED_IN, self::STATE_FAILED],
            self::STATE_CONDITIONAL_DOCUMENTS_HANDED_IN => [self::STATE_DEFENDED, self::STATE_FAILED],
        ];

        return array_key_exists($state, $nextStates) ? $nextStates[$state] : [];
    }

    /**
     * Get list of projects states in which the project is running.
     * Running states: proposal, accepted, analysis, analysis handed in, implementation, implementation handed in
     * @return array    List of running states
     */
    public static function getProjectRunningStates() : array
    {
        return [
            self::STATE_PROPOSAL, self::STATE_ACCEPTED, self::STATE_ANALYSIS, self::STATE_ANALYSIS_HANDED_IN,
            self::STATE_IMPLEMENTATION, self::STATE_IMPLEMENTATION_HANDED_IN
        ];
    }

    /**
     * @param string $state     Given state
     * @return bool     True if given states is running state
     */
    public static function canUpdateRunningProject(string $state) : bool
    {
        return in_array($state, self::getProjectRunningStates());
    }

    /**
     * Determine whether project can be assigned to defence. It can be if it is in state
     * in one of the states: analysis, analysis handed in, implementation or implementation handed in
     * @param string $state     Given state
     * @return bool True whether the project with given state can be assigned to defence
     */
    public static function canAssignToDefence(string $state) : bool
    {
        return in_array($state, [self::STATE_ANALYSIS, self::STATE_ANALYSIS_HANDED_IN, self::STATE_IMPLEMENTATION, self::STATE_IMPLEMENTATION_HANDED_IN]);
    }

    /**
     * True whether the project is finished so other users can see uploaded advices.
     * Project is finished if it is in state defenced or failed.
     * @param string $state     Given state
     * @return bool True whether the project is finished so other users can see uploaded advices
     */
    public static function canSeeAdvice(string $state) : bool
    {
        return in_array($state, [self::STATE_DEFENDED, self::STATE_FAILED]);
    }

    /**
     * Get 1 if next defence project can be assigned into is small defence
     * (analysis), 2 if it is big defence, or 0 otherwise. Project can be assigned for
     * analysis defence, if it is in state analysis, or analysis handed in. Project can
     * be assigned for final defence if it is in states implementation, implementation handed in,
     * conditionally defended, conditional documents handed in
     * @return int Get 1 if next defence project can be assigned into is small defence
     *             (analysis), 2 if it is big defence, or 0 otherwise
     */
    public function getNextDefenceType() : int
    {
        if (in_array($this->state, [self::STATE_ANALYSIS, self::STATE_ANALYSIS_HANDED_IN])) {
            return 1; // small defence
        } elseif (in_array($this->state, [self::STATE_IMPLEMENTATION, self::STATE_IMPLEMENTATION_HANDED_IN, self::STATE_CONDITIONALLY_DEFENDED, self::STATE_CONDITIONAL_DOCUMENTS_HANDED_IN])) {
            return 2; // big defence
        }
        return 0;
    }

    /**
     * Upload documents for upcoming defence
     * @return bool True if upcoming defence exists and has no result yet, false otherwise
     */
    public function sendDefenceDocuments() : bool
    {
        /** @var ProjectDocument[] $projectDocs */
        $projectDocs = $this->getProjectDocuments()
            ->where([ProjectDocument::COL_TYPE => ProjectDocument::TYPE_FOR_DEFENSE_ZIP])
            ->orWhere([ProjectDocument::COL_TYPE => ProjectDocument::TYPE_FOR_DEFENSE_PDF])
            ->all();
        /** @var ProjectDefence $defence */
        $defence = $this->getUpcomingDefence();
        if ($defence && $defence->result == null) {
            /** @var ProjectDocument $document */
            foreach ($projectDocs as $document) {
                $document->defence_number = $defence->number;
                $document->team_private = 0;
                $document->save();
            }
            return true;
        }
        return false;
    }

    /**
     * Check whether deadline for the project is after starting date
     * and if it is not, add error
     */
    public function validateDeadline() : void
    {
        $runStartDate = new DateTime($this->run_start_date);
        $deadline = new DateTime($this->deadline);
        if ($runStartDate->isAfter($deadline)) {
            $this->addError(
                self::COL_DEADLINE,
                Yii::t('app', 'The deadline must be after the start.')
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $document = $this->document;
        if (!isset($this->document)) {
            $document = new Document();
        }
        $document->content_md = $this->contentMarkdown;

        if (!$document->save()) {
            $transaction->rollBack();
            return false;
        }

        $this->content = $document->id;

        if (!parent::save(false, $attributeNames)) {
            $transaction->rollBack();
            return false;
        }

        $projectState = null;
        if (!$this->state) {
            $projectState = new ProjectStateHistory();
            $projectState->state = self::STATE_PROPOSAL;
        } else if (isset($this->lastState)) {
            if ($this->state != $this->lastState->state) {
                $projectState = new ProjectStateHistory();
                $projectState->state = $this->state;
            }
        }

        if (is_object($projectState)) {
            $projectState->project_id = $this->id;

            if (!$projectState->save()) {
                $transaction->rollBack();
                return false;
            }
        }

        $proposal = $this->lastProposal;
        if (!isset($proposal)) {
            $proposal = new Proposal();
        }
        $proposal->project_id = $this->id;

        //we need this because in the rules of Proposal the state is required and if we don't specify it, the validation fails
        //and the transaction is rolled back
        if ($proposal->state != Proposal::STATE_ACCEPTED) {
            $proposal->state = Proposal::STATE_DRAFT;

            if (!isset($this->state) || $this->state == self::STATE_PROPOSAL) {
                switch ($this->scenario) {
                    case self::SCENARIO_CREATE:
                        $proposal->state = Proposal::STATE_DRAFT;
                        // put text from proposal.md into a document markdown
                        $proposal->documentMarkdown = MarkdownTemplateStorage::getFileContent('proposal.md');
                        break;
                    case self::SCENARIO_SEND:
                        $proposal->state = Proposal::STATE_SENT;
                        break;
                }
            }
        }

        if (!$proposal->save()) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    //Concatenates the names of users in the given array.
    private function concatenateParticipantNames(array $participants) : string
    {
        $result = "";
        foreach ($participants as $participant) {
            $result .= $participant->getFullNameWithDegrees() . ', ';
        }
        return substr($result, 0, -2);
    }

    private function getDocumentForDefence(string $type, int $number) : ? Document
    {
        /** @var ProjectDocument $projectDoc */
        $projectDoc = $this->getProjectDocuments()
            ->andWhere([
                ProjectDocument::COL_DEFENCE_NUMBER => $number,
                ProjectDocument::COL_TYPE => $type
            ])
            ->one();

        if ($projectDoc) {
            return $projectDoc->document;
        } else {
            return null;
        }
    }

    private function getDocumentByType(string $type) : ? Document
    {
        /** @var ProjectDocument $projectDoc */
        $projectDoc = $this->getProjectDocuments()
            ->andWhere([ProjectDocument::COL_TYPE => $type])
            ->limit(1)
            ->one();
        if ($projectDoc) return $projectDoc->document;
        return null;
    }
}
