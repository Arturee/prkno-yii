<?php
namespace app\models\records;

use app\consts\Param;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "page".
 * Page table store static pages. Those represents general document,
 * rulse, guide through the life cycle of software projects
 * and other pages with other usefull information. Statig
 * pages has to be provided in both English and Czech languages.
 * Each page can have several versions evolved continuosly.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property string $title_en       Title of the page in English
 * @property string $title_cs       Title of the page in Czech
 * @property string $path_pdf_download  Path to the uploaded document.
 * @property string $url            Name of the view used in URL
 * @property int $order_in_menu     List of pages in view are sorted. This value represents the order number.
 * @property int $published         True/false whether the page is visible for all users, or if it is in draw state visible only for the creator.
 *
 * @property PageVersion[] $pageVersions    Getter generated from getPageVersions method
 */
class Page extends OrmRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'order_in_menu', 'published'], 'required'],
            [['deleted', 'published'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['order_in_menu'], 'integer'],
            [['title_en', 'title_cs'], 'string', 'max' => 100],
            [['path_pdf_download', 'url'], 'string', 'max' => 300],
            ['url', 'validateUrlNotException']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'title_en' => Yii::t('app', 'Title En'),
            'title_cs' => Yii::t('app', 'Title Cs'),
            'path_pdf_download' => Yii::t('app', 'Path Pdf Download'),
            'url' => Yii::t('app', 'Url'),
            'order_in_menu' => Yii::t('app', 'Order In Menu'),
            'published' => Yii::t('app', 'Published'),
        ];
    }

    /**
     * @return ActiveQuery  Get all page version from this page
     */
    public function getPageVersions() : ActiveQuery
    {
        return $this->hasMany(PageVersion::className(), ['page_id' => 'id']);
    }

    /**
     * @return PageVersion|null  Get last page version from given page
     */
    public function getLastPageVersion() : ? PageVersion
    {
        return PageVersion::find()
            ->where(['page_id' => $this->id])
            ->andWhere(['deleted' => false])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }

    /** Returns the currently selected page version. If no page version is selected, returns the last one. */
    public function getSelectedOrLastPageVersion() : ?PageVersion
    {
        $currentlySelected = PageVersion::find()
            ->where(['page_id' => $this->id])
            ->andWhere(['deleted' => false])
            ->andWhere(['published' => true])
            ->one();

        if ($currentlySelected)
        {
            return $currentlySelected;
        } else {
            return $this->getLastPageVersion();
        }
    }

    /**
     * @return PageVersion|null  Get last page version from given page
     */
    public function getCurrentlyShowingPageVersion() : ? PageVersion
    {
        return PageVersion::find()
            ->where(['page_id' => $this->id])
            ->andWhere(['deleted' => false])
            ->andWhere(['published' => true])
            ->one();
    }

    /**
     * Validate whether the url is reserved. Reserver url are
     * located in const/params.php under the key Param::PAGE_RESERVED_URLS
     * @param string $attributeName     Class attribute containing url
     */
    public function validateUrlNotException(string $attributeName) : void
    {
        $url = $this->{$attributeName};

        foreach (Yii::$app->params[Param::PAGE_RESERVED_URLS] as $urlException) {
            if (trim($url) === $urlException) {
                $this->addError($attributeName, Yii::t(
                    'app',
                    'Chosen URL is a reserved URL address! The reserved URLs are: {0}',
                    [implode(', ', Yii::$app->params[Param::PAGE_RESERVED_URLS])]
                ));
                break;
            }
        }
    }

    /**
     * @return string    Page title based on users language
     */
    public function getTitleBasedOnSiteLanguage() : string
    {
        $isSiteLanguageCzech = Yii::$app->language == 'cs-CZ';

        if ($isSiteLanguageCzech) {
            return $this->getTitle($this->title_cs, $this->title_en);
        } else {
            return $this->getTitle($this->title_en, $this->title_cs);
        }
    }

    /** Returns the main title if there is one. If there isn't, returns the backup title. If neither of those exists,
     * returns a constant saying no_title.
     */
    public function getTitle(? string $mainTitle, ? string $backupTitle)
    {
        $title = $mainTitle ? $mainTitle : $backupTitle;
        return $title ? $title : Param::PAGE_NO_TITLE;
    }
}
