<?php
namespace app\models\records;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "project_state_history".
 * project_state_history is a table storing project states in specific time. The record
 * with the greatest time for specific project is the actual state.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $project_id        Project id
 * @property string $state          Enum class representing project state in given time. Allowed values: 'proposal', 'accepted', 'analysis', 'analysis_handed_in', 'implementation', 'implementation_handed_in', 'conditionally_defended', 'conditional_documents_handed_in', 'defended', 'failed', 'canceled'.
 * @property string $comment        Optional comment
 *
 * @property Project $project       Getter generated from getProject method
 */
class ProjectStateHistory extends OrmRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_state_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'state'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['project_id'], 'integer'],
            [['deleted'], 'boolean'],
            [['state'], 'in', 'range' => Project::allStates() ],
            [['comment'], 'string', 'max' => 500],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'project_id' => Yii::t('app', 'Project ID'),
            'state' => Yii::t('app', 'State'),
            'comment' => Yii::t('app', 'Comment')
        ];
    }

    /**
     * @return ActiveQuery  Get project
     */
    public function getProject() : ActiveQuery
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }
}
