<?php
namespace app\models\records;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "proposal_vote".
 * Table stores votes of the committee members for particular proposal.
 * Based on these votes, final decision for the proposal will be made.
 *
 * @property int $id                Record id
 * @property string $created_at     Datetime of the record creation
 * @property string $updated_at     Datetime of the last record update
 * @property int $deleted           True if the record is deleted, false otherwise
 * @property int $user_id           User id of the voter
 * @property int $proposal_id       Proposal id
 * @property string $vote           Value of the vole. Allowed values: 'yes', 'no'
 * 
 * @property Proposal $proposal     Getter generated from getProposal method
 * @property User $user             Getter generated from getUser method
 */
class ProposalVote extends OrmRecord
{
    const
        VOTE_YES = 'yes',
        VOTE_NO = 'no';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proposal_vote';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'proposal_id'], 'required'],
            [['user_id', 'proposal_id'], 'integer'],
            [['deleted'], 'boolean'],
            [['vote'], 'in', 'range' => [self::VOTE_NO, self::VOTE_YES]],
            [['proposal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proposal::class, 'targetAttribute' => ['proposal_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted' => Yii::t('app', 'Deleted'),
            'user_id' => Yii::t('app', 'User ID'),
            'proposal_id' => Yii::t('app', 'Proposal ID'),
            'vote' => Yii::t('app', 'Vote'),
        ];
    }

    /**
     * @return ActiveQuery  List with Proposal record, which this vote belongs into
     */
    public function getProposal() : ActiveQuery
    {
        return $this->hasOne(Proposal::class, ['id' => 'proposal_id']);
    }

    /**
     * @return ActiveQuery  List with User record, which this vote belongs to
     */
    public function getUser() : ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Get label for vote enum value
     * @param string|null $vote ProposalVote enum value
     * @return string Label text of the vote
     */
    public static function getVoteLabel(? string $vote) : string
    {
        $map = [
            self::VOTE_YES => Yii::t('app', 'Yes'),
            self::VOTE_NO => Yii::t('app', 'No'),
            '' => Yii::t('app', 'Unknown'),
            null => Yii::t('app', 'Unknown')
        ];
        return $map[$vote];
    }
}
