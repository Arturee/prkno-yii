<?php
namespace app\models\search;

use app\models\records\Project;
use app\models\records\ProjectUser;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProjectSearch represents the model behind the search form of `app\models\records\Project`.
 */
class ProjectSearch extends Project
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'content', 'extra_credits' ], 'integer'],
            [['deleted', 'waiting_for_proposal_evaluation', 'requested_to_run', 'team_members_visible'], 'boolean'],
            [['updated_at', 'created_at', 'state', 'run_start_date', 'deadline', 'name', 'short_name', 'description', 'keywords', 'supervisorIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params     Class params
     * @return ActiveDataProvider Data provider for Project records
     */
    public function search($params)
    {
        $tableName = self::tableName();
        $projectUserTableName = ProjectUser::tableName();

        $query = Project::find()->joinWith('activeUsers');
        $query->groupBy($tableName.'.id'); // count only projects

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $updated_at = $this->updated_at;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $tableName.'.id' => $this->id,
            $tableName.'.created_at' => $this->created_at,
            $tableName.'.deleted' => false,
            $tableName.'.run_start_date' => $this->run_start_date,
            $tableName.'.deadline' => $this->deadline,
            $tableName.'.content' => $this->content,
            $tableName.'.waiting_for_proposal_evaluation' => $this->waiting_for_proposal_evaluation,
            $tableName.'.requested_to_run' => $this->requested_to_run,
            $tableName.'.extra_credits' => $this->extra_credits,
            $tableName.'.team_members_visible' => $this->team_members_visible,
            $projectUserTableName.'.user_id' => $this->supervisorIds,
        ]);

        $query->andFilterWhere(['like', $tableName.'.state', $this->state])
            ->andFilterWhere(['like', $tableName.'.name', $this->name])
            ->andFilterWhere(['like', $tableName.'.short_name', $this->short_name])
            ->andFilterWhere(['like', $tableName.'.description', $this->description])
            ->andFilterWhere(['like', $tableName.'.keywords', $this->keywords]);

        if (!($this->supervisorIds)) {
            // not show draft projects in index
            $query->andFilterWhere(['<>', $tableName.'.state', Project::STATE_PROPOSAL]);
        }

        if ($updated_at) {
            $this->updated_at = $updated_at;
            $query->andFilterWhere(['like', $tableName.'.updated_at', $updated_at]);
        } else {
            $query->addOrderBy($tableName.'.updated_at DESC');
        }
        return $dataProvider;
    }
}
