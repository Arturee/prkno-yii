<?php
namespace app\models\search;

use app\consts\Sql;
use app\models\records\Advertisement;
use app\utils\DateTime;
use yii\data\ActiveDataProvider;

/**
 * AdvertisementSearch represents the model behind the search form of `app\models\records\Advertisement`.
 */
class AdvertisementSearch extends Advertisement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'description'], 'integer'],
            [['deleted','subscribed_via_email', 'published'], 'boolean'],
            [['created_at', 'updated_at', 'title', 'type', 'keywords'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params     Class params
     * @return ActiveDataProvider Data provider for Advertisement
     */
    public function search($params)
    {
        $query = Advertisement::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'user_id' => $this->user_id,
            'description' => $this->description,
            'subscribed_via_email' => $this->subscribed_via_email,
            'published' => $this->published,
        ]);

        if ($this->updated_at) {
            $start = new DateTime($this->updated_at);
            $query->andFilterWhere(['>', 'updated_at', $start]);
        }

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'keywords', $this->keywords]);

        $query->andFilterWhere(Sql::CONDITION_NOT_DELETED);

        $query->addOrderBy('updated_at DESC');
        return $dataProvider;
    }
}
