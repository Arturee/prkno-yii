<?php
namespace app\models\search;

use app\consts\Sql;
use app\models\records\Proposal;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProposalSearch represents the model behind the search form of `app\models\records\Proposal`.
 */
class ProposalSearch extends Proposal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'document_id'], 'integer'],
            [['deleted'], 'boolean'],
            [['created_at', 'updated_at', 'state'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params     Class params
     * @return ActiveDataProvider Data provider for Proposals records
     */
    public function search($params)
    {
        $query = Proposal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'project_id' => $this->project_id,
            'document_id' => $this->document_id,
        ]);
        $query->andFilterWhere(Sql::CONDITION_NOT_DELETED);
        $query->andFilterWhere(['like', 'state', $this->state]);
        $query->addOrderBy('updated_at DESC');

        return $dataProvider;
    }
}
