<?php
namespace app\models\search;

use app\models\records\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form of `app\models\Users`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['deleted', 'role_is_from_CAS', 'banned'], 'boolean'],
            [['created_at', 'updated_at', 'name', 'degree_prefix', 'degree_suffix', 'email',
                'password', 'news_subscription', 'last_login', 'custom_url', 'last_CAS_refresh',
                'role', 'role_expiration', 'login_type', 'password_recovery_token',
                'password_recovery_token_expiration'], 'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params     Class params
     * @return ActiveDataProvider Data provider for User records
     */
    public function search($params): ActiveDataProvider
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted' => 0,
            'last_login' => $this->last_login,
            'last_CAS_refresh' => $this->last_CAS_refresh,
            'role_expiration' => $this->role_expiration,
            'role_is_from_CAS' => $this->role_is_from_CAS,
            'password_recovery_token_expiration' => $this->password_recovery_token_expiration,
            'banned' => $this->banned,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'degree_prefix', $this->degree_prefix])
            ->andFilterWhere(['like', 'degree_suffix', $this->degree_suffix])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'news_subscription', $this->news_subscription])
            ->andFilterWhere(['like', 'custom_url', $this->custom_url])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'login_type', $this->login_type])
            ->andFilterWhere(['like', 'password_recovery_token', $this->password_recovery_token]);

        return $dataProvider;
    }
}
