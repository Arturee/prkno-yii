# CONVERSIONS #

MySQL uses 0,1 instead of booleans and ORM loads that, but in code it can be used
the same way as booleans

```php

1 // evaluates to true
0 // evaluates to false
!1 === true
!0 === false

```

assgning a bool and saving the ORM object also works. And the same is true for queries.

```php

$model->deleted = false;
$model->save();

// saves to the DB as 0

Keywords::find()->where([
    'deleted' => false
])->all();

// works OK

```
