<?php
namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * Abstract form
 */
class AbstractForm extends Model
{
    /**
     * Load data from post variables
     * @return true if user send some data with POST method
     */
    public function loadFromPost(...$args) : bool
    {
        return $this->load(Yii::$app->request->post(), ...$args);
    }

    /**
     * Get variable from POST vars
     * @param $key  Key
     * @return Value or null if not defined
     */
    public static function get(string $key)
    {
        $post = Yii::$app->request->post();
        return isset($post[$key]) ? $post[$key] : null;
    }
}