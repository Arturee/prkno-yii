<?php
namespace app\models\forms;

use yii\web\UploadedFile;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends AbstractForm
{
    /** @var UploadedFile[]|Null files attribute */
    public $files;

    /** @var UploadedFile |Null file attribute */
    public $zip, $pdf, $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['files[]'], 'file', 'maxFiles' => 10],
            [['file'], 'file', 'skipOnEmpty' => false],
            [['zip'], 'file', 'skipOnEmpty' => true, 'extensions' => 'zip'],
            [['pdf'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
        ];
    }
}