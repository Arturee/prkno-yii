<?php
namespace app\models\forms;

/**
 * Form for choosing opponent for the project defence
 */
class ChooseOpponentForm extends AbstractForm
{
    /** @var int Opponent id */
    public $opponent_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['opponent_id'], 'integer'],
            [['opponent_id'], 'required']
        ];
    }
}