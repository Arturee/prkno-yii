<?php
namespace app\models\forms;

/**
 * Form for sending ids
 */
class IdsForm extends AbstractForm
{
    /** @var string */
    public $ids;

    /**
     * @inheritdoc
     */
    public function loadFromPost(...$args) : bool
    {
        $ids = self::get('id');
        $this->ids = $ids == null ? array() : $ids;
        return true;
    }
}