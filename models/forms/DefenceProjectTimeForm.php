<?php
namespace app\models\forms;

/**
 * Form for setting time for project defence
 */
class DefenceProjectTimeForm extends AbstractForm
{
    /** @var int List of ids */
    public $ids;

    /** @var int List of times */
    public $times;

    /**
     * @inheritdoc
     */
    public function loadFromPost(...$args) : bool
    {
        $ids = self::get('id');
        $times = self::get('time');
        if ($ids == null || $times == null || sizeof($ids) !== sizeof($times)) {
            return false;
        }
        $this->ids = $ids;
        $this->times = $times;
        return true;
    }
}