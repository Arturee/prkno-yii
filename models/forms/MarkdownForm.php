<?php
namespace app\models\forms;

use yii\web\UploadedFile;

/**
 * Form for submitting markdown text
 */
class MarkdownForm extends AbstractForm
{
    const
        TYPE_SUPERVISOR_REVIEW = 'supervisor-review',
        TYPE_OPPONENT_REVIEW = 'opponent-review',
        TYPE_FINAL_STATEMENT = 'final-statement',

        SUCCESS_BUTTON_SUBMIT = 'submit',
        SUCCESS_BUTTON_PREVIEW = 'preview';

    /** @var UploadedFile | null files attribute */
    public $file;

    /** @var string | null */
    public $markdown, $type;

    /** @var int */
    public $author_id;

    /** @var string | null */
    public $customFormName;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => 'pdf'],
            [['markdown'], 'string'],
            [['type', 'author_id'], 'required'],
            [['author_id'], 'integer'],
            [['type'], 'in', 'range' => [self::TYPE_SUPERVISOR_REVIEW, self::TYPE_OPPONENT_REVIEW, self::TYPE_FINAL_STATEMENT]],
            [['successFunction'], 'in', 'range' => [self::SUCCESS_BUTTON_SUBMIT, self::SUCCESS_BUTTON_PREVIEW]],
        ];
    }

    /**
     * @return string The form name of this model class
     */
    public function formName() : string
    {
        return $this->customFormName ? $this->customFormName : parent::formName();
    }
}