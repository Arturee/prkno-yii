<?php
namespace app\models\forms;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends AbstractForm
{
    /** @var string Email address */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email', 'exist',
                'targetClass' => '\app\models\records\User',
                'filter' => ['deleted' => false, 'banned' => false],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }
}
