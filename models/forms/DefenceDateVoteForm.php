<?php
namespace app\models\forms;

use app\consts\Permission;
use Yii;

/**
 * Form for sending room change of defence date
 */
class DefenceDateVoteForm extends AbstractForm
{
    /** @var string */
    public $vote;

    /** @var string */
    public $comment;

    /**
     * @inheritdoc
     */
    public function loadFromPost(...$args) : bool
    {
        $post = Yii::$app->request->post('DefenceAttendance');
        if (!($post && isset($post['vote']) && isset($post['comment']) && Yii::$app->user->can(Permission::DEFENCES_VOTE_ABOUT_DATE))) {
            return false;
        }
        $this->vote = $post['vote'];
        $this->comment = $post['comment'];
        return true;
    }
}