<?php
namespace app\models\forms;

use app\models\records\User;
use yii\base\InvalidArgumentException;

/**
 * Password reset form
 */
class ResetPasswordForm extends AbstractForm
{
    /** @var string Password */
    public $password;

    /** @var User */
    private $_user;

    /**
     * Creates a form model given a token.
     * @param string $token     Token for resetting password
     * @param array $config     name-value pairs that will be used to initialize the object properties
     * @throws InvalidArgumentException if token is empty or not valid
     */
    public function __construct(string $token, array $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordRecoveryToken($token);
        if (!$this->_user) {
            throw new InvalidArgumentException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Resets password.
     * @return bool true if password was reset.
     */
    public function resetPassword() : bool
    {
        $user = $this->_user;
        $user->password = $this->password;
        $user->removePasswordRecoveryToken();

        return $user->save(false);
    }
}
