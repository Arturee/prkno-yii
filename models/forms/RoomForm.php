<?php
namespace app\models\forms;

/**
 * Form for sending room change of defence date
 */
class RoomForm extends AbstractForm
{
    /** @var string */
    public $room;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['room', 'string', 'max' => 200],
        ];
    }
}