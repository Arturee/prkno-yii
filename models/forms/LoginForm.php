<?php
namespace app\models\forms;

use app\consts\Param;
use app\models\records\User;
use app\utils\DateTime;
use Yii;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends AbstractForm
{
    /** @var string Email address */
    public $email;

    /** @var string Password */
    public $password;

    /** @var bool Remember me value */
    public $rememberMe = true;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() : array
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'rememberMe' => Yii::t('app', 'Remember me')
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     * @param string $attribute     The attribute currently being validated
     */
    public function validatePassword(string $attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, Yii::t('app', 'Incorrect email or password.'));
            } elseif (!$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('app', 'Incorrect email or password.'));
            } elseif ($user->banned) {
                $this->addError($attribute, Yii::t('app', 'You are banned. Contact the committee for an explanation.'));
            } elseif ($user->role_expiration && (new DateTime($user->role_expiration))->isInPast()) {
                $this->addError($attribute, Yii::t('app', 'Your account has expired. Ask the committee to refresh it.'));
            } elseif (!$user->role) {
                $this->addError($attribute, Yii::t('app', 'You do not have sufficient privileges to log in. (code 4)'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool Whether the user is logged in successfully
     */
    public function login() : bool
    {
        if ($this->validate()) {
            $duration = Yii::$app->params[Param::LOGIN_EXPIRATION];
            if (!$this->rememberMe) {
                $duration = 0;
            }
            $user = $this->getUser();
            $loginOk = Yii::$app->user->login($user, $duration);
            return $loginOk;
        }
        return false;
    }

    /**
     * Get user from private member $_user
     * @return User     User
     */
    public function getUser() : ? User
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
