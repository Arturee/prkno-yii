<?php
namespace app\models\forms;

use app\models\records\ProjectDefence;

/**
 * Form for accepting project from project defence
 */
class DefenceAcceptForm extends AbstractForm
{
    /** @var string Result of the project defence */
    public $result;

    /** @var int Number of extra credits */
    public $extraCredits;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['result'], 'required'],
            [['result'], 'in', 'range' => [ProjectDefence::RESULT_DEFENDED, ProjectDefence::RESULT_CONDITIONALLY_DEFENDED, ProjectDefence::RESULT_FAILED]],
            [['extraCredits'], 'integer'],
        ];
    }
}
