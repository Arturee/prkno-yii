<?php
namespace app\models;

/**
 * Class DefenceDocument
 *
 * @property $zip ?Document
 * @property $pdf ?Document
 *
 * @package app\models
 */
class DefenceDocument
{
    /**
     * @var ?Document
     */
    public $zip = null, $pdf = null;
}