<?php

namespace app;
use app\models;
use app\utils\DateTime;

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

// How to work with Nette\Utils\DateTime:
//
// $now = new DateTime();
// $now->modifyClone('+10 years, 1 month, 1 day');
// $now->modifyClone('-10 hours');
//
// (very simple, very nice)
// clone prevents modification of $now


function insertAdvertisement(
    int $id,
    int $user_id,
    string $title,
    string $description,
    ?string $type,
    ?string $keywords,
    int $subscribed_via_email,
    int $published,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $ad = new models\records\Advertisement();

    $ad->id = $id;
    $ad->user_id = $user_id;
    $ad->title = $title;
    $ad->description = $description;
    $ad->type = $type;
    $ad->keywords = $keywords;
    $ad->subscribed_via_email = $subscribed_via_email;
    $ad->published = $published;
    $ad->created_at = $now->modifyClone($created_at)->__toString();
    $ad->updated_at = $now->modifyClone($updated_at)->__toString();
    $ad->deleted = $deleted;

    if (!$ad->validate()) {
        throw new \Exception(implode(' AND ', $ad->getErrorSummary(true)));
    }

    $ad->save();
}

function insertAdComment(
    int $id,
    int $ad_id,
    ?int $user_id,
    string $content,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $adComment = new models\records\AdvertisementComment();

    $adComment->id = $id;
    $adComment->ad_id = $ad_id;
    $adComment->user_id = $user_id;
    $adComment->content = $content;
    $adComment->created_at = $now->modifyClone($created_at)->__toString();
    $adComment->updated_at = $now->modifyClone($updated_at)->__toString();
    $adComment->deleted = $deleted;

    if (!$adComment->validate()) {
        throw new \Exception(implode(' AND ', $adComment->getErrorSummary(true)));
    }

    $adComment->save();
}

function insertDefenceAttendance(
    int $id,
    int $defence_date_id,
    int $user_id,
    string $vote,
    ?string $comment,
    int $must_come,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $defenceAttendance = new models\records\DefenceAttendance();

    $defenceAttendance->id = $id;
    $defenceAttendance->defence_date_id = $defence_date_id;
    $defenceAttendance->user_id = $user_id;
    $defenceAttendance->vote = $vote;
    $defenceAttendance->comment = $comment;
    $defenceAttendance->must_come = $must_come;
    $defenceAttendance->created_at = $now->modifyClone($created_at)->__toString();
    $defenceAttendance->updated_at = $now->modifyClone($updated_at)->__toString();
    $defenceAttendance->deleted = $deleted;

    if (!$defenceAttendance->validate()) {
        throw new \Exception(implode(' AND ', $defenceAttendance->getErrorSummary(true)));
    }

    $defenceAttendance->save();
}

function insertDefenceDate(
    int $id,
    string $date,
    string $signup_deadline,
    int $active_voting,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $defenceDate = new models\records\DefenceDate();

    $defenceDate->id = $id;
    $defenceDate->date = $now->modifyClone($date)->format(DateTime::SQL_DATE);
    $defenceDate->signup_deadline = $now->modifyClone($signup_deadline)->format(DateTime::SQL_DATE);
    $defenceDate->active_voting = $active_voting;
    $defenceDate->created_at = $now->modifyClone($created_at)->__toString();
    $defenceDate->updated_at = $now->modifyClone($updated_at)->__toString();
    $defenceDate->deleted = $deleted;

    if (!$defenceDate->validate()) {
        throw new \Exception(implode(' AND ', $defenceDate->getErrorSummary(true)));
    }

    $defenceDate->save();
}

function insertDocument(
    ?int $id,
    ?string $content_md,
    ?string $fallback_path,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $document = new models\records\Document();

    $document->id = $id;
    $document->content_md = $content_md;
    $document->fallback_path = $fallback_path;
    $document->created_at = $now->modifyClone($created_at)->__toString();
    $document->updated_at = $now->modifyClone($updated_at)->__toString();
    $document->deleted = $deleted;

    if (!$document->validate()) {
        throw new \Exception(implode(' AND ', $document->getErrorSummary(true)));
    }

    $document->save();
}

function insertKeyword(
    int $id,
    string $keywordText,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $keyword = new models\records\Keyword();

    $keyword->id = $id;
    $keyword->keyword = $keywordText;
    $keyword->created_at = $now->modifyClone($created_at)->__toString();
    $keyword->updated_at = $now->modifyClone($updated_at)->__toString();
    $keyword->deleted = $deleted;

    if (!$keyword->validate()) {
        throw new \Exception(implode(' AND ', $keyword->getErrorSummary(true)));
    }

    $keyword->save();
}

function insertNews(
    int $id,
    ?int $user_id,
    string $title_en,
    string $title_cs,
    string $content_en,
    string $content_cs,
    int $public,
    string $type,
    ?string $hide_at,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $news = new models\records\News();

    $news->id = $id;
    $news->user_id = $user_id;
    $news->title_en = $title_en;
    $news->title_cs = $title_cs;
    $news->content_en = $content_en;
    $news->content_cs = $content_cs;
    $news->public = $public;
    $news->type = $type;
    $news->hide_at = $hide_at ? $now->modifyClone($hide_at)->__toString() : NULL;
    $news->created_at = $now->modifyClone($created_at)->__toString();
    $news->updated_at = $now->modifyClone($updated_at)->__toString();
    $news->deleted = $deleted;

    if (!$news->validate()) {
        throw new \Exception(implode(' AND ', $news->getErrorSummary(true)));
    }

    $news->save();
}

function insertPage(
    int $id,
    ?string $title_en,
    ?string $title_cs,
    ?string $path_pdf_download,
    string $url,
    int $order_in_menu,
    int $published,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $page = new models\records\Page();

    $page->id = $id;
    $page->title_en = $title_en;
    $page->title_cs = $title_cs;
    $page->path_pdf_download = $path_pdf_download;
    $page->url = $url;
    $page->order_in_menu = $order_in_menu;
    $page->published = $published;
    $page->created_at = $now->modifyClone($created_at)->__toString();
    $page->updated_at = $now->modifyClone($updated_at)->__toString();
    $page->deleted = $deleted;

    if (!$page->validate()) {
        throw new \Exception(implode(' AND ', $page->getErrorSummary(true)));
    }

    $page->save();
}

function insertPageVersion(
    int $id,
    int $page_id,
    int $user_id,
    ?int $content_en,
    ?int $content_cs,
    int $published,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $pageVersion = new models\records\PageVersion();

    $pageVersion->id = $id;
    $pageVersion->page_id = $page_id;
    $pageVersion->user_id = $user_id;
    $pageVersion->content_en = $content_en;
    $pageVersion->content_cs = $content_cs;
    $pageVersion->published = $published;
    $pageVersion->created_at = $now->modifyClone($created_at)->__toString();
    $pageVersion->updated_at = $now->modifyClone($updated_at)->__toString();
    $pageVersion->deleted = $deleted;

    if (!$pageVersion->validate()) {
        throw new \Exception(implode(' AND ', $pageVersion->getErrorSummary(true)));
    }

    $pageVersion->save();
}

function insertProject(
    ?int $id,
    string $state,
    ?string $run_start_date,
    ?string $deadline,
    string $name,
    string $short_name,
    string $description,
    ?int $content,
    int $waiting_for_proposal_evaluation,
    int $requested_to_run,
    int $extra_credits,
    int $team_members_visible,
    ?string $keywords,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $project = new models\records\Project();

    $project->id = $id;
    $project->state = $state;
    $project->run_start_date = $now->modifyClone($run_start_date)->__toString();
    $project->deadline = $now->modifyClone($deadline)->__toString();
    $project->name = $name;
    $project->short_name = $short_name;
    $project->description = $description;
    $project->content = $content;
    $project->waiting_for_proposal_evaluation = $waiting_for_proposal_evaluation;
    $project->requested_to_run = $requested_to_run;
    $project->extra_credits = $extra_credits;
    $project->team_members_visible = $team_members_visible;
    $project->keywords = $keywords;
    $project->created_at = $now->modifyClone($created_at)->__toString();
    $project->updated_at = $now->modifyClone($updated_at)->__toString();
    $project->deleted = $deleted;

    if (!$project->validate()) {
        throw new \Exception(implode(' AND ', $project->getErrorSummary(true)));
    }

    $project->save();
}

function insertDefence(
    int $id,
    int $project_id,
    ?string $state,
    ?int $defence_date_id,
    int $number,
    ?int $opponent_id,
    ?int $statement_author_id,
    ?string $time,
    ?int $defence_statement,
    ?int $opponent_review,
    ?int $supervisor_review,
    ?string $result,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $projectDefence = new models\records\ProjectDefence();

    $projectDefence->id = $id;
    $projectDefence->project_id = $project_id;
    $projectDefence->project_state = $state;
    $projectDefence->defence_date_id = $defence_date_id;
    $projectDefence->opponent_id = $opponent_id;
    $projectDefence->statement_author_id = $statement_author_id;
    $projectDefence->time = $time;
    $projectDefence->number = $number;
    $projectDefence->defence_statement = $defence_statement;
    $projectDefence->opponent_review = $opponent_review;
    $projectDefence->supervisor_review = $supervisor_review;
    $projectDefence->result = $result;
    $projectDefence->created_at = $now->modifyClone($created_at)->__toString();
    $projectDefence->updated_at = $now->modifyClone($updated_at)->__toString();
    $projectDefence->deleted = $deleted;

    if (!$projectDefence->validate()) {
        throw new \Exception(implode(' AND ', $projectDefence->getErrorSummary(true)));
    }

    $projectDefence->save();
}

function insertProjectDocument(
    ?int $id,
    int $project_id,
    int $document_id,
    ?string $comment,
    int $team_private,
    ?int $defence_number,
    ?string $type,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $projectDocument = new models\records\ProjectDocument();

    $projectDocument->id = $id;
    $projectDocument->project_id = $project_id;
    $projectDocument->document_id = $document_id;
    $projectDocument->comment = $comment;
    $projectDocument->team_private = $team_private;
    $projectDocument->defence_number = $defence_number;
    $projectDocument->type = $type;
    $projectDocument->created_at = $now->modifyClone($created_at)->__toString();
    $projectDocument->updated_at = $now->modifyClone($updated_at)->__toString();
    $projectDocument->deleted = $deleted;

    if (!$projectDocument->validate()) {
        throw new \Exception(implode(' AND ', $projectDocument->getErrorSummary(true)));
    }

    $projectDocument->save();

}

function insertProjectStateHistory(
    ?int $id,
    int $project_id,
    string $state,
    ?string $comment,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $projectStateHistory = new models\records\ProjectStateHistory();

    $projectStateHistory->id = $id;
    $projectStateHistory->project_id = $project_id;
    $projectStateHistory->state = $state;
    $projectStateHistory->comment = $comment;
    $projectStateHistory->created_at = $now->modifyClone($created_at)->__toString();
    $projectStateHistory->updated_at = $now->modifyClone($updated_at)->__toString();
    $projectStateHistory->deleted = $deleted;

    if (!$projectStateHistory->validate()) {
        throw new \Exception(implode(' AND ', $projectStateHistory->getErrorSummary(true)));
    }

    $projectStateHistory->save();
}

function insertProposal(
    ?int $id,
    int $project_id,
    int $document_id,
    string $state,
    ?string $comment,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $proposal = new models\records\Proposal();

    $proposal->id = $id;
    $proposal->project_id = $project_id;
    $proposal->document_id = $document_id;
    $proposal->state = $state;
    $proposal->comment = $comment;
    $proposal->created_at = $now->modifyClone($created_at)->__toString();
    $proposal->updated_at = $now->modifyClone($updated_at)->__toString();
    $proposal->deleted = $deleted;

    if (!$proposal->validate()) {
        throw new \Exception(implode(' AND ', $proposal->getErrorSummary(true)));
    }

    $proposal->save();
}

function insertProposalComment(
    int $id,
    int $creator_id,
    int $proposal_id,
    string $content,
    int $committee_only,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $proposalComment = new models\records\ProposalComment();

    $proposalComment->id = $id;
    $proposalComment->creator_id = $creator_id;
    $proposalComment->proposal_id = $proposal_id;
    $proposalComment->content = $content;
    $proposalComment->committee_only = $committee_only;
    $proposalComment->created_at = $now->modifyClone($created_at)->__toString();
    $proposalComment->updated_at = $now->modifyClone($updated_at)->__toString();
    $proposalComment->deleted = $deleted;

    if (!$proposalComment->validate()) {
        throw new \Exception(implode(' AND ', $proposalComment->getErrorSummary(true)));
    }

    $proposalComment->save();
}

function insertProposalVote(
    int $id,
    int $user_id,
    int $proposal_id,
    ?string $vote,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $proposalVote = new models\records\ProposalVote();

    $proposalVote->id = $id;
    $proposalVote->user_id = $user_id;
    $proposalVote->proposal_id = $proposal_id;
    $proposalVote->vote = $vote;
    $proposalVote->created_at = $now->modifyClone($created_at)->__toString();
    $proposalVote->updated_at = $now->modifyClone($updated_at)->__toString();
    $proposalVote->deleted = $deleted;

    if (!$proposalVote->validate()) {
        throw new \Exception(implode(' AND ', $proposalVote->getErrorSummary(true)));
    }

    $proposalVote->save();
}

function insertRoleHistory(
    int $id,
    int $user_id,
    ?string $role,
    ?string $comment,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $roleHistory = new models\records\RoleHistory();

    $roleHistory->id = $id;
    $roleHistory->user_id = $user_id;
    $roleHistory->role = $role;
    $roleHistory->comment = $comment;
    $roleHistory->created_at = $now->modifyClone($created_at)->__toString();
    $roleHistory->updated_at = $now->modifyClone($updated_at)->__toString();
    $roleHistory->deleted = $deleted;

    if (!$roleHistory->validate()) {
        throw new \Exception(implode(' AND ', $roleHistory->getErrorSummary(true)));
    }

    $roleHistory->save();
}

function insertUser(
    int $id,
    string $loginType,
    ?string $role,
    ?string $name,
    ?string $degreePref,
    ?string $degreeSuf,
    string $email,
    ?string $password,
    bool $newsSubscribe,
    ?string $lastLogin,
    ?string $customUrl,
    ?string $lastCasRefresh,
    ?string $roleExpiration,
    int $roleIsFromCas,
    ?string $passwordRecoveryToken,
    ?string $passwordTokenExpiration,
    int $banned,
    string $createdAt,
    string $updatedAt,
    int $deleted
){
    $now = new DateTime();

    $user = new models\records\User();

    $user->id = $id;
    $user->login_type = $loginType;
    $user->role = $role;
    $user->name = $name;
    $user->degree_prefix = $degreePref;
    $user->degree_suffix = $degreeSuf;
    $user->email = $email;
    $user->password = $password;
    $user->news_subscription = $newsSubscribe;
    $user->last_login = $now->modifyClone($lastLogin)->__toString();
    $user->custom_url = $customUrl;
    $user->last_CAS_refresh = $now->modifyClone($lastCasRefresh)->__toString();
    $user->role_expiration = $now->modifyClone($roleExpiration)->__toString();
    $user->role_is_from_CAS = $roleIsFromCas;
    $user->password_recovery_token = $passwordRecoveryToken;
    $user->password_recovery_token_expiration = $now->modifyClone($passwordTokenExpiration)->__toString();
    $user->banned = $banned;
    $user->created_at = $now->modifyClone($createdAt)->__toString();
    $user->updated_at = $now->modifyClone($updatedAt)->__toString();
    $user->deleted = $deleted;

    if (!$user->validate()) {
        throw new \Exception(implode(' AND ', $user->getErrorSummary(true)));
    }

    $user->save();
}

function insertUserOnProject(
    ?int $id,
    int $user_id,
    int $project_id,
    string $project_role,
    ?string $comment,
    string $created_at,
    string $updated_at,
    int $deleted
){
    $now = new DateTime();

    $userOnProject = new models\records\ProjectUser();

    $userOnProject->id = $id;
    $userOnProject->user_id = $user_id;
    $userOnProject->project_id = $project_id;
    $userOnProject->project_role = $project_role;
    $userOnProject->comment = $comment;
    $userOnProject->created_at = $now->modifyClone($created_at)->__toString();
    $userOnProject->updated_at = $now->modifyClone($updated_at)->__toString();
    $userOnProject->deleted = $deleted;

    if (!$userOnProject->validate()) {
        throw new \Exception(implode(' AND ', $userOnProject->getErrorSummary(true)));
    }

    $userOnProject->save();
}