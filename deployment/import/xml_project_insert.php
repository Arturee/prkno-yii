<?php

namespace app;
use yii;
use app\utils\storage\ProjectStorage;
use yii\base\ErrorException;


//require __DIR__ . '/../data_insert_functions.php';

const PROJECT_DUPLICATE_PRESTRING = "duplicate";

/** Convert project state from their notation to ours.
 * @param string $stateSymbol
 * @return string
 * @throws \Exception if the state character in their notation is not defined
 */
function convertStateSymbolToString(string $stateSymbol) : string {
    switch ($stateSymbol) {
        case "=":
            return models\records\Project::STATE_IMPLEMENTATION;

        case "V":
            return models\records\Project::STATE_ACCEPTED;

        case "N":
            return models\records\Project::STATE_PROPOSAL;

        case "O":
            return models\records\Project::STATE_DEFENDED;

        case "X":
            return models\records\Project::STATE_CANCELED;

        default:
            throw new \Exception("Unidentified project state character");
    }
}

/** Calculates the deadline from the run start date parameter.
 * @param null|string $run_start_date_string
 * @return null|string
 */
function getDeadlineDateString(?string $run_start_date_string) : ?string {
    if ($run_start_date_string == NULL) {
        return NULL;
    }

    $deadlineDate = date_create_from_format('Y-m-d H:i:s', $run_start_date_string);
    if (!$deadlineDate) {
        $deadlineDate = date_create_from_format('Y-m-d', $run_start_date_string);
    }

    $deadlineDate = $deadlineDate->modify('+9 months');
    return $deadlineDate->format('Y-m-d H:i:s');
}

/** Finds the project entry in the database based on the shortname (duplicate entry accounted for).
 * @param string $projectShortName
 * @return null|yii\db\ActiveRecord
 */
function getProjectEntryByShortName(string $projectShortName) : ?yii\db\ActiveRecord {
    if (substr($projectShortName, 0, strlen(PROJECT_DUPLICATE_PRESTRING)) === PROJECT_DUPLICATE_PRESTRING) {
        $projectShortName = PROJECT_DUPLICATE_PRESTRING . $projectShortName;
    }
    return $project = models\records\Project::find()->where(['short_name' => $projectShortName])->one();
}

function insertSingleXmlProjectStateHistory(int $projectId, string $projectState, string $validSince) : void {
    insertProjectStateHistory(
        NULL,
        $projectId,
        $projectState,
        NULL,
        "now",
        "now",
        0
    );
}

/** Populates the ProjectStateHistory table with the appropriate entries (derived from Projekt.xml)
 * @param array $projectArray
 */
function insertXmlProjectStateHistories(array $projectArray, string $currentState) : void {
    $projectEntry = getProjectEntryByShortName($projectArray["ID"]);

    if (!$projectEntry) {
        echo "Project with short name " . $projectArray["ID"] . " not found in database.\n";
        return;
    }

    $projectId = $projectEntry->getAttribute("id");

    if ($projectArray["DNavrh"]) {
        insertSingleXmlProjectStateHistory($projectId, models\records\Project::STATE_PROPOSAL, $projectArray["DNavrh"]);
    }

    if ($projectArray["DVypsan"]) {
        insertSingleXmlProjectStateHistory($projectId, models\records\Project::STATE_ACCEPTED, $projectArray["DVypsan"]);
    }

    if ($projectArray["DObhajen"]) {
        insertSingleXmlProjectStateHistory($projectId, models\records\Project::STATE_DEFENDED, $projectArray["DObhajen"]);
    }

    //insert the actual state as well
    if ($projectArray["StavOd"]) {
        insertSingleXmlProjectStateHistory($projectId, $currentState, $projectArray["StavOd"]);
    }
}

/**
 * @param int $projectId
 * @param string $fileName
 * @return string the path to the imported file
 */
function importProposal(int $projectId, string $fileName) : ?string
{
    $sourceFile = "http://www.ksi.mff.cuni.cz/sw-projekty/zadani/" . $fileName;
    $sourceFile = encodeSpacesInUrl($sourceFile);

    $fileExtension = pathinfo($sourceFile, PATHINFO_EXTENSION);
    $newFileName = 'PROPOSAL.' . $fileExtension;

    $importSuccessful = NULL;

    try
    {
        $importSuccessful =
            ProjectStorage::copyProjectFileIn($sourceFile, $projectId, ProjectStorage::FILETYPE_DOCUMENT, $newFileName);
    } catch (ErrorException $errorException)
    {
        echo "Error during the proposal import. Most likely the file is not found at the address: $sourceFile\n";
        return NULL;
    }

    if (!$importSuccessful)
    {
        return NULL;
    }

    return ProjectStorage::getProjectFileUrl($projectId, ProjectStorage::FILETYPE_DOCUMENT, $newFileName);
}

/** Inserts the project proposal into the database if there is one in Projekt.xml.
 * @param array $projectArray
 */
function insertSingleXmlProposal(array $projectArray) : void {
    if (!$projectArray["Zadani"]) {
        return;
    }

    $projectEntry = getProjectEntryByShortName($projectArray["ID"]);

    if (!$projectEntry) {
        echo "Project with short name " . $projectArray["ID"] . " not found in database.\n";
        return;
    }

    $projectId = $projectEntry->getAttribute("id");

    $fallbackPath = importProposal($projectId, $projectArray["Zadani"]);
    if (!$fallbackPath)
    {
        return;
    }

    insertDocument(
        NULL,
        NULL,
        $fallbackPath,
        "now",
        "now",
        0
    );

    $documentId = models\records\Document::find()->where(['fallback_path' => $fallbackPath])->one()->getAttribute("id");

    insertProposal(
        NULL,
        $projectId,
        $documentId,
        $projectEntry->getAttribute("state") == models\records\Project::STATE_PROPOSAL ?
            models\records\Proposal::STATE_SENT : models\records\Proposal::STATE_ACCEPTED,
        NULL,
        "now",
        "now",
        0
    );
}

function encodeSpacesInUrl(string $url) : string
{
    $encodedUrl = preg_replace('/\s+/', '%20', $url);
    return $encodedUrl ? $encodedUrl : $url;
}

function isUrlFound(string $url) : bool
{
    $headers = @get_headers($url);
    $status = substr($headers[0], 9, 3);
    return $status >= 200 && $status < 400;
}

/** Imports the advice document if its possible and returns the URL address to it. If the advice cannot be imported
 * (is a webpage, etc.) the original URL gets returned. If the page is not found - 404 - it returns NULL.
 * @param int $projectId
 * @param string $fileName
 * @return string the path to the imported file
 */
function importAdvice(int $projectId, string $url) : ?string
{
    $encodedUrl = encodeSpacesInUrl($url);

    if (!isUrlFound($encodedUrl))
    {
        echo "Error during advice import. The page is not found at the address: $encodedUrl\n";
        return NULL;
    }

    $newFileName = 'advices/' . pathinfo($encodedUrl, PATHINFO_BASENAME);

    $importSuccessful = NULL;

    try
    {
        $importSuccessful =
            ProjectStorage::copyProjectFileIn($encodedUrl, $projectId, ProjectStorage::FILETYPE_DOCUMENT, $newFileName);
    } catch (ErrorException $errorException)
    {
        echo "Error during advice import. The file is not downloadable from the address: $encodedUrl\n";
        return $encodedUrl;
    }

    if (!$importSuccessful)
    {
        return $encodedUrl;
    }

    return ProjectStorage::getProjectFileUrl($projectId, ProjectStorage::FILETYPE_DOCUMENT, $newFileName);
}

function insertSingleXmlAdvice(int $projectId, string $url) : void {
    $fallbackPath = importAdvice($projectId, $url);
    if (!$fallbackPath)
    {
        return;
    }

    insertDocument(
        NULL,
        NULL,
        $fallbackPath,
        "now",
        "now",
        0
    );

    $documentId = models\records\Document::find()->where(['fallback_path' => $fallbackPath])->one()->getAttribute("id");
    insertProjectDocument(
        NULL,
        $projectId,
        $documentId,
        NULL,
        0,
        NULL,
        models\records\ProjectDocument::TYPE_ADVICE,
        "now",
        "now",
        0
    );
}

function insertXmlAdvices(array $projectArray) : void {
    $projectEntry = getProjectEntryByShortName($projectArray["ID"]);

    if (!$projectEntry) {
        echo "Project with short name " . $projectArray["ID"] . " not found in database.\n";
        return;
    }

    $projectId = $projectEntry->getAttribute("id");

    if ($projectArray["Zkusenosti"]) {
        insertSingleXmlAdvice($projectId, $projectArray["Zkusenosti"]);
    }

    if ($projectArray["Zkus2"]) {
        insertSingleXmlAdvice($projectId, $projectArray["Zkus2"]);
    }

    if ($projectArray["Zkus3"]) {
        insertSingleXmlAdvice($projectId, $projectArray["Zkus3"]);
    }
}

function insertSingleXmlProject(array $project) : void {

    if (!$project["ID"] || !$project["Nazev"]) {
        throw new \Exception("No ID(short name) or name for project!");
    }

    $shortName = $project["ID"];
    $name = $project["Nazev"];

    $state = convertStateSymbolToString($project["Stav"]);

    $extraCredits = $project["Body"] ? intval($project["Body"]) : 0;

    $runStartDate = $project["DZahajen"] ? $project["DZahajen"] : NULL;
    $deadline = getDeadlineDateString($runStartDate);

    $url = $project["URL"] ? $project["URL"] : "";

    try {
        insertProject(
            NULL,
            $state,
            $runStartDate,
            $deadline,
            $name,
            $shortName,
            $url,
            NULL,
            $state == models\records\Project::STATE_PROPOSAL ? 1 : 0,
            $runStartDate == NULL ? 0 : 1,
            $extraCredits,
            1,
            NULL,
            "now",
            "now",
            0
        );
    } catch (\Exception $e) {
        echo "Short name duplicate for $shortName. Inserting second entry into table with dummy short name.\n";
        insertProject(
            NULL,
            $state,
            $runStartDate,
            $deadline,
            $name,
            PROJECT_DUPLICATE_PRESTRING . $shortName,
            $url,
            NULL,
            $state == models\records\Project::STATE_PROPOSAL ? 1 : 0,
            $runStartDate == NULL ? 0 : 1,
            $extraCredits,
            1,
            NULL,
            "now",
            "now",
            0
        );
    }

    insertXmlProjectStateHistories($project, $state);
    insertSingleXmlProposal($project);
    insertXmlAdvices($project);
}

function insertXmlProjects(array $projectArray) : void {
    foreach ($projectArray["item"] as $project) {
        insertSingleXmlProject($project);
    }
}