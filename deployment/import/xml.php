<?php

namespace app;
use yii;

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
require __DIR__ . '/../../vendor/autoload.php';
//require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';

require __DIR__ . '/../data_insert_functions.php';
require __DIR__ . '/xml_user_insert.php';
require __DIR__ . '/xml_project_insert.php';
require __DIR__ . '/xml_useronproject_insert.php';

/**
 * @Author: Artur, Gergely
 */
$config = require __DIR__ . '/../../config/console.php';
$application = new yii\console\Application($config);

/**
 * @param string $xmlFileName
 * @return array returns null if the file doesn't exist or is invalid, otherwise returns array constructed from XML
 */
function xmlToArray(string $xmlFileName): array {

    if (!file_exists($xmlFileName)) {
        return null;
    }

    $xml = simplexml_load_file($xmlFileName, "SimpleXMLElement", LIBXML_NOCDATA);
    if ($xml === false) {
        return null;
    }

    $json = json_encode($xml);
    return json_decode($json,TRUE);
}

function loadXmlFileToArray(string $xmlFileName) : array {

    $array = xmlToArray($xmlFileName);
    if (!$array) {
        throw new \InvalidArgumentException("XML file not found or XML file invalid: " . $xmlFileName);
        //return;
    }

    return $array;
}

function clearAllTables() : void {
    models\records\ProjectUser::deleteAll();
    models\records\RoleHistory::deleteAll();
    models\records\ProposalVote::deleteAll();
    models\records\ProposalComment::deleteAll();
    models\records\Proposal::deleteAll();
    models\records\ProjectStateHistory::deleteAll();
    models\records\ProjectDocument::deleteAll();
    models\records\ProjectDefence::deleteAll();
    models\records\Project::deleteAll();
    models\records\PageVersion::deleteAll();
    models\records\Page::deleteAll();
    models\records\News::deleteAll();
    models\records\AdvertisementComment::deleteAll();
    models\records\Advertisement::deleteAll();
    models\records\Document::deleteAll();
    models\records\DefenceAttendance::deleteAll();
    models\records\DefenceDate::deleteAll();
    //models\Users::deleteAll();
}

clearAllTables();

$users = loadXmlFileToArray('lidi.xml');
//insertXmlUsers($users);

$projects = loadXmlFileToArray('projekt.xml');
insertXmlProjects($projects);

$users_on_projects = loadXmlFileToArray('vzprlidi.xml');
//insertXmlUserOnProjects($users_on_projects);

exit();