<?php

namespace app;

//require __DIR__ . '/../data_insert_functions.php';

const USER_DUMMY_EMAIL_ENDING = '@null.com';

/** Since email is a required and unique identifier in the User model, we create a dummy one.
 * @param int $userId
 * @return string returns a dummy email for the user who doesn't have one specified in XML
 */
function createDummyEmail(int $userId) : string {
    return "null-" . $userId . USER_DUMMY_EMAIL_ENDING;
}

/** Finds the first(!) email in the string given in the <Mail> item and trims it so it is a valid email address.
 * @param string $emails
 * @return string
 * @throws \Exception if the email format is invalid.
 */
function extractEmailFromString(string $emails) : string {
    $emailPattern = '/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}/';
    $matchArray = array();
    preg_match($emailPattern, $emails, $matchArray);

    $email = $matchArray[0];

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        throw new \Exception("$email is not a valid email address");
    }

    return $email;
}

/** Splits the first name into degree prefix and the first name itself.
 * @param string $firstName
 * @return array two item array, first is the degree (NULL if none), second is the first name
 */
function separateDegreePrefixFromName(string $firstName) : array {
    $nameParts = explode(".", $firstName);
    if (count($nameParts) == 1) {
        return array(NULL, $firstName);
    }
    if (count($nameParts) == 2) {
        if ($nameParts[0] == "" || $nameParts[1] == "") {
            return array(NULL, $firstName);
        }
    }

    $name = array_pop($nameParts);
    $name = trim($name);

    $degrees = implode(".", $nameParts);
    $degrees .= ".";

    return array($degrees, $name);
}

/** Splits the last name into degree suffix and the last name itself.
 * @param string $lastName
 * @return array two item array, first is the last name, second is the degree (NULL if none)
 */
function separateDegreeSuffixFromName(string $lastName) : array {
    $nameParts = explode(",", $lastName);
    if (count($nameParts) == 1) {
        return array($lastName, NULL);
    }

    return array(trim($nameParts[0]), trim($nameParts[1]));
}

function insertSingleXmlUser(array $user) : void {

    $id = intval($user["ID"]);
    if ($id == 0) {
        throw new \Exception("Invalid user ID, string to int conversion failed with: " . $user["ID"]);
    }

    list($degreePrefix, $firstName) = $user["Jmeno"] ? separateDegreePrefixFromName($user["Jmeno"]) : array(NULL, "");
    list($lastName, $degreeSufix) = $user["Prijmeni"] ? separateDegreeSuffixFromName($user["Prijmeni"]) : array("", NULL);

    $fullName = $firstName . " " . $lastName;
    $fullName = $fullName == " " ? NULL : $fullName;

    $email = $user["Mail"] ? extractEmailFromString($user["Mail"]) : createDummyEmail($id);

    try {
        insertUser(
            $id,
            models\records\User::LOGIN_TYPE_MANUAL,
            NULL,
            $fullName,
            $degreePrefix,
            $degreeSufix,
            $email,
            $user["Login"] ? $user["Login"] : NULL,
            NULL,
            models\Lang::LANG_CS,
            NULL,
            1,
            NULL,
            NULL,
            NULL,
            0,
            NULL,
            NULL,
            0,
            'now',
            'now',
            0
        );
    } catch (\Exception $e) {
        $firstUserEntry = models\records\User::find()->where(['email' => $email])->one();
        $firstUserEntryId = $firstUserEntry->getAttribute("id");

        echo "Email duplicate for ID $id - $email. Change ID $id to $firstUserEntryId in user-project relations.\n";
    }

}

function insertXmlUsers(array $userArray) : void {
    foreach ($userArray["item"] as $user) {
        insertSingleXmlUser($user);
    }
}