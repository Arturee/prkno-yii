<?php

namespace app;

//require __DIR__ . '/../data_insert_functions.php';

/** Convert user role on the project from their notation to ours.
 * @param string $role
 * @return string
 * @throws \Exception if the role character in their notation is not defined.
 */
function convertRoleSymbolToString(string $role) : ?string {
    switch ($role) {
        case "V":
            return models\records\ProjectUser::ROLE_SUPERVISOR;

        case "s":
            return models\records\ProjectUser::ROLE_TEAM_MEMBER;

        case "W":
            echo "Role symbol W - ignoring entry.\n";
            return NULL;

        default:
            throw new \Exception("Role symbol not recognized!");
    }
}

/** Changes the role of the user (in the User entry) based on their role on one of the projects (we assume that every
 * supervisor is an academic and every team member is a student).
 * @param int $userId
 * @param string $projectRole
 */
function setUserRoleBasedOnProjectRole(int $userId, string $projectRole) : void {
    $user = models\records\User::find()->where(['id' => $userId])->one();
    switch ($projectRole) {
        case models\records\ProjectUser::ROLE_TEAM_MEMBER:
            $user->setAttribute("role", models\records\User::ROLE_STUDENT);
            $user->save();
            break;

        case models\records\ProjectUser::ROLE_SUPERVISOR:
        case models\records\ProjectUser::ROLE_CONSULTANT:
            $user->setAttribute("role", models\records\User::ROLE_ACADEMIC);
            $user->save();
            break;
    }
}

function insertRelationshipIntoDatabase(int $personId, int $projectId, string $role) : void {
    try {
        setUserRoleBasedOnProjectRole($personId, $role);

        insertUserOnProject(
            NULL,
            $personId,
            $projectId,
            $role,
            NULL,
            "now",
            "now",
            0
        );
    } catch (\Exception $e) {
        echo "User with ID " . $personId . " not found in database.\n";
    }
}

function insertSingleXmlUserOnProject(array $userOnProject) : void {

    if (!$userOnProject["Projekt"] || !$userOnProject["Clovek"] || !$userOnProject["Vztah"]) {
        throw new \Exception("Required info for UserOnProject is missing!");
    }

    $project = getProjectEntryByShortName($userOnProject["Projekt"]);
    if (!$project) {
        echo "Project with short name " . $userOnProject["Projekt"] . " not found in database.\n";
        return;
    }
    $projectId = $project->getAttribute("id");

    $role = convertRoleSymbolToString($userOnProject["Vztah"]);
    if (!$role) {
        return;
    }

    insertRelationshipIntoDatabase($userOnProject["Clovek"], $projectId, $role);

    if ($userOnProject["V2"]) {
        insertRelationshipIntoDatabase($userOnProject["V2"], $projectId, models\records\ProjectUser::ROLE_CONSULTANT);
    }
}

function insertXmlUserOnProjects(array $usersOnProjectArray) : void {
    foreach ($usersOnProjectArray["item"] as $userOnProject) {
        insertSingleXmlUserOnProject($userOnProject);
    }
}