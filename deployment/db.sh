#!/bin/sh

COL_YELLOW='\033[1;33m'
COL_DARK='\033[1;30m'
COL_RED='\033[0;31m'
COL_NONE='\033[0m'

if [ "$#" -ne 1 ]
  then echo -e "${COL_RED}Usage:${COL_NONE}${COL_YELLOW}sudo${COL_NONE} bash db.sh ${COL_YELLOW}[password_for_mysql_user_root]${COL_NONE}."
  exit
fi

mysql -u root -p$1 prkno < _drop.sql
mysql -u root -p$1 prkno < db_structure.sql
# php data.php