SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `advertisement_comment`
-- ----------------------------
DROP TABLE IF EXISTS `advertisement_comment`;
CREATE TABLE `advertisement_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `advertisement_comment_ad_id_idx` (`ad_id`),
  KEY `advertisement_comment_user_id_idx` (`user_id`),
  CONSTRAINT `advertisement_comment_ad_id_fk` FOREIGN KEY (`ad_id`) REFERENCES `advertisement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `advertisement_comment_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `advertisement`
-- ----------------------------
DROP TABLE IF EXISTS `advertisement`;
CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `type` enum('project','person') DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `subscribed_via_email` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `advertisement_user_id_idx` (`user_id`),
  CONSTRAINT `advertisement_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `audit`
-- ----------------------------
DROP TABLE IF EXISTS `audit`;
CREATE TABLE `audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `function_name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `comment` varchar(2000) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- ----------------------------
-- Table structure for `defence_attendance`
-- ----------------------------
DROP TABLE IF EXISTS `defence_attendance`;
CREATE TABLE `defence_attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `defence_date_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vote` enum('yes','no', 'unknown') NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `must_come` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `defence_attendance_user_id_idx` (`user_id`),
  KEY `defence_attendance_defence_date_id_idx` (`defence_date_id`) USING BTREE,
  CONSTRAINT `defence_attendance_defence_date_id_fk` FOREIGN KEY (`defence_date_id`) REFERENCES `defence_date` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `defence_attendance_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `defence_date`
-- ----------------------------
DROP TABLE IF EXISTS `defence_date`;
CREATE TABLE `defence_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date` date NOT NULL,
  `room` varchar(200) DEFAULT '',
  `signup_deadline` date NOT NULL,
  `active_voting` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `document`
-- ----------------------------
DROP TABLE IF EXISTS `document`;
CREATE TABLE `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `content_md` text,
  `fallback_path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title_en` varchar(100) NOT NULL,
  `title_cs` varchar(100) NOT NULL,
  `content_en` text NOT NULL,
  `content_cs` text NOT NULL,
  `public` tinyint(1) NOT NULL,
  `type` enum('generated','normal','important') NOT NULL,
  `hide_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `news_user_id_idx` (`user_id`),
  CONSTRAINT `news_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `page`
-- ----------------------------
DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `title_en` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `title_cs` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `path_pdf_download` varchar(300) COLLATE utf8_czech_ci DEFAULT NULL,
  `url` varchar(300) COLLATE utf8_czech_ci NOT NULL,
  `order_in_menu` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- ----------------------------
-- Table structure for `page_version`
-- ----------------------------
DROP TABLE IF EXISTS `page_version`;
CREATE TABLE `page_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_en` int(11) DEFAULT NULL,
  `content_cs` int(11) DEFAULT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `pages_page_id_idx` (`page_id`),
  KEY `pages_user_id_idx` (`user_id`),
  KEY `pages_content_cs_idx` (`content_cs`) USING BTREE,
  KEY `pages_content_en_idx` (`content_en`) USING BTREE,
  CONSTRAINT `pages_content_cs_idx` FOREIGN KEY (`content_cs`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pages_content_en_idx` FOREIGN KEY (`content_en`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pages_page_id_fk` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pages_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `project_defence`
-- ----------------------------
DROP TABLE IF EXISTS `project_defence`;
CREATE TABLE `project_defence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `project_id` int(11) NOT NULL,
  `project_state` varchar(50) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `defence_date_id` int(11) NOT NULL,
  `opponent_id` int(11) DEFAULT NULL,
  `statement_author_id` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `defence_statement` int(11) DEFAULT NULL,
  `opponent_review` int(11) DEFAULT NULL,
  `supervisor_review` int(11) DEFAULT NULL,
  `result` enum('defended', 'conditionally_defended', 'failed') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_defence_project_id_idx` (`project_id`),
  KEY `project_defence_defence_date_id_idx` (`defence_date_id`),
  KEY `project_defence_statement_author_id_idx` (`statement_author_id`),
  KEY `project_defence_defence_statement_idx` (`defence_statement`),
  KEY `project_defence_opponent_review_idx` (`opponent_review`),
  KEY `project_defence_supervisor_review_idx` (`supervisor_review`),
  KEY `project_defence_opponent_id_fk` (`opponent_id`),
  CONSTRAINT `project_defence_defence_date_id_fk` FOREIGN KEY (`defence_date_id`) REFERENCES `defence_date` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_defence_defence_statement_fk` FOREIGN KEY (`defence_statement`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_defence_opponent_id_fk` FOREIGN KEY (`opponent_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `project_defence_opponent_review_fk` FOREIGN KEY (`opponent_review`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_defence_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_defence_statement_author_id_fk` FOREIGN KEY (`statement_author_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_defence_supervisor_review_fk` FOREIGN KEY (`supervisor_review`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `project_document`
-- ----------------------------
DROP TABLE IF EXISTS `project_document`;
CREATE TABLE `project_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `project_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `team_private` tinyint(1) NOT NULL,
-- `submitted_at` datetime DEFAULT NULL,
  `defence_number` int(11) DEFAULT NULL,
  `type` enum('for_defense_pdf', 'for_defense_zip','read_only','advice') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_document_project_id_idx` (`project_id`),
  KEY `project_document_document_id_idx` (`document_id`),
  CONSTRAINT `project_document_document_id_fk` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_document_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `project_state_history`
-- ----------------------------
DROP TABLE IF EXISTS `project_state_history`;
CREATE TABLE `project_state_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `project_id` int(11) NOT NULL,
  `state` enum('proposal','accepted','analysis','analysis_handed_in','implementation','implementation_handed_in','conditionally_defended','conditional_documents_handed_in','defended','failed','canceled') NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_state_history_project_id_idx` (`project_id`),
  CONSTRAINT `project_state_history_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `project`
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `state` enum('proposal','accepted','analysis','analysis_handed_in','implementation','implementation_handed_in','conditionally_defended','conditional_documents_handed_in','defended','failed','canceled') NOT NULL,
  `run_start_date` datetime DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `short_name` varchar(45) NOT NULL,
  `description` varchar(2000) NOT NULL DEFAULT '',
  `content` int(11) DEFAULT NULL,
  `waiting_for_proposal_evaluation` tinyint(1) NOT NULL DEFAULT 0,
  `requested_to_run` tinyint(1) NOT NULL DEFAULT 0,
  `extra_credits` int(11) NOT NULL DEFAULT 0,
  `team_members_visible` tinyint(1) NOT NULL DEFAULT 0,
  `keywords` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects_short_name_uq` (`short_name`),
  KEY `projects_content_idx` (`content`),
  CONSTRAINT `projects_content_fk` FOREIGN KEY (`content`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `proposal_comment`
-- ----------------------------
DROP TABLE IF EXISTS `proposal_comment`;
CREATE TABLE `proposal_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `creator_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `content` varchar(2000) NOT NULL,
  `committee_only` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id_idx` (`creator_id`),
  KEY `proposal_id_idx` (`proposal_id`),
  CONSTRAINT `creator_id` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `proposal_comment_creator_id_idx` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `proposal_comment_proposal_id_idx` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `proposal_id` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `proposal_vote`
-- ----------------------------
DROP TABLE IF EXISTS `proposal_vote`;
CREATE TABLE `proposal_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `vote` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proposal_vote_user_id_idx` (`user_id`),
  KEY `proposal_vote_proposal_id_idx` (`proposal_id`),
  CONSTRAINT `proposal_vote_proposal_id_fk` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `proposal_vote_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `proposal`
-- ----------------------------
DROP TABLE IF EXISTS `proposal`;
CREATE TABLE `proposal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `project_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `state` enum('draft','sent','accepted','declined') NOT NULL,
  `comment` text,
  PRIMARY KEY (`id`),
  KEY `proposal_project_id_idx` (`project_id`),
  KEY `proposal_document_id_idx` (`document_id`),
  CONSTRAINT `proposal_document_id_fk` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `proposal_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `role_history`
-- ----------------------------
DROP TABLE IF EXISTS `role_history`;
CREATE TABLE `role_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `role` enum('student','academic','committee_member','committee_chair') DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `role_history_user_id_fk` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(100) DEFAULT NULL,
  `degree_prefix` varchar(45) DEFAULT NULL,
  `degree_suffix` varchar(45) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varbinary(200) DEFAULT NULL,
  `news_subscription` tinyint(1) NOT NULL DEFAULT 0,
  `last_login` datetime DEFAULT NULL,
  `custom_url` varchar(500) DEFAULT NULL,
  `last_CAS_refresh` datetime DEFAULT NULL,
  `role` enum('student','academic','committee_member','committee_chair') DEFAULT NULL,
  `role_expiration` datetime DEFAULT NULL,
  `role_is_from_CAS` tinyint(1) NOT NULL,
  `login_type` enum('cas','manual') NOT NULL,
  `password_recovery_token` varchar(100) DEFAULT NULL,
  `password_recovery_token_expiration` datetime DEFAULT NULL,
  `banned` tinyint(1) NOT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_uq` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `project_user`
-- ----------------------------
DROP TABLE IF EXISTS `project_user`;
CREATE TABLE `project_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_role` enum('team_member','supervisor','opponent','consultant') NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_user_user_id_idx` (`user_id`),
  KEY `project_user_project_id_idx` (`project_id`),
  CONSTRAINT `project_user_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Insert admin user; email: admin@prkno.mff, password: Test1234
-- ----------------------------
INSERT INTO `user` (`id`, `created_at`, `updated_at`, `deleted`, `name`, `degree_prefix`, `degree_suffix`, `email`, `password`, `news_subscription`, `last_login`, `custom_url`, `last_CAS_refresh`, `role`, `role_expiration`, `role_is_from_CAS`, `login_type`, `password_recovery_token`, `password_recovery_token_expiration`, `banned`, `auth_key`) VALUES
(1, '2018-08-27 00:00:00', '2018-08-27 00:00:00', 0, 'admin', NULL, NULL, 'admin@prkno.mff', 0x243279243133246f496e596c324d667048413752667a4b6c4f7871497575555270436231456a6144616c31573173506f712e3335756a544a3257486d, 0, NULL, NULL, NULL, 'committee_chair', NULL, 0, 'manual', NULL, NULL, 0, NULL);
