
# deployment on @cuni.cz #

(0. sudo bash cuni_setup.sh (once))
1. composer update (/usr/local/bin/composer update)
2. sudo bash db.sh
(3. Comment out first two lines in web/index.php to hide debugger messages in production)


# deployment on XAMPP localhost #

1. composer update
2. phpmyadmin: drop prkno
3. phpmyadmin: import db_structure.sql
4. php data.php