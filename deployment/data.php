<?php

namespace app;

use yii;
use app\models;
use app\utils\DateTime;
use app\consts\Lang;


require __DIR__ . '/data_insert_functions.php';



/**
 * @Author: Artur, Gergely
 *
 * Later on we will turn this into Yii commandline command
 * and maybe put it into composer after-install scripts.
 *
 */
$config = require __DIR__ . '/../config/console.php';
$application = new yii\console\Application($config);





//NOTE: when inserting the values to the database, we use the constant only when it represents a string





function insertAdvertisements()
{
    insertAdvertisement(1,7,"Facebook performance analysis","Performance analysis of Facebook. We will explore all the public data.", models\records\Advertisement::TYPE_PROJECT,"facebook, performance",0,1,"2018-01-16 00:00:00","2018-01-29 00:00:00",0);
    insertAdvertisement(2,1,"Student looking for C# projects","Hi. I am student looking for C# project", models\records\Advertisement::TYPE_PERSON,NULL,1,0,"2018-01-21 00:00:00","2018-01-21 00:00:00",1);
    insertAdvertisement(3,8,"Advertising the null","Advertising something", models\records\Advertisement::TYPE_PROJECT,NULL,1,1,"2018-01-15 00:00:00","2018-02-01 00:00:00",0);
    insertAdvertisement(4,3,"Advertising the first document","this is the content md of the first first first first first first first first  ID document", models\records\Advertisement::TYPE_PROJECT,NULL,0,0,"2018-01-21 00:00:00","2018-01-21 00:00:00",0);
}



function insertAdComments()
{
    insertAdComment(1,1,8,"This project looks really promising.","2018-01-30 00:00:00","2018-01-30 00:00:00",0);
    insertAdComment(2,2,1,"Want to join my project?","2018-01-21 00:00:00","2018-01-28 00:00:00",1);
    insertAdComment(3,4,5,"Looks awful.","2018-01-23 00:00:00","2018-01-23 00:00:00",0);
}


function insertDefenceAttendances()
{
    insertDefenceAttendance(1,1,8, models\records\DefenceAttendance::VOTE_YES,"I'll definitely be there.",1,"2018-01-22 00:00:00","2018-01-23 00:00:00",0);
    insertDefenceAttendance(2,1,10, models\records\DefenceAttendance::VOTE_NO,"I have a dentist appointment.",0,"2018-01-22 00:00:00","2018-01-22 00:00:00",1);
    insertDefenceAttendance(3,4,6, models\records\DefenceAttendance::VOTE_UNKNOWN,NULL,1,"2018-01-07 00:00:00","2018-01-07 00:00:00",0);
    insertDefenceAttendance(4,3,7, models\records\DefenceAttendance::VOTE_YES,NULL,0,"2018-02-05 14:00:00","2018-02-05 14:00:00",1);
    insertDefenceAttendance(5,3,2, models\records\DefenceAttendance::VOTE_NO,"Jsem na státnicích.",0,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
    insertDefenceAttendance(6,3,3, models\records\DefenceAttendance::VOTE_YES,"Kdykoliv po 17:00, předtím přednáším.",0,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
    insertDefenceAttendance(7,3,5, models\records\DefenceAttendance::VOTE_UNKNOWN,NULL,0,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
    insertDefenceAttendance(8,3,6, models\records\DefenceAttendance::VOTE_UNKNOWN,NULL,0,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
    insertDefenceAttendance(9,3,7, models\records\DefenceAttendance::VOTE_UNKNOWN,NULL,0,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
    insertDefenceAttendance(10,3,8, models\records\DefenceAttendance::VOTE_UNKNOWN,NULL,0,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
    // PNPREL
    insertDefenceAttendance(11,5,6, models\records\DefenceAttendance::VOTE_YES,NULL,1,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
    insertDefenceAttendance(12,5,7, models\records\DefenceAttendance::VOTE_YES,NULL,1,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
    insertDefenceAttendance(13,5,16, models\records\DefenceAttendance::VOTE_YES,NULL,1,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
    insertDefenceAttendance(14,5,17, models\records\DefenceAttendance::VOTE_YES,NULL,1,"2018-02-05 14:00:00","2018-02-05 14:00:00",0);
}



function insertDefenceDates()
{
    global $nowdate;

    insertDefenceDate(1,$nowdate->modifyClone('+25 days'),$nowdate->modifyClone('+24 days'),1,"2018-02-18 00:00:00","2018-02-19 00:00:00",0);
    insertDefenceDate(2,$nowdate->modifyClone('+45 days'),$nowdate->modifyClone('+41 days'),0,"2018-01-21 00:00:00","2018-01-30 00:00:00",1);
    insertDefenceDate(3,$nowdate->modifyClone('+12 days'),$nowdate->modifyClone('+3 days'),1,"2018-02-04 00:00:00","2018-02-04 00:00:00",0);
    insertDefenceDate(4,$nowdate->modifyClone('+3 months 1 day'),$nowdate->modifyClone('+3months'),1,"2018-02-21 00:00:00","2018-02-21 00:00:00",0);
    insertDefenceDate(5,$nowdate->modifyClone('-3 months 1 day'),$nowdate->modifyClone('-3 months 2 days'),1,"2018-02-21 00:00:00","2018-02-21 00:00:00",0);
    insertDefenceDate(6,$nowdate->modifyClone('-2 days'),$nowdate->modifyClone('-4 days'),0,"2018-02-21 00:00:00","2018-02-21 00:00:00",0);

    //tests for CRON
	global $cronTesting;

    insertDefenceDate(7,$cronTesting->modifyClone('+1 hour'),$cronTesting->modifyClone('-1 day'),1,"2018-02-18 00:00:00","2018-02-19 00:00:00",0);
    insertDefenceDate(8,$cronTesting->modifyClone('+3 hours'),$cronTesting->modifyClone('-1 day'),0,"2018-01-21 00:00:00","2018-01-30 00:00:00",1);
    insertDefenceDate(9,$cronTesting->modifyClone('+1 week'),$cronTesting->modifyClone('+3 days'),1,"2018-02-04 00:00:00","2018-02-04 00:00:00",0);
    insertDefenceDate(10,$cronTesting->modifyClone('+2 days'),$cronTesting->modifyClone('+1 day'),1,"2018-02-21 00:00:00","2018-02-21 00:00:00",0);
}


function insertDocuments()
{
    insertDocument(1,"this is the content md of the first first first first first first first first  ID document","fallback_path/doc","2018-01-28 00:00:00","2018-01-30 17:41:17",0);
    insertDocument(2,NULL,NULL,"2018-01-09 00:00:00","2018-01-17 00:00:00",0);
    insertDocument(3,"this is the content md of the third third third third third third third third third third third third  document",NULL,"2018-01-30 17:42:27","2018-01-30 17:42:27",0);
    insertDocument(4,NULL,"random_fallback_path","2018-01-07 00:00:00","2018-01-15 00:00:00",1);
    insertDocument(5,"Performance analysis of Facebook. We will explore all the public data.",NULL,"2017-12-11 00:00:00","2018-01-21 00:00:00",0);
    insertDocument(6,NULL,"fallback_ex/folder","2018-01-21 00:00:00","2018-01-21 00:00:00",1);
    insertDocument(7,"Musím opustit projekt.",NULL,"2018-01-28 00:00:00","2018-01-28 00:00:00",0);
    insertDocument(8,"Posudek první",NULL,"2018-01-23 00:00:00","2018-01-23 00:00:00",0);
    insertDocument(9,"Posudek oponenta",NULL,"2018-01-20 00:00:00","2018-01-20 00:00:00",0);
    insertDocument(10,"Defense review",NULL,"2018-02-12 00:00:00","2018-02-12 00:00:00",0);
    insertDocument(11,NULL,NULL,"2018-01-31 00:00:00","2018-02-01 00:00:00",0);
    insertDocument(12,NULL,NULL,"2017-10-23 00:00:00","2017-10-31 00:00:00",1);
    insertDocument(13,"Third proposal.",NULL,"2018-01-30 00:00:00","2018-01-30 00:00:00",0);
    insertDocument(14,"Supervisor review for project 8",NULL,"2017-12-26 00:00:00","2017-12-26 00:00:00",0);
    insertDocument(15,"Proposal for project 3",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(16,"Proposal for project 4",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(17,"Proposal for project 5",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(18,"Proposal for project 6",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(19,"Proposal for project 7",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(20,"Proposal for project 8",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(21,"Proposal for project 9",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(22,"Proposal for project 10",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(23,"Proposal for project 11",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(24,"Proposal for project 12",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    //PNPREL
//    insertDocument(25,"The project is **finished**, but the *tests are missing*, which is understandable because: \n\n* One student left he project after 3 months.\n* Bonus features had to be implemented. \n\n*I am in favour of accepting the project without tests, because it is too much work.*",NULL,"2017-12-11 19:30:00","2017-12-11 19:30:00",0);
//    insertDocument(26,"The code is **really hard to read** and not refactored at all. However all specified\nfunctionality is there.\n\n# VERTICT #\n\nI would accept the work on condition that the code gets refactored and documented and basic tests are added.\n",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
//    insertDocument(27,"# Final statement for NPREL #\n\nStudents had good reason to leave out tests, but the quality of code is extremetly bad.\nThat is why the committee decided that the team <span style='color: orange;' title=\"That's harsh, I know\">failed to defend.</span><script>alert('haha ur hacked')</script>",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(25,"The analysis is finished, but there aren't many diagrams.",NULL,"2017-12-11 19:30:00","2017-12-11 19:30:00",0);
    insertDocument(26,"I would decline due to the lack of diagrams, and let the team work a bit harder on them.",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(27,"# Final statement for NPREL #\n Due to the lack of nice diagrams, the project <span style='color: orange;' title=\"That's harsh, I know\">failed to defend.</span><script>alert('haha ur hacked')</script>",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(28,null,'docs/sources.zip',"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(29,null,'docs/sources2.zip',"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(30,null,'docs/text1.zip',"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(31,null,'docs/text2.zip',"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(32,null,'docs/src-num2.zip',"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(33,null,'docs/text-num2.pdf',"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(34,null,'docs/sources-num3.zip',"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(35,null,'docs/textpdf-num3.pdf',"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(36,"Diagrams done",NULL,"2017-12-11 19:30:00","2017-12-11 19:30:00",0);
    insertDocument(37,"I vote the let them pass.\n",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(38,"Diagrams are sufficient, the project passes. \n",NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertDocument(39,null,NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
}

function insertMultipleNews()
{
    insertNews(1,10,"No defences on 11th week of the year","Nejsou obhajoby během 11. týdna roku","this is the content md of the first first first first first first first first  ID document","Cesky text novinky v markdownu",1, models\records\News::TYPE_NORMAL,"2018-05-21 00:00:00","2018-01-14 00:00:00","2018-01-14 00:00:00",0);
    insertNews(2,8,"End of semester moved one week earlier","Konec semestru se posunul o týden dřív","Content in english","Obsah česky",1, models\records\News::TYPE_IMPORTANT,"2018-05-30 00:00:00","2018-01-21 00:00:00","2018-01-29 00:00:00",1);
    insertNews(3,5,"Project leaving","Opustení projektu","I have to leave the project","Musím opustit projekt",0, models\records\News::TYPE_GENERATED,NULL,"2018-01-29 00:00:00","2018-01-29 00:00:00",0);
    insertNews(4,6,"English title","Český titulek","English content","Obsah česky",0, models\records\News::TYPE_NORMAL,"2018-02-01 00:00:00","2018-01-29 00:00:00","2018-01-30 00:00:00",1);
}

function insertPages()
{
    insertPage(1,"Rules of software project","Pravidlá softwarových projektu","pages/rules.pdf","projectrules",2,0,"2018-01-14 00:00:00","2018-01-29 00:00:00",0);
    insertPage(2,NULL,"Prubeh práce na projektu",NULL,"projectflow",3,1,"2018-02-01 00:00:00","2018-02-01 00:00:00",1);
    insertPage(3,"Tutorial",NULL,"pages/tutorial.pdf","tutorial",1,0,"2018-01-14 00:00:00","2018-01-14 00:00:00",0);
    insertPage(4,"Committee members",NULL,NULL,"committee",4,1,"2017-11-13 00:00:00","2018-01-28 00:00:00",0);
}


function insertPageVersions()
{
    insertPageVersion(1,2,8,6,3,1,"2017-12-17 00:00:00","2018-01-22 00:00:00",0);
    insertPageVersion(2,2,10,4,6,0,"2018-01-23 00:00:00","2018-01-30 00:00:00",1);
    insertPageVersion(3,3,5,5,1,1,"2018-01-21 00:00:00","2018-01-21 00:00:00",1);
    insertPageVersion(4,1,6,5,2,0,"2018-01-23 00:00:00","2018-01-23 00:00:00",0);
}


function insertProjects()
{
    insertProject(1, models\records\Project::STATE_PROPOSAL,NULL,NULL,"Study of dolphins","STODO","This study studies dolphins.",NULL,1,0,0,0,"dolphin, sea, fish, mammal","2018-01-30 17:37:35","2018-01-30 17:37:35",0);
    insertProject(2, models\records\Project::STATE_ACCEPTED,NULL,NULL,"Theory of everything","THEOR","Project description",NULL,0,0,0,1,NULL,"2018-01-15 00:00:00","2018-01-30 17:59:48",0);
    insertProject(3, models\records\Project::STATE_ANALYSIS,"2018-01-29 00:00:00",NULL,"Numbers and stuff","NUST","Explores the great mysteries of numbers.",1,0,1,3,0,"number","2018-01-14 00:00:00","2018-01-22 00:00:00",0);
    insertProject(4, models\records\Project::STATE_ANALYSIS_HANDED_IN,"2018-01-09 00:00:00","2018-09-09 00:00:00","Whales and their behaviour","WHAL","",4,0,1,3,1,NULL,"2017-11-21 00:00:00","2018-01-14 00:00:00",0);
    insertProject(5, models\records\Project::STATE_IMPLEMENTATION,"2017-09-11 00:00:00","2018-06-11 00:00:00","P=NP relevation","PNPREL","The hundred year long question answered.",1,0,1,15,0,"p, np, it","2017-08-30 00:00:00","2018-01-30 18:09:37",0);
    insertProject(6, models\records\Project::STATE_IMPLEMENTATION_HANDED_IN,NULL,NULL,"Theory of a project without dates","THEPW","",1,0,1,0,0,NULL,"2017-11-16 00:00:00","2018-01-15 00:00:00",0);
    insertProject(7, models\records\Project::STATE_PROPOSAL,NULL,NULL,"Idea of nothing","IONO","This is idea of nothing",NULL,0,0,0,0,"idea","2018-01-30 17:37:35","2018-01-30 17:37:35",0);
    insertProject(8, models\records\Project::STATE_CONDITIONALLY_DEFENDED,"2017-09-16 00:00:00","2018-06-16 00:00:00","Theory about tests","THEOR1","the original short name is THEOR, the other one is marked deleted though so this should be revisited",NULL,0,1,0,0,"test","2017-08-16 00:00:00","2018-01-30 18:10:20",0);
    insertProject(9, models\records\Project::STATE_CONDITIONAL_DOCUMENTS_HANDED_IN,"2017-11-09 00:00:00","2018-08-09 00:00:00","Running out of ideas thesis","RUNOOI","",3,0,1,10,1,"idea","2017-11-01 00:00:00","2018-01-30 18:02:23",0);
    insertProject(10, models\records\Project::STATE_DEFENDED,"2017-06-24 00:00:00","2018-03-24 00:00:00","Mailing habits of young males","MAILH","We study everything about mail.",NULL,0,1,0,0,NULL,"2017-05-24 00:00:00","2018-01-30 18:04:11",0);
    insertProject(11, models\records\Project::STATE_FAILED,"2017-09-16 00:00:00","2018-06-11 00:00:00","Christmas in North Dakota","CHRISIN","What do we celebrate here.",NULL,0,1,0,1,"christmas, dakota","2017-09-05 07:56:12","2018-01-30 18:11:01",0);
    insertProject(12, models\records\Project::STATE_CANCELED,NULL,NULL,"General theory","GENTHE","",4,0,1,0,0,"theory","2017-10-16 00:00:00","2017-11-24 00:00:00",0);
    insertProject(13, models\records\Project::STATE_ANALYSIS,"2018-01-29 00:00:00",NULL,"Writing a C++ compiler for fun","CPC","See title.",null,0,0,0,0,"C++, compiler, fun","2018-01-14 00:00:00","2018-01-22 00:00:00",0);
}


function insertDefences()
{
    insertDefence(1,6,null, 1, 1, 6,5,"2018-01-31 13:30:00",NULL,8,NULL,NULL,"2018-01-22 00:00:00","2018-01-22 00:00:00",0);
    insertDefence(2,8,null, 3, 1, NULL,7,"2018-01-31 17:00:00",NULL,NULL,14,NULL,"2018-02-05 00:00:00","2018-02-05 00:00:00",0);
    insertDefence(3,1,null, 3, 1, NULL,7,"2018-01-31 17:30:00",NULL,NULL,14,NULL,"2018-02-05 00:00:00","2018-02-05 00:00:00",0);
    insertDefence(4,11,null, 3, 1, 2,7,NULL,NULL,NULL,14,NULL,"2018-02-05 00:00:00","2018-02-05 00:00:00",0);
    insertDefence(5,9,null, 4, 1, 5,NULL,"2018-01-31 18:00:00",NULL,9,NULL,NULL,"2018-01-21 00:00:00","2018-01-21 00:00:00",1);
    insertDefence(7,4,null, 5, 1, 5,NULL,"2018-01-31 18:20:00",NULL,9,NULL,NULL,"2018-01-21 00:00:00","2018-01-21 00:00:00",0);
    insertDefence(8,3,null, 3, 1, NULL,NULL,NULL,NULL,9,NULL,NULL,"2018-01-21 00:00:00","2018-01-21 00:00:00",0);


    // PNPREL
    insertDefence(6,5, models\records\Project::STATE_ANALYSIS_HANDED_IN, 5, 1, 7, 8,"2018-01-31 13:30:00",27,26,25,'failed',"2018-01-21 00:00:00","2018-01-21 00:00:00",0);
    insertDefence(9,5, models\records\Project::STATE_ANALYSIS_HANDED_IN, 6, 2, 7, 8,"2018-01-31 12:00:00",38,37,36,'defended',"2018-01-21 00:00:00","2018-01-21 00:00:00",0);
    insertDefence(10,5,null, 1, 3, 7, 8,"2018-01-31 18:00:00",null,null, null,null,"2018-01-21 00:00:00","2018-01-21 00:00:00",0);

    global $cronTesting;
    insertDefence(11,6, models\records\Project::STATE_ANALYSIS_HANDED_IN,7,1,5,NULL,$cronTesting->modifyClone('+1 hour')->toYii(),NULL,8,NULL, models\records\ProjectDefence::RESULT_DEFENDED,"2018-01-22 00:00:00","2018-01-22 00:00:00",0);
    insertDefence(12,3,NULL, 8,1,7,8,$cronTesting->modifyClone('+3 hour')->toYii(),10,NULL,14, models\records\ProjectDefence::RESULT_DEFENDED,"2018-02-05 00:00:00","2018-02-05 00:00:00",1);
    insertDefence(13,4, models\records\Project::STATE_ANALYSIS_HANDED_IN, 9,1,7,8,$cronTesting->modifyClone('+1 week')->toYii(),10,NULL,14, models\records\ProjectDefence::RESULT_DEFENDED,"2018-02-05 00:00:00","2018-02-05 00:00:00",0);
    insertDefence(14,8,NULL,10,1,7,8,$cronTesting->modifyClone('+2 days')->toYii(),10,NULL,14, models\records\ProjectDefence::RESULT_DEFENDED,"2018-02-05 00:00:00","2018-02-05 00:00:00",0);
    insertDefence(15,9, models\records\Project::STATE_ANALYSIS_HANDED_IN, 6,1,NULL,8,NULL,NULL,9,NULL, models\records\ProjectDefence::RESULT_DEFENDED,"2018-01-21 00:00:00","2018-01-21 00:00:00",0);
}

function insertProjectDocuments()
{
    insertProjectDocument(1,3,3,"First draft of analysis.",1,2, models\records\ProjectDocument::TYPE_READ_ONLY,"2018-01-21 00:00:00","2018-01-30 00:00:00",0);
    insertProjectDocument(2,9,2,NULL,1,NULL, models\records\ProjectDocument::TYPE_FOR_DEFENSE_ZIP,"2018-02-03 00:00:00","2018-02-04 00:00:00",0);
    insertProjectDocument(3,11,1,"Supervisor advice.",0,NULL, models\records\ProjectDocument::TYPE_ADVICE,"2018-01-14 00:00:00","2018-01-14 00:00:00",1);
    insertProjectDocument(4,5,4,NULL,1,NULL, models\records\ProjectDocument::TYPE_READ_ONLY,"2018-01-07 00:00:00","2018-01-11 00:00:00",0);

    // PNPREL
    insertProjectDocument(5,5,28,NULL,0,1, models\records\ProjectDocument::TYPE_FOR_DEFENSE_ZIP,"2018-01-07 00:00:00","2018-01-11 00:00:00",0);
    insertProjectDocument(6,5,29,NULL,0,1, models\records\ProjectDocument::TYPE_FOR_DEFENSE_ZIP,"2018-01-08 00:00:00","2018-01-11 00:00:00",0);
    insertProjectDocument(7,5,30,NULL,0,1, models\records\ProjectDocument::TYPE_FOR_DEFENSE_PDF,"2018-01-07 00:00:00","2018-01-11 00:00:00",0);
    insertProjectDocument(8,5,31,NULL,0,1, models\records\ProjectDocument::TYPE_FOR_DEFENSE_PDF,"2018-01-08 00:00:00","2018-01-11 00:00:00",0);
    insertProjectDocument(9,5,32,NULL,0,2, models\records\ProjectDocument::TYPE_FOR_DEFENSE_ZIP,"2018-03-08 00:00:00","2018-01-11 00:00:00",0);
    insertProjectDocument(10,5,33,NULL,0,2, models\records\ProjectDocument::TYPE_FOR_DEFENSE_PDF,"2018-03-08 00:00:00","2018-01-11 00:00:00",0);
    insertProjectDocument(11,5,34,NULL,0,3, models\records\ProjectDocument::TYPE_FOR_DEFENSE_ZIP,"2018-07-08 00:00:00","2018-01-11 00:00:00",0);
    insertProjectDocument(12,5,35,NULL,0,3, models\records\ProjectDocument::TYPE_FOR_DEFENSE_PDF,"2018-07-08 00:00:00","2018-01-11 00:00:00",0);
}



function insertProjectStateHistories()
{
    insertProjectStateHistory(1,4, models\records\Project::STATE_ANALYSIS,"The proposal was accepted, so the project entered the analysis stage.","2018-01-22 00:00:00","2018-01-22 00:00:00",0);
    insertProjectStateHistory(2,8, models\records\Project::STATE_ANALYSIS_HANDED_IN,NULL,"2017-12-20 00:00:00","2017-12-20 00:00:00",0);
    insertProjectStateHistory(3,2, models\records\Project::STATE_PROPOSAL,NULL,"2017-12-20 00:00:00","2017-12-20 00:00:00",0);
    insertProjectStateHistory(4,1, models\records\Project::STATE_PROPOSAL,NULL,"2018-01-15 00:00:00","2018-01-15 00:00:00",0);
    insertProjectStateHistory(5,3, models\records\Project::STATE_PROPOSAL,NULL,"2018-01-14 00:00:00","2018-01-14 00:00:00",0);
    insertProjectStateHistory(6,3, models\records\Project::STATE_ACCEPTED,"Interesting project.","2018-01-19 00:00:00","2018-01-19 00:00:00",0);
    insertProjectStateHistory(7,4, models\records\Project::STATE_PROPOSAL,NULL,"2017-11-21 00:00:00","2017-11-21 00:00:00",0);
    insertProjectStateHistory(8,4, models\records\Project::STATE_ACCEPTED,"Looks promising","2018-01-09 00:00:00","2018-01-09 00:00:00",1);
    insertProjectStateHistory(9,5, models\records\Project::STATE_PROPOSAL,NULL,"2017-08-30 00:00:00","2017-08-30 00:00:00",0);
    insertProjectStateHistory(10,5, models\records\Project::STATE_ACCEPTED,NULL,"2017-09-01 00:00:00","2017-09-01 00:00:00",0);
    insertProjectStateHistory(11,5, models\records\Project::STATE_ANALYSIS,NULL,"2017-09-11 00:00:00","2017-09-11 00:00:00",0);
    insertProjectStateHistory(12,5, models\records\Project::STATE_ANALYSIS_HANDED_IN,NULL,"2017-11-01 00:00:00","2017-11-01 00:00:00",0);
    insertProjectStateHistory(13,6, models\records\Project::STATE_PROPOSAL,NULL,"2017-11-16 00:00:00","2017-11-16 00:00:00",0);
    insertProjectStateHistory(14,6, models\records\Project::STATE_ACCEPTED,"Useful","2017-11-17 00:00:00","2017-11-17 00:00:00",0);
    insertProjectStateHistory(15,6, models\records\Project::STATE_ANALYSIS,NULL,"2017-11-30 00:00:00","2017-11-30 00:00:00",1);
    insertProjectStateHistory(16,6, models\records\Project::STATE_ANALYSIS_HANDED_IN,NULL,"2017-12-30 00:00:00","2017-12-30 00:00:00",0);
    insertProjectStateHistory(17,6, models\records\Project::STATE_IMPLEMENTATION,"Professor Jekyll had trouble at this stage","2018-02-25 00:00:00","2018-02-25 00:00:00",0);
    insertProjectStateHistory(18,8, models\records\Project::STATE_PROPOSAL,NULL,"2017-08-16 00:00:00","2017-08-16 00:00:00",0);
    insertProjectStateHistory(19,8, models\records\Project::STATE_ACCEPTED,NULL,"2017-08-18 00:00:00","2017-08-18 00:00:00",0);
    insertProjectStateHistory(20,8, models\records\Project::STATE_ANALYSIS,NULL,"2017-09-16 00:00:00","2017-09-16 00:00:00",0);
    insertProjectStateHistory(21,8, models\records\Project::STATE_IMPLEMENTATION,"The analysis was \Exceptional","2017-11-18 00:00:00","2017-11-18 00:00:00",0);
    insertProjectStateHistory(22,8, models\records\Project::STATE_IMPLEMENTATION_HANDED_IN,NULL,"2017-12-18 00:00:00","2017-12-18 00:00:00",1);
    insertProjectStateHistory(23,9, models\records\Project::STATE_PROPOSAL,NULL,"2017-11-01 00:00:00","2017-11-01 00:00:00",0);
    insertProjectStateHistory(24,9, models\records\Project::STATE_ACCEPTED,NULL,"2017-11-03 00:00:00","2017-11-03 00:00:00",0);
    insertProjectStateHistory(25,9, models\records\Project::STATE_ANALYSIS,NULL,"2017-11-09 00:00:00","2017-11-09 00:00:00",0);
    insertProjectStateHistory(26,9, models\records\Project::STATE_ANALYSIS_HANDED_IN,NULL,"2017-12-31 00:00:00","2017-12-31 00:00:00",0);
    insertProjectStateHistory(27,9, models\records\Project::STATE_IMPLEMENTATION,NULL,"2018-01-02 00:00:00","2018-01-02 00:00:00",0);
    insertProjectStateHistory(28,9, models\records\Project::STATE_IMPLEMENTATION_HANDED_IN,NULL,"2018-03-02 00:00:00","2018-03-02 00:00:00",0);
    insertProjectStateHistory(29,9, models\records\Project::STATE_CONDITIONALLY_DEFENDED,"Some problems with documentation","2018-04-02 00:00:00","2018-04-02 00:00:00",0);
    insertProjectStateHistory(30,10, models\records\Project::STATE_PROPOSAL,NULL,"2017-05-24 00:00:00","2017-05-24 00:00:00",0);
    insertProjectStateHistory(31,10, models\records\Project::STATE_ACCEPTED,"I think this will be successful","2017-05-27 00:00:00","2017-05-27 00:00:00",0);
    insertProjectStateHistory(32,10, models\records\Project::STATE_ANALYSIS,NULL,"2017-06-24 00:00:00","2017-06-24 00:00:00",0);
    insertProjectStateHistory(33,10, models\records\Project::STATE_ANALYSIS_HANDED_IN,NULL,"2017-07-27 00:00:00","2017-07-27 00:00:00",0);
    insertProjectStateHistory(34,10, models\records\Project::STATE_IMPLEMENTATION,NULL,"2017-08-29 00:00:00","2017-08-29 00:00:00",0);
    insertProjectStateHistory(35,10, models\records\Project::STATE_IMPLEMENTATION_HANDED_IN,NULL,"2017-11-29 00:00:00","2017-11-29 00:00:00",0);
    insertProjectStateHistory(36,11, models\records\Project::STATE_PROPOSAL,NULL,"2017-09-05 07:56:12","2017-09-05 07:56:12",0);
    insertProjectStateHistory(37,11, models\records\Project::STATE_ACCEPTED,NULL,"2017-09-11 07:56:12","2017-09-11 07:56:12",0);
    insertProjectStateHistory(38,12, models\records\Project::STATE_PROPOSAL,NULL,"2017-10-16 00:00:00","2017-10-16 00:00:00",0);
    insertProjectStateHistory(39,12, models\records\Project::STATE_CANCELED,NULL,"2017-10-17 00:00:00","2017-10-17 00:00:00",0);
    insertProjectStateHistory(40,2, models\records\Project::STATE_ACCEPTED,NULL,"2018-01-15 00:00:00","2018-01-15 00:00:00",0);
    insertProjectStateHistory(41,7, models\records\Project::STATE_PROPOSAL,NULL,"2018-01-30 17:37:35","2018-01-30 17:37:35",0);
    insertProjectStateHistory(42,13, models\records\Project::STATE_PROPOSAL,NULL,"2018-01-30 17:37:35","2018-01-30 17:37:35",0);
}



function insertProposals()
{
    insertProposal(1,1,11, models\records\Proposal::STATE_SENT, NULL,"2018-01-31 00:00:00","2018-02-01 00:00:00",0);
    insertProposal(2,2,25, models\records\Proposal::STATE_ACCEPTED, "Good","2018-01-17 00:00:00","2018-01-17 00:00:00",0);
    insertProposal(3,1,13, models\records\Proposal::STATE_DRAFT, NULL,"2018-01-30 19:00:00","2018-01-30 19:00:00",1);
    insertProposal(4,12,12, models\records\Proposal::STATE_DECLINED, "Serious issues with this proposal", "2017-10-25 00:00:00","2017-11-07 00:00:00",1);
    insertProposal(5,3,15, models\records\Proposal::STATE_ACCEPTED, NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertProposal(6,4,16, models\records\Proposal::STATE_ACCEPTED, "Nice","0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertProposal(7,5,17, models\records\Proposal::STATE_ACCEPTED, NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertProposal(8,6,18, models\records\Proposal::STATE_ACCEPTED, "Cool","0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertProposal(9,8,20, models\records\Proposal::STATE_ACCEPTED, NULL, "0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertProposal(10,9,21, models\records\Proposal::STATE_ACCEPTED, NULL, "0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertProposal(11,10,22, models\records\Proposal::STATE_ACCEPTED, NULL,"0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertProposal(12,11,23, models\records\Proposal::STATE_ACCEPTED, NULL, "0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertProposal(13,12,24, models\records\Proposal::STATE_ACCEPTED, NULL, "0000-00-00 00:00:00","0000-00-00 00:00:00",0);
    insertProposal(14,7,19, models\records\Proposal::STATE_DRAFT, NULL,"2018-01-30 19:00:00","2018-01-30 19:00:00",0);

    //these are only for testing so the project state will be incorrect
    insertProposal(
        15,3,22, models\records\Proposal::STATE_SENT, NULL,"2018-01-31 00:00:00","2018-02-01 00:00:00",0
    );
    insertProposal(
        16,4,23, models\records\Proposal::STATE_SENT, NULL,"2018-01-31 00:00:00","2018-02-01 00:00:00",0
    );
    insertProposal(
        17,5,24, models\records\Proposal::STATE_SENT, NULL,"2018-01-31 00:00:00","2018-02-01 00:00:00",0
    );

    insertProposal(
        18,13,24, models\records\Proposal::STATE_DRAFT, NULL,"2018-01-31 00:00:00","2018-02-01 00:00:00",0
    );
}



function insertProposalComments()
{
    insertProposalComment(1,5,3,"Seems good",0,"2018-02-01 00:00:00","2018-02-01 00:00:00",0);
    insertProposalComment(2,7,1,"I think this is great.",1,"2018-02-03 00:00:00","2018-02-04 00:00:00",1);
    insertProposalComment(3,10,4,"Awful.",1,"2017-11-11 00:00:00","2017-11-13 00:00:00",0);
    insertProposalComment(4,5,2,"MFF worthy stuff",0,"2018-01-19 00:00:00","2018-01-19 00:00:00",1);
}



function insertProposalVotes()
{
    insertProposalVote(1,8,1, models\records\ProposalVote::VOTE_YES,"2018-02-01 00:00:00","2018-02-01 00:00:00",0);
    insertProposalVote(2,5,1, models\records\ProposalVote::VOTE_NO,"2018-02-01 00:00:00","2018-02-02 00:00:00",0);

    //CRON tests
    insertProposalVote(4,7,15,null,"2018-01-18 00:00:00","2018-01-18 00:00:00",0);
    insertProposalVote(5,6,15,null,"2018-01-18 00:00:00","2018-01-18 00:00:00",0);
    insertProposalVote(6,7,16,null,"2018-01-18 00:00:00","2018-01-18 00:00:00",0);
    insertProposalVote(7,10,2,null,"2018-01-18 00:00:00","2018-01-18 00:00:00",0);
}



function insertRoleHistories()
{
    insertRoleHistory(1,1, models\records\User::ROLE_STUDENT,"MFF student working on the project","2018-01-15 00:00:00","2018-01-15 00:00:00",0);
    insertRoleHistory(2,5, models\records\User::ROLE_STUDENT,NULL,"2017-12-27 00:00:00","2017-12-28 00:00:00",0);
    insertRoleHistory(3,7, models\records\User::ROLE_STUDENT,"Opponent assigned because of project properties","2018-01-14 00:00:00","2018-01-14 00:00:00",0);
    insertRoleHistory(4,10, models\records\User::ROLE_STUDENT,NULL,"2018-01-16 00:00:00","2018-01-29 00:00:00",0);
    insertRoleHistory(5,5, models\records\User::ROLE_STUDENT,NULL,"2017-12-27 00:00:00","2017-12-28 00:00:00",1);
    insertRoleHistory(6,5, models\records\User::ROLE_STUDENT,NULL,"2017-12-27 00:00:00","2017-12-28 00:00:00",0);
    insertRoleHistory(7,5, models\records\User::ROLE_STUDENT,NULL,"2017-12-27 00:00:00","2017-12-28 00:00:00",0);
    insertRoleHistory(8,5, models\records\User::ROLE_STUDENT,NULL,"2017-12-27 00:00:00","2017-12-28 00:00:00",0);
}

function insertUsers()
{
    insertUser(
        1,
        models\records\User::LOGIN_TYPE_MANUAL,
        models\records\User::ROLE_COMMITTEE_CHAIR,
        "Petr Hnětynka",
        "Doc. Mrg.",
        "PhD.",
        "hnetynka@gmail.com",
        "Test1234",
        1,
        '-10 hours',
        "www.seznam.cz",
        '-1 year',
        '+1 year',
        1,
        "JIFPOSDifjsdfjjapjOIJP",
        '+4 days',
        0,
        '-4 years',
        '-2 days',
        0
    );

    insertUser(2, models\records\User::LOGIN_TYPE_CAS, models\records\User::ROLE_STUDENT,"Jiří Novák",NULL,NULL,"jiri.novak@gmail.com","fajoščáíéáé+24908716#*!@&(&&%",1,NULL,NULL,"2018-01-30 00:00:00","2019-01-30 00:00:00",1,"GwTqWKlcjtsmObUDkLXP","2018-07-26 00:00:00",0,"2018-01-30 14:32:34","2018-01-30 14:32:34",0);
    insertUser(3, models\records\User::LOGIN_TYPE_CAS, models\records\User::ROLE_STUDENT,"Jan Svoboda","Bc.",NULL,"jan.svoboda@gmail.com","Test1234",1,"2018-01-30 00:00:00","www.jansvob.czhost.cz","2018-01-15 10:01:07","2019-04-30 00:13:00",1,"corajenny","2018-07-26 00:00:00",0,"2017-11-12 07:12:00","2018-01-30 18:35:35",0);
    insertUser(5, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_ACADEMIC,"Jana Novotná",NULL,NULL,"jana.novotna65@seznam.cz","Test1234",1,"2018-01-27 07:54:00","www.jannov.cz",NULL,"2019-01-30 00:00:00",0,"bobeštristan","2018-07-26 00:23:00",0,"2018-01-02 00:01:00","2018-01-30 17:33:34",0);
    insertUser(6, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_COMMITTEE_MEMBER,"Eva Dvořáková","Mgr.","Mudr.","eva_dvorakova85@seznam.cz","Test1234",0,NULL,NULL,NULL,"2019-01-28 00:00:00",0,"brokbublina",NULL,0,"2017-09-18 17:36:12","2017-09-18 17:36:12",0);
    insertUser(7, models\records\User::LOGIN_TYPE_CAS, models\records\User::ROLE_COMMITTEE_MEMBER,"Josef Černý",NULL,"Phd.","josef.cerny.asd@gmail.com","Test1234",0,"2018-01-30 00:04:12","http://www.josefcerny.cz","2018-01-30 00:00:00","2019-01-28 00:00:00",1,NULL,NULL,1,"2017-10-18 17:36:12","2017-11-11 17:42:12",0);
    insertUser(8, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_COMMITTEE_CHAIR,"Miroslav Procházka","Mgr.","Phd.","miro.procha@gmail.com","Test1234",1,"2018-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00","2019-01-28 00:00:00",0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(9, models\records\User::LOGIN_TYPE_CAS,NULL,"Asaif Abulgan",NULL,NULL,"asaifab454@gmail.com","6D79677265656E646F6C7068696E",0,"2017-11-30 15:06:12",NULL,"0000-00-00 00:00:00","2019-01-28 00:00:00",0,"flipper",NULL,1,"2017-02-14 17:36:12","2017-03-25 17:00:00",1);
    insertUser(10, models\records\User::LOGIN_TYPE_CAS, models\records\User::ROLE_COMMITTEE_CHAIR,"Miroslav Veselý","Ing.",NULL,"miro_vesel@gmail.com","Test1234",1,"2018-01-29 15:15:12","http://www.mff.cuni.cz/~mirves","2018-01-28 00:00:00","2019-01-28 00:00:00",1,"columbo","2018-08-14 07:00:00",0,"2017-06-14 17:48:13","2017-11-25 17:32:14",0);
    insertUser(11, models\records\User::LOGIN_TYPE_CAS, models\records\User::ROLE_STUDENT,"John Doe","Ing.",NULL,"jonh_doe@gmail.com","Test78945",0,"2018-01-29 15:15:12","http://www.mff.cuni.cz/~johnd","2018-01-28 00:00:00","2019-01-28 00:00:00",1,NULL,NULL,0,"2017-06-14 17:48:13","2017-11-25 17:32:14",0);
    insertUser(12, models\records\User::LOGIN_TYPE_CAS, models\records\User::ROLE_STUDENT,"Sam Williams",NULL,"Phd.","samwilliams@gmail.com","T4654121945",0,"2018-01-29 15:15:12","http://www.mff.cuni.cz/~samwil","2018-01-28 00:00:00","2019-01-28 00:00:00",1,NULL,NULL,0,"2017-06-14 17:48:13","2017-11-25 17:32:14",0);
    insertUser(13, models\records\User::LOGIN_TYPE_CAS, models\records\User::ROLE_STUDENT,"Jessica Twain",NULL,NULL,"jestwain@gmail.com","89454a6sd54as",0,"2018-01-29 15:15:12","http://www.mff.cuni.cz/~jestwa","2018-01-28 00:00:00","2019-01-28 00:00:00",1,NULL,NULL,0,"2017-06-14 17:48:13","2017-11-25 17:32:14",0);
    insertUser(14, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_STUDENT,"Student name","Mgr.","Phd.","student@gmail.com","Test1234",0,"2018-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00","2019-01-28 00:00:00",0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(15, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_ACADEMIC,"Academic name","Mgr.","Phd.","academic@gmail.com","Test1234",0,"2018-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00","2019-01-28 00:00:00",0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(16, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_COMMITTEE_MEMBER,"member name","Mgr.","Phd.","member@gmail.com","Test1234",0,"2018-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00","2019-01-28 00:00:00",0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(17, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_COMMITTEE_CHAIR,"chair name","Mgr.","Phd.","chair@gmail.com","Test1234",0,"2018-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00","2019-01-28 00:00:00",0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(18, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_STUDENT,"student OfWhales1",NULL,NULL,"student1@gmail.com","Test1234",0,"2018-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00","2019-01-28 00:00:00",0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(19, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_STUDENT,"student OfWhales2",NULL,NULL,"student2@gmail.com","Test1234",0,"2018-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00","2019-01-28 00:00:00",0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);

    insertUser(20, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_STUDENT,"John Doe",NULL,NULL,"john@gmail.com","Test1234",Lang::CS,"2018-01-30 15:06:12","http://example.cz","2017-04-11 00:00:00",NULL,0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(21, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_STUDENT,"Dan Joe",NULL,NULL,"dan@gmail.com","Test1234",Lang::CS,"2018-01-30 15:06:12","http://example.cz","2017-04-11 00:00:00",NULL,0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(22, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_STUDENT,"Ping Pong",NULL,NULL,"ching@gmail.com","Test1234",Lang::CS,"2018-01-30 15:06:12","http://example.cz","2017-04-11 00:00:00",NULL,0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(23, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_STUDENT,"Ching Chong",NULL,NULL,"bon@gmail.com","Test1234",Lang::CS,"2018-01-30 15:06:12","http://example.cz","2017-04-11 00:00:00",NULL,0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);

    //anonymization
    insertUser(24, models\records\User::LOGIN_TYPE_CAS, models\records\User::ROLE_STUDENT,"Jessica Twain",NULL,NULL,"jestasdwain@gmail.com","89454a6sd54as",0,"2009-01-29 15:15:12","http://www.mff.cuni.cz/~jestwa","2018-01-28 00:00:00","2019-01-28 00:00:00",1,NULL,NULL,0,"2017-06-14 17:48:13","2017-11-25 17:32:14",0);
    insertUser(25, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_STUDENT,"Student name","Mgr.","Phd.","stuasddent@gmail.com","Test1234",0,"2011-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00",NULL,0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(26, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_ACADEMIC,"Academic name","Mgr.","Phd.","academasdic@gmail.com","Test1234",0,"2013-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00",NULL,0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
    insertUser(27, models\records\User::LOGIN_TYPE_MANUAL, models\records\User::ROLE_COMMITTEE_MEMBER,"member name","Mgr.","Phd.","memasdber@gmail.com","Test1234",0,"2015-01-30 15:06:12","http://www.mirproch.mff.cz","2017-04-11 00:00:00",NULL,0,"babette45",NULL,0,"2017-02-14 17:36:12","2018-01-30 17:34:20",0);
}

function insertUserOnProjects()
{
    insertUserOnProject(1,10,1, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2018-03-02 00:00:00","2018-03-02 00:00:00",0);
    insertUserOnProject(2,10,2, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2018-03-02 00:00:00","2018-03-02 00:00:00",0);
    insertUserOnProject(3,10,3, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2018-03-02 00:00:00","2018-03-02 00:00:00",0);
    insertUserOnProject(4,8,4, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2018-03-02 00:00:00","2018-03-02 00:00:00",0);
    insertUserOnProject(5,8,5, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(6,8,6, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(7,15,7, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(8,15,8, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(9,15,9, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(10,15,10, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(11,15,11, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(12,17,12, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);

    insertUserOnProject(13,3,2, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2018-01-15 00:00:00","2018-01-22 09:52:17",0);
    insertUserOnProject(15,6,6, models\records\ProjectUser::ROLE_OPPONENT,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(16,8,10, models\records\ProjectUser::ROLE_CONSULTANT,NULL,"2017-07-19 00:00:00","2017-07-19 00:00:00",0);
    insertUserOnProject(17,11,2, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2018-01-15 00:00:00","2018-01-22 09:52:17",0);
    insertUserOnProject(18,12,2, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2018-01-15 00:00:00","2018-01-22 09:52:17",0);
    insertUserOnProject(19,13,2, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2018-01-15 00:00:00","2018-01-22 09:52:17",0);
    insertUserOnProject(20,18,4, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2018-01-15 00:00:00","2018-01-22 09:52:17",0);
    insertUserOnProject(21,19,4, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2018-01-15 00:00:00","2018-01-22 09:52:17",0);

    insertUserOnProject(22,15,5, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(23,20,5, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(24,21,5, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(25,22,5, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);
    insertUserOnProject(26,23,5, models\records\ProjectUser::ROLE_TEAM_MEMBER,NULL,"2017-11-28 00:00:00","2018-01-30 18:42:11",0);

    insertUserOnProject(27,8,13, models\records\ProjectUser::ROLE_SUPERVISOR,NULL,"2018-01-15 00:00:00","2018-01-22 09:52:17",0);
}





// DEBUGGING AREA _---------------------------------
function debug() {
    $nowdate = new DateTime();
    insertDefenceDate(
        1,$nowdate->modifyClone('+25 days'),$nowdate->modifyClone('+24 days'),1,"2018-02-18 00:00:00","2018-02-19 00:00:00",0
    );
}

//debug();












//--------------------------------------------------

Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
$nowdate = new DateTime();
$relativedate = new DateTime("2018-03-07 00:00:00");
$cronTesting = new DateTime('2018-06-20 14:00:00');

models\records\User::deleteAll();
insertUsers();

models\records\DefenceDate::deleteAll();
insertDefenceDates();

models\records\DefenceAttendance::deleteAll();
insertDefenceAttendances();

models\records\Document::deleteAll();
insertDocuments();

models\records\Advertisement::deleteAll();
insertAdvertisements();

models\records\AdvertisementComment::deleteAll();
insertAdComments();

models\records\News::deleteAll();
insertMultipleNews();

models\records\Page::deleteAll();
insertPages();

models\records\PageVersion::deleteAll();
insertPageVersions();

models\records\Project::deleteAll();
insertProjects();

models\records\ProjectDefence::deleteAll();
insertDefences();

models\records\ProjectDocument::deleteAll();
insertProjectDocuments();

models\records\ProjectStateHistory::deleteAll();
insertProjectStateHistories();

models\records\Proposal::deleteAll();
insertProposals();

models\records\ProposalComment::deleteAll();
insertProposalComments();

models\records\ProposalVote::deleteAll();
insertProposalVotes();

models\records\RoleHistory::deleteAll();
insertRoleHistories();

models\records\ProjectUser::deleteAll();
insertUserOnProjects();

Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();

exit();