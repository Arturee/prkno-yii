#!/bin/sh

# this script is run once after the first deployemnt.
# at @cuni.cz


# =================================
# INFO
# ---------------------------------
# Apache serves from /var/www/prkno/web/.
# /var/www/prkno is owned by group apache.
# Apache seems to run PHP scripts under the user "bettes".
#
# service httpd status						#check status of apache
# httpd -V									#look how apache is configured
# cat /etc/httpd/conf/httpd.conf | more 	#look at apache config file

# =================================
# ONE-TIME INSTALLS (on centOS)
# ---------------------------------
#
# git:										#needed by composer
# sudo yum install git
# zip stuff:								#needed by composer
# sudo yum install zip unzip php7.0-zip
# composer:									#needed by libs
# curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
# mv /usr/local/bin/composer/composer /usr/local/bin/composer

COL_YELLOW='\033[1;33m'
COL_DARK='\033[1;30m'
COL_RED='\033[0;31m'
COL_NONE='\033[0m'

if [ "$EUID" -ne 0 ]
  then echo -e "${COL_RED}Usage: ${COL_YELLOW}sudo${COL_NONE} bash cuni_setup.sh"
  exit
fi



PRKNO='/var/www/prkno'

make_writable_by_apache()
{
	chcon --recursive --verbose --type=httpd_sys_content_rw_t $1
	chgrp --recursive --verbose apache $1
	chmod --recursive --verbose 770 $1
	
	# chcon turns off linux security features (SELinux) which normally prevent yii from
	# reading/writing files.
}



echo -e "${COL_DARK}"

# set timezone correctly (for CRON to work)
unlink /etc/localtime
ln -s /usr/share/zoneinfo/Europe/Prague /etc/localtime

mkdir --verbose $PRKNO/runtime/logs
mkdir --verbose $PRKNO/runtime/debug

make_writable_by_apache $PRKNO/web/assets
make_writable_by_apache $PRKNO/runtime
make_writable_by_apache $PRKNO/rbac
make_writable_by_apache $PRKNO/web/files

echo -e "${COL_YELLOW}"
echo "=============================================="
echo "Now run repeated_deployment.sh"
echo -e "${COL_NONE}"