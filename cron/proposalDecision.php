<?php

namespace app;

use yii;
use app\exceptions\BugError;
use app\models\records\User;
use app\models\records\Proposal;
use app\models\records\ProposalVote;
use app\mail\Emails;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require_once __DIR__ . '/commonFunctions.php';

$config = require __DIR__ . '/../config/console.php';
$application = new yii\console\Application($config);

/**
 * @Author: Gergely
 */

/** This is a necessary function, because we won't require the committee member to vote, if the proposal was already
 * accepted or declined.
 * @param int $proposalId
 * @return bool
 */
function isProposalSent(Proposal $proposal) : bool {
    return $proposal->state == Proposal::STATE_SENT;
}

$undecidedProposalVotes = ProposalVote::find()->where(["vote" => NULL])->all();

$userProposalArray = [];
foreach ($undecidedProposalVotes as $proposalVote) {
    if ($proposalVote->getAttribute("deleted"))
    {
        continue;
    }

    $userId = $proposalVote->getAttribute("user_id");
    $proposalId = $proposalVote->getAttribute("proposal_id");
    $proposal = Proposal::findOne($proposalId);

    if (isProposalSent($proposal)) {
        $userProposalArray[$userId][] = $proposal;
    }
}

foreach ($userProposalArray as $userId => $proposals) {
    $user = User::findOne($userId);

    if (!isUserCommitteeMember($user)) {
        throw new BugError("User asked to vote on a proposal NOT a committee member!");
    }
    Emails::reminderProposalEvaluation($user, $proposals);

    if (!$wasEmailSuccessful) {
        Yii::warning("Email sending to user with email $user->email about the missing proposal decision failed. \n", __METHOD__);
    }
}



