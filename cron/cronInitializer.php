<?php

namespace app;
use yii;

use TiBeN\CrontabManager\CrontabRepository;
use TiBeN\CrontabManager\CrontabAdapter;
use TiBeN\CrontabManager\CrontabJob;

use app\consts\Param;

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/console.php';
$application = new yii\console\Application($config);

/**
 * @Author: Gergely
 */

//The library used for managing the crontab is at link:
// https://github.com/TiBeN/CrontabManager

//This script needs to be run to setup the cron.

function setupCronJobForArrayTime(array $cronTime) : CrontabJob
{
    $crontabJob = new CrontabJob();
    $crontabJob->minutes = $cronTime['minutes'];
    $crontabJob->hours = $cronTime['hours'];
    $crontabJob->dayOfMonth = $cronTime['dayOfMonth'];
    $crontabJob->months = $cronTime['months'];
    $crontabJob->dayOfWeek = $cronTime['dayOfWeek'];

    return $crontabJob;
}

function createProposalDecisionReminderJob() : CrontabJob
{
    $cronTime = Yii::$app->params[Param::CRON_TIME_PROPOSAL_DECISION_REMINDER];

    $crontabJob = setupCronJobForArrayTime($cronTime);

    $crontabJob->taskCommandLine = 'php ' . getcwd() . '/proposalDecision.php';
    $crontabJob->comments = 'proposal-decision';

    return $crontabJob;
}

function createDefenceParticipationReminderJob() : CrontabJob
{
    $cronTime = Yii::$app->params[Param::CRON_TIME_DEFENCE_PARTICIPATION_REMINDER];

    $crontabJob = setupCronJobForArrayTime($cronTime);

    $crontabJob->taskCommandLine = 'php ' . getcwd() . '/defenceParticipation.php';
    $crontabJob->comments = 'defence-participation';

    return $crontabJob;
}

function createUpcomingDefenceReminderJob() : CrontabJob
{
    $cronTime = Yii::$app->params[Param::CRON_TIME_UPCOMING_DEFENCE_SETUP];

    $crontabJob = setupCronJobForArrayTime($cronTime);

    $crontabJob->taskCommandLine = 'php ' . getcwd() . '/upcomingDefence.php';
    $crontabJob->comments = 'upcoming-defence';

    return $crontabJob;
}

function createProjectDeadlineJob() : CrontabJob
{
    $cronTime = Yii::$app->params[Param::CRON_TIME_PROJECT_DEADLINE];

    $crontabJob = setupCronJobForArrayTime($cronTime);

    $crontabJob->taskCommandLine = 'php ' . getcwd() . '/projectDeadline.php';
    $crontabJob->comments = 'project-deadline';

    return $crontabJob;
}

function clearJobs(CrontabRepository $repository) : void
{
    $jobs = $repository->getJobs();

    foreach ($jobs as $job) {
        $repository->removeJob($job);
    }

    $repository->persist();
}

$crontabRepository = new CrontabRepository(new CrontabAdapter());
clearJobs($crontabRepository);

$crontabRepository->addJob(createProposalDecisionReminderJob());
$crontabRepository->addJob(createDefenceParticipationReminderJob());
$crontabRepository->addJob(createUpcomingDefenceReminderJob());
$crontabRepository->addJob(createProjectDeadlineJob());

$crontabRepository->persist();

