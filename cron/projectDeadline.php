<?php

namespace app;

use app\consts\Param;
use app\consts\Sql;
use app\mail\Emails;
use app\models\records\DefenceDate;
use app\models\records\Project;
use app\models\records\User;
use yii;
use app\exceptions\BugError;
use app\utils\DateTime;

use TiBeN\CrontabManager\CrontabRepository;
use TiBeN\CrontabManager\CrontabAdapter;
use TiBeN\CrontabManager\CrontabJob;

// This is the script that runs every day at 8:00.

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/console.php';

$now = new DateTime();

$projectsArray = Project::find()->where(Sql::CONDITION_NOT_DELETED)->all();
$openDefenceDates = DefenceDate::getFutureDates();

$openDefenceDateForImplementation = false;
$openDefenceDateForAnalysis = false;

$committeeChair = User::findCommitteeChair();

/** @var DefenceDate $defenceDate */
foreach ($openDefenceDates as $defenceDate) {
    $date = new DateTime($defenceDate->date);
    if ($date->isBefore($now->modify('+ ' . Param::AMOUNT_OF_DAYS_FOR_IMPLEMENTATION_OPEN_DEFENCE))) {
        $openDefenceDateForImplementation = true;
    }
    if ($date->isBefore($now->modify('+ ' . Param::AMOUNT_OF_DAYS_FOR_ANALYSIS_OPEN_DEFENCE))) {
        $openDefenceDateForAnalysis = true;
    }
}

/** @var Project $project */
foreach ($projectsArray as $project) {

    // Project documents deadline
    // Send email to project team members
    if ($project->getUpcomingDefence() && $project->canSendForDefence()) {

        $documentsDeadline = new DateTime($project->getUpcomingDefenceDocDeadline());
        /** @var DateTime $result */
        $result = $documentsDeadline->modify('+ ' . Param::AMOUNT_OF_DAYS_BEFORE_PROJECT_DOCUMENTS_DEADLINE);
        if ($result->isInPast() && $documentsDeadline->isInFuture()) {
            Emails::projectDocumentsDeadline($project);
        }
    }

    /** @var DateTime $deadline */
    $deadline = new DateTime($project->deadline);
    $start = new DateTime($project->run_start_date);

    // Project implementation deadline
    // Send email to committee chair
    if ($deadline->isInPast() && !$openDefenceDateForImplementation
        // Send email at the first day after deadline
        && DateTime::equalsDate($now, $deadline->modify('+ 1 days'))) {
        Emails::projectImplementationDeadline($project, $committeeChair);
    }

    // Project analysis deadline
    // Send email to committee chair
    $deadlineForAnalysis = $start->modify('+ ' . Param::PROJECT_DEADLINE_FOR_ANALYSIS);
    if ($deadlineForAnalysis->isInPast() && !$openDefenceDateForAnalysis
        // Send email at the first day after deadline
        && DateTime::equalsDate($now, $deadlineForAnalysis->modify('+ 1 days'))) {
        Emails::projectAnalysisDeadline($project, $committeeChair);
    }

    // Project conditionally defended deadline
    // Send email to committee chair
    $deadlineForConditionDefence = $deadline->modify('+ ' . Param::PROJECT_CONDITIONAL_DEFEND_DEADLINE);
    if ($deadlineForConditionDefence->isInPast()
        // Send email at the first day after deadline
        && DateTime::equalsDate($now, $deadlineForConditionDefence->modify('+ 1 days'))) {
        Emails::projectConditionallyDefendedDeadline($project, $committeeChair);
    }
}
