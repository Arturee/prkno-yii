<?php

namespace app;

use yii;
use app\models\records\DefenceAttendance;
use app\models\records\DefenceDate;
use app\models\records\User;
use app\mail\Emails;
use app\utils\DateTime;

/**
 * @Author: Gergely
 */

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require_once __DIR__ . '/commonFunctions.php';

$config = require __DIR__ . '/../config/console.php';
$application = new yii\console\Application($config);


function isPastSignupDeadline(int $defenceDateId) : bool {
    $defenceDate = DefenceDate::findOne($defenceDateId);
    $signupDate = DateTime::from($defenceDate->signup_deadline);

    return $signupDate->isBefore(new DateTime("now"));
}

$unknownDefencesAttendances = DefenceAttendance::find()->where(["vote" => DefenceAttendance::VOTE_UNKNOWN])->all();

$userAttendanceArray = [];
foreach ($unknownDefencesAttendances as $attendance) {
    if ($attendance->getAttribute("deleted"))
    {
        continue;
    }

    $userId = $attendance->getAttribute("user_id");
    $defenceDateId = $attendance->getAttribute("defence_date_id");
    $attendanceEntry = DefenceAttendance::findOne($attendance->getAttribute("id"));

    if (!isPastSignupDeadline($defenceDateId)) {
        $userAttendanceArray[$userId][] = $attendanceEntry;
    }
}

foreach ($userAttendanceArray as $userId => $attendances) {
    $user = User::findOne($userId);
    if (!isUserCommitteeMember($user)) {
        Yii::warning("User $user->name asked to attend on a defence NOT a committee member (in user/role)!", __METHOD__);
    }
    Emails::reminderDefenceAttendance($user, $attendances);
}