<?php

namespace app;

use app\utils\DateTime;
use yii;
use app\models\records\DefenceDate;
use app\consts\Param;

use TiBeN\CrontabManager\CrontabRepository;
use TiBeN\CrontabManager\CrontabAdapter;

/**
 * @Author: Gergely
 */

// This is the script that runs 2 hours before the first defence of the day.

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require_once __DIR__ . '/upcomingDefenceFunctions.php';

$config = require __DIR__ . '/../config/console.php';
$application = new yii\console\Application($config);

/**
 * Deletes the CronJob that ran this script, because we no longer have use for it and we can't let it trigger again
 * (in the next year).
 */
function deleteTriggeringCronJob() : void {
    $crontabRepository = new CrontabRepository(new CrontabAdapter());

    $cronJobs = $crontabRepository->findJobByRegex('/' . UPCOMING_DEFENCE_IN_HOURS_COMMENT . '/');
    if (!$cronJobs) {
        Yii::error("Upcoming defence in hours triggering CronJob not found for deletion!", __METHOD__);
        return;
    }

    $cronJob = $cronJobs[0];
    $crontabRepository->removeJob($cronJob);

    $crontabRepository->persist();
}


$now = new DateTime();

$defencesToday = [];

$allDefenceDates = DefenceDate::find()->all();
foreach ($allDefenceDates as $defenceDate) {
    if ($defenceDate->deleted)
    {
        continue;
    }

    $dateOfDefence = DateTime::from($defenceDate->date);

    if ($dateOfDefence->isOnSameDayAs($now)) {
        $defencesToday[] = $defenceDate;
    }
}

if (count($defencesToday) > 0) {
    sendOutEmails($defencesToday, Yii::$app->params[Param::AMOUNT_OF_HOURS_BEFORE_DEFENCE_REMINDER]);
}

deleteTriggeringCronJob();