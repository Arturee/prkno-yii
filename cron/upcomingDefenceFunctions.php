<?php

namespace app;

use app\models\records\DefenceAttendance;
use app\utils\DateTime;
use yii;
use app\models\records\DefenceDate;
use app\models\records\User;
use app\exceptions\BugError;
use app\models\records\ProjectDefence;
use app\models\records\ProjectUser;
use app\mail\Emails;

require_once __DIR__ . '/commonFunctions.php';

/**
 * @Author: Gergely
 */


/**
 * This is the CRON comment for the 2 hours before the defence job. We use this to match the job when we want to delete
 * it.
 */
const UPCOMING_DEFENCE_IN_HOURS_COMMENT = 'upcoming-defence-hours';


function getAttendingCommitteeMemberIds(int $defenceDateId) : array {
    $attendances = DefenceAttendance::find()
        ->where(["defence_date_id" => $defenceDateId])
        ->andWhere(["vote" => DefenceAttendance::VOTE_YES])
        ->all();

    $attendingCommitteeMemberIds = [];

    foreach ($attendances as $attendance) {
        $memberId = $attendance->getAttribute("user_id");
        $user = User::findOne($memberId);

        if (!isUserCommitteeMember($user)) {
            throw new BugError("User attending a project defence as a committee member NOT a committee member (in user/role)!");
        }

        $attendingCommitteeMemberIds[] = $user->id;
    }

    return $attendingCommitteeMemberIds;
}

function getTeamMemberIds(ProjectDefence $projectDefence) : array {
    $teamMemberEntries = ProjectUser::find()
        ->where(["project_id" => $projectDefence->project_id])
        ->andWhere(["project_role" => ProjectUser::ROLE_TEAM_MEMBER])
        ->all();

    $teamMemberIds = [];
    foreach ($teamMemberEntries as $teamMemberEntry) {
        $memberId = $teamMemberEntry->getAttribute("user_id");
        $teamMember = User::findOne($memberId);

        if ($teamMember) {
            $teamMemberIds[] = $memberId;
        }
    }

    return $teamMemberIds;
}

/**
 * @param \app\models\records\ProjectDefence $projectDefence
 * @return int|null the supervisor ID
 */
function getSupervisorId(ProjectDefence $projectDefence) : ?int {
    $supervisorRelation = ProjectUser::findOne([
        "project_id" => $projectDefence->project_id,
        "project_role" => ProjectUser::ROLE_SUPERVISOR
    ]);

    if (!$supervisorRelation) {
        return NULL;
    }

    return $supervisorRelation->user_id;
}

/** Returns an array populated with the IDs of the Users who we need to notify about the upcoming defence:
 * committee who voted yes, team members, project supervisor.
 * @param \app\models\records\DefenceDate $defenceDate
 * @return array - empty array, if there is no ProjectDefence assigned to the DefenceDate
 */
function getPeopleToNotify(DefenceDate $defenceDate) : array {
    $peopleToNotify = getAttendingCommitteeMemberIds($defenceDate->id);

    $projectDefence = ProjectDefence::findOne(["defence_date_id" => $defenceDate->id]);

    if (!$projectDefence)
    {
        return [];
    }

    $peopleToNotify = array_merge($peopleToNotify, getTeamMemberIds($projectDefence));

    $supervisorId = getSupervisorId($projectDefence);
    if ($supervisorId) {
        if (User::findOne($supervisorId)) {
            $peopleToNotify[] = $supervisorId;
        }
    }

    return $peopleToNotify;
}

function sortArrayBasedOnDate(array $defenceDates) : void {
    usort($defenceDates, function(DefenceDate $firstDate, DefenceDate $secondDate) {
        $firstProjectDefence = ProjectDefence::findOne(["defence_date_id" => $firstDate->id]);
        $secondProjectDefence = ProjectDefence::findOne(["defence_date_id" => $secondDate->id]);

        if (!$secondProjectDefence) {
            return 1;
        } else {
            if (!$secondProjectDefence->time) {
                //this means that the $firstDate is greater than the $secondDate
                //we check the second project defence first, because if they are both null, we keep the order
                return 1;
            }
        }

        if (!$firstProjectDefence) {
            return -1;
        } else {
            if (!$firstProjectDefence->time) {
                return -1;
            }
        }

        $firstProjectDefenceTime = DateTime::from($firstProjectDefence->time);
        $secondProjectDefenceTime = DateTime::from($secondProjectDefence->time);

        if ($firstProjectDefenceTime >= $secondProjectDefenceTime)
        {
            return 1;
        } else {
            return -1;
        }
    });
}

/** Creates a dictionary array, where the key is the user ID and the value is an array of DefenceDate, of which the user
 * should get notified about.
 * @param array $defenceDates
 * @return array
 */
function getUserDefenceAssociationArray(array $defenceDates) : array {
    $userDefenceArray = [];

    foreach ($defenceDates as $defenceDate) {
        $peopleToNotify = getPeopleToNotify($defenceDate);

        foreach ($peopleToNotify as $userId) {
            $userDefenceArray[$userId][] = $defenceDate;
        }
    }

    return $userDefenceArray;
}

function sendOutEmails(array $defenceDates, string $timeLeft) : void {
    sortArrayBasedOnDate($defenceDates);
    $userDefenceArray = getUserDefenceAssociationArray($defenceDates);

    foreach ($userDefenceArray as $userId => $defences) {
        $user = User::findOne($userId);
        Emails::reminderUpcomingDefence($user, $defences, $timeLeft);
    }
}

