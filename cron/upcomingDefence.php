<?php

namespace app;

use app\consts\Param;
use app\models\records\ProjectDefence;
use app\utils\DateTime;
use yii;
use app\models\records\DefenceDate;
use app\exceptions\BugError;

use TiBeN\CrontabManager\CrontabRepository;
use TiBeN\CrontabManager\CrontabAdapter;
use TiBeN\CrontabManager\CrontabJob;

/**
 * @Author: Gergely
 */

// This is the script that runs every day at 7:00.

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require_once __DIR__ . '/upcomingDefenceFunctions.php';

$config = require __DIR__ . '/../config/console.php';
$application = new yii\console\Application($config);


function createCronJobForTime(DateTime $jobTime) : ?CrontabJob {
    if ($jobTime->isBefore(new DateTime('now'))) {
        $jobTime = new DateTime('now');
        $jobTime->modify('+1 minute');
    }

    $crontabJob = new CrontabJob();
    $crontabJob->minutes = $jobTime->format('i');
    $crontabJob->hours = $jobTime->format('G');
    $crontabJob->dayOfMonth = $jobTime->format('j');
    $crontabJob->months = $jobTime->format('n');
    $crontabJob->dayOfWeek = '*';

    $crontabJob->taskCommandLine = 'php ' . getcwd() . '/upcomingDefenceInHours.php';
    $crontabJob->comments = UPCOMING_DEFENCE_IN_HOURS_COMMENT;

    return $crontabJob;
}

/** Returns NULL if the time in the ProjectDefence is NULL.
 * @param \app\models\records\DefenceDate $defenceDate
 * @return DateTime|null
 * @throws BugError
 */
function getDefenceTime(DefenceDate $defenceDate) : ?DateTime {
    $defenceDateId = $defenceDate->id;
    $projectDefence = ProjectDefence::findOne(["defence_date_id" => $defenceDateId]);
    if(!$projectDefence) {
        return NULL;
    }

    if (!$projectDefence->time) {
        return NULL;
    }

    return DateTime::from($projectDefence->time);
}

/** Finds the first defence date in the sorted (by datetime) array whose ProjectDefence->time is specified. Returns
 * NULL if none exist.
 * @param array $defenceDates
 * @return DateTime|null
 */
function findFirstSpecifiedDefenceTime(array $defenceDates) : ?DateTime {
    $arrayIndex = 0;
    $firstDefenceTime = NULL;

    while (!$firstDefenceTime) {
        if ($arrayIndex > count($defenceDates) - 1) {
            return NULL;
        }

        $firstDefenceTime = getDefenceTime($defenceDates[$arrayIndex]);
        $arrayIndex++;
    }

    return $firstDefenceTime;
}

function scheduleCronJobForToday(array $defenceDates, string $timeLeft) : void {
    sortArrayBasedOnDate($defenceDates);

    $firstDefenceTime = findFirstSpecifiedDefenceTime($defenceDates);
    if (!$firstDefenceTime) {
        return;
    }

    $notificationTime = $firstDefenceTime->modifyClone('-' . $timeLeft);

    $cronJob = createCronJobForTime($notificationTime);

    $crontabRepository = new CrontabRepository(new CrontabAdapter());
    $crontabRepository->addJob($cronJob);
    $crontabRepository->persist();
}

function getTimeBasedOnParameterString(?string $timeAmountBeforeReminder) : ?DateTime
{
    if (!$timeAmountBeforeReminder)
    {
        return NULL;
    }

    if ($timeAmountBeforeReminder == '')
    {
        return NULL;
    }

    return new DateTime('+' . $timeAmountBeforeReminder);
}

$today = getTimeBasedOnParameterString(Yii::$app->params[Param::AMOUNT_OF_HOURS_BEFORE_DEFENCE_REMINDER]);
$daysFromNow = getTimeBasedOnParameterString(Yii::$app->params[Param::AMOUNT_OF_DAYS_BEFORE_DEFENCE_REMINDER]);
$weekFromNow = getTimeBasedOnParameterString(Yii::$app->params[Param::AMOUNT_OF_WEEKS_BEFORE_DEFENCE_REMINDER]);

$defencesWeekLater = [];
$defencesDaysLater = [];
$defencesToday = [];

$allDefenceDates = DefenceDate::find()->all();
foreach ($allDefenceDates as $defenceDate) {
    if ($defenceDate->deleted)
    {
        continue;
    }

    $dateOfDefence = DateTime::from($defenceDate->date);

    if ($dateOfDefence->isOnSameDayAs($weekFromNow)) {
        $defencesWeekLater[] = $defenceDate;
    }
    else if ($dateOfDefence->isOnSameDayAs($daysFromNow)) {
        $defencesDaysLater[] = $defenceDate;
    }
    else if ($dateOfDefence->isOnSameDayAs($today)) {
        $defencesToday[] = $defenceDate;
    }
}

if (count($defencesWeekLater) > 0) {
    sendOutEmails($defencesWeekLater, Yii::$app->params[Param::AMOUNT_OF_WEEKS_BEFORE_DEFENCE_REMINDER]);
}

if (count($defencesDaysLater) > 0) {
    sendOutEmails($defencesDaysLater, Yii::$app->params[Param::AMOUNT_OF_DAYS_BEFORE_DEFENCE_REMINDER]);
}

if (count($defencesToday) > 0) {
    scheduleCronJobForToday($defencesToday, Yii::$app->params[Param::AMOUNT_OF_HOURS_BEFORE_DEFENCE_REMINDER]);
}

