<?php

use app\models\records\DefenceDate;
use app\models\records\ProjectDefence;
use app\models\records\Project;
use app\models\records\DefenceAttendance;
use app\models\records\User;
use yii\helpers\Html;


/**
 * @Author: Gergely
 */

/**
 * @param array $defenceAttendance
 * @return array first element is the link to the defence, second is the signup deadline, third is the defence date,
 * fourth is the name of the project
 * The fourth element may be non-existent, if there is no ProjectDefence associated
 */
function getDefenceProperties(DefenceAttendance $defenceAttendance) : array {
    $defenceProperties = [];

    $defenceProperties["defenceLink"] =
        Yii::$app->urlManager->createAbsoluteUrl(['defences/view', 'id' => $defenceAttendance->id]);

    $defenceDateEntry = DefenceDate::findOne($defenceAttendance->defence_date_id);
    $defenceProperties["signupDeadline"] = $defenceDateEntry->signup_deadline;

    $projectDefence = ProjectDefence::findOne($defenceDateEntry->id);
    if (!$projectDefence) {
        $defenceProperties["defenceDate"] = $defenceDateEntry->date;
        return $defenceProperties;
    }
    $defenceProperties["defenceDate"] = $projectDefence->time ? $projectDefence->time : $defenceDateEntry->date;

    $project = Project::findOne($projectDefence->project_id);
    $defenceProperties["projectName"] = $project->name;

    return $defenceProperties;
}

function isUserCommitteeMember(User $user) : bool {
    $userRole = $user->role;

    return $userRole == User::ROLE_COMMITTEE_MEMBER
        || $userRole == User::ROLE_COMMITTEE_CHAIR;
}

function createDefenceAttendanceHTML(DefenceAttendance $defenceAttendance,  string $language) : string {
    $defenceProperties = getDefenceProperties($defenceAttendance);
    $projectName = $defenceProperties["projectName"] ? $defenceProperties["projectName"] : 'not specified';

    $resultString = "<li> ";
    $resultString .= Html::a(Html::encode($projectName), $defenceProperties["defenceLink"]) . "\n" ;
    $resultString .= " " . Yii::t('app', 'at', [], $language) . " <em> " . Html::encode($defenceProperties["defenceDate"]) . " </em>";
    $resultString .= " - " . Yii::t('app', 'sign up until', [], $language) . " <strong> " . Html::encode($defenceProperties["signupDeadline"]) . " </strong> !";
    $resultString .= " </li>";

    return $resultString;
}

function createDefenceAttendanceText(DefenceAttendance $defenceAttendance, string $language) : string {
    $defenceProperties = getDefenceProperties($defenceAttendance);
    $projectName = $defenceProperties["projectName"] ? $defenceProperties["projectName"] : 'not specified';

    $resultString = $projectName . " [" . $defenceProperties["defenceLink"] . "] \n" ;
    $resultString .= " " . Yii::t('app', 'at', [], $language) . " " . $defenceProperties["defenceDate"];
    $resultString .= " - " . Yii::t('app', 'sign up until', [], $language) . " " . $defenceProperties["signupDeadline"] . "!";

    return $resultString;
}

function createDefenceHTML(ProjectDefence $projectDefence, string $language) : string {
    $resultString = "";
    $defenceRoom = $projectDefence->room ? $projectDefence->room : Yii::t('app','not specified', [], $language);

    $resultString .= "<ul> ";
    $resultString .= "<li>" . Yii::t('app', 'Defence date', [], $language) . ": " . Html::encode($projectDefence->time) . " </li>";
    $resultString .= "<li>" . Yii::t('app', 'Room', [], $language) . ": " . Html::encode($defenceRoom) . " </li>";
    $resultString .= "<li>" . Yii::t('app', 'Project', [], $language) . ": " . Html::a(Html::encode($projectDefence->project->name),
            Yii::$app->urlManager->createAbsoluteUrl(['projects/view', 'id' => $projectDefence->project->id])) . " </li>";
    $resultString .= " </ul>";

    return $resultString;
}

function createDefenceText(ProjectDefence $projectDefence, string $language) : string {
    $resultString = "";
    $defenceRoom = $projectDefence->room ? $projectDefence->room : "not specified";

    $resultString .= Yii::t('app', 'Defence date', [], $language) . ": " . $projectDefence->time . " \n";
    $resultString .= Yii::t('app', 'Room', [], $language) . ": " . $defenceRoom . "\n";
    $resultString .= Yii::t('app', 'Project', [], $language) . ": " . $projectDefence->project->name
        . " [ " . Yii::$app->urlManager->createAbsoluteUrl(['projects/view', 'id' => $projectDefence->project->id]) . " ] \n \n";

    return $resultString;
}

