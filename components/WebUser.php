<?php
namespace app\components;

use app\consts\Sql;
use app\exceptions\BugError;
use app\exceptions\GuestAttemptsToLoginException;
use app\models\records\User;
use Yii;
use yii\rbac\PhpManager;
use yii\rbac\Role;

class WebUser extends \yii\web\User
{
    /**
     * @inheritdoc
     *
     * @throws GuestAttemptsToLoginException
     */
    protected function beforeLogin($identity, $cookieBased, $duration) : bool
    {
        parent::beforeLogin($identity, $cookieBased, $duration);

        /** @var $auth PhpManager */
        $auth = Yii::$app->authManager;
        $this->assertRbacItemsFileExists($auth);

        $roleId = User::findIdentity($identity->getId())->role;
        if ($roleId === null) {
            throw new GuestAttemptsToLoginException();
        }

        /** @var Role[] $roles */
        $roles = $auth->getRoles();
        $this->assertRoleExists($roleId, $roles);

        return true;
    }

    /**
     * @inheritdoc
     *
     * Actions after login - e.g. assign RBAC role
     * We must assign RBAC roles only after successful login,
     * while login does not depend on assigned RBAC role(s) in any way
     */
    protected function afterLogin($identity, $cookieBased, $duration)
    {
        parent::afterLogin($identity, $cookieBased, $duration);

        $userId = $identity->getId();
        $user = User::findIdentity($userId);
        $roleId = $user->role;
        /** @var $auth PhpManager */
        $auth = Yii::$app->authManager;
        $roles = $auth->getRoles();
        /** @var Role[] $roles */
        $role = $roles[$roleId];
        $this->assignRoleTo($role, $userId, $auth);

        $user->last_login = Sql::now();
        $user->save();
    }


    /**
     * @param PhpManager $auth
     * @throws BugError
     */
    private function assertRbacItemsFileExists(PhpManager $auth) : void
    {
        if (!is_file($auth->itemFile)) {
            throw new BugError('Missing RBAC files.');
        }
    }

    /**
     * @param null|string $role
     * @param array $roles
     * @throws BugError
     */
    private function assertRoleExists(? string $role, array $roles) : void
    {
        if (!isset($roles[$role])) {
            throw new BugError('Unknown role (' . $role . ').');
        }
    }

    private function assignRoleTo(Role $role, int $userId, PhpManager $auth) : void
    {
        // remove any previous assignments stored in RBAC file for this user
        // we must do this because roles are not automatically revoked when e.g. changed user permission via CRUD
        $auth->revokeAll($userId);

        // assign only one role to current user ID (assigned role is written into RBAC file)
        $auth->assign($role, $userId);

        // issue: instant redirect wont refresh cached RBAC file - so if role changed it will apply on next request!
        // fix: add to etc/php.d/opcache-default.blacklist:
        // /app/backend/rbac
        // this will exclude caching of rbac files and apply changed roles
        // better solution would be to support storing assigned roles simply into session (like Yii.1)
        clearstatcache(true);
    }
}