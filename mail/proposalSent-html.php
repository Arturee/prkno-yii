<?php
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $project \app\models\records\Project */

?>
<div class="proposal-sent">
    <p>
        Návrh projektu
        <?= Html::a($project->short_name, [ 'projects/view', 'id' => $project->id ]) ?>
        byl zaslán a nyní čeká na schválení.
    </p>
    <p>........ konec e-mailu v českém jazyce ........</p>
    <p>
        Proposal of the project
        <?= Html::a($project->short_name, [ 'projects/view', 'id' => $project->id ]) ?>
        was sent and now is waiting for approval.
    </p>
    <p>........ end ........</p>
</div>
