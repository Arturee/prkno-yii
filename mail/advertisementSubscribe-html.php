<?php
use yii\helpers\Html;
use app\models\records\Advertisement;
use app\models\records\AdvertisementComment;

/* @var $this yii\web\View */
/* @var $comment \app\models\records\AdvertisementComment */
/* @var $advertisement \app\models\records\Advertisement */
$advertisementLink = Yii::$app->urlManager->createAbsoluteUrl(['advertisements/view', 'id' => $advertisement->id]);
?>
<div class="advertisement-subscribe">
    <p>Nový komentář na tématu: <?= Html::a(Html::encode($advertisement->title), $advertisementLink) ?></p>

    <div>
        <p><?= Html::encode($comment->user->getFullNameWithDegrees()) ?></p>
        <p><?= Html::encode($comment->content) ?></p>
    </div>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>New comment in your advertisement: <?= Html::a(Html::encode($advertisement->title), $advertisementLink) ?></p>

    <div>
        <p><?= Html::encode($comment->user->getFullNameWithDegrees()) ?></p>
        <p><?= Html::encode($comment->content) ?></p>
    </div>
    <p>........ end ........</p>
</div>
