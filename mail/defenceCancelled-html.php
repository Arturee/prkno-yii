<?php
use app\utils\DateTime;

/* @var $this yii\web\View */
/* @var $defence \app\models\records\ProjectDefence */
$time = $defence->time;
if ($time) {
    $time = DateTime::time($defence->time);
} else {
    $time = '';
}
?>
<div class="defence-cancelled">
    <p>Obhajoba byla zrušena
        <?= $defence->project->short_name ?>
        <?= $defence->project->name ?>
        <?= DateTime::time($defence->time) ?>
        <?= DateTime::date($defence->defenceDate->date) ?>
    </p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>Defence cancelled
        <?= $defence->project->short_name ?>
        <?= $defence->project->name ?>
        <?= DateTime::time($defence->time) ?>
        <?= DateTime::date($defence->defenceDate->date) ?>
    </p>
    <p>........ end ........</p>
</div>
