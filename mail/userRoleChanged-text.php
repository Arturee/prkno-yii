<?php
use app\models\records\User;

/* @var $this yii\web\View */
/* @var $user User */

?>
Zdravím <?= $user->name ?>.

Vaše role byla změněna na: <?= Yii::t('app', User::roleToString($user->role), [], 'cs') ?>.

........ konec e-mailu v českém jazyce ........

Hello <?= $user->name ?>.

Your role was changed to: <?= Yii::t('app', User::roleToString($user->role), [], 'en') ?>.

........ end ........