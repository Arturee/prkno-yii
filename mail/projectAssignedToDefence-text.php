<?php
use \app\utils\DateTime;

/* @var $this yii\web\View */
/* @var $defence \app\models\records\ProjectDefence */

$date = DateTime::from($defence->defenceDate->date)->format(DateTime::DATE);
?>
Váš projekt <?= $defence->project->short_name ?> byl přihlášen k obhajobě dne <?= $date ?>.

........ konec e-mailu v českém jazyce ........

Your project <?= $defence->project->short_name ?> was assigned to a defence on <?= $date ?>.

........ end ........