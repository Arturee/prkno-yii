<?php
use app\models\records\Advertisement;
use app\models\records\AdvertisementComment;

/* @var $this yii\web\View */
/* @var $comment AdvertisementComment */
/* @var $advertisement Advertisement */
$advertisementLink = Yii::$app->urlManager->createAbsoluteUrl(['advertisements/view', 'id' => $advertisement->id]);
?>
Nový komentář na tématu: <?= $advertisement->title ?> [ <?= $advertisementLink ?> ]


    <?= $comment->user->getFullNameWithDegrees() ?>

    <?= $comment->content ?>

........ konec e-mailu v českém jazyce ........

New comment in your advertisement: <?= $advertisement->title ?> [ <?= $advertisementLink ?> ]


    <?= $comment->user->getFullNameWithDegrees() ?>

    <?= $comment->content ?>

........ end ........