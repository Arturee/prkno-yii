<?php

/* @var $this yii\web\View */
/* @var $defence \app\models\records\ProjectDefence */

$project = $defence->project;
?>
Prosim, napište <?= $project->short_name ?> a zašlete svůj posudek alespoň 2 dny před datem obhajoby, což je
<? $defence->defenceDate->date ?>.

........ konec e-mailu v českém jazyce ........

Please review <?= $project->short_name ?> and submit your review at least 2 days before the defense date, which is
<? $defence->defenceDate->date ?>.

........ end ........