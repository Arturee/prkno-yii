<?php

namespace app\mail;

use app\models\records\DefenceAttendance;
use app\models\records\DefenceDate;
use app\models\records\Proposal;
use app\models\records\AdvertisementComment;
use app\models\records\Advertisement;
use app\models\records\News;
use app\models\records\ProjectDefence;
use app\models\records\Project;
use app\models\records\User;
use Yii;

class Emails
{
    /**
     * Send reminder e-mail that committee member hasn't set
     * whether he or she can come to the defence date
     * @param User $user                        Recipient
     * @param DefenceAttendance[] $attendances  Array of defence attendances
     */
    public static function reminderDefenceAttendance(User $user, array $attendances): void {
        self::email(
            [ $user ],
            'cron/defenceParticipation',
            ['user' => $user, 'defenceAttendances' => $attendances],
            Yii::t('app', 'Reminder - missing project defence attendance' )
        );
    }

    /**
     * Send reminder e-mail about missing proposal evaluation
     * @param User $user            Recipient
     * @param Proposal[] $proposals Array of proposals
     */
    public static function reminderProposalEvaluation(User $user, array $proposals): void {
        self::email(
            [ $user ],
            'cron/proposalDecision',
            ['user' => $user, 'proposals' => $proposals],
            Yii::t('app', 'Reminder - missing proposal evaluation' )
        );
    }

    /**
     * Notify by e-mail user that defence is upcoming
     * @param User $user    Recipient
     * @param DefenceDate[] $defenceDates   Array of defence dates
     * @param string $timeLeft      Time left to the defence Date
     */
    public static function reminderUpcomingDefence(User $user, array $defenceDates, string $timeLeft): void {
        self::email(
            [ $user ],
            'cron/upcomingDefence',
            ['user' => $user, 'defenceDates' => $defenceDates, 'timeLeft' => $timeLeft],
            Yii::t('app', 'Reminder - project defence is coming up in {0}', [ $timeLeft ] )
        );
    }

    /**
     * Send e-mail about resetting password
     * @param User $user    Recipient
     */
    public static function userResetPassword(User $user): void {
        self::email(
            [ $user ],
            'passwordRecoveryToken',
            [ 'user' => $user ],
            Yii::t('app', 'Password reset for {0}',[ Yii::$app->name ])
        );
    }

    /**
     * Send e-mail about changing the role
     * @param User $user    Recipient
     */
    public static function userRoleChanged(User $user): void
    {
        self::email(
            [ $user ],
            'userRoleChanged',
            [ 'user' => $user ],
            Yii::t('app', 'Role changed for {0}',[ Yii::$app->name ])
        );
    }

    /**
     * Send e-mail about changing the login type to manual.
     * User is asked to change a password.
     * @param User $user
     */
    public static function userLoginTypeChanged(User $user)
    {
        self::email(
            [ $user ],
            'passwordChange',
            [ 'user' => $user ],
            Yii::t('app', 'New password for {0}',[ Yii::$app->name ])
        );
    }

    /**
     * Send email to the user about changing state of the project
     * @param Project $project          Project
     * @param string $previousState     Previous state of the project
     * @param User $user                Recipient
     */
    public static function projectStateChanged(Project $project, string $previousState, User $user): void
    {
        self::email(
            [ $user ],
            'projectStateChange',
            [ 'project' => $project, 'previousState' => $previousState ],
            Yii::t('app', 'Your project is now in the state of {0}',[ $project->state ])
        );
    }

    /**
     * Send e-mail about aproaching deadline for uploading documents for the defence
     * @param Project   $project    Project
     */
    public static function projectDocumentsDeadline(Project $project): void
    {
        self::email(
            $project->getTeamAndSupervisor(),
            'cron/projectDocumentsDeadline',
            [ 'project' => $project ],
            Yii::t('app', 'Deadline for documents')
        );
    }

    /**
     * Send e-mail notifying that project documents were uploaded
     * @param Project $project  Project
     * @param User $user        Recipient
     */
    public static function projectDocumentsUploaded(Project $project, User $user): void
    {
        self::email(
            [ $user ],
            'projectDocumentsUploaded',
            [ 'project' => $project ],
            Yii::t('app', 'Defence documents was uploaded')
        );
    }

    /**
     * Send e-mail about project analysis deadline has passed
     * @param Project $project      Project
     * @param User[] $users         Recipients
     */
    public static function projectAnalysisDeadline(Project $project, array $users): void
    {
        self::email(
            $users,
            'cron/projectAnalysisDeadline',
            [ 'project' => $project],
            Yii::t('app', 'The project {0} analysis deadline has passed', [ $project->short_name ])
        );
    }

    /**
     * Send e-mail to recipients about project implementation deadline has passed
     * @param Project $project  Project
     * @param User[] $users     Recipients
     */
    public static function projectImplementationDeadline(Project $project, array $users): void
    {
        self::email(
            $users,
            'cron/projectImplementationDeadline',
            [ 'project' => $project],
            Yii::t('app', 'The project {0} implementation deadline has passed', [ $project->short_name ])
        );
    }

    /**
     * Send e-mail to recipients that deadline for conditional
     * defence has passed
     * @param Project $project  Project
     * @param User[] $users     Recipients
     */
    public static function projectConditionallyDefendedDeadline(Project $project, array $users): void
    {
        self::email(
            $users,
            'cron/projectConditionallyDefendedDeadline',
            [ 'project' => $project],
            Yii::t('app', 'The project {0} conditionally defended deadline has passed', [ $project->short_name ])
        );
    }

    /**
     * Send e-mail that someone commented the proposal
     * @param Proposal $proposal    Proposal
     * @param User $user            Recipients
     */
    public static function proposalCommented(Proposal $proposal, User $user): void
    {
        self::email(
            [ $user ],
            'proposalCommented',
            [ 'proposal' => $proposal],
            Yii::t('app', 'Proposal has new comment')
        );
    }

    /**
     * Send e-mail that decision was made about given proposal
     * @param Proposal $proposal    Proposal
     * @param User $user            User
     */
    public static function proposalDecision(Proposal $proposal, User $user): void
    {
        self::email(
            [ $user ],
            'proposalDecision',
            [ 'proposal' => $proposal],
            Yii::t('app', 'Proposal decision')
        );
    }

    /**
     * Send e-mail that proposal was submitted and
     * know is waiting for approval
     * @param Project $project  Project
     * @param User $user        User
     */
    public static function proposalSent(Project $project, User $user): void
    {
        self::email(
            [ $user ],
            'proposalSent',
            [ 'project' => $project],
            Yii::t('app', 'Proposal was sent for approval')
        );
    }

    /**
     * Send e-mail that the project was assigned to a defence date
     * @param ProjectDefence $model         Project defence
     * @param User[] $teamAndSupervisor     Team members and the supervisor
     */
    public static function projectAssignedToDefence(ProjectDefence $model, array $teamAndSupervisor): void
    {
        self::email(
            $teamAndSupervisor,
            'projectAssignedToDefence',
            [ 'defence' => $model ],
            Yii::t('app', 'Your project was assigned to defence on {0}', [ $model->defenceDate->date ])
        );
    }

    /**
     * Send e-mail that the supervisor should upload the review for the project defence
     * @param ProjectDefence $model     Project defence
     * @param User $supervisor          Recipient
     */
    public static function requestReviewFromASupervisor(ProjectDefence $model, User $supervisor): void
    {
        self::email(
            [ $supervisor ],
            'defenceSupervisorReview',
            [ 'defence' => $model ],
            Yii::t('app', 'Please review {0}', [ $model->project->short_name ])
        );
    }

    /**
     * Send e-mail to a committee member that
     * he/she has to come to a defence
     * @param DefenceDate $model    Defence date
     * @param User $user            Recipient
     */
    public static function informACommitteeMemberThatTheyMustComeToADefence(DefenceDate $model, User $user): void
    {
        self::email(
            [ $user ],
            'defenceMustCome',
            [ 'defenceDate' => $model ],
            Yii::t('app', 'You were selected to attend defences on {0}', [ $model->date ])
        );
    }

    /**
     * Send e-mail that defence date was created
     * @param DefenceDate $model    DefenceDate
     */
    public static function defenceDateCreated(DefenceDate $model): void
    {
        self::email(
            User::findNewsSubscribedUsers(),
            'defenceNewDate',
            [ 'defenceDate' => $model ],
            Yii::t('app', 'New defence date {0}', [ $model->date ])
        );
    }

    /**
     * Send e-mail that defence time for given project was scheduled
     * @param ProjectDefence $model     Project defence
     * @param User[] $users             Recipients
     * @param string $dateTime          Defence time
     */
    public static function defenceTimeScheduled(ProjectDefence $model, array $users, string $dateTime): void
    {
        self::email(
            $users,
            'defenceTime', [ 'defence' => $model, 'dateTime' => $dateTime ],
            Yii::t('app', 'Defence of {0} scheduled at {1}', [ $model->project->short_name, $dateTime ])
        );
    }

    /**
     * Send e-mail that opponent for the given project was chosen
     * @param ProjectDefence $model     Project defence
     * @param User $user                User
     */
    public static function defenceOpponentChosen(ProjectDefence $model, User $user): void
    {
        self::email(
            [ $user ],
            'defenceOpponentChosen',
            [ 'model' => $model ],
            Yii::t('app', 'You were selected to oppose {0}', [ $model->project->short_name ])
        );
    }

    /**
     * Send e-mail that defence review was uploaded
     * @param ProjectDefence $model     Project defence
     * @param string $type              Type of the review (supervisor review, opponent review, final review)
     */
    public static function defenceReviewUploaded(ProjectDefence $model, string $type): void
    {
        self::email(
            $model->project->getTeamAndSupervisor(),
            'defenceReviewUploaded',
            [ 'type' => $type ],
            Yii::t('app', 'Your project was reviewed.')
        );
    }

    /**
     * Send e-mail that defence was cancelled
     * @param ProjectDefence $defence   Project defence
     */
    public static function defenceCancelled(ProjectDefence $defence): void
    {
        $project = $defence->project;
        $committee = User::findCommitteeMembers();
        $team = $project->getTeamAndSupervisor();
        $users = array_merge($committee, $team);
        self::email(
            $users,
            'defenceCancelled',
            [ 'defence' => $defence ],
            Yii::t('app', 'Defence of {0} cancelled', [ $project->short_name ])
        );
    }

    /**
     * Send e-mail that advertisement which I was subscribed to, was commented
     * @param AdvertisementComment $comment     Advertisement comment
     */
    public static function advertisementAddComment(AdvertisementComment $comment): void
    {
        self::email(
            [ $comment->ad->user ],
            'advertisementSubscribe',
            [ 'advertisement' => $comment->ad, 'comment' => $comment ],
            Yii::t('app','New comment in {advertisement}', ['advertisement' => $comment->ad])
        );
    }

    /**
     * Send e-mail that news was created
     * @param User $user    Recipient
     * @param News $news    News
     */
    public static function newsCreated(User $user, News $news): void
    {
        self::email(
            [ $user ],
            'newsCreated',
            [ 'news' => $news ],
            Yii::t('app', 'News update')
        );
    }

    /**
     * General method for sending e-mails
     * @param User[] $recipients    Recipients
     * @param string $templateName  File name of the e-mail template
     * @param array $templateData   Key -> value object containing data for template
     * @param string $subject       Subject of the e-mail
     */
    private static function email(array $recipients, string $templateName, array $templateData, string $subject): void
    {
        /** @var Mailer $mailer */
        $mailer = Yii::$app->mailer;
        foreach ($recipients as $recipient) {
            $mailer->fromTemplates($templateName, $templateData)
                ->setTo($recipient->email)
                ->setSubject($subject)
                ->send();
        }
    }
}