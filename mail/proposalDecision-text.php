<?php

/* @var $this yii\web\View */

use app\models\records\Proposal;

/* @var $proposal \app\models\records\Proposal */

?>
Návrh projektu <?= $proposal->project->short_name ?> byl rozhodnut jako <?= Proposal::getStateToString($proposal->state, 'cs') ?>.
Viz stránka hlasování o návrhu.

........ konec e-mailu v českém jazyce ........

The proposal of the project <?= $proposal->project->short_name ?> was decided as <?= Proposal::getStateToString($proposal->state, 'en') ?>.
See on proposal voting page.

........ end ........