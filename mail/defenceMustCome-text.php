<?php

/* @var $this yii\web\View */
/* @var $defenceDate \app\models\records\DefenceDate */

$defenceLink = Yii::$app->urlManager->createAbsoluteUrl(['defence-dates/view', 'id' => $defenceDate->id]);
?>
Byl jste vybrán k účasti na obhajobě dne <?= $defenceDate->date ?> jako část výboru.
<?= $defenceLink ?>

........ konec e-mailu v českém jazyce ........

You were selected to attend defences at <?= $defenceDate->date ?>, as part of the committee.
<?= $defenceLink ?>

........ end ........