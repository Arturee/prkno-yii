<?php
use app\utils\DateTime;

/* @var $this yii\web\View */
/* @var $defenceDate \app\models\records\DefenceDate */

$defenceLink = Yii::$app->urlManager->createAbsoluteUrl(['defence-dates/view', 'id' => $defenceDate->id]);
$date = DateTime::from($defenceDate->date)->format(DateTime::DATE);
?>
Nový termín obhajob byl vytvořen: <?= $date ?>.
<?= $defenceLink ?>

........ konec e-mailu v českém jazyce ........

New defence date was created: <?= $date ?>.
<?= $defenceLink ?>

........ end ........
