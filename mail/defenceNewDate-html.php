<?php
use yii\helpers\Html;
use app\utils\DateTime;

/* @var $this yii\web\View */
/* @var $defenceDate \app\models\records\DefenceDate */

$defenceLink = Yii::$app->urlManager->createAbsoluteUrl(['defence-dates/view', 'id' => $defenceDate->id]);
$date = DateTime::from($defenceDate->date)->format(DateTime::DATE);
?>
<div class="defence-date-created">

    <p>Nový termín obhajoby byl vytvořen: <?= Html::a($date, $defenceLink) ?>.</p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>New defence date was created: <?= Html::a($date, $defenceLink) ?>.</p>
    <p>........ end ........</p>

</div>
