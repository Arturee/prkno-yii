<?php

/* @var $this yii\web\View */
/* @var $project \app\models\records\Project */
/* @var $previousState string */

?>
Zdravím.
Váš projekt <?= $project->short_name ?> přešel na stav <?= Yii::t('app', $project->state, [], 'cs') ?> (ze stavu <?= Yii::t('app', $previousState, [], 'cs') ?>).

........ konec e-mailu v českém jazyce ........

Hello.
Your project <?= $project->short_name ?> has transitioned to state <?= Yii::t('app', $project->state, [], 'en') ?> (from state <?= Yii::t('app', $previousState, [], 'en') ?>).

........ end ........