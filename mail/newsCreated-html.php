<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $news \app\models\records\News */

?>
<div class="news-created">
    <p>........ <?= Yii::t('app', 'News', [], 'cs') . ' / ' .  Yii::t('app', 'News', [], 'en') ?> (english below) ........</p>

    <p>
        <h1><?= Html::encode($news->getLocalTitle('cs')) ?></h1>
    </p>
    <p><?= Html::encode($news->getLocalContent('cs')) ?></p>
    <p>........ konec e-mailu v českém jazyce ........</p>
    <br>
    <p>
        <h1><?= Html::encode($news->getLocalTitle('en')) ?></h1>
    </p>
    <p><?= Html::encode($news->getLocalContent('en')) ?></p>
    <p>........ end ........</p>
</div>