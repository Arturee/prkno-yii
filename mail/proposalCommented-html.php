<?php
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $proposal \app\models\records\Proposal */

?>
<div class="proposal-commented">
    <p>
        Návrh projektu <?= Html::a($proposal->project->short_name, [ 'projects/view', 'id' => $proposal->project_id ]) ?> má nový komentář.
        Viz <?= Html::a('stránka hlasování o návrhu', ['proposals/view', 'id' => $proposal->id]) ?>.
    </p>
    <p>........ konec e-mailu v českém jazyce ........</p>
    <p>
        The proposal of the project <?= Html::a($proposal->project->short_name, [ 'projects/view', 'id' => $proposal->project_id ]) ?> has a new comment.
        See it on <?= Html::a('proposal voting page', ['proposals/view', 'id' => $proposal->id]) ?>.
    </p>
    <p>........ end ........</p>
</div>