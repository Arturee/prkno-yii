<?php

/* @var $this yii\web\View */
/* @var $proposal \app\models\records\Proposal */

?>
Návrh projektu <?= $proposal->project->short_name ?> má nový komentář.
Viz stránka hlasování o návrhu.

........ konec e-mailu v českém jazyce ........

The proposal of the project <?= $proposal->project->short_name ?> has a new comment.
See it on proposal voting page.

........ end ........