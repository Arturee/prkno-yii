<?php

/* @var $this yii\web\View */
/* @var $user \app\models\records\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_recovery_token]);
?>
Zdravím, <?= $user->name ?>.

Pro obnovení hesla postupujte podle níže uvedeného odkazu:

<?= $resetLink ?>

........ konec e-mailu v českém jazyce ........

Hello <?= $user->name ?>.

Follow the link below to reset your password:
<?= $resetLink ?>

........ end ........
