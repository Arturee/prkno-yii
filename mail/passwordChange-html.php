<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \app\models\records\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_recovery_token]);
?>
<div class="password-reset">
    <p>Zdravím <?= Html::encode($user->name) ?>.</p>
    <p>Váš typ přihlášení byl změněn na manuální. Pro přihlášení je nutné nové heslo.
        Pro vytvoření nového hesla postupujte podle níže uvedeného odkazu:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>Hello <?= Html::encode($user->name) ?>.</p>
    <p>Your login type was changed to manual. New password is needed.
        Please, follow the link below to create new password:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>........ end ........</p>
</div>
