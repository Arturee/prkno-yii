<?php

/* @var $this yii\web\View */
/* @var $type string */

?>
<div class="defence-review">
    <p><?= Yii::t('app', $type, [], 'cs') ?> posúdil Váš projekt.</p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p><?= Yii::t('app', $type, [], 'en') ?> has reviewed your project.</p>
    <p>........ end ........</p>
</div>
