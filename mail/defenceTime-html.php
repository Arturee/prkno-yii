<?php
use yii\helpers\Html;

/* @var $dateTime string */
/* @var $defence \app\models\records\ProjectDefence */
?>
<div class="defence-time">
    <p>
        <?= Html::a("Obhajoba", ['defences/view', 'id' => $defence->id]) ?> vašeho projektu <?= $defence->project->short_name ?> byla naplánována na <?= $dateTime ?>.
    </p>
    <p>........ konec e-mailu v českém jazyce ........</p>
    <br>
    <p>
        <?= Html::a("The defence", ['defences/view', 'id' => $defence->id]) ?> of your project <?= $defence->project->short_name ?> was scheduled for <?= $dateTime ?>.
    </p>
    <p>........ end ........</p>
</div>
