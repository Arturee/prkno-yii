<?php
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $project \app\models\records\Project */
/* @var $previousState string */

?>
<div class="project-state-change">
    <p>Zdravím.</p>
    <p>Váš projekt <?= Html::a($project->short_name, [ 'projects/view', 'id' => $project->id ]) ?> přešel na stav
        <?= Yii::t('app', $project->state, [], 'cs') ?> (ze stavu <?= Yii::t('app', $previousState, [], 'cs') ?>).
    </p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>Hello.</p>
    <p>Your project <?= Html::a($project->short_name, [ 'projects/view', 'id' => $project->id ]) ?> has transitioned
        to state <?= Yii::t('app', $project->state, [], 'en') ?> (from state <?= Yii::t('app', $previousState, [], 'en') ?>).
    </p>
    <p>........ end ........</p>
</div>
