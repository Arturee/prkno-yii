<?php

/* @var $this yii\web\View */
/* @var $defenceDate \app\models\records\DefenceDate */

?>
<div class="defence-date-created">
    <p>Obhajoby dne <?= $defenceDate->date ?> byly zrušeny.</p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>Defences on  <?= $defenceDate->date ?> are cancelled.</p>
    <p>........ end ........</p>
</div>
