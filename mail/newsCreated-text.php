<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $news \app\models\records\News */
?>
........ <?= Yii::t('app', 'News', [], 'cs') . ' / ' .  Yii::t('app', 'News', [], 'en') ?> (english below) ........

<?= Html::encode($news->getLocalTitle('cs')) ?>

<?= Html::encode($news->getLocalContent('cs')) ?>
........ konec e-mailu v českém jazyce ........


<?= Html::encode($news->getLocalTitle('en')) ?>

<?= Html::encode($news->getLocalContent('en')) ?>
........ end ........