<?php
use yii\helpers\Html;
use app\models\records\User;

/* @var $this yii\web\View */
/* @var $user \app\models\records\User */

?>
<div class="user-role-changed">
    <p>Zdravím <?= Html::encode($user->name) ?>.</p>
    <p>Vaše role byla změněna na: <?=Html::encode(Yii::t('app', User::roleToString($user->role), [], 'cs')) ?>.</p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>Hello <?= Html::encode($user->name) ?>.</p>
    <p>Your role was changed to: <?=Html::encode(Yii::t('app', User::roleToString($user->role), [], 'en')) ?>.</p>
    <p>........ end ........</p>
</div>
