<?php
use yii\helpers\Html;

/* @var $dateTime string */
/* @var $defence \app\models\records\ProjectDefence */
?>
<?= Html::a("Obhajoba", ['defences/view', 'id' => $defence->id]) ?> vašeho projektu <?= $defence->project->short_name ?> byla naplánována na <?= $dateTime ?>.

........ konec e-mailu v českém jazyce ........

<?= Html::a("The defence", ['defences/view', 'id' => $defence->id]) ?> of your project <?= $defence->project->short_name ?> was scheduled for <?= $dateTime ?>.

........ end ........
