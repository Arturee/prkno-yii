<?php

use app\utils\DateTime;

/* @var $this yii\web\View */
/* @var $defence \app\models\records\ProjectDefence */
$time = $defence->time;
$time = $time ? DateTime::time($defence->time) : '';
?>
Obhajoba byla zrušena
<?= $defence->project->short_name ?>
<?= $defence->project->name ?>
<?= DateTime::date($defence->defenceDate->date) ?>

........ konec e-mailu v českém jazyce ........

Defence cancelled
    <?= $defence->project->short_name ?>
    <?= $defence->project->name ?>
    <?= DateTime::date($defence->defenceDate->date) ?>

........ end ........