<?php

/* @var $this yii\web\View */
/* @var $user \app\models\records\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_recovery_token]);
?>
Zdravím, <?= $user->name ?>.

Váš typ přihlášení byl změněn na manuální. Pro přihlášení je nutné nové heslo.
Pro vytvoření nového hesla postupujte podle níže uvedeného odkazu:

<?= $resetLink ?>

........ konec e-mailu v českém jazyce ........

Hello <?= $user->name ?>.

Your login type was changed to manual. New password is needed.
Please, follow the link below to create new password:
<?= $resetLink ?>

........ end ........
