<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $defenceDate \app\models\records\DefenceDate */

$defenceLink = Yii::$app->urlManager->createAbsoluteUrl(['defence-dates/view', 'id' => $defenceDate->id]);
?>
<div class="defence-date-created">
    <p>Byl jste vybrán k účasti na obhajobě dne <?= $defenceDate->date ?> jako část výboru.</p>
    <p><?= Html::a(Html::encode($defenceLink), $defenceLink) ?></p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>You were selected to attend defences at <?= $defenceDate->date ?>, as part of the committee.</p>
    <p><?= Html::a(Html::encode($defenceLink), $defenceLink) ?></p>
    <p>........ end ........</p>
</div>
