<?php
use \app\utils\DateTime;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\records\ProjectDefence */

?>
<div class="defence-opponent-chosen">
    <p>
        Byl jste vybrán jako oponent pro <?= Html::a('obhajobu', [ 'defences/view', 'id' => $model->id ]) ?>
        projektu
        <?= $model->project->name ?>
        ( <?= $model->project->short_name ?> )
        <?= DateTime::date($model->defenceDate->date) ?>. Nahrajte, prosím, svůj posudek alespoň 2 dny předem.
    </p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>You were chosen as the opponent for the <?= Html::a('defence', [ 'defences/view', 'id' => $model->id ]) ?>
        of
        <?= $model->project->name ?>
        ( <?= $model->project->short_name ?> )
        <?= DateTime::date($model->defenceDate->date) ?>. Please upload your review at least 2 days in advance.
    </p>
    <p>........ end ........</p>
</div>
