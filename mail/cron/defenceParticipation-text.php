<?php

use app\models\records\DefenceAttendance;

require_once __DIR__ . '/../../cron/commonFunctions.php';

$mustComeDefenceAttendances = [];
$optionalDefenceAttendances = [];

foreach ($defenceAttendances as $defenceAttendance) {
    if ($defenceAttendance->must_come) {
        $mustComeDefenceAttendances[] = $defenceAttendance;
    } else {
        $optionalDefenceAttendances[] = $defenceAttendance;
    }
}

$textEN = "";
$textCS = "";

if (count($mustComeDefenceAttendances)>0) {
    $textEN .= Yii::t('app', "IMPORTANT! For the following defences your presence is marked as required:", [],
        'en');
    $textCS .= Yii::t('app', "IMPORTANT! For the following defences your presence is marked as required:", [],
            'cs');

    foreach ($mustComeDefenceAttendances as $defenceAttendance) {
        $textEN .= createDefenceAttendanceText($defenceAttendance, 'en');
        $textCS .= createDefenceAttendanceText($defenceAttendance, 'cs');
    }
}

if (count($optionalDefenceAttendances)>0) {
    $textEN .= Yii::t('app', "At the following defences your presence is optional:", [],
        'en');
    $textCS .= Yii::t('app', "At the following defences your presence is optional:", [],
        'cs');

    foreach ($optionalDefenceAttendances as $defenceAttendance) {
        $textEN .= createDefenceAttendanceText($defenceAttendance, 'en');
        $textCS .= createDefenceAttendanceText($defenceAttendance, 'cs');
    }
}


?>
Zdravím <?= $user->name ?>,
toto je týdenní připomínka, že jste ještě nehlasovali, zda jste schopni se zúčastnit na uvedených obhajobách níže.
Obhajobu můžete zobrazit kliknutím na název projektu. Odpovězte, prosím, před konečným termínem pro přihlásení.

<?=
    $textCS;
?>
Děkuji.

........ konec e-mailu v českém jazyce ........

Hello <?= $user->name ?>,
this is a weekly reminder that you still haven't specified whether you are able to attend the project defences listed
below.
You can view the defence by clicking on the title of the project. Please respond before the showcased sign up deadline.

<?=
    $textEN;
?>
Thank you.