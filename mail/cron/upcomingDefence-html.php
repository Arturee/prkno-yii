<?php

use yii\helpers\Html;
use app\models\records\Project;
use app\models\records\ProjectDefence;

$textEN = "";
$textCS = "";

foreach ($defenceDates as $defenceDate) {
    $projectDefence = ProjectDefence::findOne(["defence_date_id" => $defenceDate->id]);
    if (!$projectDefence) {
        break;
    }

    $defenceDate = $projectDefence->time;
    if (!$defenceDate) {
        //the defence date array is sorted, so if we break at the first non-specified time, we know, that those
        //remaining are non-specified as well
        break;
    }

    $textEN .= createDefenceHTML($projectDefence, 'en');
    $textCS .= createDefenceHTML($projectDefence, 'cs');
}

?>
<div class="upcoming-defence">

    <p>Dear <?= Html::encode($user->name) ?>,</p>

    <p>to je připomínka, že máte <?= Html::encode($timeLeft) ?> času do obhajoby projektu.
    </p>

    <p>Podrobnosti naleznete níže. Klepnutím na odkaz vedle názvu projektu získáte další informace o projektu.</p>

    <?=
        $textCS;
    ?>

    <p>Děkuji.</p>

    <p>........ konec e-mailu v českém jazyce ........</p>


    <p>Dear <?= Html::encode($user->name) ?>,</p>

    <p>this is reminder, that in <?= Html::encode($timeLeft) ?> from now you have one or more project defences to
        attend.</p>

    <p>You can find the details below. Click on the project title to get more information about the project.</p>

    <?=
        $textEN;
    ?>

</div>