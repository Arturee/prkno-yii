<?php

use app\models\records\Project;
use app\models\records\ProjectDefence;

$textEN = "";
$textCS = "";

foreach ($defenceDates as $defenceDate) {
    $projectDefence = ProjectDefence::findOne(["defence_date_id" => $defenceDate->id]);
    if (!$projectDefence) {
        break;
    }

    $defenceDate = $projectDefence->time;
    if (!$defenceDate) {
        //the defence date array is sorted, so if we break at the first non-specified time, we know, that those
        //remaining are non-specified as well
        break;
    }

    $textEN .= createDefenceText($projectDefence, 'en');
    $textCS .= createDefenceText($projectDefence, 'cs');
}


?>
Zdravím <?= $user->name ?>,
to je připomínka, že máte <?= $timeLeft ?> času do obhajoby projektu.
Podrobnosti naleznete níže. Klepnutím na odkaz vedle názvu projektu získáte další informace o projektu.

<?=
    $textCS;
?>
........ konec e-mailu v českém jazyce ........

Dear <?= $user->name ?>,
this is reminder, that in <?= $timeLeft ?> you have a project defence to attend.
You can find the details below. Click the link next to the project title to get more information about the project.

<?=
    $textEN;
?>


