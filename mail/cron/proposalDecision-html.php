<?php
use yii\helpers\Html;
use app\models\records\Project;

$proposalsText = "";

foreach ($proposals as $proposal) {
    $proposalLink = Yii::$app->urlManager->createAbsoluteUrl(['proposals/view', 'id' => $proposal->id]);
    $project = Project::findOne($proposal->project_id);
    $projectName = $project->name;

    $proposalsText .= "
        <li>" . Html::a(Html::encode($projectName), $proposalLink) . "</li>
    ";
}

?>
<div class="proposal-decision">
    <p>Zdravím <?= Html::encode($user->name) ?>,</p>

    <p>toto je týdenní připomínka, že vaše hodnocení návrhu projektu stále chybí.</p>

    <p>Projekty jsou níže uvedené. Chcete-li získat přístup k návrhu, klikněte na odkaz vedle názvu projektu.</p>

    <ul>
        <?= $proposalsText ?>
    </ul>

    <p>Děkuji.</p>

    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>Dear <?= Html::encode($user->name) ?>,</p>

    <p>this is a weekly reminder that your evaluation of a project proposal is still missing.</p>

    <p>You can find the projects in question below. In order to access the proposal, please click on the title of
        the project.</p>

    <ul>
        <?= $proposalsText ?>
    </ul>

    <p>Thank you.</p>
</div>