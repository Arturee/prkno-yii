<?php

use app\models\records\Project;

$proposalsText = "";

foreach ($proposals as $proposal) {
    $proposalLink = Yii::$app->urlManager->createAbsoluteUrl(['proposals/view', 'id' => $proposal->id]);
    $project = Project::findOne($proposal->project_id);
    $projectName = $project->name;

    $proposalsText .= "
        $projectName [ $proposalLink ]
    ";
}

?>
Zdravím <?= $user->name ?>,

toto je týdenní připomínka, že vaše hodnocení návrhu projektu stále chybí.
Projekty jsou níže uvedené. Chcete-li získat přístup k návrhu, klikněte na odkaz vedle názvu projektu.
<?=
    $proposalsText
?>
Děkuji.

........ konec e-mailu v českém jazyce ........

Hello <?= $user->name ?>,

this is a weekly reminder that your evaluation of a project proposal is still missing.
You can find the projects in question below. In order to access the proposal, please click the link next to the title
of the project.
<?=
    $proposalsText
?>
Thank you.
