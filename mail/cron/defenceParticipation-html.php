<?php
use yii\helpers\Html;
use app\models\records\DefenceAttendance;

require_once __DIR__ . '/../../cron/commonFunctions.php';

$mustComeDefenceAttendances = [];
$optionalDefenceAttendances = [];

foreach ($defenceAttendances as $defenceAttendance) {
    if ($defenceAttendance->must_come) {
        $mustComeDefenceAttendances[] = $defenceAttendance;
    } else {
        $optionalDefenceAttendances[] = $defenceAttendance;
    }
}

$textEN = "";
$textCS = "";

if (count($mustComeDefenceAttendances)>0) {

    $textEN .= "<p><strong>"
        . Yii::t('app', "IMPORTANT! For the following defences your presence is marked as required:", [],
            'en')
        . "</strong></p>" . "<ul>";
    $textCS .= "<p><strong>"
        . Yii::t('app', "IMPORTANT! For the following defences your presence is marked as required:", [],
            'cs')
        . "</strong></p>" . "<ul>";

    foreach ($mustComeDefenceAttendances as $defenceAttendance) {
        $textEN .= createDefenceAttendanceHTML($defenceAttendance, 'en');
        $textCS .= createDefenceAttendanceHTML($defenceAttendance, 'cs');
    };

    $textEN .= "</ul>"; $textCS .= "</ul>";
}

if (count($optionalDefenceAttendances)>0) {
    $textEN .= "<p> <em>"
        . Yii::t('app', "At the following defences your presence is optional:", [],'en')
        . " </em> </p>" . "<ul>";
    $textCS .= "<p> <em> "
        . Yii::t('app', "At the following defences your presence is optional:", [],'cs')
        . " </em> </p>" . "<ul>";

    foreach ($optionalDefenceAttendances as $defenceAttendance) {
        $textEN .= createDefenceAttendanceHTML($defenceAttendance, 'en');
        $textCS .= createDefenceAttendanceHTML($defenceAttendance, 'cs');
    };

    $textEN .= "</ul>"; $textCS .= "</ul>";
}

?>
<div class="defence-participation">
    <p>Zdravím <?= Html::encode($user->name) ?>,</p>

    <p>toto je týdenní připomínka, že jste ještě nehlasovali, zda jste schopni se zúčastnit na uvedených obhajobách níže.
    </p>

    <p>Obhajobu můžete zobrazit kliknutím na název projektu. Odpovězte, prosím, před konečným termínem pro přihlásení.
    </p>

    <?=
        $textCS;
    ?>

    <p>Děkuji.</p>

    <p>........ konec e-mailu v českém jazyce ........</p>


    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>this is a weekly reminder that you still haven't specified whether you are able to attend the project defences
        listed below.</p>

    <p>You can view the defence by clicking on the title of the project. Please respond before the showcased sign
        up deadline.</p>

    <?=
        $textEN;
    ?>

    <p>Thank you.</p>
</div>