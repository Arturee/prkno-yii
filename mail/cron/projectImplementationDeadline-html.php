<?php
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $project \app\models\records\Project */

?>
<div class="project-implementation-deadline">
    <p>
        Na projektu <?= Html::a($project->short_name, [ 'projects/view', 'id' => $project->id ]) ?> uplynul termín pro implementaci.
    </p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>
        The project <?= Html::a($project->short_name, [ 'projects/view', 'id' => $project->id ]) ?> implementation deadline has passed.
    </p>
</div>
