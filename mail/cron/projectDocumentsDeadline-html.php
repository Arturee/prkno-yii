<?php

/* @var $this yii\web\View */
/* @var $project \app\models\records\Project */

?>
<div class="project-implementation-deadline">
    <p>
        Váš projekt <?= $project->short_name ?> je blízko k termínu pro nahrání dokumentů na obhajobu.
    </p>
    <p>........ konec e-mailu v českém jazyce ........</p>

    <p>
        Your project <?= $project->short_name ?> is close to the deadline for upload documents to defence.
    </p>
    <p>........ end ........</p>
</div>