<?php

require_once __DIR__ . '/../consts/Param.php';
use app\consts\Param;

return [
    /** The email address of the sender of email messages from the site. */
    Param::EMAIL_ROBOT => 'robot@prkno.cz',
    /** After the specified time (in seconds), the password recovery tokens will expire. */
    Param::PASSWORD_RECOVERY_TOKEN_EXPIRATION => 3600, // 1 hour
    /** After the specified time (in seconds), the user's login session will expire (he will be logged out). */
    Param::LOGIN_EXPIRATION => 3600*24*30, // 30 days
    /** Minimum number of people associated with the project (in any role - team member, supervisor, consultant)
     * for it to be possible to be initialized. */
    Param::MIN_USERS_ON_PROJECT => 4,
    /** The domain name of the website (important for the console config, so the previous data import functions know
     * where to put the read files). */
    Param::DOMAIN_NAME => 'http://prkno.cz/',
    /** The path where the uploaded files (project documents, evaluations, etc.) are saved. */
    Param::FILE_STORAGE_DIRECTORY => dirname(__DIR__) . '/web/files',
    /** CAS configuration. */
    Param::CAS_PARAMETERS => [
        'host' => 'idp.cuni.cz',
        'port' => 443,
        'url' => 'cas',
        'log' => '@runtime/logs/cas.log',
    ],

    //The times when the designated CRON scripts are meant to be run (adhering to CRON standard).
    //(dayOfWeek => 0-6 (0 is Sunday))
    //The time for sending out emails to those committee members, who have not rated a proposal yet.
    Param::CRON_TIME_PROPOSAL_DECISION_REMINDER => [
        'minutes' => '0',
        'hours' => '8',
        'dayOfMonth' => '*',
        'months' => '*',
        'dayOfWeek' => '1'
    ],
    //The time for sending out emails to those committee members, who have not yet indicated whether they will be present
    //at the given defence.
    Param::CRON_TIME_DEFENCE_PARTICIPATION_REMINDER => [
        'minutes' => '0',
        'hours' => '8',
        'dayOfMonth' => '*',
        'months' => '*',
        'dayOfWeek' => '1'
    ],
    //The time when the system will check whether there are defences scheduled in the near-future (specified below) and
    //possibly set up the CRON jobs for sending out the reminder emails at the specified times.
    Param::CRON_TIME_UPCOMING_DEFENCE_SETUP => [
        'minutes' => '0',
        'hours' => '7',
        'dayOfMonth' => '*',
        'months' => '*',
        'dayOfWeek' => '*'
    ],
    //The time for sending out emails about a project's missed deadline (analysis deadline, implementation deadline, etc.).
    Param::CRON_TIME_PROJECT_DEADLINE => [
        'minutes' => '0',
        'hours' => '8',
        'dayOfMonth' => '*',
        'months' => '*',
        'dayOfWeek' => '*'
    ],
    //How much before a defence begins will the defence reminder emails be sent out.
    //If the parameter is empty or null, the reminder doesn't get sent.
    Param::AMOUNT_OF_HOURS_BEFORE_DEFENCE_REMINDER => '2 hours',
    Param::AMOUNT_OF_DAYS_BEFORE_DEFENCE_REMINDER => '2 days',
    Param::AMOUNT_OF_WEEKS_BEFORE_DEFENCE_REMINDER => '1 week',
    //The URL names which are preserved for the system and therefore one can't create a static page for.
    Param::PAGE_RESERVED_URLS => [
        'index','create','update','delete','view-page-version','move-up','move-down','publish','download'
    ],
];
