<?php

require_once __DIR__ . '/../consts/Permission.php';
require_once __DIR__ . '/../models/records/OrmRecord.php';
require_once __DIR__ . '/../models/records/User.php';

use app\consts\Permission;
use app\models\records\User;
use \app\consts\Param;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['guest'],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => \app\mail\Mailer::class,
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/file-storage.log',
                    'categories' => ['yii2tech\filestorage\*'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'scriptUrl' => $params[Param::DOMAIN_NAME]
        ],

        'fileStorage' => [
            'class' => 'app\utils\storage\authorization\AuthorizedLocalStorage',
            'basePath' => $params[Param::FILE_STORAGE_DIRECTORY],
            'baseUrl' => $params[Param::DOMAIN_NAME] . 'files',
            'buckets' => [
                'templatesToDownload' => [
                    'baseSubPath' => 'templates/download',
                    //if permissions and roleBlackList is empty, then all roles are whitelisted
                ],
                'templatesMarkdown' => [
                    'baseSubPath' => 'templates/md',
                ],
                'projectUploads' => [
                    'baseSubPath' => 'documents/project',
                    'permissions' => [
                        Permission::PROJECT_DOWNLOAD_FILE
                    ],
                    'roleWhiteList' => [
                        User::ROLE_COMMITTEE_CHAIR
                    ]
                ],
                'defenceUploads' => [
                    'baseSubPath' => 'documents/project',
                    'permissions' => [
                        Permission::PROJECT_IS_ASSIGNED
                    ],
                    'roleWhiteList' => [
                        User::ROLE_COMMITTEE_MEMBER,
                        User::ROLE_COMMITTEE_CHAIR
                    ]
                ],
                'pages' => [
                    'baseSubPath' => 'pages',
                ],
                'otherFiles' => [
                    'baseSubPath' => 'other',

                    /*'roleBlackList' => [
                    ],*/

                    'roleWhiteList' => [
                        User::ROLE_COMMITTEE_CHAIR
                    ]
                ]
            ]
        ],
    ],
    'params' => $params,


    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
 */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
