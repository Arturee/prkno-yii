<?php

require_once __DIR__ . '/../consts/Permission.php';
require_once __DIR__ . '/../models/records/OrmRecord.php';
require_once __DIR__ . '/../models/records/User.php';

use kartik\mpdf\Pdf;
use app\consts\Param;
use app\consts\Permission;
use app\models\records\User;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$pageReservedURLs = implode('|', $params[Param::PAGE_RESERVED_URLS]);

$config = [
    'id' => 'basic',
    'name' => 'PRKNO',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
    	'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['guest'],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Fqvtd0GUbU6zNV1t0rXulON9DAk3lPdm',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
        	'class' => 'app\components\WebUser',
            'identityClass' => 'app\models\records\User',
            'enableAutoLogin' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => \app\mail\Mailer::class,
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/main.log',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        Param::LOG_CATEGORY_AUDIT,
                        'yii\web\HttpException:404'
                    ],
                    'logVars' => [],
                    'prefix' => function ($message) {
                        $user = Yii::$app->has('user', true) ? Yii::$app->get('user') : null;
                        $userID = $user ? $user->getId(false) : '-';
                        return "[User:$userID]";
                    }
                ],
                [
                    /** Gets called after every successful record->save().
                     * Don't forget that this log file will contain SENSITIVE information with personal information
                     * about users, so make sure to delete them periodically.
                     */
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/audit.log',
                    'categories' => [Param::LOG_CATEGORY_AUDIT],
                    'logVars' => [],

                    //we override this to remove sensitive IP and Session information
                    'prefix' => function ($message) {
                        $user = Yii::$app->has('user', true) ? Yii::$app->get('user') : null;
                        $userID = $user ? $user->getId(false) : '-';
                        return "[User:$userID]";
                    }
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/file-storage.log',
                    'categories' => ['yii2tech\filestorage\*'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['pattern' => 'api/<requestedTable:(ads|projects|defences|news)>', 'route' => 'api/handler'],
                ['pattern' => 'api/<requestedTable:(projects|defences)>/id/<id:\d+>', 'route' => 'api/handler'],
                ['pattern' => 'pages/<url:\b(?!' . $pageReservedURLs . ')\b\S+>', 'route' => 'pages/view'],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => '00',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],

        ],
        'fileStorage' => [
            'class' => '\app\utils\storage\authorization\AuthorizedLocalStorage',
            'basePath' => '@webroot/files',
            'baseUrl' => ['/files/download'],
            'buckets' => [
                'templatesToDownload' => [
                    'baseSubPath' => 'templates/download',
                    //if permissions and roleBlackList is empty, then all roles are whitelisted
                ],
                'templatesMarkdown' => [
                    'baseSubPath' => 'templates/md',
                ],
                'projectUploads' => [
                    'baseSubPath' => 'documents/project',
                    'permissions' => [
                        Permission::PROJECT_DOWNLOAD_FILE
                    ],
                    'roleWhiteList' => [
                        User::ROLE_COMMITTEE_CHAIR
                    ]
                ],
                'defenceUploads' => [
                    'baseSubPath' => 'documents/project',
                    'permissions' => [
                        Permission::PROJECT_IS_ASSIGNED
                    ],
                    'roleWhiteList' => [
                        User::ROLE_COMMITTEE_MEMBER,
                        User::ROLE_COMMITTEE_CHAIR
                    ]
                ],
                'pages' => [
                    'baseSubPath' => 'pages',
                ],
                'otherFiles' => [
                    'baseSubPath' => 'other',

                    /*'roleBlackList' => [
                    ],*/

                    'roleWhiteList' => [
                        User::ROLE_COMMITTEE_CHAIR
                    ]
                ]
            ]
        ],
        'pdf' => [
            'class' => Pdf::classname(),
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_DOWNLOAD,
        ]
    ],
//    'container' => [
//        'singletons' => [
//            \app\mail\Mailer::class => [
//                // send all mails to a file by default. You have to set
//                // 'useFileTransport' to false and configure a transport
//                // for the mailer to send real emails.
//                'useFileTransport' => true,
//            ]
//        ]
//    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ],
    'params' => $params,
    'language' => 'cs-CZ',
    'sourceLanguage' => 'en-US',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
	'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*'] // adjust this to your needs

    ];
}

return $config;
