<?php
namespace app\cas;

use phpCAS;
use app\models\records\User;
use app\exceptions\BugError;
use app\exceptions\CasConnectionException;

/**
 * @author: Artur
 *
 * phpCAS doc examples:
 * https://wiki.jasig.org/display/CASC/phpCAS+examples
 *
 * Currently using phpCAS version 1.3.5
 * API doc:
 * http://developer.jasig.org/cas-clients/php/1.3.5/docs/api/classphpCAS.html
 *
 * cuni.cz CAS config:
 * https://supercomp.cuni.cz/wiki/index.php/Pravidla_pou%C5%BEit%C3%AD_CAS#Vztahy_k_UK
 *
 * It seems that phpCAS throws exceptions but the API doesn't say which methods throws what.
 * Since it is not obvious which exceptions can arise where, I defensively wrap any
 * code that could potentially throw and rethrow a CasConnectionException.
 *
 * Nice diagram here: https://wiki.jasig.org/display/casc/phpcas+examples
 */
class CasClient
{

    const
    /**
     * (eg. Petr Novak)
     */
        ATTRIBUTE_FULL_NAME = 'cn',

        /**
         * can be missing
         * (eg. novakp@mff.cuni.cz or novakp@gmail.com)
         */
        ATTRIBUTE_EMAIL = 'mail',

        /**
         * SHOULD BE:
         * Can be multiple or missing
         * (eg. staff@mff.cuni.cz)
         * Only accessible from @cuni.cz
         *
         * These attributes might also be relevant:
         *  -   eduPersonAffiliation - can be multiple or missing (eg. staff)
         *  -   eduPersonPrimaryAffiliation - can be missing  (eg. staff)
         *  -   eduPersonOrgDN (eg. mff dc=mff,dc=cuni,dc=cz)
         *
         * IN REALITY:
         * - returns empty array both on localhost and on @cuni.cz
         * - 'eduPersonAffiliation' also returns empty
         *
         */
        ATTRIBUTE_AFFILIATIONS = 'eduPersonScopedAffiliation',

        /**
         * SHOULD BE:
         * - Only accessible from @cuni.cz
         * - If the code runs on the domain cuni.cz, then UKCO is returned. (eg. 12341234)
         * Otherwise, a randomized token is returned. (eg. JA4Gsujf7Pw+6zdptGuINpYxRoo=)
         *
         * IN REALITY:
         *  - returns empty string on @cuni.cz
         */
         // ATTRIBUTE_UKCO_OLD = 'cuniPersonalId',

        /**
         * UKCO, alias
         *
         * Can be multiple or missing.
         * Only accessible from @cuni.cz
         */
        ATTRIBUTE_UKCO_AND_ALIAS = 'uid',

        /**
         * Can be missing.
         * Only accessible from @cuni.cz
         */
        ATTRIBUTE_PREFERRED_LANGUAGE = 'preferredLanguage';


    /**
     * @param string $hostname      Hostname
     * @param int $port             Port
     * @param string $relativeUrl   Relative url
     * @param string $logFilePath   File where path is logged
     * @param bool $debugMode       Whether to turn on logging or not
     */
    public function __construct(
        string $hostname,
        int $port,
        string $relativeUrl,
        string $logFilePath,
        bool $debugMode
    ) {
        $this->configureCasClient($hostname, $port, $relativeUrl, $logFilePath, $debugMode);
    }

    /**
     * Redirects to CAS web page, where the user logs in and after a successful login, he is
     * redirected back to the original url.
     *
     * @throws CasConnectionException
     */
    public function redirectToCasForAuthentication() : void
    {
        try {
            phpCAS::renewAuthentication();

            // phpCAS::forceAuthentication();
            // Ironically, foreAuthentication() does not force the user to re-input his credentials to CAS

        } catch (\Exception $e) {
            throw new CasConnectionException($e);
        }
    }

    /**
     * Sometimes re-triggers the HTTP request, sometimes not.
     *
     * @return bool true whether the user is authenticated on CAS
     * @throws CasConnectionException
     */
    public function isCurrentUserAuthenticated() : bool
    {
        try {
            return phpCAS::isAuthenticated();
        } catch (\Exception $e) {
            throw new CasConnectionException($e);
        }
    }

    /**
     * Redirects to CAS web page, which logs the user out and immediately redirects back to
     * our url argument.
     * 
     * @param string $urlToRedirectBackTo   Url where user has to be redirected
     */
    public function logout(string $urlToRedirectBackTo) : void
    {
        phpCAS::logoutWithRedirectService($urlToRedirectBackTo);
    }

    /**
     * @return string
     * @throws CasConnectionException
     */
    public function getCurrentUserEmail() : string
    {
        if (!$this->isCurrentUserAuthenticated()) {
            throw new BugError('not authenticated via CAS');
        }

        try {
            return phpCAS::getAttribute(self::ATTRIBUTE_EMAIL);
        } catch (\Exception $e) {
            throw new CasConnectionException($e);
        }
    }

    /**
     * @return CasInfo
     * @throws CasConnectionException
     */
    public function downloadCurrentUserInfo() : CasInfo
    {
        if (!$this->isCurrentUserAuthenticated()) {
            throw new BugError('not authenticated via CAS');
        }

        try {
            $ukco = null;
            $alias = null;
            $ukcoAndAlias = phpCAS::getAttribute(self::ATTRIBUTE_UKCO_AND_ALIAS);
            if (isset($ukcoAndAlias[0])) {
                $ukco = $ukcoAndAlias[0];
            }
            if (isset($ukcoAndAlias[1])) {
                $alias = $ukcoAndAlias[1];
            }

            $affiliations = phpCAS::getAttribute(self::ATTRIBUTE_AFFILIATIONS);
            if (!$affiliations) {
                $affiliations = [];
            }
            $role = self::extractRole($affiliations);

            $info = new CasInfo(
                phpCAS::getAttribute(self::ATTRIBUTE_FULL_NAME),
                phpCAS::getAttribute(self::ATTRIBUTE_EMAIL),
                $role,
                phpCAS::getAttribute(self::ATTRIBUTE_PREFERRED_LANGUAGE),
                $alias,
                $ukco
            );
            return $info;
        } catch (\Exception $e) {
            throw new CasConnectionException($e);
        }
    }

    /**
     * @param string[] $affiliations eg. [ staff@mff.cuni.cz, student@mff.cuni.cz ]
     */
    private static function extractRole(array $affiliations) : ? string
    {
        $role = null; //guest
        $CUNI_CZ = preg_quote('.cuni.cz') . '$/';

        $patternsStudent = [];
        $patternsStudent[] = '/^student@.*' . $CUNI_CZ;
        $patternsStudent[] = '/^interrupted-student@.*' . $CUNI_CZ;
        $patternsStudent[] = '/^applicant@.*' . $CUNI_CZ;
        if (self::hasMatch($affiliations, $patternsStudent)) {
            $role = User::ROLE_STUDENT;
        }

        $MFF_CUNI_CZ = preg_quote('@mff.cuni.cz') . '$/';;

        $patternsStaff = [];
        $patternsStaff[] = '/^staff' . $MFF_CUNI_CZ;
        if (self::hasMatch($affiliations, $patternsStudent)) {
            $role = User::ROLE_ACADEMIC;
        }

        $patternsStudent2 = [];
        $patternsStudent2[] = '/^member@.*' . $CUNI_CZ;
        $patternsStudent2[] = '/^affiliate@.*' . $CUNI_CZ;
        if (self::hasMatch($affiliations, $patternsStudent2)) {
            $role = User::ROLE_STUDENT;
        }

        if (!$role) {
            // CARE:
            // currently CAS does not return any roles
            // so I make everybody with CAS access a student.
            $role = User::ROLE_STUDENT;
        }
        return $role;



        //EMAIL Krulis:
        //        1) pokud je tam student, interrupted-student, nebo applicant @*.cuni.cz -> je to student
        //2) pokud je tam staff@mff.cuni.cz -> je to ucitel
        //3) pokud je tam member nebo affiliate @*.cuni.cz -> je to student (asi)
        //4) pokud je cokoli jineho, mel by se mu vytvorit ucet, ale mel by videt stejne jako anonymni uzivatel
        //(tajemnik/vedouci ho muzou rucne zmenit na studenta nebo ucitele, takze je potreba zalozit ten ucet).
        //
        //Vsimnete si, ze ucitel je az v bode 2, kdyz NENI jasne definovany student. Pokud k tomu mate pripominky nebo
        //dotazy, ptejte se.

        /**
         * AFFILIATIONS in API:
         *
         *   staff      Zaměstnanci - osoby s pracovní smlouvou, dohodou o provedení práce nebo dohodou o provedení činnosti.
         *   student    Studenti všech forem studia včetně studentů programu Erasmus.
         *   interrupted-student 	Studenti s přerušeným studiem.
         *   applicant 	Uchazeči přijatí ke studiu, ale ještě nikoliv studenti, tj. před zápisem.
         *   member 	Účastníci kurzů CŽV, externí uživatelé služeb nárokovaní fakultami, studenti CERGE.
         *   affiliate 	Stážisté.
         *   guest 	Držitelé nepersonalizovaných průkazů.
         *   alumn 	Absolventi - studenti, kteří úspěšně dokončili studium.
         */
    }

    /**
     * @param string[] $affiliations
     * @param string[] $patterns
     * @return bool
     */
    private static function hasMatch(array $affiliations, array $patterns) : bool
    {
        foreach ($affiliations as $affiliation) {
            foreach ($patterns as $pattern) {
                if (preg_match($pattern, $affiliation)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function configureCasClient(string $hostname, int $port, string $relativeUrl, string $logFilePath, bool $debugMode) : void
    {
        if ($debugMode) {
            if (!file_exists($logFilePath)) {
                touch($logFilePath);
            }
            phpCAS::setDebug($logFilePath);
            // this generates A LOT of log lines, that's why I don't use it in production
        }
        $CAS_VERSION = '3.0';
        phpCAS::client($CAS_VERSION, $hostname, $port, $relativeUrl);
        $this->disableSslValidation();
    }

    /**
     * Disables SSL validation for the CAS server.
     */
    private function disableSslValidation() : void
    {
        phpCAS::setNoCasServerValidation();
        // this function cannot be called before phpCAS::client() call
    }
}
