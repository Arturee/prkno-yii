<?php
namespace app\cas;

use app\consts\Lang;

/**
 * @author Artur
 *
 * Object storing data about the user extracted from CAS serer
 */
class CasInfo
{
    private $fullName, $email, $role, $language, $alias, $ukco;

    /**
     * Create new cas info object storing records about the user extracted from CAS server
     * @param string|null $fullName  User full name
     * @param string|null $email     E-mail address
     * @param string|null $role      User role extracted from CAS
     * @param string|null $language  Users language
     * @param string|null $alias     Alias
     * @param string|null $ukco      Ukco
     */
    public function __construct(
        ? string $fullName,
        ? string $email,
        ? string $role,
        ? string $language,
        ? string $alias,
        ? string $ukco
    ) {
        $this->fullName = self::load($fullName);
        $this->email = self::load($email);
        $this->role = $role;
        $this->language = self::load($language, [Lang::CS, Lang::EN]);
        $this->alias = self::load($alias);
        $this->ukco = self::load($ukco);
    }

    /**
     * @return string|null  Users first name and his last name
     */
    public function getFullName() : ? string
    {
        return $this->fullName;
    }

    /**
     * @return string|null  Users email
     */
    public function getEmail() : ? string
    {
        return $this->email;
    }

    /**
     * @return string|null  Users role
     */
    public function getRole() : ? string
    {
        return $this->role;
    }

    /**
     * @return string|null  Users language
     */
    public function getLanguage() : ? string
    {
        return $this->language;
    }

    /**
     * @return string|null  Users alias
     */
    public function getAlias() : ? string
    {
        return $this->alias;
    }

    /**
     * @return string|null Ukco
     */
    public function getUkco() : ? string
    {
        return $this->ukco;
    }

    private static function load(? string $attr, array $enum = null) : ? string
    {
        if (is_null($attr)) {
            return null;
        }
        $attr = trim($attr);
        if (empty($attr) || ($enum && !array_search($attr, $enum))) {
            $attr = null;
        }
        return $attr;
    }
}