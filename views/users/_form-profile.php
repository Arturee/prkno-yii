<?php

use app\models\records\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-profile-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-6 col-md-6">
        <?= $form->field($model, 'degree_prefix')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'degree_suffix')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'custom_url')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'password')->passwordInput(['autocomplete' => 'new-password']) ?>
        <?= $form->field($model, 'news_subscription')->radioList(User::getBoolMap())?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>