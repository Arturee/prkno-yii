<?php

use app\models\records\User;
use app\utils\DateTime;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-6 col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'degree_prefix')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'degree_suffix')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'role')->dropDownList(User::getRoleMap()) ?>
        <?= $form->field($model, 'role_expiration')->widget(DatePicker::class, [
            'dateFormat' => DateTime::YII_DATE,
            'options' => ['class' => 'form-control'],
            'value' => DateTime::from($model->role_expiration)->format(DateTime::DATE)
        ]) ?>
    </div>

    <div class="col-lg-6 col-md-6">
        <?= $form->field($model, 'custom_url')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'news_subscription')
            ->radioList([
                '0' => Yii::t('app', 'No'),
                '1' => Yii::t('app', 'Yes')
            ])
        ?>
        <?= $form->field($model, 'role_is_from_CAS')->radioList(User::getBoolMap()) ?>
        <?= $form->field($model, 'login_type')->radioList(['cas' => 'Cas', 'manual' => 'Db', ]) ?>
        <?= $form->field($model, 'banned')->radioList(User::getBoolMap()) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>