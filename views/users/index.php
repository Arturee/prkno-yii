<?php

use app\consts\Param;
use app\models\records\User;
use app\utils\Anonymizer;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;

$usersToAnonymize = Anonymizer::getUsersWhoShouldBeAnonymized();

?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p class="pull-right">
        <?= Html::a(
            Yii::t('app', 'Anonymize inactive'),
            ['inactive-anonymize'],
            [
                'class' => 'btn btn-danger',
                'title' => Yii::t(
                    'app',
                    'The user is considered inactive if he/she hasn\'t logged in in {0} years or was deleted by the committee chair.',
                    [Param::ANONYMIZATION_INACTIVITY_THRESHOLD_YEARS]
                ),
                'data-confirm' => Yii::t(
                    'app',
                    "Are you sure you want to anonymize these users? \n \n{0}\nThis action can NOT be reverted!",
                    Anonymizer::createUserAnonymizationConfirmation($usersToAnonymize)
                ),
                'data-method' => 'post',
            ]
        ) ?>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>
            function (User $model, $key, $index, $grid) {
            return $model->banned ? ['style' => "background-color: #ffeee6"] : [];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'role',
                'filter' => User::getRoleMap(),
                'headerOptions' => ['width' => '190'],
                'value' => function ($model) {
                    return Yii::t('app', User::roleToString($model->role));
                },
            ],
            'email:email',
            [
                'attribute' => 'banned',
                'headerOptions' => ['width' => '50'],
            ],
            //'degree_prefix',
            //'degree_suffix',
            //'email:email',
            //'password',
            //'news_subscription',
            //'last_login',
            //'custom_url:url',
            //'last_CAS_refresh',
            //'role',
            //'role_expiration',
            //'role_is_from_CAS',
            //'login_type',
            //'password_recovery_token',
            //'password_recovery_token_expiration',
            //'banned',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {anonymize} {ban} {delete}',
                'headerOptions' => ['width' => '86'],
                'buttons' => [
                    'anonymize' => function ($url) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-question-sign"></span>',
                            $url,
                            [
                                'title' => Yii::t('app', 'Anonymize'),
                                'data-confirm' => Yii::t('app', 'Are you sure you want to anonymize this user? This action can NOT be reverted!'),
                                'data-method' => 'post',
                            ]
                        );
                    },

                    'ban' => function ($url) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-ban-circle"></span>',
                            $url,
                            [
                                'title' => 'Ban',
                                'data-pjax' => '1',
                            ]
                        );
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                        ]);
                    },
                ],
                'visibleButtons' => [
                    'delete' => Yii::$app->user->can('deleteUsers'),
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
