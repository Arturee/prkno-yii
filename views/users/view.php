<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\assets\UsersViewAsset;
use app\consts\Permission;
use app\models\records\User;
use app\utils\DateTime;

UsersViewAsset::register($this);

/* @var $this yii\web\View */
/* @var $model \app\models\records\User */

$this->title = $model->getNameWithEmail();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$canManageUser = Yii::$app->user->can(Permission::USERS_MANAGE);
$canDeleteUser = Yii::$app->user->can(Permission::USERS_DELETE);
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-lg-6 col-md-6">
    <p class="pull-right">
        <?php
        if ($canManageUser) {
            echo Html::a(
                Yii::t('app', 'Update'),
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']
            );

            echo Html::a(
                Yii::t('app', 'Anonymize'),
                ['anonymize', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to anonymize this user? This action can NOT be reverted!'),
                        'method' => 'post',
                    ]
                ]
            );
        }
        ?>
        <?php
        if ($canDeleteUser) {
            echo Html::a(
                Yii::t('app', 'Delete'),
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ]
                ]
            );
        }
        ?>
    </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'created_at',
                    'value' => function ($model) {
                        return DateTime::from($model->created_at)->format(DateTime::DATETIME);
                    }
                ],
                [
                    'attribute' => 'updated_at',
                    'value' => function ($model) {
                        return DateTime::from($model->updated_at)->format(DateTime::DATETIME);
                    }
                ],
                'name',
                'degree_prefix',
                'degree_suffix',
                'email:email',
                [
                    'attribute' => 'news_subscription',
                    'value' => function ($model) {
                        return User::mapBool($model->news_subscription);
                    },
                ],
                [
                    'attribute' => 'last_login',
                    'value' => function ($model) {
                        return DateTime::dateTime($model->last_login);
                    }
                ],
                'custom_url:url',
                [
                    'attribute' => 'last_CAS_refresh',
                    'value' => function ($model) {
                        return DateTime::dateTime($model->last_CAS_refresh);
                    }
                ],
                [
                    'attribute' => 'role',
                    'value' => function ($model) {
                        return Yii::t('app', User::roleToString($model->role));
                    },
                ],
                [
                    'attribute' => 'role_expiration',
                    'value' => function ($model) {
                        return DateTime::dateTime($model->role_expiration);
                    }
                ],
                [
                    'attribute' => 'role_is_from_CAS',
                    'value' => function ($model) {
                        return User::mapBool($model->role_is_from_CAS);
                    },
                ],
                'login_type',
                [
                    'attribute' => 'banned',
                    'value' => function ($model) {
                        return User::mapBool($model->banned);
                    },
                ],
            ],
        ]) ?>
    </div>
</div>
