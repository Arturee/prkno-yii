<?php

use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use app\models\records\User;
use app\utils\DateTime;

/* @var $this yii\web\View */
/* @var $model \app\models\records\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-6 col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'degree_prefix')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'degree_suffix')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'autocomplete' => 'new-password']) ?>
        <?= $form->field($model, 'password')->passwordInput(['autocomplete' => 'new-password']) ?>
    </div>
    <div class="col-lg-6 col-md-6">
        <?= $form->field($model, 'role')->dropDownList(User::getRoleMap()); ?>
        <?php
        $model->role_expiration = User::defaultRoleExpiration()->format(DateTime::DATE);
        echo $form->field($model, 'role_expiration')->widget(DatePicker::class, [
            'dateFormat' => DateTime::YII_DATE,
            'options' => ['class' => 'form-control']
        ]);
        ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
