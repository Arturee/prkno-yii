<?php

use app\consts\Permission;
use app\models\records\User;
use app\utils\DateTime;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\records\User */

$this->title = $model->getNameWithEmail();
$this->params['breadcrumbs'][] = $this->title;
$canUpdate = Yii::$app->user->can(Permission::USERS_UPDATE_OWN_PROFILE, [Permission::PARAM_USER => $model]);
?>
<div class="user-profile">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row" style="margin-top: 4em">
        <div class="col-md-6">
            <p><label><?= Yii::t('app', 'Name') . ': ' ?></label>
                <?= Html::encode($model->getFullNameWithDegrees()) ?>
            </p>
            <p><label><?= Yii::t('app', 'Role') . ': ' ?></label>
                <?= Yii::t('app', User::roleToString($model->role)) ?>
            </p>
            <p><label><?= Yii::t('app', 'Email') . ': ' ?></label>
                <?= Html::a($model->email, 'mailto:' . $model->email) ?>
            </p>
            <p><label><?= Yii::t('app', 'Custom Url') . ': ' ?></label>
                <?= Html::a(Html::encode($model->custom_url), $model->custom_url) ?>
            </p>
            <?= (Yii::$app->user->id == $model->id) ? ('<p><label>' . Yii::t('app', 'News Subscription') . ':&nbsp;</label>'
                . User::mapBool($model->news_subscription)
                . '</p>'
                . '<p><label>' . Yii::t('app', 'Last Login') . ':&nbsp;</label><span class="last-login">'
                . DateTime::dateTime($model->last_login)
                . '</span></p>'
                . '<p><label>' . Yii::t('app', 'Last Cas Refresh') . ':&nbsp;</label><span class="last-login">'
                . DateTime::dateTime($model->last_CAS_refresh)
                . '</span></p>'
                . '<p><label>' . Yii::t('app', 'Role Expiration') . ':&nbsp;</label><span class="role-expiration">'
                . DateTime::dateTime($model->role_expiration)
                . '</span></p>'
                . '<p><label>' . Yii::t('app', 'Role Is From Cas') . ':&nbsp;</label>'
                . User::mapBool($model->role_is_from_CAS)
                . '</p>'
                . '<p><label>' . Yii::t('app', 'Login Type') . ':&nbsp;</label>'
                . $model->login_type
                . '</p>'
                . '<p><label>' . Yii::t('app', 'Banned') . ':&nbsp;</label>'
                . User::mapBool($model->banned)
                . '</p>') : ("") ?>
        </div>

        <div class="col-md-6">
            <?php
            if ($canUpdate) {
                echo Html::a(
                    Yii::t('app', 'Update'),
                    ['update-profile', 'id' => $model->id],
                    ['class' => 'btn btn-primary']
                );
                if ($model->login_type === User::LOGIN_TYPE_CAS) {
                    echo Html::a(
                        Yii::t('app', 'Reload data from CAS'),
                        ['reload-cas', 'id' => $model->id],
                        [
                            'class' => 'btn btn-success',
                            'style' => "margin-left: 1em",
                            'data' => [
                                'method' => 'post',
                            ]
                        ]
                    );
                }
            }
            ?>
        </div>
    </div>

</div>
