<?php

use yii\helpers\Html;
use yii\helpers\Markdown;
use app\consts\Permission;
use app\models\records\News;
use app\utils\DateTime;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/news/index.css');
$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;

$canManageNews = Yii::$app->user->can(Permission::NEWS_MANAGE);

?>
<div class="news-index">

    <?php if ($canManageNews) {
        echo Html::a(Yii::t('app', 'Create News'), ['/news/create'], ['class' => 'btn btn-success pull-right']);
    } ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php

    /**
     * @var \app\models\records\News $value
     */
    foreach ($news as $key => $value) {
        if (Yii::$app->user->isGuest && !$value->public) continue;
        ?>
        <div class="news-active-record <?= $value->type == News::TYPE_IMPORTANT ? "news-important" : "news-normal"; ?>">

            <?php if ($canManageNews) { ?>
                <div class="pull-right">
                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ["news/update?id=" . $value->id]) ?>
                    <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ["news/delete?id=" . $value->id], ['data' => [
                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                        'method' => 'post'
                    ]]) ?>
                </div>
            <?php 
        } ?>

            <h3><?= Html::encode($value->getLocalTitle()) ?></h3>

            <div>
                <?= DateTime::from($value->updated_at)->format(DateTime::DATE) ?>
                <span class="news-important-label">
                    <?= $value->type == News::TYPE_IMPORTANT ? "(" . Yii::t('app', 'important') . ")" : ""; ?>
                </span>
            </div>

            <div><?= Markdown::process(Html::encode($value->getLocalContent())) ?>&nbsp;</div>
        </div>
        <?php

    }
    ?>
</div>
