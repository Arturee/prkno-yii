<?php

use app\utils\DateTime;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\News */
?>

<div class="news-form">
    <?php
        /* @var $form yii\widgets\ActiveForm */
    $form = ActiveForm::begin();

    $fieldType = $form->field($model, 'type')
        ->dropDownList(
            [
                'normal' => Yii::t('app', 'Normal'),
                'important' => Yii::t('app', 'Important'),
            ],
            ['class' => 'form-control', 'style' => 'width: 150px']
        );

    $fieldPublic = $form->field($model, 'public')
        ->dropDownList(
            [
                1 => Yii::t('app', 'Public'),
                0 => Yii::t('app', 'Private')
            ],
            ['class' => 'form-control', 'style' => 'width: 150px']
        );

    $hideAt = $model->hide_at ? (new DateTime($model->hide_at))->format(DateTime::DATE) : "";
    $filedHideAt = $form->field($model, 'hide_at')
        ->widget(DatePicker::class, DateTime::getDateOptions())
        ->textInput(['value' => $hideAt]);

    ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-lg-6 col-md-6">
        <div>
            <?= Html::label(Yii::t('app', 'Author') . ':') ?>
            <?= $model->user ? $model->user->getNameWithEmail() : '' ?>
        </div>
        <div class="news-form-row row">
            <div class="news-selector col-lg-4 col-md-4 col-sm-4"><?= $fieldType ?></div>
            <div class="news-selector col-lg-4 col-md-4 col-sm-4"><?= $fieldPublic ?></div>
            <div class="news-selector col-lg-4 col-md-4 col-sm-4"><?= $filedHideAt ?></div>
        </div>

        <?= $form->field($model, 'title_cs')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'content_cs')->textarea(['rows' => '6']) ?>
        <br>
        <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'content_en')->textarea(['rows' => '6']) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
