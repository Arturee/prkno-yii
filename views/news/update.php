<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\records\News */

$this->title = Yii::t('app', 'Update News');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<?= $this->render('_form', [
    'model' => $model,
    'ln' => substr(Yii::$app->language, 0, 2)
]) ?>
