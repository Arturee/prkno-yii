<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\consts\Permission;
use app\models\records\User;
use app\models\records\Page;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

if (!function_exists("getMenuItemsForStaticPages")) {
    function getMenuItemsForStaticPages(\yii\web\User $user) : array
    {
        $publishedPagesForDropdown = Page::find();
        if (!$user->can(Permission::PAGES_MANAGE)) {
            $publishedPagesForDropdown = $publishedPagesForDropdown->where(['published' => true, 'deleted' => false]);
        }
        $publishedPagesForDropdown = $publishedPagesForDropdown->orderBy('order_in_menu')->all();
        $pageItems = [];
        /** @var Page $page */
        foreach ($publishedPagesForDropdown as $page) {
            $pageItems[] = [
                'label' => Yii::t('app', $page->getTitleBasedOnSiteLanguage()),
                'url' => ['/pages/' . $page->url]
            ];
        }
        if ($user->can(Permission::PAGES_MANAGE)) {
            $pageItems[] = '<li class="divider"></li>';
            $pageItems[] = [
                'label' => Yii::t('app', 'Manage pages'),
                'url' => ['/pages'],
                'options' => ['style' => 'font-style: italic; color: grey'],
            ];
        }
        return $pageItems;
    }
}

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<script>
var baseurl="<?php print Yii::$app->request->baseUrl;?>";
</script>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('app', 'Software projects'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $lang = Yii::$app->language;
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            [
                'label' => 'CS',
                'url' => ['/site/change-language', 'language' => 'cs-CZ'],
                'options' => ['title' => 'switch to Czech language'],
                'visible' => $lang !== 'cs-CZ'
            ],
            [
                'label' => 'EN',
                'url' => ['/site/change-language', 'language' => 'en-US'],
                'options' => ['title' => 'switch to English language'],
                'visible' => $lang !== 'en-US'
            ]
        ],
    ]);

    $user = Yii::$app->user;
    $pageItems = getMenuItemsForStaticPages($user);

    $navLinks = [
        [
            'label' => Yii::t('app', 'Doc'),
            'url' => ['/doc'],
            'visible' => $user->can(Permission::DOC_VIEW)
        ],
        [
            'label' => Yii::t('app', 'Pages'),
            'items' => $pageItems,
        ],
        [
            'label' => Yii::t('app', 'Advertisements'),
            'url' => ['/advertisements']
        ],
        [
            'label' => Yii::t('app', 'Proposals'),
            'url' => ['/proposals'],
            'visible' => $user->can(Permission::PROPOSAL_LIST)
        ],
        [
            'label' => Yii::t('app', 'Projects'),
            'url' => ['/projects']
        ],
        [
            'label' => Yii::t('app', 'Defences'),
            'url' => ['/defence-dates']
        ],
        [
            'label' => Yii::t('app', 'Users'),
            'url' => ['/users'],
            'visible' => $user->can(Permission::USERS_MANAGE)
        ],
        [
            'label' => Yii::t('app', 'Login'),
            'url' => ['/site/login'],
            'visible' => $user->isGuest
        ]
    ];
    if (!$user->isGuest) {
        $dbUser = User::findOne($user->getId());
        $navLinks[] = [
            'label' => Html::encode($dbUser->name),
            'url' => ['/users/profile', 'id' => $dbUser->id],
            'options' => ['style' => 'font-style: italic', 'title' => Yii::t('app', $dbUser->role)],
        ];
        $navLinks[] = [
            'label' => Yii::t('app', 'Logout'), //' <span class="glyphicon glyphicon-log-out"></span>'
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post'] // ['class' => 'btn btn-link logout']
        ];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $navLinks
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left copyright">
            <?= Yii::t("app", "copyright", [ date('Y') ]) ?> 
        </p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
