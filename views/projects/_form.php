<?php

use app\models\records\User;
use app\utils\DateTime;
use nex\chosen\Chosen;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true])->label(
        Yii::t('app', 'Short Name') . ' <span class="glyphicon glyphicon-question-sign" 
            title="' . Yii::t('app', 'Project short name hint') . '"></span>'
    ) ?>

    <?= ($model->run_start_date && $model->deadline) ?
        '<div class="row">'
        . '<div class="col-lg-6 col-md-6">'
        . $form->field($model, 'run_start_date')->widget(DatePicker::class, [
        'dateFormat' => DateTime::YII_DATE,
        'options' => ['class' => 'form-control'],
        'value' => (new DateTime($model->run_start_date))->format(DateTime::DATE)
    ])->input('text', ['value' => (new DateTime($model->run_start_date))->format(DateTime::DATE)])
        . '</div>'
        . '<div class="col-lg-6 col-md-6">'
        . $form->field($model, 'deadline')->widget(DatePicker::class, [
        'dateFormat' => DateTime::YII_DATE,
        'options' => ['class' => 'form-control'],
        'value' => (new DateTime($model->deadline))->format(DateTime::DATE)
    ])->input('text', ['value' => (new DateTime($model->deadline))->format(DateTime::DATE)])
        . '</div>'
        . '</div>'
        : ('') ?>

    <?= $form->field($model, 'supervisorIds')->widget(
        Chosen::className(),
        [
            'items' => array_map(function($item){
                return $item->getNameWithEmail();
            }, User::findPossibleSupervisorConsultant()),
            'clientOptions' => [
                'search_contains' => true,
            ],
        ]
    ); ?>

	<?= $form->field($model, 'consultantIds')->widget(
    Chosen::class,
    [
        'items' => array_map(function($item){
            return $item->getNameWithEmail();
        }, User::findPossibleSupervisorConsultant()),
        'multiple' => true,
        'clientOptions' => [
            'search_contains' => true,
        ],
    ]
); ?>

    <?= $form->field($model, 'memberIds')->widget(
        Chosen::class,
        [
            'items' => array_map(function($item){
                return $item->getNameWithEmail();
            }, User::findAll(['deleted' => 0, 'role' => User::ROLE_STUDENT])),
            'multiple' => true,
            'clientOptions' => [
                'search_contains' => true,
            ],
        ]
    ); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => '4'])->label(
        Yii::t('app', 'Description') . ' <span class="glyphicon glyphicon-question-sign" 
            title="' . Yii::t('app', 'Project description hint') . '"></span>'
    ) ?>

    <div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
