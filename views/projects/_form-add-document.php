<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\forms\UploadForm;
use app\models\records\ProjectDocument;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Project */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="project-add-document-form">
    <?php
    $uploadModel = new UploadForm();
    $newProjectDocument = new ProjectDocument();
    $newProjectDocument->team_private = true;
    $form = ActiveForm::begin(['action' => ['upload', 'id' => $model->id], 'options' => ['enctype' => 'multipart/form-data']]);
    ?>
    <?= $form->field($uploadModel, 'file')->fileInput(['accept' => '*'])->label(Yii::t('app', 'Upload file')); ?>
    <?= $form->field($newProjectDocument, 'comment')->textInput(['required' => true])->label(Yii::t('app', 'File name')); ?>
    <?= $form->field($newProjectDocument, 'team_private')->checkbox(); ?>
    <?= Html::submitButton(Yii::t('app', 'Upload'), ['class' => 'btn btn-success']); ?>

    <?php ActiveForm::end(); ?>
</div>
