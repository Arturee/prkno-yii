<?php

use app\consts\Param;
use app\utils\DateTime;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Project */
/* @var $form yii\widgets\ActiveForm */

$today = new DateTime();
?>

<div class="project-request-run-form">

    <?php $form = ActiveForm::begin(['action' => ['request-to-run', 'id' => $model->id]]); ?>

    <?= $form->field($model, 'run_start_date')->widget(DatePicker::class, [
        'dateFormat' => DateTime::YII_DATE,
        'options' => ['class' => 'form-control', 'required' => true],
        'value' => (new DateTime($model->run_start_date))->format(DateTime::DATE)
    ])->input('text', ['value' => ($today)->format(DateTime::DATE)])?>

    <?= $form->field($model, 'deadline')->widget(DatePicker::class, [
            'dateFormat' => DateTime::YII_DATE,
            'options' => ['class' => 'form-control', 'required' => true],
            'value' => (new DateTime($model->deadline))->format(DateTime::DATE)
        ])->input('text', ['value' => ($today->modify(Param::PROJECT_LENGTH))->format(DateTime::DATE)]) ?>

    <div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
