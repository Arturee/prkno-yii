<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Project */
/* @var $form yii\widgets\ActiveForm */

$markdownIcon = '<img src="' . Yii::$app->homeUrl . 'img/md.png" style="width: 20px;" title="markdown"/>';
?>

<div class="advice-document-form">

    <?php $form = ActiveForm::begin(['action' => ['advice-document', 'id' => $model->id]]); ?>

    <?= $form->field($model, 'adviceMarkdown')->textarea(['rows' => '4'])->label($markdownIcon) ?>

    <div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
