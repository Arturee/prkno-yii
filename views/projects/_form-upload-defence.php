<?php

use app\models\forms\UploadForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Project */
/* @var $form yii\widgets\ActiveForm */

$defenceDocumentPair = $model->getDefenceDocuments();
?>

<div class="project-upload-defence-form">
    <?php
        $uploadModel = new UploadForm();
        $form = ActiveForm::begin(['action' => ['upload-defence', 'id' => $model->id], 'options' => ['enctype' => 'multipart/form-data']]);
    ?>
    <div class="row">
        <div class="col-sm-4">
            <label><?= Yii::t('app', 'Zip file') ?>: </label>
            <?= ($defenceDocumentPair->zip) ?
                Html::a(Yii::t('app', 'ZIP'), $defenceDocumentPair->zip->fallback_path, ['style' => 'text-decoration: underline;']) : ('') ?>
            <?= $form->field($uploadModel, 'zip')->fileInput(['multiple' => true, 'accept' => 'zip'])->label(false); ?>
        </div>
        <div class="col-sm-4">
            <label><?= Yii::t('app', 'PDF file') ?>: </label>
            <?= ($defenceDocumentPair->pdf) ?
                Html::a(Yii::t('app', 'PDF'), $defenceDocumentPair->pdf->fallback_path, ['style' => 'text-decoration: underline;']) : ('') ?>
            <?= $form->field($uploadModel, 'pdf')->fileInput(['multiple' => true, 'accept' => 'pdf'])->label(false); ?>
        </div>

        <div class="col-sm-4">
            <?php
            if ($model->upcomingDefence != null) { ?>
                <p class="bg-danger">
                    <?= Yii::t('app', 'Deadline for sending documents') . ': ' . $model->upcomingDefenceDocDeadline ?>
                </p>
            <?php } ?>
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']); ?>
            <?php
            if ($model->upcomingDefence != null && $model->canSendForDefence()) { ?>
                <?= Html::a(Yii::t('app', 'Send for defence'), ['send-documents', 'id' => $model->id], ['class' => 'btn btn-danger',
                    'data-confirm' => Yii::t('app', 'Did you save documents? You can not change documents after sending.')]) ?>
            <?php } ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
</div>
