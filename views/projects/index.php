<?php

use app\consts\Permission;
use app\consts\Param;
use app\models\records\Project;
use app\models\records\ProjectUser;
use app\utils\DateTime;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <p class="pull-right">
        <?php if (Yii::$app->user->id) : ?>
            <?= Html::a(Yii::t('app', 'My Projects'), ['my'], ['class' => 'btn btn-info']) ?>
        <?php endif; ?>
        <?php if (Yii::$app->user->can(Permission::PROJECT_CREATE)) : ?>
            <?= Html::a(Yii::t('app', 'Create Project'), ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <h1><?= Html::encode(Yii::t('app', 'Projects')) ?></h1>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'hover' => true,
        'columns' => [
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->name), ['projects/view', 'id' => $model->id]);
                },
            ],
            [
                'attribute' => 'short_name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::encode($model->short_name);
                },
            ],
            [
                'label' => Yii::t('app', 'Supervisor'),
                'attribute' => 'supervisorIds',
                'value' => function ($model) {
                    $supervisors = '';
                    if (isset($model->supervisors)) {
                        $supervisorArray = [];
                        /** @var \app\models\records\User $supervisor */
                        foreach ($model->supervisors as $supervisor)
                        {
                            $supervisorArray[] = Html::encode($supervisor->getNameWithEmail());
                        }
                        $supervisors = implode(',', $supervisorArray);
                    }
                    return $supervisors;
                },
                'filter' => ArrayHelper::map(ProjectUser::getAllSupervisors(), 'user.id', 'user.name'),
            ],
            [
                'label' => Yii::t('app', 'State'),
                'attribute' => 'state',
                'value' => function ($model) {
                    return Yii::t('app', $model->state);
                },
                'filter' => Project::getOptionsStates(),
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('app', 'State from'),
                'value' => function ($model) {
                    if ($model->lastState == null) return Yii::$app->formatter->asDate(strtotime($model->updated_at), DateTime::YII_DATE);
                    return Yii::$app->formatter->asDate(strtotime($model->lastState->updated_at), DateTime::YII_DATE);
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'updated_at',
                    array_combine(
                        range(Param::YEAR_OF_LAST_PROJECT_STATE_UPDATE, date('Y')),
                        range(Param::YEAR_OF_LAST_PROJECT_STATE_UPDATE, date('Y'))
                    ),
                    ['class' => 'form-control', 'prompt' => '']
                ),
            ],
            [
                'class' => \yii\grid\ActionColumn::class,
                'template' => '{delete}',
                'headerOptions' => ['width' => '30'],
                'visible' => Yii::$app->user->can(Permission::PROJECT_DELETE),
            ]
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
