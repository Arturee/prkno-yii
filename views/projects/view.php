<?php

use app\models\records\Project;
use app\consts\Permission;
use app\utils\DateTime;
use app\models\records\Proposal;
use app\models\records\DefenceDate;
use app\widgets\UserLinks;
use kartik\editable\Editable;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Project */

$this->title = Html::encode($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$projectProgress = Project::getProgress($model->state);
$maxProjectProgress = Project::getProgress(Project::STATE_DEFENDED);
$lastState = $model->lastState;

$user = Yii::$app->user;
$canChangeState = $user->can(Permission::PROJECT_UPDATE_STATUS);
$canUpdateOwnProject = $user->can(Permission::PROJECT_UPDATE_OWN_PROJECT, ['project' => $model]);
$canUpdateProject = $user->can(Permission::PROJECT_UPDATE);
?>
<div class="projects-view">

    <?php
    // if (Yii::$app->user->can(Permission::PROJECT_UPDATE)
    ?>

    <div class="pull-right">
        <?php
        if ($canUpdateOwnProject && $model->state === Project::STATE_ACCEPTED) {
            if ($model->requested_to_run) {
                echo Html::label(
                    Yii::t('app', 'Request to run sent'),
                    '',
                    [
                        'class' => 'label label-success',
                        'style' => 'margin-right: 1em'
                    ]
                );
            } else {
                Modal::begin([
                    'header' => Yii::t('app', 'Request to run'),
                    'closeButton' => ['label' => 'x', 'class' => 'close'],
                    'toggleButton' => [
                        'label' => Yii::t('app', 'Request to run'),
                        'class' => 'btn btn-success'
                    ],
                ]);
                echo $this->render('_form-request-run', [
                    'model' => $model,
                ]);
                Modal::end();
            }
        }

        if ($canChangeState && $model->requested_to_run && $model->state === Project::STATE_ACCEPTED) {
            echo Html::a(
                Yii::t('app', 'Run'),
                ['run', 'id' => $model->id],
                [
                    'class' => 'btn btn-success',
                    'style' => 'margin-right: 1em'
                ]
            );
        }

        if ($canUpdateProject || $canUpdateOwnProject) {
            echo Html::a(
                Yii::t('app', 'Update'),
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']
            );
        }
        ?>
    </div>
    <h1><?= Html::encode($this->title) ?></h1>

	<div class="progress">
		<div class="progress-bar progress-bar-info" role="progressbar"
             aria-valuenow="<?= $projectProgress ?>" aria-valuemin="0"
             aria-valuemax="<?= $maxProjectProgress ?>"
             style="width:<?= $projectProgress ?>%">
            <?= Yii::t('app', 'Project progress') ?>
		</div>
	</div>

	<div class="row">
        <div class="col-md-6">
            <?= "<h4>" . Yii::t('app', 'Project info') . "</h4>"; ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'label' => Yii::t('app', 'State from'),
                        'value' => function ($model) use (&$lastState) {
                            if ($lastState) {
                                return DateTime::from($lastState->created_at)->format(DateTime::DATETIME);
                            }
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'State'),
                        'format' => 'raw',
                        'value' => function ($model) use (&$canChangeState, &$user, &$lastState) {
                            if ($canChangeState) {
                                $data = array_merge(
                                    array_intersect_key(Project::getOptionsStates(), array_flip(Project::getNextStates($model->state))),
                                    ['-----'],
                                    array_intersect_key(Project::getOptionsStates(), array_flip(Project::allStates()))
                                );
                                $editableStatus = Editable::widget([
                                    'model' => $model,
                                    'attribute' => 'state',
                                    'asPopover' => true,
                                    'header' => Yii::t('app', 'Status'),
                                    'displayValue' => Yii::t('app', $model->state),
                                    'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                    'afterInput' => function ($form, $widget) {
                                        return Html::textarea('statusComment', '', ['rows' => '4']);
                                    },
                                    'data' => $data,
                                    'options' => ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select status...')],
                                    'formOptions' => ['action' => ['status-update', 'id' => $model->id]],
                                    'buttonsTemplate' => '{submit}'
                                ]);
                                return $editableStatus;
                            } else {
                                return Yii::t('app', $model->state);
                            }
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Start Date'),
                        'value' => function ($model) {
                            return DateTime::from($model->run_start_date)->format(DateTime::DATE);
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Deadline'),
                        'value' => function ($model) {
                            return DateTime::from($model->deadline)->format(DateTime::DATE);
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Supervisors'),
                        'format' => 'raw',
                        'value' => function ($model) {
                            return isset($model->supervisors) ? UserLinks::widget([UserLinks::USERS => $model->supervisors]) : "";
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Consultants'),
                        'format' => 'raw',
                        'value' => function ($model) {
                            return isset($model->consultants) ? UserLinks::widget([UserLinks::USERS => $model->consultants]) : "";
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Members'),
                        'format' => 'raw',
                        'value' => function ($model) {
                            return isset($model->members) ? UserLinks::widget([UserLinks::USERS => $model->members]) : "";
                        }
                    ],
                ]
            ]);
            ?>
            </div>
            <?php if (Yii::$app->user->id) { ?>
            <div class="col-md-6">
               <?php

                echo "<h4>" . Yii::t('app', 'History') . "</h4>";

                echo GridView::widget([
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => $model->projectStateHistories,
                        'pagination' => ['pageSize' => 10]
                    ]),
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'value' => function ($model) {
                                return DateTime::from($model->created_at)->format(DateTime::DATETIME);
                            },
                            'headerOptions' => ['width' => '170'],
                        ],
                        [
                            'attribute' => 'state',
                            'value' => function ($model) {
                                return Yii::t('app', $model->state);
                            },
                        ],
                        [
                            'attribute' => 'comment'
                        ]
                    ],
                ]);
                ?>
    		</div>
            <?php } ?>
        </div>
    	<div class="row">
    		<div class="col-sm-12">
					<div class="panel panel-info">
                        <div class="panel-heading">
                            <?= Yii::t('app', 'Description') ?>
                        </div>
                        <div class="panel-body">
                            <?= $model->descriptionMarkdownProcessed ?>
                        </div>
                    </div>
    		</div>
    	</div>

        <?php if (Yii::$app->user->can(Permission::PROPOSAL_VIEW, ['proposal' => $model->lastProposal]) ||
            Yii::$app->user->can(Permission::PROPOSALS_MANAGE) ||
            Yii::$app->user->can(Permission::PROPOSAL_UPDATE, ['project' => $model])) : ?>
        <div class="row">

            <div class="col-sm-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?= Yii::t('app', 'Proposal') ?>

                        <?php if ($model->lastProposal->state == Proposal::STATE_ACCEPTED) : ?>
                            <label class="label label-success"><?= Proposal::getStateToString($model->lastProposal->state) ?></label>
                        <?php elseif ($model->lastProposal->state == Proposal::STATE_DECLINED) : ?>
                            <label class="label label-danger"><?= Proposal::getStateToString($model->lastProposal->state) ?></label>
                        <?php elseif ($model->lastProposal->state == Proposal::STATE_DRAFT || $model->lastProposal->state == Proposal::STATE_SENT) : ?>
                            <label class="label label-info"><?= Proposal::getStateToString($model->lastProposal->state) ?></label>
                        <?php endif; ?>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                            <?php if (Yii::$app->user->can(Permission::PROPOSAL_UPDATE, ['project' => $model])) : ?>
                                <?php if ($model->lastProposal->state == Proposal::STATE_DRAFT) : ?>
                                    <?= Html::a(Yii::t('app', 'Update'), ['/proposals/update', 'id' => $model->lastProposal->id], ['class' => 'btn btn-primary']) ?>
                                    <?= Html::a(Yii::t('app', 'Send for approval'), ['send', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                                <?php elseif ($model->lastProposal->state == Proposal::STATE_SENT) : ?>
                                    <p class="bg-success">
                                        <?= Yii::t('app', 'Proposal was send for approval') ?>
                                    </p>
                                    <?= Html::a(Yii::t('app', 'Voting page'), ['/proposals/view', 'id' => $model->lastProposal->id], ['class' => 'btn btn-info']) ?>
                                <?php elseif ($model->lastProposal->state == Proposal::STATE_DECLINED) : ?>
                                    <?= Html::a(Yii::t('app', 'Create'), ['/proposals/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                                    <?= Html::a(Yii::t('app', 'Voting page'), ['/proposals/view', 'id' => $model->lastProposal->id], ['class' => 'btn btn-info']) ?>
                                <?php elseif ($model->lastProposal->state == Proposal::STATE_ACCEPTED) : ?>
                                    <?= Html::a(Yii::t('app', 'Voting page'), ['/proposals/view', 'id' => $model->lastProposal->id], ['class' => 'btn btn-info']) ?>
                                <?php endif; ?>
                            <?php endif; ?>

                            <?php
                            Modal::begin([
                                'header' => '<img src="' . Yii::$app->homeUrl . 'img/md.png" style="width: 20px;" title="markdown"/>',
                                'toggleButton' => ['label' => Yii::t('app', 'Show'), 'class' => 'btn btn-info'],
                            ]);

                            echo $model->lastProposal->documentMarkdownProcessed;

                            Modal::end();
                            ?>

                            <?= ($model->lastProposal->document->fallback_path) ?
                                Html::a(Yii::t('app', 'PDF'), $model->lastProposal->document->fallback_path, ['class' => 'btn btn-info']) : ('')
                            ?>

                            <?php
                            Modal::begin([
                                'header' => Yii::t('app', 'Proposal history'),
                                'toggleButton' => ['label' => Yii::t('app', 'Proposal history'), 'class' => 'btn btn-info'],
                            ]);

                            $proposalHistoriesDataProvider = new ArrayDataProvider([
                                'allModels' => $model->proposals,
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                            ]);

                            echo GridView::widget([
                                'dataProvider' => $proposalHistoriesDataProvider,
                                'columns' => [
                                    [
                                        'attribute' => 'created_at',
                                        'value' => function ($model) {
                                            return DateTime::from($model->created_at)->format(DateTime::DATETIME);
                                        }
                                    ],
                                    [
                                        'label' => Yii::t('app', 'State'),
                                        'attribute' => 'state',
                                        'value' => function ($model) {
                                            return Yii::t('app', $model->state);
                                        },
                                    ],
                                    [
                                        'label' => Yii::t('app', 'Link'),
                                        'attribute' => 'link',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return Html::a(Yii::t('app', 'Proposal'), ['/proposals/view', 'id' => $model->id]);
                                        },
                                    ],
                                ],
                            ]);

                            Modal::end();
                            ?>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
        <?php endif; ?>

        <?php
        if ((Yii::$app->user->can(Permission::PROJECT_ASSIGN_TO_DEFENCE, ['project' => $model]) && $model::canAssignToDefence($model->state))
            || sizeof($model->projectDefences) > 0) : ?>
        <div class="row">
            <div class="col-sm-12">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <?= Yii::t('app', 'Project defences') ?>
                    </div>

                    <div class="panel-body">
                        <?php
                        if (sizeof($model->projectDefences) > 0) : ?>
                        <div class="col-sm-6">
                            <?php
                            $defencesDataProvider = new ArrayDataProvider([
                                'allModels' => $model->projectDefences,
                                'pagination' => false
                            ]);
                            echo GridView::widget([
                                'summary' => '',
                                'dataProvider' => $defencesDataProvider,
                                'columns' => [
                                    [
                                        'label' => Yii::t('app', 'Date'),
                                        'attribute' => 'date',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            $time = $model->time ? DateTime::time($model->time) : '--';
                                            $dateTime = $time . '  ' . DateTime::date($model->defenceDate->date);
                                            return Html::a(Yii::t('app', $dateTime), ['/defences/view', 'id' => $model->id]);
                                        },
                                    ],
                                    [
                                        'label' => Yii::t('app', 'Result'),
                                        'attribute' => 'link',
                                        'value' => function ($model) {
                                            return Yii::t('app', $model->result);
                                        },
                                    ],
                                ],
                            ]);
                            ?>
                        </div>
                        <?php endif; ?>
                        <?php
                        $canAddToDefence = Yii::$app->user->can(Permission::DEFENCE_ADD_PROJECT, ['project' => $model]);
                        if ($model::canAssignToDefence($model->state) && $canAddToDefence && $model->getUpcomingDefence() == null) { ?>
                        <div class="col-sm-6">
                            <?php
                            echo '<h4>' . Yii::t('app', "Future defence dates") . '</h4>';
                            $futureDateProvider = new ActiveDataProvider([
                                'query' => DefenceDate::getSignUpDates(),
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                            ]);
                            echo GridView::widget([
                                'dataProvider' => $futureDateProvider,
                                'columns' => [
                                    [
                                        'label' => Yii::t('app', 'Date'),
                                        'attribute' => 'date',
                                        'value' => function ($model) {
                                            return DateTime::date($model->date);
                                        },
                                    ],
                                    [
                                        'label' => Yii::t('app', 'Add deadline'),
                                        'attribute' => 'signup_deadline',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return DateTime::date($model->signup_deadline) . ' <span class="glyphicon glyphicon-question-sign" 
                                                title="' . Yii::t('app', 'Add deadline hint') . '"></span>';
                                        },
                                    ],
                                    [
                                        'label' => '',
                                        'attribute' => 'link',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return Html::a(
                                                Yii::t('app', 'Add project to defence date'),
                                                ['defence-dates/add-project?id=' . $model->id . '&projectId=' . Yii::$app->request->getQueryParams()['id']],
                                                ['data' => [
                                                    'confirm' => Yii::t('app', 'Are you sure to assign to this date?'),
                                                    'method' => 'post'
                                                ]]
                                            );
                                        },
                                    ],
                                ],
                            ]); ?>
                        </div>
                        <?php

                    }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can(Permission::PROJECT_UPDATE_CONTENT, ['project' => $model])) : ?>
        <div class="row">

            <div class="col-sm-12">

                <div class="panel panel-success">

                    <div class="panel-heading">
                        <?= Yii::t('app', 'Wiki') ?>
                        <span class="glyphicon glyphicon-question-sign" title="
                            <?= Yii::t('app', 'Visible only for team members') ?> "></span>
                    </div>

                    <div class="panel-body">
                        <?php echo $model->contentMarkdownProcessed; ?>
                        <?php
                        Modal::begin([
                            'header' => Yii::t('app', 'Update wiki'),
                            'toggleButton' => ['label' => '<i class="glyphicon glyphicon-pencil"></i> ', 'class' => 'btn btn-sm btn-default kv-editable-button kv-editable-toggle'],
                        ]); ?>
                        <?= $this->render('_form-content', [
                            'model' => $model,
                        ]) ?>
                        <?php
                        Modal::end();
                        ?>
                    </div>

                </div>

            </div>

        </div>
        <?php endif; ?>

        <?php
        if (Yii::$app->user->can(Permission::PROJECT_UPLOAD_FILE, ['project' => $model]) ||
            sizeof($model->projectDocuments) > sizeof($model->privateDocuments)) : ?>
		<div class="row">
        	<div class="col-sm-12">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <?= Yii::t('app', 'Documents') ?>
                    </div>

                    <div class="panel-body">
                        <?php
                        if (Yii::$app->user->can(Permission::PROJECT_UPLOAD_FILE, ['project' => $model]) && ($model::canAssignToDefence($model->state))) { ?>
                        <div>
                            <?php
                            $defenceType = $model->getNextDefenceType();
                            if ($defenceType == 1) { ?>
                                <h4><?= Yii::t('app', 'Documents to small defence') ?> </h4>
                            <?php

                        } elseif ($defenceType == 2) { ?>
                                <h4><?= Yii::t('app', 'Documents to defence') ?> </h4>
                            <?php

                        } ?>
                            <?= $this->render('_form-upload-defence', [
                                'model' => $model,
                            ]) ?>
                        </div>
                        <?php 
                    } ?>

                        <?php
                        if (Yii::$app->user->can(Permission::PROJECT_UPLOAD_FILE, ['project' => $model])
                            && Project::canUpdateRunningProject($model->state)) { ?>
                        <div>
                            <?php
                            if ($model->privateDocuments) { ?>
                            <h4><?= Yii::t('app', 'Private documents') ?></h4>
                            <table class="table">
                                <tbody>
                                <?php

                                /** @var \app\models\records\ProjectDocument $privateDocument */
                                foreach ($model->privateDocuments as $privateDocument) {
                                    echo '<tr><td>';
                                    echo DateTime::from($privateDocument->created_at)->format(DateTime::DATETIME) . ' ';
                                    if ($privateDocument->comment) echo Html::a(Html::encode($privateDocument->comment), $privateDocument->document->fallback_path, ['style' => 'text-decoration: underline;']);
                                    else echo Html::a(Yii::t('app', 'LINK'), $privateDocument->document->fallback_path, ['style' => 'text-decoration: underline;']);
                                    echo '</td><td style="width: 20px;">';
                                    echo Html::a(
                                        '<span class="glyphicon glyphicon-trash"></span>',
                                        ["projects/delete-document?doc=" . $privateDocument->id . "&id=" . $model->id],
                                        ['data' => [
                                            'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                            'method' => 'post'
                                        ]]
                                    );
                                    echo '</td></tr>';
                                } ?>
                                </tbody>
                            </table>
                            <?php 
                        } ?>
                            <?php
                            if ($model->publicDocuments) { ?>
                            <h4><?= Yii::t('app', 'Public documents') ?></h4>
                                <table class="table">
                                    <tbody>
                                    <?php

                                    /** @var \app\models\records\ProjectDocument $publicDocument */
                                    foreach ($model->publicDocuments as $publicDocument) {
                                        echo '<tr><td>';
                                        echo DateTime::from($publicDocument->created_at)->format(DateTime::DATETIME) . ' ';
                                        if ($publicDocument->comment) echo Html::a(Html::encode($publicDocument->comment), $publicDocument->document->fallback_path, ['style' => 'text-decoration: underline;']);
                                        else echo Html::a(Yii::t('app', 'LINK'), $publicDocument->document->fallback_path, ['style' => 'text-decoration: underline;']);
                                        echo '</td><td style="width: 20px;">';
                                        echo Html::a('<span class="glyphicon glyphicon-trash"></span>', ["projects/delete-document?doc=" . $publicDocument->id . "&id=" . $model->id], ['data' => [
                                            'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                            'method' => 'post'
                                        ]]);
                                        echo '</td></tr>';
                                    } ?>
                                    </tbody>
                                </table>
                            <?php 
                        } ?>
                            <?php
                            Modal::begin([
                                'header' => Yii::t('app', 'Add document'),
                                'toggleButton' => ['label' => Yii::t('app', 'Add document'), 'class' => 'btn btn-success'],
                            ]); ?>
                            <?= $this->render('_form-add-document', [
                                'model' => $model,
                            ]) ?>
                            <?php
                            Modal::end();
                            ?>
                        </div>
                        <?php
                            // Public Documents for everyone else
                    } else if ($model->publicDocuments) { ?>
                            <table class="table">
                                <tbody>
                                <?php

                                /** @var \app\models\records\ProjectDocument $publicDocument */
                                foreach ($model->publicDocuments as $publicDocument) {
                                    echo '<tr><td>';
                                    echo DateTime::from($publicDocument->created_at)->format(DateTime::DATETIME) . ' ';
                                    if ($publicDocument->comment) echo Html::a(Html::encode($publicDocument->comment), $publicDocument->document->fallback_path, ['style' => 'text-decoration: underline;']);
                                    else echo Html::a(Yii::t('app', 'LINK'), $publicDocument->document->fallback_path, ['style' => 'text-decoration: underline;']);
                                    echo '</td></tr>';
                                } ?>
                                </tbody>
                            </table>
                            <?php
                        } ?>

                        <div>
                            <?php if (Yii::$app->user->can(Permission::PROJECT_UPDATE_ADVICE, ['project' => $model]) ||
                                $model->adviceDocument && $model::canSeeAdvice($model->state)) : ?>
                                <h4><?= Yii::t('app', 'Experience') ?>
                            <?php endif; ?>
                            <?php if (Yii::$app->user->can(Permission::PROJECT_UPDATE_ADVICE, ['project' => $model])) : ?>
                                <?php
                                Modal::begin([
                                    'header' => Yii::t('app', 'Update experience'),
                                    'toggleButton' => ['label' => '<i class="glyphicon glyphicon-pencil"></i> ', 'class' => 'btn btn-sm btn-default kv-editable-button kv-editable-toggle'],
                                ]); ?>
                                <?= $this->render('_form-advice', [
                                    'model' => $model,
                                ]) ?>
                                <?php
                                Modal::end();
                                ?>
                            <?php endif; ?>
                            </h4>
                            <?php if ($model->adviceDocument && (Yii::$app->user->can(Permission::PROJECT_UPDATE_ADVICE, ['project' => $model]) || $model::canSeeAdvice($model->state))) : ?>
                                <div class="panel-body">
                                    <?= $model->getAdviceMarkdownProcessed() ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>

        	</div>
		</div>
        <?php endif; ?>

	</div>


<?php
$tooltipScript = <<<JS
			$('[data-toggle="tooltip"]').tooltip();
JS;
$this->registerJs($tooltipScript, yii\web\View::POS_READY, 'tooltip');
?>