<?php

use app\consts\Permission;
use app\models\records\Project;
use app\utils\DateTime;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModelMyProjects \app\models\search\ProjectSearch */
/* @var $dataProviderMyProjects yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Back to Projects'), ['index'], ['class' => 'btn btn-info']) ?>
        <?php if (Yii::$app->user->can(Permission::PROJECT_CREATE)) : ?>
            <?= Html::a(Yii::t('app', 'Create Project'), ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <h1><?= Html::encode(Yii::t('app', 'My Projects')) ?></h1>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProviderMyProjects,
        'hover' => true,
        'columns' => [
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->name), ['projects/view', 'id' => $model->id]);
                },
            ],
            [
                'attribute' => 'short_name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->short_name), ['projects/view', 'id' => $model->id]);
                },
            ],
            [
                'label' => Yii::t('app', 'State'),
                'attribute' => 'state',
                'value' => function ($model) {
                    return Yii::t('app', $model->state);
                },
                'filter' => Project::getOptionsStates(),
            ],
            [
                'label' => Yii::t('app', 'State from'),
                'attribute' => 'updated_at',
                'value' => function ($model) {
                    if ($model->lastState == null) return Yii::$app->formatter->asDate(strtotime($model->updated_at), DateTime::YII_DATE);
                    return Yii::$app->formatter->asDate(strtotime($model->lastState->updated_at), DateTime::YII_DATE);
                },
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
