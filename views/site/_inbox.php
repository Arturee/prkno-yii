<?php

use app\consts\Permission;
use app\models\records\Project;
use app\models\records\DefenceDate;
use app\models\records\DefenceAttendance;
use app\models\records\Proposal;
use app\models\records\ProposalVote;
use app\utils\DateTime;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $inboxProjects yii\data\ActiveDataProvider */
/* @var $inboxProposals yii\data\ActiveDataProvider */
/* @var $inboxDefenceDates yii\data\ActiveDataProvider */

?>
<div class="inbox-index">

    <h1><?= Html::encode(Yii::t('app', 'Inbox')) ?></h1>

    <?php Pjax::begin(); ?>

    <?php if (Yii::$app->user->can(Permission::INBOX_PROJECTS)) { ?>
        <?= GridView::widget([
            'dataProvider' => $inboxProjects,
            'summary' => '',
            'columns' => [
                [
                    'label' => Yii::t('app', 'Project ID'),
                    'attribute' => 'short_name',
                    'format' => 'raw',
                    'value' => function (Project $model) {
                        return Html::a(Html::encode($model->short_name), ['projects/view', 'id' => $model->id]);
                    },
                ],
                [
                    'label' => Yii::t('app', 'State'),
                    'attribute' => 'state',
                    'value' => function (Project $model) {
                        return Yii::t('app', $model->state);
                    },
                    'filter' => Project::getOptionsStates(),
                ],
                [
                    'label' => Yii::t('app', 'State from'),
                    'attribute' => 'updated_at',
                    'value' => function(Project $model) {
                        if ($model->lastState == null) return Yii::$app->formatter->asDate(strtotime($model->updated_at), DateTime::YII_DATE);
                        return Yii::$app->formatter->asDate(strtotime($model->lastState->updated_at), DateTime::YII_DATE);
                    },
                ],
                [
                    'value' => function($model) {
                        $now = new DateTime();
                        if ($model->deadline && $model->deadline < $now) return Yii::t('app', 'After deadline');
                        else if ($model->requested_to_run == true) return Yii::t('app', 'Request to run');
                        else if ($model->waiting_for_proposal_evaluation == true) return Yii::t('app', 'Waiting For Proposal Evaluation');
                    },
                    'visible' => Yii::$app->user->can(Permission::PROJECT_UPDATE_STATUS),
                ],
            ],
        ]); ?>
    <?php } ?>

    <?= GridView::widget([
        'dataProvider' => $inboxProposals,
        'summary' => '',
        'columns' => [
            [
                'label' => Yii::t('app', 'Project ID'),
                'attribute' => 'short_name',
                'format' => 'raw',
                'value' => function (Proposal $model) {
                    return Html::a(Html::encode($model->project->short_name), ['projects/view', 'id' => $model->project->id]);
                }
            ],
            [
                'label' => Yii::t('app', 'My vote'),
                'format' => 'text',
                'value' => function(Proposal $model) {
                    $vote = $model->getProposalVoteByUserId(Yii::$app->getUser()->getId());
                    if ($vote) {
                        return ProposalVote::getVoteLabel($vote->vote);
                    }
                    else {
                        return ProposalVote::getVoteLabel('');
                    }
                }
            ],
            [
                'label' => Yii::t('app', 'Proposal'),
                'format' => 'raw',
                'value' => function(Proposal $model) {
                    $vote = $model->getProposalVoteByUserId(Yii::$app->getUser()->getId());
                    if ($vote == null) {
                        return Html::a(Yii::t('app', 'Please vote here'), ['proposals/view', 'id' => $model->id]);
                    } else {
                        return Html::a(Yii::t('app', 'Proposal'), ['proposals/view', 'id' => $model->id]);
                    }
                },
                'visible' => function(Proposal $model) {
                    return Yii::$app->user->can(Permission::PROPOSAL_VOTE, ['proposal' => $model]);
                },
            ],
        ],
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $inboxDefenceDates,
        'summary' => '',
        'columns' => [
            [
                'label' => Yii::t('app', 'Defence Dates'),
                'attribute' => 'short_name',
                'format' => 'raw',
                'value' => function (DefenceDate $model) {
                    $date = new DateTime($model->date);
                    return Html::a($date->format(DateTime::DATE), ['defence-dates/update', 'id' => $model->id]);
                }
            ],
            [
                'label' => Yii::t('app', 'My vote'),
                'format' => 'text',
                'value' => function(DefenceDate $model) {
                    $attendance = $model->getDefenceAttendanceByUserId(Yii::$app->getUser()->getId());
                    return DefenceAttendance::getVoteLabel($attendance->vote);
                }
            ],
            [
                'format' => 'raw',
                'value' => function($model) {
                    $attendance = $model->getDefenceAttendanceByUserId(Yii::$app->getUser()->getId());
                    if ($attendance->vote == DefenceAttendance::VOTE_UNKNOWN) {
                        return Html::a(Yii::t('app', 'Please vote here'), ['defence-dates/update', 'id' => $model->id]);
                    } else return '';
                },
                'visible' => Yii::$app->user->can(Permission::DEFENCES_VOTE_ABOUT_DATE),
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
