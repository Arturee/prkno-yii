<?php

use app\consts\Permission;
use app\models\records\News;
use app\utils\DateTime;
use yii\helpers\Html;
use yii\helpers\Markdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/news/index.css');

$canManageNews = Yii::$app->user->can(Permission::NEWS_MANAGE);

?>
<div class="news-index">

    <?php if ($canManageNews) {
        echo Html::a(Yii::t('app', 'Create News'), ['/news/create'], ['class' => 'btn btn-success pull-right']);
    } ?>

    <h1><?= Html::encode(Yii::t('app', 'News')) ?></h1>

    <?php

    /**
     * @var \app\models\records\News $value
     */
    foreach ($news as $key => $value) {
        if (Yii::$app->user->isGuest && !$value->public) continue;
        ?>
        <div class="news-active-record <?= $value->type == News::TYPE_IMPORTANT ? "news-important" : "news-normal"; ?>">

            <?php if ($canManageNews) { ?>
                <div class="pull-right">
                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ["news/update?id=" . $value->id]) ?>
                    <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ["news/delete?id=" . $value->id], ['data' => [
                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                        'method' => 'post'
                    ]]) ?>
                </div>
            <?php 
        } ?>

            <strong><?= $value->getLocalTitle() ?></strong>
            <span class="news-important-label">
                <?= $value->type == News::TYPE_IMPORTANT ? "(" . Yii::t('app', 'important') . ")" : ""; ?>
            </span>
            <span class="news-date"><?= DateTime::from($value->updated_at)->format(DateTime::DATE) ?></span>

            <div class="news-content"><?= Markdown::process(Html::encode($value->getLocalContent())) ?>&nbsp;</div>
        </div>
        <?php

    }
    ?>
</div>
