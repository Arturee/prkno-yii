<?php

use app\models\records\Advertisement;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $advertisements yii\data\ActiveDataProvider */

?>
<div class="advertisements-index">

    <h1><?= Html::encode(Yii::t('app', 'New Advertisements')) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $advertisements,
        'bordered' => false,
        'headerRowOptions' => ['class' => 'hidden'],
        'striped' => false,
        'summary' => '',
        'columns' => [
            [
                'attribute' => 'title',
                'label' => false,
                'format' => 'raw',
                'filter' => false,
                'value' => function ($model) {
                    return Yii::$app->controller->renderPartial('_advertisements_model-view', ['model' => $model]);
                },
            ],
            [
                'attribute' => 'type',
                'label' => false,
                'filter' => Html::checkboxList('type', 'value', [
                    Advertisement::TYPE_PERSON => Yii::t('app', 'person'),
                    Advertisement::TYPE_PROJECT => Yii::t('app', 'project'),
                ]),
                'value' => function ($model) {
                    return Yii::t('app', $model->type);
                },
                'contentOptions' => function ($model) {
                    return ['width' => '70'];
                },
            ],
        ],
    ]); ?>

</div>
