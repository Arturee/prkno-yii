<?php

use app\consts\Permission;

/* @var $this yii\web\View */
/* @var $news */
/* @var $advertisements */
/* @var $inboxProjects yii\data\ActiveDataProvider*/
/* @var $inboxDefenceDates yii\data\ActiveDataProvider*/
/* @var $inboxProposals yii\data\ActiveDataProvider*/
/* @var $myProjects yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Software projects MFF UK course NPRG023');
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-12 intro">
                <?= Yii::t('app', 'intro');?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?php if (Yii::$app->user->can(Permission::INBOX_LIST)) { ?>
                    <?= $this->render('_inbox', [
                        'inboxProjects' => $inboxProjects,
                        'inboxDefenceDates' => $inboxDefenceDates,
                        'inboxProposals' => $inboxProposals
                    ]); ?>
                <?php 
            } ?>
                <?= $this->render('_news', [
                    'news' => $news
                ]); ?>
            </div>
            <div class="col-lg-6">
                <?php
                if (Yii::$app->user->id && $myProjects->getCount()) {
                    echo $this->render('_my-projects', [
                        'myProjects' => $myProjects,
                    ]);
                }
                ?>
                <?= $this->render('_advertisements', [
                    'advertisements' => $advertisements,
                ]); ?>
            </div>
        </div>
    </div>
</div>
