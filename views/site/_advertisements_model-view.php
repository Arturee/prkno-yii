<?php

use app\models\records\AdvertisementComment;
use app\utils\DateTime;
use app\widgets\UserLinks;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Advertisement */

$dataProvider = new ActiveDataProvider(['query' => $model->getAdComments()]);
$commentsName = 'comments-' . $model->id;
$keywords = "";
if ($model->keywords != "") {
    foreach (explode(',', $model->keywords) as $value) {
        $keywords .= Html::a('#' . $value, Url::to(['advertisements/index', 'AdvertisementSearch[keywords]' => $value])) . ' ';
    }
}
?>

<h3><?= Html::a($model->title, ['advertisements/view', 'id' => $model->id]) ?></h3>

<p>
    <small><?= UserLinks::widget([UserLinks::USERS => $model->user]) ?></small>
    <small class="pull-right"><?= DateTime::from($model->updated_at)->format(DateTime::DATETIME) ?></small>
</p>

<p><?= $model->contentMarkdownProcessed ?></p>