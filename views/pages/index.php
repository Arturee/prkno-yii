<?php

use app\consts\Permission;
use app\models\records\Page;
use app\models\records\PageVersion;
use app\utils\DateTime;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pages');
$this->params['breadcrumbs'][] = Yii::t('app', $this->title);

$canManagePages = Yii::$app->user->can(Permission::PAGES_MANAGE);

?>
<div class="pages-index">

    <p class="pull-right">
        <?= $canManagePages ?
            Html::a(Yii::t('app', 'Create New Page'), ['create'], ['class' => 'btn btn-success'])
            : ("") ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'bordered' => false,
        'columns' => [
            [
                'attribute' => 'title',
                'label' => false,
                'format' => 'raw',
                'filter' => false,
                'value' => function (Page $model) {
                    $title = $model->getTitleBasedOnSiteLanguage();
                    return Html::tag('h3', Html::a(Yii::t('app', htmlspecialchars($title)), ['view', 'url' => $model->url]));
                },
                'contentOptions' => function ($model) {
                    return ['width' => '220'];
                },
            ],
            [
                'attribute' => 'url',
                'label' => Yii::t('app', 'URL'),
                'format' => 'raw',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'visible' => $canManagePages,
                'value' => function (Page $model) {
                    return
                        Html::tag('span', Html::encode($model->url));
                },
                'contentOptions' => function ($model) {
                    return ['width' => '120'];
                },
            ],
            [
                'attribute' => 'last_change',
                'label' => Yii::t('app', 'Last change'),
                'format' => 'raw',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'visible' => $canManagePages,
                'value' => function (Page $model) {
                    /** @var PageVersion $lastlyUpdatedPageVersion */
                    $lastlyUpdatedPageVersion = PageVersion::find()
                        ->where(['page_id' => $model->id])
                        ->andWhere(['deleted' => false])
                        ->orderBy(['updated_at' => SORT_DESC])
                        ->one();

                    $pageLastUpdate = DateTime::from($model->updated_at);

                    if ($lastlyUpdatedPageVersion) {
                        $pageVersionLastUpdate = DateTime::from($lastlyUpdatedPageVersion->updated_at);
                        $lastUpdate = $pageLastUpdate->isAfter($pageVersionLastUpdate) ? $pageLastUpdate : $pageVersionLastUpdate;
                    } else {
                        $lastUpdate = $pageLastUpdate;
                    }

                    return
                        Html::tag('span', $lastUpdate->format(DateTime::DATETIME));
                },
                'contentOptions' => function ($model) {
                    return ['width' => '120'];
                },
            ],
            [
                'attribute' => 'published',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'label' => Yii::t('app', 'Published'),
                'visible' => $canManagePages,
                'value' => function (Page $model) {
                    return Yii::t('app', $model->published ? "Yes" : "No");
                },
                'contentOptions' => function ($model) {
                    return ['width' => '70'];
                },
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update} {move-up} {move-down} {publish} {delete}',
                'dropdownOptions' => ['class' => 'pull-right'],
                'visible' => $canManagePages,
                'vAlign' => GridView::ALIGN_MIDDLE,
                'buttons' => [
                    'publish' => function ($url, $model) {
                        if ($model->published) {
                            return Html::a('<span class="glyphicon glyphicon-eye-close"></span>', $url, [
                                'title' => Yii::t('app', 'Conceal'),
                                'data-confirm' => Yii::t('app', 'Are you sure to conceal this item?'),
                                'data-method' => 'post',
                            ]);
                        }
                        return Html::a('<span class="glyphicon glyphicon-globe"></span>', $url, [
                            'title' => Yii::t('app', 'Publish'),
                            'data-confirm' => Yii::t('app', 'Are you sure to publish this item?'),
                            'data-method' => 'post',
                        ]);
                    },
                    'move-up' => function ($url) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-circle-arrow-up"></span>',
                            $url,
                            [
                                'title' => Yii::t('app', 'Move up in order'),
                                'data-pjax' => '1',
                            ]
                        );
                    },
                    'move-down' => function ($url) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-circle-arrow-down"></span>',
                            $url,
                            [
                                'title' => Yii::t('app', 'Move down in order'),
                                'data-pjax' => '1',
                            ]
                        );
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                        ]);
                    }
                ],
            ],
            [
                'attribute' => 'download',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'format' => 'raw',
                'label' => false,
                'value' => function (Page $model) {
                    /** @var PageVersion $pageVersion */
                    $pageVersion = $model->getCurrentlyShowingPageVersion();
                    $pageVersionDocument = $pageVersion ? $pageVersion->getDocumentBasedOnSiteLanguage() : null;
                    return Yii::$app->controller->renderPartial(
                        '_download-button',
                        ['page' => $model, 'pageVersion' => $pageVersion, 'pageVersionDocument' => $pageVersionDocument]
                    );
                },
                'contentOptions' => ['width' => '70', 'align' => 'center']
            ],
        ],
    ]); ?>
</div>