<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\consts\Permission;
use app\consts\Param;
use app\assets\PagesAsset;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Page */
/* @var $pageVersionDataProvider ActiveDataProvider */
/* @var $pageVersion \app\models\records\PageVersion | null */

$this->title = Html::encode($model->getTitleBasedOnSiteLanguage());

$canManagePages = Yii::$app->user->can(Permission::PAGES_MANAGE);

if ($canManagePages) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
}

$this->params['breadcrumbs'][] = Yii::t('app', $this->title);

if ($pageVersion) {
    $pageVersionDocument = $pageVersion->getDocumentBasedOnSiteLanguage();
} else {
    $pageVersionDocument = null;
}

PagesAsset::register($this);
?>
<div class="pages-view">

    <p class="pull-right">
        <?=
        Yii::$app->controller->renderPartial(
            '_download-button',
            ['page' => $model, 'pageVersion' => $pageVersion, 'pageVersionDocument' => $pageVersionDocument]
        );
        ?>
    </p>

    <h1><?= Yii::t('app', Html::encode($this->title)) ?></h1>
    <div class="page-container">
        <p><?= $pageVersionDocument ? $pageVersionDocument->getContentMarkdownProcessed() :
                Html::tag('em', Yii::t('app', Param::PAGE_NO_VERSION_SELECTED)); ?></p>
    </div>

    <?php
    if ($canManagePages) {
        echo Yii::$app->controller->renderPartial('_previous-versions', ['pageVersionDataProvider' => $pageVersionDataProvider]);
    }
    ?>

</div>
