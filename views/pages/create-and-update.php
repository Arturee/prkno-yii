<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use app\models\forms\MarkdownForm;
use app\models\records\PageVersion;
use app\widgets\Markdown;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Page */
/* @var $form yii\widgets\ActiveForm */
/* @var $updatePage PageVersion */

$this->title = Yii::t('app', 'Page create/update');
$this->params['breadcrumbs'][] = Yii::t('app', $this->title);

$pageVersionUser = $updatePage ? $updatePage->user : null;
$pageVersionCSDocument = $updatePage ? $updatePage->contentCs : null;
$pageVersionENDocument = $updatePage ? $updatePage->contentEn : null;
?>

<div class="pages-create-and-update">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'title_cs')->textInput(['maxlength' => true]) ?>

        <?php
        Modal::begin([
            'header' => "<img src=\"" . Yii::$app->homeUrl . "img/md.png\" style=\"width: 20px;\" title=\"markdown\"/>",
            'id' => 'modal'
        ]);
        Modal::end()
        ?>

        <?php
        echo Markdown::widget([
            Markdown::ACTIVE_FORM => $form,
            Markdown::AUTHOR => $pageVersionUser,
            Markdown::CAN_EDIT => true,
            Markdown::DOCUMENT => $pageVersionCSDocument,
            Markdown::TITLE => 'CS content',
            Markdown::TYPE => false,
            Markdown::SUCCESS_BUTTON_FUNCTION => MarkdownForm::SUCCESS_BUTTON_PREVIEW,
            Markdown::CUSTOM_FORM_NAME => 'CSContent',
            Markdown::FILE_UPLOAD_ENABLED => false
        ]);
        ?>

        <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

        <?php
        echo Markdown::widget([
            Markdown::ACTIVE_FORM => $form,
            Markdown::AUTHOR => $pageVersionUser,
            Markdown::CAN_EDIT => true,
            Markdown::DOCUMENT => $pageVersionENDocument,
            Markdown::TITLE => 'EN content',
            Markdown::TYPE => false,
            Markdown::SUCCESS_BUTTON_FUNCTION => MarkdownForm::SUCCESS_BUTTON_PREVIEW,
            Markdown::CUSTOM_FORM_NAME => 'ENContent',
            Markdown::FILE_UPLOAD_ENABLED => false
        ]);
        ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['name' => 'save', 'value' => 'overwrite', 'class' => 'btn btn-success']) ?>
            <?= Html::submitButton(Yii::t('app', 'Save as new version'), ['name' => 'save', 'value' => 'newVersion', 'class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>

