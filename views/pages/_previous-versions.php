<?php

use app\models\records\PageVersion;
use app\utils\DateTime;
use app\widgets\UserLinks;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $pageVersionDataProvider ActiveDataProvider */

?>

<h3><?= Html::tag('em', Yii::t('app', "List of versions")) ?></h3>

<?= GridView::widget([
    'dataProvider' => $pageVersionDataProvider,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'contentOptions' => function (PageVersion $model) {
                $idToHighlight = Yii::$app->getRequest()->getQueryParam('versionId');
                $highlightThisRow = false;

                if ((!$idToHighlight && $model->published)) {
                    $highlightThisRow = true;
                } else if ($idToHighlight) {
                    if ($idToHighlight == $model->id) {
                        $highlightThisRow = true;
                    }
                }

                if ($highlightThisRow) {
                    return ['class' => 'chosen-row'];
                }
                return [];
            },
        ],
        [
            'attribute' => 'updated_at',
            'label' => Yii::t('app', 'Updated At'),
            'format' => 'raw',
            'value' => function (PageVersion $model) {
                return
                    Html::tag('span', DateTime::from($model->updated_at)->format(DateTime::DATETIME));
            }
        ],
        [
            'attribute' => 'created_by',
            'label' => Yii::t('app', 'Last edit by'),
            'format' => 'raw',
            'value' => function (PageVersion $model) {
                return
                    Html::tag('p', UserLinks::widget([UserLinks::USERS => $model->user]));
            }
        ],
        [
            'attribute' => 'selected',
            'label' => Yii::t('app', 'Selected'),
            'value' => function (PageVersion $model) {
                return Yii::t('app', $model->published ? "Yes" : "No");
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view-page-version} {update} {select} {delete}',
            'headerOptions' => ['width' => '120'],
            'visibleButtons' => [
                'select' => function (PageVersion $model) {
                    return !$model->published;
                },
            ],
            'buttons' => [
                'view-page-version' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                        'title' => Yii::t('app', 'View page version'),
                    ]);
                },
                'update' => function ($url, PageVersion $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', "update?id=" . $model->page_id . '&versionId=' . $model->id, [
                        'title' => Yii::t('app', 'Update page version'),
                    ]);
                },
                'select' => function ($url, PageVersion $model) {
                    return Html::a('<span class="glyphicon glyphicon-check"></span>', "publish?id=" . $model->page_id . '&versionId=' . $model->id, [
                        'title' => Yii::t('app', 'Select'),
                        'data-confirm' => Yii::t('app', 'Are you sure to select this item?'),
                        'data-method' => 'post',
                    ]);
                },
                'delete' => function ($url, PageVersion $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', "delete?id=" . $model->page_id . '&versionId=' . $model->id, [
                        'title' => Yii::t('app', 'Delete'),
                        'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                        'data-method' => 'post',
                    ]);
                }
            ],
        ],
    ],
]); ?>