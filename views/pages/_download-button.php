<?php

use app\models\records\Document;
use app\models\records\Page;
use app\models\records\PageVersion;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $page \app\models\records\Page */
/* @var $pageVersion \app\models\records\PageVersion | null */
/* @var $pageVersionDocument Document | null */

if (!function_exists('createDownloadButtonIfNecessary')) {
    function createDownloadButtonIfNecessary(Page $page, ? PageVersion $pageVersion, ? Document $pageVersionDocument) : string
    {
        if (!$pageVersionDocument) {
            return '';
        }

        if (!$pageVersionDocument->content_md) {
            return '';
        }

        return Html::a(
            Yii::t('app', 'Download'),
            ['download', 'id' => $page->id, 'versionId' => $pageVersion ? $pageVersion->id : null],
            ['class' => 'btn btn-primary']
        );
    }
}

echo createDownloadButtonIfNecessary($page, $pageVersion, $pageVersionDocument);

?>
