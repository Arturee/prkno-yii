<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Proposal */

$this->title = Yii::t('app', 'Update Proposal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['projects/index']];
$this->params['breadcrumbs'][] = ['label' => $model->project->name, 'url' => ['projects/view', 'id' => $model->project->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update Proposal');
?>
<div class="proposals-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
