<?php

use app\utils\DateTime;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\search\ProposalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Proposals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposals-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'project.name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->project->name), ['projects/view', 'id' => $model->project->id]);
                }
            ],
            'project.short_name',
            [
                'label' => Yii::t('app', 'Supervisor'),
                'attribute' => 'supervisors',
                'value' => function ($model) {
                    $supervisors = '';
                    if (isset($model->project->supervisors)) {
                        $supervisorArray = [];
                        /** @var \app\models\records\User $supervisor */
                        foreach ($model->project->supervisors as $supervisor)
                        {
                            $supervisorArray[] = Html::encode($supervisor->getNameWithEmail());
                        }
                        $supervisors = implode(',', $supervisorArray);
                    }
                    return $supervisors;
                },
            ],
            [
                'class' => DataColumn::class,
                'headerOptions' => ['width' => '160'],
                'format' => 'text',
                'header' => Yii::t('app', 'Date'),
                'value' => function ($model) {
                    $date = new DateTime($model->created_at);
                    return $date->format(DateTime::DATETIME);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
