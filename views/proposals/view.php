<?php

use app\assets\MarkdownPreviewAsset;
use app\consts\Permission;
use app\consts\Sql;
use app\models\records\DefenceAttendance;
use app\models\records\Proposal;
use app\models\records\ProposalComment;
use app\models\records\ProposalVote;
use app\models\records\User;
use app\utils\DateTime;
use kartik\editable\Editable;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Proposal */

MarkdownPreviewAsset::register($this);

$this->title = Html::encode($model->project->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proposals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['projects/view', 'id' => $model->project_id]];


$userId = Yii::$app->getUser()->getId();
?>
<div class="proposal-view">

    <h1>
    	<?= Html::a($this->title, ['projects/view', 'id' => $model->project_id]) ?>

    	<?php
        	if ($model->state == Proposal::STATE_ACCEPTED):
    	?>

        	<span class="label label-success"><?= Yii::t('app', Proposal::getStateToString($model->state)) ?></span>

        <?php
    		elseif ($model->state == Proposal::STATE_DECLINED):
    	?>

    		<span class="label label-danger"><?= Yii::t('app', Proposal::getStateToString($model->state)) ?></span>

    	<?php
    		endif;
    	?>

    </h1>

    <div class="proposal-content well">

    	<p>
            <?= ($model->document->fallback_path) ?
                Html::a(Yii::t('app', 'PDF'), $model->document->fallback_path, ['class' => 'btn btn-info']) : ('')
            ?>
        </p>

        <?php if ($model->document->contentMarkdownProcessed != "") : ?>
            <div id="document-content" class="panel panel-default">

                <div class="panel-body"><?= $model->document->contentMarkdownProcessed ?></div>

            </div>
        <?php
    		endif;
    	?>

    </div>

    <?php
    if (Yii::$app->user->can(Permission::PROPOSAL_VOTE, ['proposal' => $model])) : ?>
    <div class="proposal-votes">

    	<div class="row">

			<div class="col-md-4">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h3 class="panel-title"><?= Yii::t('app', 'My vote') ?></h3>
                    </div>

                    <div class="panel-body">

                    <?php
					    $userVoteModel = $model->getProposalVoteByUserId($userId);
					    $userVoteValue = isset($userVoteModel->vote)? Yii::t('app', $userVoteModel->vote) : null;

					    echo Editable::widget([
                            'model' => $model,
                            'name' => 'proposalUserVote',
                            'value' => $userVoteValue,
                            'asPopover' => true,
                            'header' => Yii::t('app', 'Vote'),
                            'format' => Editable::FORMAT_BUTTON,
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => [
                                ProposalVote::VOTE_YES => Yii::t('app', 'Yes'),
                                ProposalVote::VOTE_NO => Yii::t('app', 'No')
                            ],
                            'options' => ['class'=>'form-control'],
                            'buttonsTemplate' => '{submit}',
                            'editableValueOptions' =>['class' => 'text-danger'],
                            'displayValueConfig'=> [
                                ProposalVote::VOTE_YES => '<i class="glyphicon glyphicon-thumbs-up"></i> ' . Yii::t('app', 'Yes'),
                                ProposalVote::VOTE_NO => '<i class="glyphicon glyphicon-thumbs-down"></i> ' . Yii::t('app', 'No'),
                            ],
                        ]);
				    ?>

                    </div>

                </div>

            </div>

            <div class="col-md-8">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h3 class="panel-title"><?= Yii::t('app', 'Votes') ?></h3>
                    </div>

                    <div class="panel-body">

                    <?php
					    if ($model->state == Proposal::STATE_SENT) {
					    	$committeeUsers = User::find()->where([
					    	        Sql::COL_DELETED => false,
                                    'role' => [ User::ROLE_COMMITTEE_MEMBER, User::ROLE_COMMITTEE_CHAIR]])
					    		->andWhere(['!=', 'id', $userId ])
					    		->orderBy('name')->all();

							$proposalVotes = [];
					    	foreach ($committeeUsers as $committee) {
                            	$voteModel = $model->getProposalVoteByUserId($committee->id);
                            	if (empty($voteModel)) {
                                    $voteModel = new ProposalVote();
                                    $voteModel->user_id = $committee->id;
                            	}

                                $proposalVotes[] = $voteModel;
					    	}
					    } else {
                        	$proposalVotes = $model->proposalVotes;
					    }

					    foreach ($proposalVotes as $voteModel) {
                            $voteClass = 'default';
                            $voteValue = 'No vote';

                            if (isset($voteModel->vote)) {
								if ($voteModel->vote == ProposalVote::VOTE_YES) {
                                    $voteClass = 'success';
                               		$voteValue = 'Yes';
								}
								else {
                                    $voteClass = 'danger';
                               		$voteValue = 'No';
								}
                            }

                            echo '<span class="label label-'. $voteClass .'">' . $voteModel->user->getNameWithEmail() . ' (' . Yii::t('app', $voteValue) .')</span> ';
					    }

				    ?>

                    </div>

                </div>

            </div>

    	</div>

    </div>
    <?php endif; ?>

<div class="row">

    <div class="col-md-6">

        <div class="proposal-comments">

            <div class="panel panel-default">

                <div class="panel-heading">
                    <span class="input-group-btn">
                        <span class="panel-title"><?= Yii::t('app', 'Comments') ?></span>

                        <?php if (Yii::$app->user->can(Permission::PROPOSAL_VOTE, ['proposal' => $model])): ?>

                            <span title="<?= Yii::t('app', 'Add comment') ?>"><?php
                            Modal::begin([
                                'header' => Yii::t('app', 'Add comment'),
                                'toggleButton' => [
                                        'label' => '',
                                        'class' => 'glyphicon glyphicon-comment btn btn-sm btn-default pull-right'],
                            ]); ?></span>
                            <div id="proposal-comments-form">
                                <?php
                                    $ProposalComments = new ProposalComment();
                                    $form = ActiveForm::begin(['action' => ['add-comment', 'id' => $model->id]]);
                                ?>

                                <?= $form->field($ProposalComments, 'content')->textarea(['rows' => '6'])->label(false) ?>

                                <div class="form-group">
                                    <?= Html::submitButton(Yii::t('app', 'Add comment'), ['class' => 'btn btn-success']) ?>
                                </div>

                                <?php ActiveForm::end(); ?>
                            </div>
                            <?php
                            Modal::end();
                            ?>
                        <?php endif; ?>
                    </span>
                </div>

                <div class="panel-body" style="padding: 5px">

                    <table class="table table-bordered" style="margin-bottom: 0px">
                    <?php
                        foreach ($model->getProposalViewComments(Yii::$app->user->can(Permission::PROPOSAL_COMMENTS_VIEW) ||
                            Yii::$app->user->can(Permission::PROPOSALS_MANAGE)) as $comment): ?>
                        <tr>
                            <td class=" <?= $comment->committee_only? 'bg-danger' : 'bg-success' ?>">
                            <div>

                                <?= Html::encode($comment->creator->getNameWithEmail()) ?>

                                <span class="pull-right">

                                    <?= DateTime::from($comment->created_at)->format(DateTime::DATETIME) ?>

                                    <?php if (Yii::$app->user->can(Permission::PROPOSAL_VOTE, ['proposal' => $model])
                                        || Yii::$app->user->can(Permission::PROPOSALS_MANAGE)): ?>

                                        <?= ($comment->committee_only) ?
                                            Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ["proposals/publish-comment?id=".$comment->id], [
                                                'title' => Yii::t('app', 'Publish'),
                                                'data' => [
                                                    'confirm' => Yii::t('app', 'Are you sure to publish this comment?'),
                                                    'method' => 'post'
                                            ]]) :
                                            Html::a('<span class="glyphicon glyphicon-eye-close"></span>', ["proposals/publish-comment?lock=1&id=".$comment->id], [
                                                'title' => Yii::t('app', 'Unpublish'),
                                                'data' => [
                                                    'confirm' => Yii::t('app', 'Are you sure to unpublish this comment?'),
                                                    'method' => 'post'
                                            ]])
                                        ?>

                                    <?php endif; ?>
                                </span>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <div class="panel-body">

                            <?php
                                if (Yii::$app->user->can(Permission::PROPOSAL_UPDATE_OWN_COMMENT,
                                    ['comment' => $comment]))
                                {
                                    echo Editable::widget([
                                        'name'=> 'comment',
                                        'asPopover' => true,
                                        'format' => Editable::FORMAT_BUTTON,
                                        'inputType' => Editable::INPUT_TEXTAREA,
                                        'value' => Html::encode($comment->content),
                                        'submitOnEnter' => false,
                                        'buttonsTemplate' => '{submit}',
                                        'size' => 'lg',
                                        'options' => [
                                            'class' => 'form-control',
                                            'rows' => 6,
                                            'style' => 'width: 100%',
                                        ],
                                        'formOptions' => [
                                            'action' => ['update-comment', 'id' => $comment->id],
                                        ],
                                    ]);
                                }
                                else {
                                    echo Html::encode($comment->content);
                                }
                            ?>

                            <?php
                                if (Yii::$app->user->can(Permission::PROPOSAL_UPDATE_OWN_COMMENT, ['comment' => $comment])) {
                                    echo Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-comment', 'id' => $comment->id], [
                                            'title' => Yii::t('app', 'Delete'),
                                            'data' => [
                                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                            'method' => 'post',
                                        ],
                                    ]);
                                }
                            ?>

                            </div>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </table>

                </div>

            </div>

        </div>
    </div>


    <div class="col-md-6">
        <div class="proposal-decision">

            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('app', 'Final decision') ?></h3>
                </div>

                <div class="panel-body">

                <?php if ($model->state == Proposal::STATE_SENT && Yii::$app->user->can(Permission::PROPOSALS_MANAGE)): ?>

                <?php $form = ActiveForm::begin(['action' => ['update-state', 'id' => $model->id]]); ?>

                    <?php
                    Modal::begin([
                        'header' => "<img src=\"" . Yii::$app->homeUrl . "img/md.png\" style=\"width: 20px;\" title=\"markdown\"/>",
                        'id' => 'modal'
                    ]);
                    Modal::end()
                    ?>

                    <label><?= Yii::t('app', 'Note') ?></label>

                    <?= Html::button(Yii::t('app', 'Markdown preview'),
                        ['name' => 'preview', 'value' => 1, 'class' => 'btn btn-info solo-preview-button pull-right', 'style' => 'margin-bottom: 1em']) ?>

                    <?= Html::textarea('proposal-comment', '', ['rows' => '4', 'class' => 'solo-markdown-content', 'style' => ['width' => '100%']]) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Accept'), ['name' => 'accept', 'value' => 1, 'class' => 'btn btn-success']) ?>
                        <?= Html::submitButton(Yii::t('app', 'Decline'), ['name' => 'decline', 'value' => 1, 'class' => 'btn btn-danger pull-right']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

                <?php elseif ($model->state != Proposal::STATE_SENT): ?>

                    <?= $model->commentMarkdownProcessed ?>

                <?php endif; ?>

                </div>

            </div>

        </div>
    </div>

</div>

</div>
