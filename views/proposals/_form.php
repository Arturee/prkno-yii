<?php

use app\models\forms\UploadForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Proposal */
/* @var $form yii\widgets\ActiveForm */

$markdownIcon = '<img src="' . Yii::$app->homeUrl . 'img/md.png" style="width: 20px;" title="markdown"/>';
?>

<div class="proposals-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $uploadModel = new UploadForm(); ?>

    <?= $form->field($model, 'documentMarkdown')->textarea(['rows' => '4'])->label($markdownIcon) ?>

    <?= ($model->document->fallback_path) ?
        Yii::t('app', 'Uploaded file: ') . Html::a(Yii::t('app', 'PDF'), $model->document->fallback_path) : ('')
    ?>

    <?= $form->field($uploadModel, 'files')->fileInput(['multiple' => false, 'accept' => 'application/pdf'])->label(Yii::t('app', 'Upload file')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
