<?php

use app\utils\DateTime;
use app\models\records\Advertisement;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\search\AdvertisementSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="advertisements-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get'
    ]);
    $model->type = "";
    ?>

    <div class="row table" style="display: flex; align-items: flex-end;">
        <div class="form-group col-lg-4 col-md-4 col-xs-6">
            <?= $form->field($model, 'updated_at')
                ->widget(DatePicker::class, DateTime::getDateOptions())
                ->textInput()->label(Yii::t('app', 'From the date'))
            ?>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-xs-6" style="width: 220px;">
            <?= $form->field($model, 'type')->dropDownList([
                "" => Yii::t('app', 'All'),
                Advertisement::TYPE_PERSON => Yii::t('app', 'person'),
                Advertisement::TYPE_PROJECT => Yii::t('app', 'project'),
            ], ['class' => 'form-control'])->label(
                Yii::t('app', 'Type') . ' <span class="glyphicon glyphicon-question-sign" style="color: #aaa"
            title="' . Yii::t('app', 'Advertisements type hint') . '"></span>'
            ) ?>
        </div>
        <div class="form-group col-lg-2 col-md-2 col-xs-6">
            <?= $form->field($model, 'keywords')->textInput()->label(Yii::t('app', 'Keywords')) ?>
        </div>
        <div class="form-group col-lg-2 col-md-2 col-xs-6" style="height: 49px;">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
