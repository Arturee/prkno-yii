<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use sjaakp\taggable\TagEditor;

/* @var $this yii\web\View */
/* @var $model app\models\records\Advertisement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advertisements-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-6 col-md-6">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'contentMarkdown')->textarea(['rows' => '4']) ?>

        <?= $form->field($model, 'type')->dropDownList([
            'project' => Yii::t('app', 'project'),
            'person' => Yii::t('app', 'person'),
        ], ['prompt' => Yii::t('app', 'Not selected')])->label(
            Yii::t('app', 'Type') . ' <span class="glyphicon glyphicon-question-sign" 
            title="' . Yii::t('app', 'Advertisements type hint') . '"></span>'
        ) ?>

        <?= $form->field($model, 'keywords')->widget(TagEditor::className(), [
            'tagEditorOptions' => []
        ]) ?>

        <?= $form->field($model, 'subscribed_via_email')->checkbox() ?>

        <?= $form->field($model, 'published')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
