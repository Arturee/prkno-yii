<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\utils\DateTime;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\search\AdvertisementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Advertisements');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Advertisements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertisements-index">

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Back to Advertisements'), ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a(Yii::t('app', 'Create Advertisement'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'bordered' => false,
        'headerRowOptions' => ['class' => 'hidden'],
        'columns' => [
            [
                'attribute' => 'title',
                'label' => false,
                'format' => 'raw',
                'filter' => false,
                'value' => function ($model) {
                    return Yii::$app->controller->renderPartial('_model-view', ['model' => $model]);
                },
            ],
            [
                'attribute' => 'updated_at',
                'label' => false,
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::tag('span', DateTime::from($model->updated_at)->format(DateTime::DATE), ['class' => 'ads-date']) .
                        Html::tag('p', Html::encode($model->user->getNameWithEmail()));
                },
                'contentOptions' => function ($model) {
                    return ['width' => '220'];
                },
            ],
            [
                'attribute' => 'type',
                'label' => false,
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->published) $value = " <p><b>" . Yii::t('app', 'Published Advertisement') . "</b></p>";
                    else $value = " <p><b>" . Yii::t('app', 'Draft Advertisement') . "</b></p>";
                    return $value . Yii::t('app', $model->type);
                },
                'contentOptions' => function ($model) {
                    return ['width' => '70'];
                },
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update} {delete}',
                'dropdownOptions' => ['class' => 'pull-right'],
                'vAlign' => GridView::ALIGN_TOP,
                'buttons' => [
                    'delete' => function ($url, $model) {
                        $url .= '&view=my';
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
