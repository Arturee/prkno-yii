<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Advertisement */

$this->title = Yii::t('app', 'Create Advertisement');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Advertisement'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My Advertisement'), 'url' => ['my']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertisements-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'button' => 'Create'
    ]) ?>

</div>
