<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Advertisement */

$this->title = Yii::t('app', 'Update Advertisements: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Advertisements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My Advertisements'), 'url' => ['my']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="advertisements-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'button' => 'Save'
    ]) ?>

</div>
