<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\utils\DateTime;
use app\widgets\UserLinks;
use app\consts\Permission;
use app\models\records\AdvertisementComment;
use app\models\records\User;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Advertisement */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Advertisements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider(['query' => $model->getAdComments()]);
$commentsName = 'comments-' . $model->id;
$subscribedViaEmail = User::mapBool($model->subscribed_via_email);
$keywords = "";
if ($model->keywords != "") {
    foreach (explode(',', $model->keywords) as $value) {
        $keywords .= Html::a('#' . $value, Url::to(['advertisements/index', 'AdvertisementSearch[keywords]' => $value])) . ' ';
    }
}
?>
<div class="advertisements-view">

    <p class="pull-right">
        <label>
            <?= ($model->published) ? Yii::t('app', 'Published Advertisement') :
                Yii::t('app', 'Draft Advertisement') ?>
        </label>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-6 col-md-6">
            <p><label><?= Yii::t('app', 'Created At') ?>:</label>
            <span class="create-date"><?= DateTime::from($model->created_at)->format(DateTime::DATE) ?></span></p>
            <p>
                <label><?= Yii::t('app', 'Type') ?>:</label>
                <?= Yii::t('app', $model->type) ?>
            </p>

            <?= (Yii::$app->user->id == $model->user_id) ?
                '<p><label>' . Yii::t('app', 'Subscribed Responses Via Email') . ':</label> ' .
                $subscribedViaEmail .
                '</p>' : ('')
            ?>
        </div>

        <div class="col-lg-6 col-md-6">
            <p><label><?= Yii::t('app', 'Updated At') ?>:</label>
                <span class="update-date"><?= DateTime::from($model->updated_at)->format(DateTime::DATE) ?></span></p>
            <?php if (!Yii::$app->user->isGuest) : ?>
            <p>
                <label><?= Yii::t('app', 'Author') ?>:</label>
                <?= UserLinks::widget([UserLinks::USERS => $model->user]) ?>
            </p>
            <?php endif; ?>
        </div>
    </div>

    <p><?= $model->getContentMarkdownProcessed() ?></p>

    <p><label><?= Yii::t('app', 'Keywords') ?>:</label> <?= $keywords ?></p>

    <p><label><?= Yii::t('app', 'Comments'); ?>:</label></p>
    <?= $this->render('../ad-comments/index', ['dataProvider' => $dataProvider]) ?>

    <?= ($model->published && Yii::$app->user->id) ?
        $this->render('../ad-comments/create', ['modelComment' => new AdvertisementComment(), 'adId' => $model->id])
        : ("") ?>
    <div class="manage-buttons">
    <?php
    $canManageAds = Yii::$app->user->can(Permission::ADS_MANAGE) || (!Yii::$app->user->isGuest && $model->user_id == Yii::$app->user->getId());

    if ($canManageAds) : ?>
        <?= Html::a(
            Yii::t('app', 'Update'),
            ['update', 'id' => $model->id],
            ['class' => 'btn btn-primary']
        ); ?>
        <?= Html::a(
            Yii::t('app', 'Delete'),
            ['delete', 'id' => $model->id, "view" => "index"],
            [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ]
            ]
        );
        ?>
    <?php endif; ?>
    </div>
</div>
