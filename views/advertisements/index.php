<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\records\Advertisement;
use app\utils\DateTime;
use app\consts\Permission;
use app\widgets\UserLinks;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\search\AdvertisementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Advertisements');
$this->params['breadcrumbs'][] = $this->title;

$canAdsCreate = Yii::$app->user->can(Permission::ADS_CREATE);
?>
<div class="advertisements-index">

    <p class="pull-right">
        <?= $canAdsCreate ?
            Html::a(Yii::t('app', 'My Advertisements'), ['my'], ['class' => 'btn btn-info'])
            : ("") ?>
        <?= $canAdsCreate ?
            Html::a(Yii::t('app', 'Create Advertisement'), ['create'], ['class' => 'btn btn-success'])
            : ("") ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'bordered' => false,
        'striped' => false,
        'headerRowOptions' => ['class' => 'hidden'],
        'columns' => [
            [
                'attribute' => 'title',
                'label' => false,
                'format' => 'raw',
                'filter' => false,
                'value' => function ($model) {
                    return Yii::$app->controller->renderPartial('_model-view', ['model' => $model]);
                },
            ],
            [
                'attribute' => 'updated_at',
                'label' => false,
                'format' => 'raw',
                'value' => function (Advertisement $model) {
                    return
                        Html::tag('span', DateTime::from($model->updated_at)->format(DateTime::DATE), ['class' => 'ads-date']) . (!Yii::$app->user->isGuest ? Html::tag('p', UserLinks::widget([UserLinks::USERS => $model->user])) : "");
                },
                'contentOptions' => function ($model) {
                    return ['width' => '220'];
                },
            ],
            [
                'attribute' => 'type',
                'label' => false,
                'filter' => Html::checkboxList('type', 'value', [
                    Advertisement::TYPE_PERSON => Yii::t('app', 'person'),
                    Advertisement::TYPE_PROJECT => Yii::t('app', 'project'),
                ]),
                'value' => function ($model) {
                    return Yii::t('app', $model->type);
                },
                'contentOptions' => function ($model) {
                    return ['width' => '70'];
                },
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update} {delete}',
                'dropdownOptions' => ['class' => 'pull-right'],
                'vAlign' => GridView::ALIGN_TOP,
                'visibleButtons' => [
                    'update' => function ($model) {
                        return Yii::$app->user->can(Permission::ADS_MANAGE) || (!Yii::$app->user->isGuest && $model->user_id == Yii::$app->user->getId());
                    },
                    'delete' => function ($model) {
                        return Yii::$app->user->can(Permission::ADS_MANAGE) || (!Yii::$app->user->isGuest && $model->user_id == Yii::$app->user->getId());
                    }
                ],
                'buttons' => [
                    'delete' => function ($url, $model) {
                        $url .= '&view=index';
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
