<?php

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\records\AdvertisementComment;

/* @var $this yii\web\View */
/* @var $model \app\models\records\Advertisement */

$dataProvider = new ActiveDataProvider(['query' => $model->getAdComments()]);
$commentsName = 'comments-' . $model->id;
$keywords = "";
if ($model->keywords != "") {
    foreach ($model->keywordsList as $value) {
        $keywords .= Html::a('#' . $value, Url::to(['advertisements/index', 'AdvertisementSearch[keywords]' => $value])) . ' ';
    }
}
?>

<h3><?= Html::a(Html::encode($model->title), ['view', 'id' => $model->id]) ?></h3>

<p><?= $model->getContentMarkdownProcessed() ?></p>

<p><?= Yii::t('app', 'Keyword') ?>: <?= $keywords ?></p>

<h4><button class="btn btn-default" data-toggle="collapse" data-target="#<?= $commentsName ?>"
        <?= (!$dataProvider->count && !Yii::$app->user->id) ? 'disabled' : ('') ?> >

        <?= Yii::t('app', 'Comments'); ?>
        <?= $dataProvider->count ? ' ' . $dataProvider->count : ('') ?>
    </button>
</h4>

<div id="<?= $commentsName ?>" class="collapse">
    <?= $this->render('../ad-comments/index', ['dataProvider' => $dataProvider]) ?>

    <?= ($model->published && Yii::$app->user->id) ?
        $this->render('../ad-comments/create', ['modelComment' => new AdvertisementComment(), 'adId' => $model->id])
        : ("") ?>
</div>
