<?php

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Documentation');
\app\assets\DocIndexAsset::register($this);

?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="header">
                <h1>Error codes</h1>
                <small>and how to deal with them</small>
            </div>

            <ul class="list">
                <li>
                    <b>1</b> -
                    <span class="error">"You cannot log in via CAS, you must use your email and password. (code 1)"</span>
                    <br>
                    The user is in the DB but his login option is "manual" (not "cas"), that's why he cannot log
                    in via CAS.
                    <br>
                    Possible explanations:
                    <ul>
                        <li>
                            a manually created user gained CAS access and logged in via CAS. He should not do this,
                            he should keep using his email+password method of login
                        </li>
                        <li>a manually created user is blocking somebody else's email</li>
                        <li>a deleted user is blocking the email</li>
                    </ul>
                </li>
                <li>
                    <b>2</b> -
                    <span class="error">"You cannot log in via CAS, because your email in CAS is invalid. (code 2)"</span>
                    <br>
                    The user cannot register (via CAS) because his email address on CAS is corrupt (doesn't match
                    the classic regexp pattern for emails). The user should have a look at his CAS account and make sure
                    the email address there is OK.
                </li>
                <li>
                    <b>3</b> -
                    <span class="error">"You do not have sufficient privileges to log in. (code 3)"</span>
                    <br>
                    A guest who is registered in our DB, but his role is <b>null</b> is trying to log in and fails,
                    which is the expected behavior. However, if the user in reality is not a consultant, then he should
                    probably have a non-null role. This can be corrected by clicking his profile and choosing the appropriate role.
                </li>
                <li>
                    <b>4</b> -
                    <span class="error">"An error occurred with CAS, please retry after a while. (code 4)"</span>
                    <br>
                    A user tries to authenticate via CAS or the app tries to download his data from CAS, but an exception
                    is thrown by the phpCAS library. <i>(Probably a network error, or CAS is offline.)</i>
                </li>
            </ul>
        </div>
    </div>
</div>
