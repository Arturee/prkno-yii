<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $modelComment \app\models\records\AdvertisementComment */
/* @var $adId integer */
?>
<div class="ad-comments-create">

    <?= $this->render('_form', [
        'modelComment' => $modelComment,
        'adId' => $adId,
    ]) ?>

</div>
