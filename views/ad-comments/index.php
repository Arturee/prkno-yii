<?php

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use app\utils\DateTime;
use app\consts\Permission;
use \app\widgets\UserLinks;

/* @var $this yii\web\View */
/* @var $dataProviderComment yii\data\ActiveDataProvider */
/* @var $adId integer */
?>
<div class="ad-comments-index">

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'headerRowOptions' => ['class' => 'hidden'],
        'showOnEmpty' => false,
        'emptyText' => '<table><tbody></tbody></table>',
        'columns' => [
            [
                'attribute' => 'content',
                'format' => 'raw',
                'label' => false,
                'value' => function ($model) {
                    return
                        "<p><small>"
                        . UserLinks::widget([UserLinks::USERS => $model->user])
                        . "</small>"
                        . "<span class=\"pull-right\"><small class=\"text-muted\">"
                        . DateTime::from($model->updated_at)->format(DateTime::DATETIME)
                        . "</small></span></p>"
                        . " <p>" . Html::encode($model->content) . "</p>";
                },
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{delete}',
                'dropdownOptions' => ['class' => 'pull-right'],
                'vAlign' => GridView::ALIGN_TOP,
                'visible' => Yii::$app->user->id, // user is log in
                'buttons' => [
                    'delete' => function ($url, $model) {
                        // check permissions
                        if (Yii::$app->user->can(Permission::ADS_MANAGE_COMMENTS) || Yii::$app->user->id == $model->user->id) {
                            $url = Url::to(['ad-comments/delete', 'id' => $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'Delete'),
                                'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'data-method' => 'post',
                            ]);
                        }
                    }
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
