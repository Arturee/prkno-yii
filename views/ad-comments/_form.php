<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $modelComment \app\models\records\AdvertisementComment */
/* @var $form yii\widgets\ActiveForm */
/* @var $adId integer */
?>

<div class="ad-comments-form">

    <?php $form = ActiveForm::begin(['action' => ['/ad-comments/create']]); ?>

    <?= $form->field($modelComment, 'ad_id')->textInput(['value' => $adId, 'class' => 'hidden'])->label(false) ?>
    <div class="form-group row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <?= $form->field($modelComment, 'content')->textarea(['rows' => '1', 'placeholder' => Yii::t("app", "Write your comment")])->label(false) ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
