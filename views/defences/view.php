<?php

use yii\helpers\Html;
use app\consts\Permission;
use app\models\records\ProjectDefence;
use app\utils\DateTime;

/* @var $this yii\web\View */
/* @var $model \app\models\records\ProjectDefence */
?>

<style>
    @media print {
        body * {
            font-family: Times;
            visibility: hidden;
        }
        .print {
            position: absolute;
            top: 50px;
            left: 0;
        }
        .print * {
            visibility: visible;
        }
        a[href]:after {
            content: none !important;
        }
    }
    /* also text must be inside <p> to be visible */

    .signature {
        font-style: italic;
        margin-bottom: 3em;
    }
</style>
<div style="text-align: right">
<?php

$user = Yii::$app->user;
$canManageDefences = $user->can(Permission::DEFENCES_MANAGE);
$canUploadOpponentReview = $user->can(Permission::DEFENCES_UPLOAD_OPPONENT_REVIEW, [Permission::PARAM_DEFENCE => $model]);
$canUploadSupervisorReview = $user->can(Permission::DEFENCES_UPLOAD_SUPERVISOR_REVIEW, [Permission::PARAM_DEFENCE => $model]);
$supervisor = $model->project->getSupervisors()->one();
$opponent = $model->getOpponent();

if ($canManageDefences || $canUploadOpponentReview || $canUploadSupervisorReview) {
    echo Html::a(
        Yii::t('app', 'Update'),
        ['update', 'id' => $model->id],
        ['class' => 'btn btn-primary']
    );
}

?>
</div>
<div class="print">
    <?= Yii::$app->controller->renderPartial('_info', ['model' => $model, 'isPrint' => true]); ?>
    <?php if (!Yii::$app->user->isGuest) : ?>
        <?php if ($model->getSupervisorReview()) : ?>
        <hr>
        <p>
            <b>
                <?= Yii::t('app', 'Supervisor review') . ":" ?>
            </b>
        </p>
        <?= Yii::$app->controller->renderPartial('_document_view', ['document' => $model->getSupervisorReview()]); ?>
        <p class="signature">
            <span class="pull-right">
                <?= $supervisor ? Html::encode($supervisor->getNameWithEmail()) : '' ?>,
                <small><?= DateTime::from($model->getSupervisorReview()->updated_at)->format(DateTime::DATETIME_MINS) ?></small>
            </span>
        </p>
        <?php endif; ?>

        <?php if ($model->getOpponentReview()) : ?>
        <hr>
        <p>
            <b>
                <?= Yii::t('app', 'Opponent statement') . ":" ?>
            </b>
        </p>
        <?= Yii::$app->controller->renderPartial('_document_view', ['document' => $model->getOpponentReview()]); ?>
        <p class="signature">
            <span class="pull-right">
                <?= $opponent ? Html::encode($opponent->getNameWithEmail()) : '' ?>,
                <small><?= DateTime::from($model->getOpponentReview()->updated_at)->format(DateTime::DATETIME_MINS) ?></small>
            </span>
        </p>
        <?php endif; ?>

        <?php if ($model->defence_statement) : ?>
        <hr>
        <p>
            <b>
                <?= Yii::t('app', 'Defence statement') . ":" ?>
            </b>
        </p>
        <?= Yii::$app->controller->renderPartial('_document_view', ['document' => $model->getDefenceStatement()]); ?>
        <p class="signature">
            <span class="pull-right">
                <?= Html::encode($model->getStatementAuthor()->getNameWithEmail()) ?>,
                <small><?= DateTime::from($model->getDefenceStatement()->updated_at)->format(DateTime::DATETIME_MINS) ?></small>
            </span>
        </p>
        <?php endif; ?>
    <?php endif; ?>

    <hr>
    <p>
        <b><?= Yii::t('app', 'Decision') ?>:</b> <?= $model->result ? ProjectDefence::mapResult($model->result) : '' ?>
    </p>
</div>

<?php if (!Yii::$app->user->isGuest) : ?>
<?= Html::a('<span class="glyphicon glyphicon-print"></span>', 'javascript:void(0)', [
    'title' => Yii::t('app', 'Print document'),
    'onclick' => "window.print()"
]);
?>
<?php endif; ?>