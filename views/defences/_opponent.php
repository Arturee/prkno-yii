<?php

use app\models\forms\ChooseOpponentForm;
use app\models\records\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\ProjectDefence */

$formModel = new ChooseOpponentForm();
$formModel->opponent_id = $model->opponent_id;

$committeMembers = User::findCommitteeMembers();
$options = [];
/** @var \app\models\records\User $member */
foreach ($committeMembers as $member) {
    $options[$member->id] = $member->getNameWithEmail();
}

?>

<div id="opponent-form" style="display: none">
    <?php $form = ActiveForm::begin([
        'action' => ['defences/choose-opponent', 'id' => $model->id],
        'method' => 'post'
    ]); ?>

        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($formModel, 'opponent_id')->dropDownList($options)->label(false); ?>
            </div>
            <div class="col-xs-6">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
