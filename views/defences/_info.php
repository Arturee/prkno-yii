<?php

use app\consts\Permission;
use app\models\records\Project;
use app\models\records\User;
use app\utils\DateTime;
use app\widgets\UserLinks;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\records\ProjectDefence */
/* @var $isPrint bool */

$project = $model->project;

$time = $model->time ? DateTime::time($model->time) : '--';
$dateTime = $time . '  ' . DateTime::date($model->defenceDate->date);

$canManageDefences = Yii::$app->user->can(Permission::DEFENCES_MANAGE);

$projectName = Html::encode($project->name . ' (' . $project->short_name . ')');
$projectLink = Html::a($projectName, ['projects/view', 'id' => $project->id]);

/** @var User $supervisor */
$supervisor = $project->getSupervisors()->one();
$supervisorHtml = UserLinks::widget([
    UserLinks::USERS => $supervisor
]);
$consultantsHtml = UserLinks::widget([
    UserLinks::USERS => $project->getConsultants()->all()
]);
$students = $project->getMembers()->all();
$studentsHtml = UserLinks::widget([
    UserLinks::USERS => $students
]);
$opponent = $model->getOpponent();
$opponentHtml = UserLinks::widget([
    UserLinks::USERS => $opponent
]);
$committeeHtml = UserLinks::widget([
    UserLinks::USERS => $model->defenceDate->getCommittee()
]);

$state = $model->getProjectState();
$state = Project::getOptionsStates()[$state];
$showOpponentPicker = $canManageDefences && !$model->opponent_review;
if ($isPrint) {
    $showOpponentPicker = false;
}

$tinyQuestionMark = '<sup style="color: #b3b3b3">?</sup>';
if ($isPrint) {
    $tinyQuestionMark = '';
}

?>

<p><b><?= Yii::t('app', 'Project') ?>:</b> <?= $projectLink ?></p>
<p><b><?= Yii::t('app', 'Date') ?>:</b> <?= $dateTime ?></p>
<p><b><?= Yii::t('app', 'Room') ?>:</b> <?= Html::encode($model->defenceDate->room) ?></p>
<p title="<?= Yii::t('app', 'Defence number. Starts at 1 and includes all defences event unsuccessful ones.') ?>">
    <b><?= Yii::t('app', 'Defence number') ?>:</b><?= $tinyQuestionMark ?> <?= $model->number ?>
</p>
<p><b><?= Yii::t('app', 'State') ?>:</b> <?= Html::encode($state) ?></p>
<p><b><?= Yii::t('app', 'Supervisor') ?>:</b> <?= $supervisorHtml ?></p>
<p><b><?= Yii::t('app', 'Consultants') ?>:</b> <?= $consultantsHtml ?></p>
<p><b><?= Yii::t('app', 'Team') ?>:</b> <?= $studentsHtml ?></p>
<p><b><?= Yii::t('app', 'Opponent') ?>:</b> <?= $opponentHtml ?>
    <?php if ($showOpponentPicker) : ?>
        <a class="edit-opponent" href="javascript:void(0)">
            <span class="glyphicon glyphicon-edit"></span>
        </a>
    <?php endif; ?>
</p>
<?php if ($showOpponentPicker) {
    echo Yii::$app->controller->renderPartial('_opponent', ['model' => $model]);
} ?>
<p><b><?= Yii::t('app', 'Committee') ?>:</b> <?= $committeeHtml ?></p>