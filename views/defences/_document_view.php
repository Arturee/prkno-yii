<?php

use app\models\records\Document;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $document Document */

?>

<?php if($document->content_md) : ?>
    <div class="rendered-markdown">
        <?= $document->getContentMarkdownProcessed() ?>
    </div>
<?php else : ?>
    <div>
        <?= Html::a(Yii::t('app', "Download"), [$document->fallback_path]); ?>
    </div>
<?php endif; ?>


