<?php

use app\assets\DefencesViewAsset;
use app\consts\Permission;
use app\exceptions\BugError;
use app\models\forms\DefenceAcceptForm;
use app\models\forms\MarkdownForm;
use app\models\records\Document;
use app\models\records\Project;
use app\models\records\ProjectDefence;
use app\widgets\Markdown;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\utils\storage\MarkdownTemplateStorage;

/* @var $this yii\web\View */
/* @var $model \app\models\records\ProjectDefence */

function renderDefenseDocument(? Document $doc, string $title) : void
{
    if ($doc) {
        echo Html::a($title, [$doc->fallback_path]);
    } else {
        echo "<span style='color:darkgrey; font-style:italic;'>" . Yii::t('app', 'missing') . "</span>";
    }
}

function getUrlByType(string $type) : string
{
    switch ($type) {
        case MarkdownForm::TYPE_OPPONENT_REVIEW:
            return 'upload-opponent-review';
        case MarkdownForm::TYPE_SUPERVISOR_REVIEW:
            return 'upload-supervisor-review';
        case MarkdownForm::TYPE_FINAL_STATEMENT:
            return 'upload-final-statement';
        default:
            throw new BugError('bad type');
    }
}

DefencesViewAsset::register($this);

$project = $model->project;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Defences'), 'url' => ['/defence-dates']];
$this->title = Yii::t('app', 'Defence of ') . Html::encode($project->short_name);
$this->params['breadcrumbs'][] = Html::encode($this->title);

$projectName = Html::encode($project->name . ' (' . $project->short_name . ')');
$projectLink = Html::a($projectName, ['projects/view', 'id' => $project->id]);

$user = Yii::$app->user;
$canManageDefences = $user->can(Permission::DEFENCES_MANAGE);
$canViewDefences = $user->can(Permission::DEFENCES_VIEW);
$canUploadOpponentReview = $user->can(Permission::DEFENCES_UPLOAD_OPPONENT_REVIEW, [Permission::PARAM_DEFENCE => $model]);

$supervisor = $project->getSupervisors()->one();
$opponent = $model->getOpponent();

$docs = $project->getDocumentsForDefence($model->number);
$supervisorReview = $model->getSupervisorReview();
$opponentReview = $model->getOpponentReview();

$isSmallDefence = $project->state === Project::STATE_ANALYSIS;
?>
<div class="defences-view">

    <?php
    Modal::begin([
        'header' => "<img src=\"" . Yii::$app->homeUrl . "img/md.png\" style=\"width: 20px;\" title=\"markdown\"/>",
        'id' => 'modal'
    ]);
    Modal::end()
    ?>

    <div class="row">

        <div class="col-sm-6"><!-- left side -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= $projectLink ?></h3>
                </div>
                <div class="panel-body">
                    <div>
                        <?= Yii::$app->controller->renderPartial('_info', ['model' => $model, 'isPrint' => false]); ?>
                    </div>
                        <p><b><?= Yii::t('app', 'Documents') ?>:</b>
                            <span class="attachment"><?php renderDefenseDocument($docs->pdf, 'text.pdf') ?></span>
                            <span class="attachment"><?php renderDefenseDocument($docs->zip, 'sources.zip') ?></span>
                        </p>
                </div>
            </div>

            <?php
                // new statement
            if (!$model->defence_statement) {
                $type = MarkdownForm::TYPE_FINAL_STATEMENT;
                $url = getUrlByType($type);
                $form = ActiveForm::begin([
                    'action' => [$url, 'id' => $model->id],
                    'options' => ['enctype' => 'multipart/form-data']
                ]);
                echo Markdown::widget([
                    Markdown::ACTIVE_FORM => $form,
                    Markdown::AUTHOR => $user,
                    Markdown::CAN_EDIT => $canManageDefences,
                    Markdown::DOCUMENT => null,
                    Markdown::TITLE => 'Final statement',
                    Markdown::TYPE => $type,
                    Markdown::SUCCESS_BUTTON_FUNCTION => MarkdownForm::SUCCESS_BUTTON_SUBMIT
                ]);
                ActiveForm::end();
            }
            ?>

            <?php if ($model->defence_statement) : ?>
            <?php if ($model->result) :

                $resultClass = "alert-success";
            if ($model->result === ProjectDefence::RESULT_FAILED) {
                $resultClass = "alert-danger";
            }

            ?>

                <div class="alert <?= $resultClass ?> result">
                    <?= ProjectDefence::mapResult($model->result) ?>
                </div>

            <?php else : ?>
            <?php if ($model->defenceStatement && $model->supervisorReview && $model->opponentReview && $opponent) : ?>

                <?php
                $acceptForm = new DefenceAcceptForm();
                $form = ActiveForm::begin([
                    'id' => 'accept-form',
                    'action' => ['accept', 'id' => $model->id]
                ]); ?>

                <div>
                    <?= Html::activeHiddenInput($acceptForm, 'result') ?>
                    <?php if (!$isSmallDefence) : ?>
                        <?= $form->field($acceptForm, 'extraCredits')->textInput([
                            'type' => 'number',
                            'class' => 'small form-control'
                        ]) ?>
                    <?php endif; ?>
                </div>

                <div>
                    <button id="accept" type="button" class="btn btn-success">
                        Accept
                    </button>

                    <?php if (!$isSmallDefence) : ?>
                        <button id="accept-condition" type="button" class="btn btn-success">
                            Accept on condition
                        </button>
                    <?php endif; ?>

                    <button id="decline" type="button" class="btn btn-danger pull-right">
                        Decline
                    </button>
                </div>
                <?php ActiveForm::end(); ?>

            <?php endif; // accept ?>
            <?php endif; // result ?>
            <?php endif; // defence_statement ?>
        </div>


        <div class="col-sm-6"> <!-- opponent review, supervisor review -->
            <?php
            $template = MarkdownTemplateStorage::getFileContent("evaluation.md");

                //supervisor review
            $type = MarkdownForm::TYPE_SUPERVISOR_REVIEW;
            $url = getUrlByType($type);
            $document = $model->getSupervisorReview();
            $isSupervisor = $user->can(
                Permission::DEFENCES_UPLOAD_SUPERVISOR_REVIEW,
                [Permission::PARAM_DEFENCE => $model]
            );
            $form = ActiveForm::begin([
                'action' => [$url, 'id' => $model->id],
                'options' => ['enctype' => 'multipart/form-data']
            ]);
            echo Markdown::widget([
                Markdown::ACTIVE_FORM => $form,
                Markdown::AUTHOR => $supervisor,
                Markdown::CAN_EDIT => $canManageDefences || $isSupervisor,
                Markdown::DOCUMENT => $document,
                Markdown::TITLE => 'Supervisor\'s review',
                Markdown::TYPE => $type,
                Markdown::SUCCESS_BUTTON_FUNCTION => MarkdownForm::SUCCESS_BUTTON_SUBMIT,
                Markdown::TEMPLATE => $template
            ]);
            ActiveForm::end();

                // opponent review
            $type = MarkdownForm::TYPE_OPPONENT_REVIEW;
            $url = getUrlByType($type);
            $document = $model->getOpponentReview();
            $isOpponent = $user->can(
                Permission::DEFENCES_UPLOAD_OPPONENT_REVIEW,
                [Permission::PARAM_DEFENCE => $model]
            );
            $form = ActiveForm::begin([
                'action' => [$url, 'id' => $model->id],
                'options' => ['enctype' => 'multipart/form-data']
            ]);
            echo Markdown::widget([
                Markdown::ACTIVE_FORM => $form,
                Markdown::MESSAGE => $opponent == null ? Yii::t('app', 'Opponent is not selected') : null,
                Markdown::AUTHOR => $opponent,
                Markdown::CAN_EDIT => $canManageDefences || $canUploadOpponentReview,
                Markdown::DOCUMENT => $document,
                Markdown::TITLE => 'Opponent\'s review',
                Markdown::TYPE => $type,
                Markdown::SUCCESS_BUTTON_FUNCTION => MarkdownForm::SUCCESS_BUTTON_SUBMIT,
                Markdown::TEMPLATE => $template
            ]);
            ActiveForm::end();

                // statement
            if ($model->defence_statement) {
                $type = MarkdownForm::TYPE_FINAL_STATEMENT;
                $url = getUrlByType($type);
                $document = $model->getDefenceStatement();
                $form = ActiveForm::begin([
                    'action' => [$url, 'id' => $model->id],
                    'options' => ['enctype' => 'multipart/form-data']
                ]);
                echo Markdown::widget([
                    Markdown::ACTIVE_FORM => $form,
                    Markdown::AUTHOR => $model->getStatementAuthor(),
                    Markdown::CAN_EDIT => $canManageDefences,
                    Markdown::DOCUMENT => $document,
                    Markdown::TITLE => 'Final statement',
                    Markdown::TYPE => $type,
                    Markdown::SUCCESS_BUTTON_FUNCTION => MarkdownForm::SUCCESS_BUTTON_SUBMIT
                ]);
                ActiveForm::end();
            }
            ?>
        </div>
    </div>
</div>
