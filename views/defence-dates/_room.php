<?php

use app\models\forms\RoomForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\DefenceDate */

$formModel = new RoomForm();
$formModel->room = Html::encode($model->room);
?>

<span id="room-show">
    <?= $formModel->room ?>
    <a href="javascript:void(0)" class="room-edit room-edit-pad" title="<?= Yii::t('app', 'Edit'); ?>">
        <span class="glyphicon glyphicon-edit"></span>
    </a>
</span>

<div id="room-form" style="display: none">
    <?php $form = ActiveForm::begin([
        'action' => ['defence-dates/set-room', 'id' => $model->id],
        'method' => 'post'
    ]); ?>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($formModel, 'room')->textInput()->label(false); ?>
        </div>
        <div class="col-xs-6">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>

            <a href="javascript:void(0)" class="room-edit pull-right">
                <span class="glyphicon glyphicon-edit"></span>
            </a>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
