<?php

use app\consts\Permission;
use app\models\records\DefenceAttendance;
use app\models\records\DefenceDate;
use app\utils\DateTime;
use app\widgets\DefenceDateVoteCounts;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $canManageDefences bool */
/* @var $canVoteAboutDefenceDates bool */

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => DataColumn::class,
            'headerOptions' => ['width' => '100'],
            'format' => 'text',
            'header' => Yii::t('app', 'Date'),
            'value' => function (DefenceDate $model) {
                $date = new DateTime($model->date);
                return $date->format(DateTime::DATE);
            }
        ],
        [
            'class' => DataColumn::class,
            'headerOptions' => ['width' => '230'],
            'label' => Yii::t('app', 'Attending'),
            'format' => 'html',
            'visible' => $future && $canVoteAboutDefenceDates,
            'value' => function (DefenceDate $model) {
                return DefenceDateVoteCounts::widget([
                    DefenceDateVoteCounts::DATE => $model
                ]);
            }
        ],
        [
            'class' => DataColumn::class,
            'label' => Yii::t('app', 'My vote'),
            'format' => 'text',
            'visible' => $future && $canVoteAboutDefenceDates,
            'value' => function (DefenceDate $model) {
                $id = Yii::$app->getUser()->getId();
                $attendance = $model->getDefenceAttendanceByUserId($id);
                return DefenceAttendance::getVoteLabel($attendance->vote);
            }
        ],
        [
            'class' => DataColumn::class,
            'format' => 'text',
            'header' => Yii::t('app', 'Room'),
            'headerOptions' => ['width' => '120'],
            'visible' => !$voting,
            'value' => function (DefenceDate $model) {
                return $model->room ? $model->room : null;
            }
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{table}',
            'header' => Yii::t('app', 'Project defences'),
            'visible' => !$voting,
            'buttons' => [
                'table' => function ($url, DefenceDate $model) {
                    return Yii::$app->controller->renderPartial('_table', ['model' => $model]);
                }
            ]
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update} {delete}',
            'headerOptions' => ['width' => '50'],
            'visibleButtons' => [
                'update' => function ($model) use (&$canVoteAboutDefenceDates) {
                    return $canVoteAboutDefenceDates;
                },
                'delete' => function ($model) use (&$future) {
                    return $future && Yii::$app->user->can(Permission::DEFENCES_MANAGE);
                }
            ],
            'visible' => $canVoteAboutDefenceDates || ($future && Yii::$app->user->can(Permission::DEFENCES_MANAGE)),
            'buttons' => [
                'update' => function ($url, $model) {
                    $url .= '&view=index';
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ["update", "id" => $model->id], [
                        'title' => Yii::t('app', 'Update'),
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ["delete", "id" => $model->id], [
                        'title' => Yii::t('app', 'Delete'),
                        'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                        'data-method' => 'post'
                    ]);
                }
            ]
        ]
    ],
]);