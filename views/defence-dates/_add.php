<?php

use app\models\records\DefenceDate;
use app\models\records\Project;
use app\widgets\ProjectLink;
use yii\widgets\ActiveForm;

/** @var $model DefenceDate */

$projects = Project::getDefendableProjects();
$defendableStates = [
    Project::STATE_ANALYSIS_HANDED_IN,
    Project::STATE_IMPLEMENTATION_HANDED_IN,
    Project::STATE_ANALYSIS,
    Project::STATE_IMPLEMENTATION
];
$defendableProjects = [];
foreach ($projects as $project) {
    if (in_array($project->state, $defendableStates)) {
        $defendableProjects[] = $project;
    }
}

?>
<div id="add-projects">
    <?php if (sizeof($projects) > 0) : ?>
    <style>
        .checkbox {
            margin-right: 2em !important;
            margin-left: 1em !important;
        }
        .add-button {
            margin-top: 1.4em;
        }
    </style>
    <div>
        <?php
        $form = ActiveForm::begin([
            'action' => ['defence-dates/add-projects', 'id' => $model->id],
            'method' => 'post',
//                'options' => [
//                    'data-pjax' => 1
//                ],
        ]);
        ?>
            <?php if (sizeof($defendableProjects) > 0) : ?>
                <table>
                    <?php foreach ($defendableProjects as $project) { ?>
                        <tr>
                            <td>
                                <input class="checkbox" name="id[]" value="<?= $project->id ?>" type="checkbox" />
                            </td>
                            <td>
                                <?= ProjectLink::widget([
                                    ProjectLink::DISABLED => false,
                                    ProjectLink::PROJECT => $project,
                                    ProjectLink::NAME => ProjectLink::NAME_SHORT
                                ]);
                                ?>
                            </td>
                        </tr>
                    <?php 
                } ?>
                </table>
                <input class='add-button btn btn-default' type='submit'
                    value="<?= Yii::t('app', 'Add') ?>"
                    title="<?= Yii::t('app', 'Assign these projects to a defence on this date') ?>"
                />
            <?php else : ?>
                <span style="color: #bfbfbf; font-style: italic;"><?= Yii::t('app', 'No defendable projects found.'); ?></span>
            <?php endif; ?>
        <?php ActiveForm::end(); ?>
    </div>
    <?php endif; ?>
</div>