<?php

use yii\helpers\Html;
use app\utils\DateTime;
use app\models\records\ProjectDefence;
use app\models\records\DefenceDate;
use app\models\records\Project;
use app\consts\Permission;

/* @var $model DefenceDate */
?>
<?php

/** @var \app\models\records\ProjectDefence[] $defences */
$defences = $model->projectDefences;

echo '<table>';
foreach ($defences as $defence) {
    $name = $defence->project->name . ' (' . $defence->project->short_name . ')';
    $urlOptions = ['/defences/view', 'id' => $defence->id];
    $time = $defence->time ? DateTime::from($defence->time)->format(DateTime::TIME_MINS) : "--";
    $defenceLink = Html::a(Html::encode($name), $urlOptions, [
        'data-pjax' => '0'      // to prevent pjaxing inside gridViews
    ]);
    $state = '<span style="font-size: .7em; color: #939393; margin-left: 1em; font-style: italic">'
        . Yii::t('app', $defence->result) . '</span>';
    echo '<tr><td class="table-time">' . $time . '</td><td class="table-project">' . $defenceLink . $state . '</td></tr>';
}
echo "</table>";
?>
