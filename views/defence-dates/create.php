<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\utils\DateTime;
use app\assets\DefencesCreateAsset;

/* @var $this yii\web\View */
/* @var $model \app\models\records\DefenceDate */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Create Defence Dates');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Defence Dates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$model->active_voting = true;
$str = date('d.m.Y', strtotime('next friday'));
$model->date = (new DateTime($str))->modifyClone("+1 week");
$model->signup_deadline = $model->date->modifyClone('-2 days');

DefencesCreateAsset::register($this);

?>
<style>
    .form-control {
        width: 150px;
    }
</style>

<div class="defence-dates-create">

<h1><?= Html::encode($this->title) ?></h1>

<div class="defence-dates-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'date')->widget(DatePicker::class, [
        'dateFormat' => DateTime::YII_DATE,
        'options' => ['class' => 'form-control']
    ]) ?>
    <?= $form->field($model, 'signup_deadline')->widget(DatePicker::class, [
        'dateFormat' => DateTime::YII_DATE,
        'options' => ['class' => 'form-control']
    ]) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

</div>
