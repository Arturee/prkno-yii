<?php

use app\models\records\DefenceAttendance;
use kartik\editable\Editable;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\records\DefenceDate */
/* @var $canVoteAboutDate bool */
/* @var $onSuccessJsCallback string */

$userId = Yii::$app->getUser()->getId();
$attendance = $model->getDefenceAttendanceByUserId($userId);

if ($canVoteAboutDate) {
    echo Editable::widget([
        'model' => $attendance,
        'attribute' => 'vote',
        'asPopover' => true,
        'preHeader' => '',
        'header' => Yii::t('app', 'My vote'),
        'formOptions' => ['action' => ['vote', 'id' => $model->id]],
        'inlineSettings' => [
            'templateBefore' => Editable::INLINE_BEFORE_2,
            'templateAfter' => Editable::INLINE_AFTER_2
        ],
        'afterInput' => function (ActiveForm $form, $widget) {
            echo $form->field($widget->model, 'comment')->textarea(['rows' => '3']);
        },
        'format' => Editable::FORMAT_BUTTON,
        'inputType' => Editable::INPUT_DROPDOWN_LIST,
        'data' => [
            DefenceAttendance::VOTE_YES => DefenceAttendance::getVoteLabel(DefenceAttendance::VOTE_YES),
            DefenceAttendance::VOTE_NO => DefenceAttendance::getVoteLabel(DefenceAttendance::VOTE_NO)
        ],
        'options' => ['class' => 'form-control'],
        'buttonsTemplate' => '{submit}',
        'displayValueConfig' => [
            DefenceAttendance::VOTE_YES => '<i class="glyphicon glyphicon-thumbs-up"></i> ' . DefenceAttendance::getVoteLabel(DefenceAttendance::VOTE_YES),
            DefenceAttendance::VOTE_NO => '<i class="glyphicon glyphicon-thumbs-down"></i> ' . DefenceAttendance::getVoteLabel(DefenceAttendance::VOTE_NO),
            DefenceAttendance::VOTE_UNKNOWN => DefenceAttendance::getVoteLabel(DefenceAttendance::VOTE_UNKNOWN)
        ],
        'pluginEvents' => [
            "editableSuccess" => $onSuccessJsCallback
        ]
    ]);
} else {
    echo DefenceAttendance::getVoteLabel($attendance->vote);
}

?>