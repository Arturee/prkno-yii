<?php

use app\assets\DefenceDatesIndexAsset;
use app\consts\Permission;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $futureDataProvider yii\data\ActiveDataProvider */
/* @var $pastDataProvider yii\data\ActiveDataProvider */

DefenceDatesIndexAsset::register($this);

$this->title = Yii::t('app', 'Defences');
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
$canVoteAboutDefenceDates = $user->can(Permission::DEFENCES_VOTE_ABOUT_DATE);
$canManageDefences = $user->can(Permission::DEFENCES_MANAGE);

?>
<div class="defence-dates-index">
    <?php if ($canManageDefences) : ?>
        <p class="pull-right">
            <?= Html::a(Yii::t('app', 'Create a new date'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <?php if ($canVoteAboutDefenceDates) : ?>
    <div id="table-voting">
    <h2><?= Yii::t('app', 'Voting') ?></h2>
    <?= Yii::$app->controller->renderPartial('_grid', [
        'dataProvider' => $votingDataProvider,
        'canManageDefences' => $canManageDefences,
        'canVoteAboutDefenceDates' => true,
        'voting' => true,
        'future' => true
    ]); ?>
    </div>
    <?php endif; ?>

    <h2><?= Yii::t('app', 'Future defences') ?></h2>
    <div id="table-future-defences">
    <?= Yii::$app->controller->renderPartial('_grid', [
        'dataProvider' => $futureDataProvider,
        'canManageDefences' => $canManageDefences,
        'canVoteAboutDefenceDates' => $canVoteAboutDefenceDates,
        'voting' => false,
        'future' => true
    ]); ?>
    </div>

    <h2><?= Yii::t('app', 'Past defences') ?></h2>
    <div id="table-past-defences">
    <?= Yii::$app->controller->renderPartial('_grid', [
        'dataProvider' => $pastDataProvider,
        'canManageDefences' => false,
        'canVoteAboutDefenceDates' => false,
        'voting' => false,
        'future' => false
    ]); ?>
    </div>

</div>
