<?php

use app\assets\DefenceDatesViewAsset;
use app\consts\Permission;
use app\models\records\DefenceAttendance;
use app\utils\DateTime;
use app\widgets\Checkbox;
use app\widgets\UserLinks;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \app\models\records\DefenceDate */
/* @var $attendancesProvider ArrayDataProvider */

DefenceDatesViewAsset::register($this);

$date = (new DateTime($model->date))->format(DateTime::DATE);
$this->title = Yii::t('app', 'Defences of {0}', [$date]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Defence Dates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
$canVoteAboutDate = $user->can(Permission::DEFENCES_VOTE_ABOUT_DATE);
$canManageDefences = $user->can(Permission::DEFENCES_MANAGE);

$countDefences = sizeof($model->projectDefences);
?>

<div class="defence-dates-view">

    <div style="margin-bottom: 2em;">
        <h1 style="display: inline"><?= Html::encode($this->title) ?></h1>
        <?php if ($canManageDefences) : ?>
            <div class="pull-right">
                <?php if ($model->active_voting == 1) : ?>
                <?= Html::a(Yii::t('app', 'Confirm Date'), ['confirm-date', 'id' => $model->id], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'method' => 'post'
                    ]
                ]) ?><?php endif; ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this defence date? All data including voting and all defences will be lost.'),
                        'method' => 'post',
                    ],
                    'title' => Yii::t('app', 'Delete defence date'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="defence-date-votes">
    	<div class="row">

			<div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading"><?= Yii::t('app', 'My vote') ?></div>
                    <div class="panel-body">
                        <?= Yii::$app->controller->renderPartial('_my_vote', [
                            'model' => $model,
                            'canVoteAboutDate' => $canVoteAboutDate,
                            'onSuccessJsCallback' => "function() { $.pjax.reload({container: '#votes-container'}); }"
                        ]); ?>
                    </div>
                </div><!-- panel My vote -->

                <?php if ($model->active_voting == 0 && $canManageDefences && $countDefences > 0) : ?>
                    <div class="panel panel-success">
                        <div class="panel-heading"><?= Yii::t('app', 'Schedule') ?></div>
                        <div class="panel-body">
                            <?= Yii::$app->controller->renderPartial('_times', ['model' => $model]); ?>
                        </div>
                    </div>
                <?php endif; ?><!-- panel pick time -->

                <?php if ($model->active_voting == 0 && $canManageDefences) : ?>
                    <div class="panel panel-success">
                        <div class="panel-heading"><?= Yii::t('app', 'Add projects') ?></div>
                        <div class="panel-body">
                            <?= Yii::$app->controller->renderPartial('_add', ['model' => $model]); ?>
                        </div>
                    </div>
                <?php endif; ?><!-- panel Add projects -->

                <?php if ($model->active_voting == 0 && $canManageDefences) : ?>
                    <div class="panel panel-success">
                        <div class="panel-heading"><?= Yii::t('app', 'Room') ?></div>
                        <div class="panel-body">
                            <?= Yii::$app->controller->renderPartial('_room', ['model' => $model]); ?>
                        </div>
                    </div>
                <?php endif; ?><!-- panel room -->
            </div><!-- left column -->


            <div class="col-md-8">
                <div class="panel panel-warning">
                    <div class="panel-heading"><?= Yii::t('app', 'Votes') ?></div>
                    <div class="panel-body">
                    <?php
                    Pjax::begin(['id' => 'votes-container']);
                    echo GridView::widget([
                        'dataProvider' => $attendancesProvider,
                        'summary' => '',
                        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                        'columns' => [
                            [
                                'label' => Yii::t('app', 'User'),
                                'attribute' => 'user',
                                'format' => 'html',
                                'headerOptions' => ['width' => '300'],
                                'value' => function ($model) {
                                    return UserLinks::widget([UserLinks::USERS => $model->user]);
                                },
                            ],
                            [
                                'attribute' => 'vote',
                                'headerOptions' => ['width' => '100'],
                                'value' => function ($model) {
                                    return DefenceAttendance::getVoteLabel($model->vote);
                                },
                            ],
                            [
                                'attribute' => 'comment',
                                'value' => function ($model) {
                                    return $model->comment;
                                },
                            ],
                            [
                                'class' => ActionColumn::class,
                                'header' => Yii::t('app', 'Must come'),
                                'headerOptions' => ['width' => '20'],
                                'template' => '{must-come}',
                                'visible' => $canManageDefences,
                                'buttons' => [
                                    'must-come' => function ($url, $model, $key) {
                                        $url = Url::to([
                                            'must-come',
                                            'id' => $model->defence_date_id,
                                            'userId' => $model->user_id,
                                            'mustCome' => !$model->must_come
                                        ]);
                                        return Checkbox::widget([
                                            Checkbox::URL => $url,
                                            Checkbox::IS_CHECKED => $model->must_come,
                                            Checkbox::RELOAD_SELECTOR => '#votes-container'
                                        ]);
                                    },
                                ]
                            ],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                    </div>
                </div>
                <?php
                if ($canManageDefences) {
                    echo Html::a(
                        Yii::t('app', 'Send emails'),
                        ['email-must-come', 'id' => $model->id],
                        [
                            'class' => 'btn btn-default',
                            'data' => [
                                'method' => 'post',
                            ],
                            'title' => Yii::t('app', 'Inform chosen committee members that they must attend the defence.')
                        ]
                    );
                } ?>
            </div><!-- right column -->
    	</div>
    </div>
</div>

