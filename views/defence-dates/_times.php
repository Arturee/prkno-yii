<?php

/**
 * cannot be done via a widget (at least not nicely), because the only way to use ActiveForm inside
 * a widget is by passing it in as a parameter (not very nice code).
 * on the other hand ActiveForms are needed, because Yii uses them for XSRF protection.
 * (classic HTML forms throw error due to missing XSRF token)
 */

use app\assets\DefenceDateTimesAsset;
use app\models\records\DefenceDate;
use app\models\records\ProjectDefence;
use app\utils\DateTime;
use app\widgets\ProjectLink;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var $model DefenceDate */

function getTime(ProjectDefence $defence) : ? string
{
    $time = $defence->time;
    if ($time) {
        $time = DateTime::from($time)->format(DateTime::TIME_MINS);
    }
    return $time;
}

$titleTime = Yii::t('app', 'Beginning of the defence (eg. 17:00).');
$titleEdit = Yii::t('app', 'Edit');
$defences = $model->getProjectDefences()->orderBy('time')->all();

DefenceDateTimesAsset::register($this);

?>
<div id="date-times">
    <a href='javascript:void(0)'>
        <span class="glyphicon glyphicon-edit pull-right" title="<?= $titleEdit ?>"></span>
    </a>
    <div class="non-editable">
        <table>
        <?php

        /** @var \app\models\records\ProjectDefence $defence */
        foreach ($defences as $defence) { ?>
            <tr>
                <td><?= getTime($defence) ?></td>
                <td>
                <?= ProjectLink::widget([
                    ProjectLink::PROJECT => $defence->project,
                    ProjectLink::NAME => ProjectLink::NAME_SHORT
                ]);
                ?>
                </td>
            </tr>
        <?php 
    } ?>
        </table>
    </div>
    <div class="editable" style="display:none">

        <?php
        $form = ActiveForm::begin([
            'action' => ['defence-dates/set-time', 'id' => $model->id],
            'method' => 'post'
        ]);
        ?>
            <table>
                <?php foreach ($defences as $defence) {
                    $time = getTime($defence);
                    ?>
                    <tr>
                        <td>
                            <input name="id[]" type="hidden" value="<?= $defence->id ?>"/>
                            <input name="time[]" value="<?= $time ?>" class="time-input"
                                   pattern="[0-9]{2}:[0-9]{2}" title="<?= $titleTime ?>"
                            />
                        </td>
                        <td>
                            <?= ProjectLink::widget([
                                ProjectLink::PROJECT => $defence->project,
                                ProjectLink::NAME => ProjectLink::NAME_SHORT
                            ]);
                            ?>
                        </td>
                        <td>
                            <?=
                            Html::a('<span class="glyphicon glyphicon-trash"></span>', ["delete-project", "id" => $defence->id], [
                                'title' => Yii::t('app', 'Delete'),
                                'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'data-method' => 'post'
                            ]);
                            ?>
                        </td>
                    </tr>
                <?php 
            } ?>
            </table>
            <input class='btn btn-default save' type='submit' value="<?= Yii::t('app', 'Save') ?>"/>
        <?php ActiveForm::end(); ?>
    </div>
</div>