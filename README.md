# NOVINKY #


# PROGRAMMER'S DOCUMENTATION
## Defences
* missing defence attendances automatically created when defence-date page is visited
* DefenceDates.active_voting (in models and DB) is currently not being used. *Its purpose is to allow/disallow voting
about defence dates*

# File storage

Use the classes in utils/storage/ depending on what file you want to work with.
It's done all according to this structure (full structure in Issues/XML import):

* files/templates/download/ - **DownloadableTemplateStorage** - downloadable templates (eg. proposal-template.docx, ditto.tex), managed via putty 
* files/templates/md/ - **MarkdownTemplates** - markdown templates for GUI, managed via putty

* **ProjectStorage:**
* files/documents/project/[project_id]/uploads/[document.name] 
* /files/documents/project/[project_id]/uploads/DEFENSE_PDF_[defence_number].PDF 
* /files/documents/project/[project_id]/uploads/DEFENSE_ZIP_[defence_number].ZIP 

* **DefenceStorage:**
* /files/documents/project/[project_id]/defence/[defence.id]/opponents_review.PDF 
* /files/documents/project/[project_id]/defence/[defence.id]/supervisors_review.PDF 
* /files/documents/project/[project_id]/defence/[defence.id]/defence_statement.PDF 

* /files/pages/[generated_pdf_id].pdf - **PageStorage**

If you use **FileStorage**. the files will be put into /files/otherFiles, which is meant for the committee admin only.

**Project file example:**
You will probably want to use the `copyProjectFileIn` method the most. Other functions work similarly.
`copyProjectFileIn(string $srcFileName, int $projectId, string $projectFileType, string $fileName, ?int $defenceNumber = NULL)`
The `srcFileName` is the file you want to upload.
The project file type parameter is a constant in ProjectStorage - defence_pdf, defence_zip or other document.
If you choose to submit a defence file, you must specify the defenceNumber and in that case the `fileName` will not matter, because the file will be named according to the structure described. If you choose to submit an *other* file, the file will be renamed to `fileName`.

**Defence file example:**
Same principle as ProjectStorage:
`copyDefenceFileIn(string $srcFileName, int $projectId, int $defenceId, string $defenceFileType)`
The file will be named according to the filetype you submit (a constant from DefenceFileStorage).


# YII bugs #

## pjax sends requests twice ##

PJAX sends two requests instead of one. Here is the explanation: (no really a bug)
https://stackoverflow.com/questions/8153263/jquery-pjax-request-sending-twice

## GridView > DataColumn > Html::a tag - all data-* attributes are ignored! ##

That's just how yii is programmed. Very stupid.
https://github.com/kartik-v/yii2-grid/issues/618


## Best practices ##

1. Pokud validace modelů někdy funguje a někdy ne, zkus použít scenarios()



## Mailování ##

Udělal jsem vylepšenej mailer. Automaticky přídává from() a je jednodušší.

```php
  //použití

  /** @var Mailer $mailer */  // Toto přetypování je nutný, jinak nebude fungovat intellisense!
  $mailer = Yii::$app->mailer;
  return $mailer->fromTemplates('newsCreated',
          [
              'title' => $title,
              'message' => $message
          ]
      )
      ->setTo($user->email)
      ->setSubject(Yii::t('app', 'News: ') . $title)
      ->send();
```



## Práce s datumama ##

* datumy v DB jsou tvaru YYYY-mm-dd
* datumy v GUI jsou tvaru dd. mm. YYYY
* všechny formáty data jsou jako konstanty v DateTime::


### konverze DB -> GUI ###

```php

  DateTime::from($model->date)->format(DateTime::DATE);

```

### konverze GUI -> DB ###

Díky třídě OrmRecord teď konverze funguje bez problémů a od OrmRecord dědí všechny naše ORM modely.

```php

$model->date = '1.12.2019';
$model->date = '2019-12-1';
$model->date = new DateTime();
$model->save();

// všechno funguje, protože OrmRecord před každou validací zkusí nejdřív zformátovat
// datumový atributy do DB formátu

// jediný co musíte udělat je explicitně říct, který atributy jsou datumový a od toho slouží:

OrmRecord::$DATE_ATTRUBUTES = [];     // bez času
OrmRecord::$DATETIME_ATTRUBUTES = []; // s časem

// stačí je předefinovat

```









# DIRECTORY STRUCTURE #

      assets/             contains assets definition (JS and CSS files for views)
      commands/           contains console commands (controllers)
      config/             contains application configurations
      consts/             useful PHP constants
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes (ORM classes and forms)
      runtime/            contains files generated during runtime (logs, cache, ...)
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application (HTML)
      web/                contains the entry script and Web resources



# REQUIREMENTS #

* PHP 7.2
* composer
* npm


# INSTALLATION #

* import SQL schema `deployment/db_schema.sql` into database named `prkno`
* run `composer update`
* run `cd web; npm install`
* run `cd deployment; php data.php`

# COMMANDS #

* `./yii rbac/init` - initializes authorization mechanisms (generates RBAC files)
* `./yii message config/message.php` - generates/updates translation files