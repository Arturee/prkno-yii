<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for defence dates index
 */
class DefenceDatesIndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/defence-dates/index.css',
    ];
    public $depends = [
        AppAsset::class
    ];
}
