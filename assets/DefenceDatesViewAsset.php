<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for defence dates view
 */
class DefenceDatesViewAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/defence-dates/view.css',
    ];
    public $js = [
        'js/defence-dates/view.js',
    ];
    public $depends = [
        AppAsset::class
    ];
}
