<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for doc index
 */
class DocIndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/doc/index.css',
    ];
    public $depends = [
        AppAsset::class
    ];
}
