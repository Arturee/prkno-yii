<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for markdown display widget
 */
class MarkdownDisplayAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $js = [
        'js/widgets/markdown-display.js',
    ];
    public $depends = [
        AppAsset::class
    ];
}