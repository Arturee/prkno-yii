<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for defence dates create
 */
class DefencesCreateAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $js = [
        'js/defences/create.js',
    ];
    public $depends = [
        AppAsset::class
    ];
}
