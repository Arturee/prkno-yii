<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for markdown input
 */
class MarkdownInputAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/widgets/markdown-input.js',
    ];
    public $depends = [
        AppAsset::class
    ];
}