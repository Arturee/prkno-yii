<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for defence dates times
 */
class DefenceDateTimesAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/widgets/defence-date-times.css',
    ];
    public $js = [
        'js/widgets/defence-date-times.js',
    ];
    public $depends = [
        AppAsset::class
    ];
}
