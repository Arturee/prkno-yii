<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for users view
 */
class UsersViewAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/users/view.css',
    ];
    public $depends = [
        AppAsset::class
    ];
}
