<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for static pages
 */
class PagesAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/pages/view.css',
    ];
    public $depends = [
        AppAsset::class
    ];
}
