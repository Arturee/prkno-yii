<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for markdown widget
 */
class MarkdownAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $js = [
        'js/widgets/markdown.js',
    ];
    public $depends = [
        AppAsset::class
    ];
}