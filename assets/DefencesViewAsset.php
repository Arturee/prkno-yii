<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for defence view
 */
class DefencesViewAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $js = [
        'js/defences/view.js',
    ];
    public $css = [
        'css/defences/view.css',
    ];
    public $depends = [
        AppAsset::class
    ];
}
