<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for markdown preview widget
 */
class MarkdownPreviewAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/widgets/markdown-preview.js',
    ];
    public $depends = [
        AppAsset::class
    ];
}