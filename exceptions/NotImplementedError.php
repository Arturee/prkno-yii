<?php
namespace app\exceptions;

/**
 * Exception thrown when executing method without implementation
 */
class NotImplementedError extends \Exception
{
}