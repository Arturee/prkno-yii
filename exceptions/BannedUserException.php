<?php
namespace app\exceptions;

/**
 * Exception thrown when banned user is trying to log in
 */
class BannedUserException extends \Exception
{
    /**
     * @inheritdoc
     */
    public function __construct() {
        $code = 6;
        $message = 'You are banned. Contact the committee for an explanation. (code '.$code.')';
        parent::__construct($message, $code, null);
    }
}