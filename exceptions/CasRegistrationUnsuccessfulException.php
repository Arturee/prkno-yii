<?php
namespace app\exceptions;

/**
 * Exception thrown when registration of the new user via CAS
 * was not successfull.
 */
class CasRegistrationUnsuccessfulException extends \Exception
{
    /**
     * Create exception object
     */
    public function __construct() {
        $code = 2;
        $message = "You cannot log in via CAS, because your email in CAS is invalid. (code $code)";
        parent::__construct($message, $code, null);
    }
}