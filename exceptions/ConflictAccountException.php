<?php
namespace app\exceptions;

/**
 * Exception thrown when user trying to log in via CAS
 * already have standard account with credentials and the
 * same email address.
 */
class ConflictAccountException extends \Exception
{
    /**
     * Create exception
     */
    public function __construct() {
        $code = 1;
        $message =
            'You cannot log in via CAS, because you already have an account. '.
            'Log in via your email and password instead. '.
            'If you actually do not have an account, then you are seeing this error because some other user has already taken your email. (code '.$code.')';
        parent::__construct($message, $code, null);
    }
}