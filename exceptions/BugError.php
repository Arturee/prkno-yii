<?php
namespace app\exceptions;

/**
 * Gets automatically logged to runtime/logs/app.log
 */
class BugError extends \Exception
{
}