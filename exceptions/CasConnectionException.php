<?php
namespace app\exceptions;

/**
 * Exception thrown when problem with connection to CAS
 * server occured
 */
class CasConnectionException extends \Exception
{
    /**
     * Create exception object
     * @param Throwable $previous   Previous exception
     */
    public function __construct(\Throwable $previous = null) {
        $code = 5;
        $message =
            'An error occurred while connecting to CAS. '.
            'Check if CAS is online, wait for a while and then retry. '.
            'If the problem persists, contact the committee. (code '.$code.')';
        parent::__construct($message, $code, $previous);
    }
}