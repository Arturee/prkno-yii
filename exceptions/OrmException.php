<?php
namespace app\exceptions;

/**
 * Exception throw when SQL error occured
 */
class OrmException extends \Exception
{
}