<?php
namespace app\exceptions;

/**
 * Exception thrown when user with unsufficient privileges
 * is trying to log in.
 */
class GuestAttemptsToLoginException extends \Exception
{
    /**
     * Create exception
     */
    public function __construct() {
        $code = 3;
        $message =
            'You do not have sufficient privileges to log in. '.
            'If you are trying to log in via CAS then probably something went wrong. '.
            'Contact the committee to resolve this issue. (code '.$code.')';
        parent::__construct($message, $code, null);
    }
}