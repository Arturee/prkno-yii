<?php
namespace app\commands;

use app\consts\Permission;
use app\models\records\User;
use app\rules\AdCommentOwnerRule;
use app\rules\AdvertisementOwnerRule;
use app\rules\DefenceDateAddProjectRule;
use app\rules\DefenceUploadOpponentReviewRule;
use app\rules\DefenceUploadSupervisorReviewRule;
use app\rules\ProfileOwnerRule;
use app\rules\ProjectAssignedRule;
use app\rules\ProjectCanDownloadFile;
use app\rules\ProjectDefenceDocumentsOwnRule;
use app\rules\ProjectMemberRule;
use app\rules\ProjectOwnerRule;
use app\rules\ProjectSupervisorRule;
use app\rules\ProjectUpdateOwnRule;
use app\rules\ProjectUserRule;
use app\rules\ProposalCommentOwnerRule;
use app\rules\ProposalViewRule;
use app\rules\ProposalVoteRule;
use Yii;
use yii\console\Controller;
use yii\rbac\PhpManager;

/**
 * Class for setting user rights and managing authorization
 * There are several user roles: student, academic, committee member, committee chair
 * and each role has specific user rights.
 */
class RbacController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actionInit()
    {
        /** @var PhpManager */
        $auth = Yii::$app->authManager;
        $auth->removeAll();



        // USERS

        // add "manageUsers" permission
        $manageUsers = $auth->createPermission(Permission::USERS_MANAGE);
        $manageUsers->description = 'Manage users';
        $auth->add($manageUsers);

        // add "deleteUsers" permission
        $deleteUsers = $auth->createPermission(Permission::USERS_DELETE);
        $deleteUsers->description = 'Delete user';
        $auth->add($deleteUsers);

        // view the name / email of other users
        $usersViewName = $auth->createPermission(Permission::USERS_VIEW_NAME);
        $usersViewName->description = 'View the names / emails of other users';
        $auth->add($usersViewName);

        // update own profile
        $rule = new ProfileOwnerRule();
        $auth->add($rule);
        $updateOwnProfile = $auth->createPermission(Permission::USERS_UPDATE_OWN_PROFILE);
        $updateOwnProfile->description = 'Update own profile';
        $updateOwnProfile->ruleName = $rule->name;
        $auth->add($updateOwnProfile);




        // PROJECT

        // create project rule
        $createProject = $auth->createPermission(Permission::PROJECT_CREATE);
        $createProject->description = 'Create a project';
        $auth->add($createProject);

        // update project rule
        $updateProject = $auth->createPermission(Permission::PROJECT_UPDATE);
        $updateProject->description = 'Update project';
        $auth->add($updateProject);

        // update own project rule
        $rule = new ProjectUpdateOwnRule();
        $auth->add($rule);
        $updateOwnProject = $auth->createPermission(Permission::PROJECT_UPDATE_OWN_PROJECT);
        $updateOwnProject->description = 'Update own project';
        $updateOwnProject->ruleName = $rule->name;
        $auth->add($updateOwnProject);

        // send documents to defence on own project
        $rule = new ProjectDefenceDocumentsOwnRule();
        $auth->add($rule);
        $sendDocsProject = $auth->createPermission(Permission::PROJECT_SEND_DOCUMENTS);
        $sendDocsProject->description = 'Send documents to defence';
        $sendDocsProject->ruleName = $rule->name;
        $auth->add($sendDocsProject);

		// update project status
        $updateProjectStatus = $auth->createPermission(Permission::PROJECT_UPDATE_STATUS);
        $updateProjectStatus->description = 'Update project status';
        $auth->add($updateProjectStatus);

        // update project content
        $rule = new ProjectMemberRule();
        $auth->add($rule);
        $updateProjectContent = $auth->createPermission(Permission::PROJECT_UPDATE_CONTENT);
        $updateProjectContent->description = 'Update project content';
        $updateProjectContent->ruleName = $rule->name;
        $auth->add($updateProjectContent);

        // update project advice
        $rule = new ProjectMemberRule();
        $auth->add($rule);
        $updateProjectAdvice = $auth->createPermission(Permission::PROJECT_UPDATE_ADVICE);
        $updateProjectAdvice->description = 'Update project advice';
        $updateProjectAdvice->ruleName = $rule->name;
        $auth->add($updateProjectAdvice);

		// upload project files
        $rule = new ProjectUserRule();
        $auth->add($rule);
        $uploadProjectFiles = $auth->createPermission(Permission::PROJECT_UPLOAD_FILE);
        $uploadProjectFiles->description = 'Upload project files';
        $uploadProjectFiles->ruleName = $rule->name;
        $auth->add($uploadProjectFiles);

        //download project files
        $rule = new ProjectCanDownloadFile();
        $auth->add($rule);
        $downloadProjectFiles = $auth->createPermission(Permission::PROJECT_DOWNLOAD_FILE);
        $downloadProjectFiles->description = 'Download project files';
        $downloadProjectFiles->ruleName = $rule->name;
        $auth->add($downloadProjectFiles);

        //is user assigned to project
        $rule = new ProjectAssignedRule();
        $auth->add($rule);
        $isUserAssignedToProject = $auth->createPermission(Permission::PROJECT_IS_ASSIGNED);
        $isUserAssignedToProject->description = 'Is user assigned to project';
        $isUserAssignedToProject->ruleName = $rule->name;
        $auth->add($isUserAssignedToProject);

        // assign project to defence
        $rule = new ProjectUserRule();
        $auth->add($rule);
        $assignProjectToDefence = $auth->createPermission(Permission::PROJECT_ASSIGN_TO_DEFENCE);
        $assignProjectToDefence->description = 'Assign project to defence';
        $assignProjectToDefence->ruleName = $rule->name;
        $auth->add($assignProjectToDefence);

        // view my defence
        $rule = new ProjectUserRule();
        $auth->add($rule);
        $projectViewDefenceOfMyProject = $auth->createPermission(Permission::PROJECT_VIEW_DEFENCE_OF_MY_PROJECT);
        $projectViewDefenceOfMyProject->description = 'View defence of my project';
        $projectViewDefenceOfMyProject->ruleName = $rule->name;
        $auth->add($projectViewDefenceOfMyProject);

        // delete project rule
        $deleteProject = $auth->createPermission(Permission::PROJECT_DELETE);
        $deleteProject->description = 'Delete project';
        $auth->add($deleteProject);

        // upload supervisor review
        $rule = new ProjectSupervisorRule();
        $auth->add($rule);
        $isSupervisorOfProject = $auth->createPermission(Permission::PROJECT_IS_SUPERVISOR_OF_PROJECT);
        $isSupervisorOfProject->description = 'Is supervisor of project';
        $isSupervisorOfProject->ruleName = $rule->name;
        $auth->add($isSupervisorOfProject);



        // PROPOSALS

		// list proposals
        $listProposals = $auth->createPermission(Permission::PROPOSAL_LIST);
        $listProposals->description = 'List proposals';
        $auth->add($listProposals);

		// view proposal rule
        $rule = new ProposalViewRule();
        $auth->add($rule);
        $viewProposal = $auth->createPermission(Permission::PROPOSAL_VIEW);
        $viewProposal->description = 'View proposal';
        $viewProposal->ruleName = $rule->name;
        $auth->add($viewProposal);

        // update proposal rule
        $rule = new ProjectOwnerRule();
        $auth->add($rule);
        $updateProposal = $auth->createPermission(Permission::PROPOSAL_UPDATE);
        $updateProposal->description = 'Update proposal';
        $updateProposal->ruleName = $rule->name;
        $auth->add($updateProposal);

		// vote proposal rule
        $rule = new ProposalVoteRule();
        $auth->add($rule);
        $voteProposal = $auth->createPermission(Permission::PROPOSAL_VOTE);
        $voteProposal->description = 'Vote for proposal';
        $voteProposal->ruleName = $rule->name;
        $auth->add($voteProposal);

		// manage proposals
        $manageProposals = $auth->createPermission(Permission::PROPOSALS_MANAGE);
        $manageProposals->description = 'Manage proposals';
        $auth->add($manageProposals);

        // vote proposal rule
        $rule = new ProposalCommentOwnerRule();
        $auth->add($rule);
        $updateOwnProposalComment = $auth->createPermission(Permission::PROPOSAL_UPDATE_OWN_COMMENT);
        $updateOwnProposalComment->description = 'Update own proposal comment';
        $updateOwnProposalComment->ruleName = $rule->name;
        $auth->add($updateOwnProposalComment);

        // view all proposal comments
        $viewProposalComments = $auth->createPermission(Permission::PROPOSAL_COMMENTS_VIEW);
        $viewProposalComments->description = 'View proposal comments';
        $auth->add($viewProposalComments);



		// DEFENCES

        // vote about defence date rule
        $voteDefenceDate = $auth->createPermission(Permission::DEFENCES_VOTE_ABOUT_DATE);
        $voteDefenceDate->description = 'Vote for defence date';
        $auth->add($voteDefenceDate);

		// manage defences
        $manageDefences = $auth->createPermission(Permission::DEFENCES_MANAGE);
        $manageDefences->description = 'Manage defences';
        $auth->add($manageDefences);

        // view a defence and it's results
        $viewDefence = $auth->createPermission(Permission::DEFENCES_VIEW);
        $viewDefence->description = 'View a defence';
        $auth->add($viewDefence);

        // upload opponent review
        $rule = new DefenceUploadOpponentReviewRule();
        $auth->add($rule);
        $opposeDefence = $auth->createPermission(Permission::DEFENCES_UPLOAD_OPPONENT_REVIEW);
        $opposeDefence->description = 'Upload opponents review';
        $opposeDefence->ruleName = $rule->name;
        $auth->add($opposeDefence);

        // upload supervisor review
        $rule = new DefenceUploadSupervisorReviewRule();
        $auth->add($rule);
        $superviseDefence = $auth->createPermission(Permission::DEFENCES_UPLOAD_SUPERVISOR_REVIEW);
        $superviseDefence->description = 'Upload supervisors review';
        $superviseDefence->ruleName = $rule->name;
        $auth->add($superviseDefence);

        // add a project to the defence date
        $rule = new DefenceDateAddProjectRule();
        $auth->add($rule);
        $defenceAddProject = $auth->createPermission(Permission::DEFENCE_ADD_PROJECT);
        $defenceAddProject->description = 'Add a project to the defence date';
        $defenceAddProject->ruleName = $rule->name;
        $auth->add($defenceAddProject);



        // NEWS

        // manage news
        $manageNews = $auth->createPermission(Permission::NEWS_MANAGE);
        $manageNews->description = 'Manage news';
        $auth->add($manageNews);


        // DOC
        $viewDoc = $auth->createPermission(Permission::DOC_VIEW);
        $viewDoc->description = 'View documentation';
        $auth->add($viewDoc);

        // ADS

        // add "manageAdvertisements" permission
        $manageAdvertisements = $auth->createPermission(Permission::ADS_MANAGE);
        $manageAdvertisements->description = 'Manage advertisements';
        $auth->add($manageAdvertisements);

        // create advertisements
        $createAdvertisements = $auth->createPermission(Permission::ADS_CREATE);
        $createAdvertisements->description = 'Create advertisements';
        $auth->add($createAdvertisements);

        // manage own advertisement
        $rule = new AdvertisementOwnerRule();
        $auth->add($rule);
        $manageOwnAdvertisement = $auth->createPermission(Permission::ADS_MANAGE_OWN_ADS);
        $manageOwnAdvertisement->description = 'Manage own advertisement';
        $manageOwnAdvertisement->ruleName = $rule->name;
        $auth->add($manageOwnAdvertisement);

        // add "manageAdComments" permission
        $manageAdComments = $auth->createPermission(Permission::ADS_MANAGE_COMMENTS);
        $manageAdComments->description = 'Manage advertisement comments';
        $auth->add($manageAdComments);

        // create adComments
        $createAdComments = $auth->createPermission(Permission::ADS_CREATE_COMMENT);
        $createAdComments->description = 'Create advertisement comments';
        $auth->add($createAdComments);

        // manage own adComment
        $rule = new AdCommentOwnerRule();
        $auth->add($rule);
        $manageOwnAdComment = $auth->createPermission(Permission::ADS_MANAGE_OWN_COMMENTS);
        $manageOwnAdComment->description = 'Manage own advertisement comment';
        $manageOwnAdComment->ruleName = $rule->name;
        $auth->add($manageOwnAdComment);



        //PAGES

        // add "managePages" permission
        $managePages = $auth->createPermission(Permission::PAGES_MANAGE);
        $managePages->description = 'Manage pages';
        $auth->add($managePages);



        // INBOX

        // list inbox
        $listInbox = $auth->createPermission(Permission::INBOX_LIST);
        $listInbox->description = 'List inbox';
        $auth->add($listInbox);

        // projects inbox
        $projectsInbox = $auth->createPermission(Permission::INBOX_PROJECTS);
        $projectsInbox->description = 'Projects inbox';
        $auth->add($projectsInbox);



        // ROLES

        // GUEST
        $guest = $auth->createRole('guest');
        $auth->add($guest);
        $auth->addChild($guest, $downloadProjectFiles);

        // common permissions for student and academic
        $common = $auth->createRole('common.for.students.and.academics');
        $auth->add($common);
        $auth->addChild($common, $guest);

        $auth->addChild($common, $uploadProjectFiles);
        $auth->addChild($common, $assignProjectToDefence);
        $auth->addChild($common, $projectViewDefenceOfMyProject);
        $auth->addChild($common, $updateOwnProfile);
        $auth->addChild($common, $createAdvertisements);
        $auth->addChild($common, $manageOwnAdComment);
        $auth->addChild($common, $createAdComments);
        $auth->addChild($common, $manageOwnAdvertisement);
        $auth->addChild($common, $usersViewName);
        $auth->addChild($common, $defenceAddProject);
        $auth->addChild($common, $downloadProjectFiles);
        $auth->addChild($common, $isUserAssignedToProject);

		// STUDENT
        $student = $auth->createRole(User::ROLE_STUDENT);
        $auth->add($student);
        $auth->addChild($student, $common);
        $auth->addChild($student, $updateProjectContent);
        $auth->addChild($student, $updateProjectAdvice);

		// ACADEMIC
        $academic = $auth->createRole(User::ROLE_ACADEMIC);
        $auth->add($academic);
        $auth->addChild($academic, $common);

        $auth->addChild($academic, $createProject);
        $auth->addChild($academic, $updateOwnProject);
        $auth->addChild($academic, $sendDocsProject);
        $auth->addChild($academic, $viewProposal);
        $auth->addChild($academic, $updateProposal);
        $auth->addChild($academic, $superviseDefence);
        $auth->addChild($academic, $isSupervisorOfProject);


        // COMMITTEE MEMBER
        $committeeMember = $auth->createRole(User::ROLE_COMMITTEE_MEMBER);
        $auth->add($committeeMember);
        $auth->addChild($committeeMember, $academic);

        $auth->addChild($committeeMember, $listProposals);
        $auth->addChild($committeeMember, $voteProposal);
        $auth->addChild($committeeMember, $viewProposalComments);
        $auth->addChild($committeeMember, $updateOwnProposalComment);
        $auth->addChild($committeeMember, $voteDefenceDate);
        $auth->addChild($committeeMember, $opposeDefence);
        $auth->addChild($committeeMember, $viewDefence);

        $auth->addChild($committeeMember, $viewDoc);
        $auth->addChild($committeeMember, $listInbox);

        // COMMITTEE CHAIR
        $committeeChair = $auth->createRole(User::ROLE_COMMITTEE_CHAIR);
        $auth->add($committeeChair);
        $auth->addChild($committeeChair, $committeeMember);

        $auth->addChild($committeeChair, $manageUsers);
        $auth->addChild($committeeChair, $manageProposals);
        $auth->addChild($committeeChair, $manageDefences);
        $auth->addChild($committeeChair, $manageAdvertisements);
        $auth->addChild($committeeChair, $manageAdComments);
        $auth->addChild($committeeChair, $manageNews);
        $auth->addChild($committeeChair, $managePages);

        $auth->addChild($committeeChair, $updateProject);
        $auth->addChild($committeeChair, $updateProjectStatus);
        $auth->addChild($committeeChair, $deleteProject);

        $auth->addChild($committeeChair, $listInbox);
        $auth->addChild($committeeChair, $projectsInbox);

        $auth->addChild($committeeChair, $deleteUsers);
    }
}