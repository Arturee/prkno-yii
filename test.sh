result=$(mysql -u root --password=$1 -e ";")
if [ $? = 0 ]
then
    mysql -u root --password=$1 -e 'DROP DATABASE IF EXISTS prkno_tests; CREATE DATABASE prkno_tests'
    mysql -u root --password=$1 prkno_tests < deployment/db_structure.sql
    ./vendor/bin/codecept build
    ./vendor/bin/codecept -v run $2

    ## FOR WINDOWS:
    # ./vendor/bin/codecept run --no-ansi --debug
      # windows needs --no-ascii. --debug is needed for \Codeception\Util\Debug::debug() messages


    # mysql -u root --password=pass -e 'DROP DATABASE IF EXISTS prkno_tests'
fi