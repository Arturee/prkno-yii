'use strict';
$(function(){
  // suggest the signup deadline date automatically (2 days before)
  $('#defencedate-date').change(function(event) {
    var date = moment(event.target.value, 'DD.MM.YYYY', true);
    var deadline = date.subtract(2, 'days');
    $('#defencedate-signup_deadline').val(deadline.format('DD.MM.YYYY'));
  });
});