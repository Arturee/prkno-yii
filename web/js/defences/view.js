'use strict';
$(function(){

  // accept buttons
  var form = $('#accept-form');
  var inputResult = $('#defenceacceptform-result');

  $('#accept').click(function(){
    inputResult.val('defended');
    form.submit();
  });
  $('#accept-condition').click(function(){
    inputResult.val('conditionally_defended');
    form.submit();
  });
  $('#decline').click(function(){
    inputResult.val('failed');
    form.submit();
  });

  // change opponent button
  $('.edit-opponent').click(function(){
    $('#opponent-form').toggle();
  });
});
