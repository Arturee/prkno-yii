'use strict';
// prevent text from accidentally getting selected, it is not nice visually
function clearSelection() {
  if (window.getSelection) {
    if (window.getSelection().empty) {  // Chrome
      window.getSelection().empty();
    } else if (window.getSelection().removeAllRanges) {  // Firefox
      window.getSelection().removeAllRanges();
    }
  } else if (document.selection) {  // IE?
    document.selection.empty();
  }
}

$(function() {
  // toggle show vs. hide text
  $('.text-widget-show').each(function(idx, el) {
    var textToggle = $(el).find('.text-toggle');
    var text = $(el).find('.text-show');
    textToggle.click(function() {
      text.toggle();
      textToggle.children().toggle();
      clearSelection();
    });
  });
});
