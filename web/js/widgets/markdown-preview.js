'use strict';

function renderMarkdown(textToRender) {
    var url = window.location.origin + baseurl + '/site/render-markdown';

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {inputText: textToRender},

        complete: function (response) {
            var modalBodies = document.getElementsByClassName("modal-body");
            var modalBody = modalBodies[modalBodies.length - 1];
            modalBody.innerHTML = response.responseText;
            $('#modal').modal('show').find('.modal-body');
        },
        error: function () {
            return 'ERROR: occurred during AJAX request!';
        }
    });
    return false;
}

$(function() {

    $('.text-widget').each(function(idx, el) {
        var previewButton = $(el).find('.preview-button');

        previewButton.click(function () {
            var textArea = $(el).find(".markdown-content");
            var text = textArea.val();
            renderMarkdown(text);
        })
    });

    var otherPreviewButtons = document.getElementsByClassName('solo-preview-button');

    var previewButtonFunction = function(i) {
        var textArea = document.getElementsByClassName("solo-markdown-content")[i];
        var text = textArea.value;
        renderMarkdown(text);
    };

    for (var i = 0; i < otherPreviewButtons.length; i++)
    {
        otherPreviewButtons[i].addEventListener('click', previewButtonFunction.bind(null, i), false);
    }
});
