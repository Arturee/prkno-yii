'use strict';

function getFileNameFromPath(fullPath) {
  var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
  var filename = fullPath.substring(startIndex);
  if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
    filename = filename.substring(1);
  }
  return filename;
}

function isPdf(filename) {
    var parts = filename.split('.');
    var extension = parts[parts.length - 1];
    return extension === 'pdf' || extension === 'PDF';
}

$(function() {
  $('.text-widget').each(function(idx, el) {
    var fileInput = $(el).find('input[type=file]');
    var filenameDisplay = $(el).find('.filename');
    var md = $(el).find('.md');
    var form = $(el).closest('form');
    var textArea = $(el).find('.markdown-content');
    var cancel = $(el).find('.cancel');
    var previewButton = $(el).find('.preview-button');

    $(el).find('.open').click(function() {
      fileInput.click();
    });

    fileInput.change(function() {
      var name = getFileNameFromPath(fileInput.val());
      if (isPdf(name)) {
        filenameDisplay.text(name);
        previewButton.hide();
        md.hide();
      } else {
        alert('Only PDF files are allowed');
        fileInput.val(null);
      }
    });

    cancel.click(function() {
        textArea.val(null);
        fileInput.val(null);
        filenameDisplay.text(null);
        md.show();
        previewButton.show();
    });

    form.submit(function() {
      if (!fileInput.val() && !textarea.val()) {
        return false;
      }
    });
  });
});
