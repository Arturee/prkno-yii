'use strict';
$(function() {
    // toggle editing mode vs. viewing mode
    $('.markdown-view-edit').each(function(idx, el) {
        var input = $(el).find('.text-widget');
        var display = $(el).find('.text-widget-show');
        $(el).find('.markdown-edit').click(function(){
            input.toggle();
            display.toggle();
        });
    });
});
