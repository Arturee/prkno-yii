/**
 * @author Artur
 *
 */
'use strict';
$(function(){
  var editable = true;
  function toggleEditable(){
    if (editable) {
      $('#date-times .editable').hide();
      $('#date-times .non-editable').show();
    } else {
      $('#date-times .editable').show();
      $('#date-times .non-editable').hide();
    }
    editable = !editable;
  }

  // make dates not editable
  toggleEditable();

  // toggle editability
  $('#date-times .glyphicon-edit').click(function() {
    toggleEditable();
  });
});