'use strict';
$(function() {
  // temporary solution which enables "ban" button to work even on very slow
  // internet or when using xdebug (which slows everything down).

  // $.pjax.defaults.timeout = 6000;

  $(document).on("pjax:timeout", function(event) {
    // Prevent default timeout redirection behavior
    event.preventDefault()
  });
});