# Motivace
*...krátký popis, proč by měl projekt vzniknout...*

# Popis projektu
*...popis funkčnosti, UI, atd....*

# Platforma, technologie
*...cílová platforma projektu; využité technologie, framework, apod. (pokud jsou již známy)...*

# Odhad náročnosti
*… počet řešitelů, případně i s odůvodněním (přibližné přidělení řešitelů jednotlivým částem projektu),
 termín dokončení, přibližný plán prací...*

# Vymezení projektu

**Diskrétní modely a algoritmy**

- [x ] diskrétní matematika a algoritmy
- [ ] geometrie a matematické struktury v informatice
- [ ] optimalizace

**Teoretická informatika**

- [ ] Teoretická informatika

**Softwarové a datové inženýrství**

- [ ] softwarové inženýrství
- [ ] vývoj software
- [ ] webové inženýrství
- [ ] databázové systémy
- [ ] analýza a zpracování rozsáhlých dat

**Softwarové systémy**

- [ ] systémové programování
- [ ] spolehlivé systémy
- [ ] výkonné systémy

**Matematická lingvistika**

- [ ] počítačová a formální lingvistika
- [ ] statistické metody a strojové učení v počítačové lingvistice

**Umělá inteligence**

- [ ] inteligentní agenti
- [ ] strojové učení
- [ ] robotika

**Počítačová grafika a vývoj počítačových her**

- [ ] počítačová grafika
- [ ] vývoj počítačových her

# Poznámky
*... další informace nezapadající do sekcí výše...*
