<small>
Legenda:

* **lepší** *označuje splnění nad rámec obvyklého softwarového projektu*
* **OK** *označuje práci, která kritérium vhodným způsobem splňuje*
* **horší** *označuje splnění pod rámec obvyklého softwarového projektu*
* **nevyhovuje** *označuje práci, která by neměla být obhájena*

*Hodnocení zdůvodněte v komentáři (zejména u jiných hodnocení než OK).*
</small>

#K celému projektu:

Obtížnost zadání: lepší | OK | horší | nevyhovující

Splnění zadání: lepší | OK | horší | nevyhovující

**Komentář:**

#Implementace:

Kvalita návrhu: lepší | OK | horší | nevyhovující

<small>*(architektura, struktury a algoritmy, použité technologie)*</small>
  
Kvalita zpracování: lepší | OK | horší | nevyhovující

<small>*(jmenné konvence, formátování, komentáře, testování)*</small>
  
Stabilita: lepší | OK | horší | nevyhovující  

**Komentář:**

#Dokumentace:

Vývojová dokumentace: lepší | OK | horší | nevyhovující
  
Uživatelská dokumentace: lepší | OK | horší | nevyhovující  

**Komentář:**