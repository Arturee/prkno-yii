<?php
namespace app\widgets;

use app\models\records\DefenceDate;
use app\models\records\DefenceAttendance;
use yii\jui\Widget;

/**
 * Widget implementing elements displaying number of votes for defence date attendance 
 */
class DefenceDateVoteCounts extends Widget
{
    const
        DATE = 'date';

    /** @var DefenceDate Defence date record */
    public $date;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $yes = $this->date->getCountVotes(DefenceAttendance::VOTE_YES);
        $no = $this->date->getCountVotes(DefenceAttendance::VOTE_NO);
        $unknown = $this->date->getCountVotes(DefenceAttendance::VOTE_UNKNOWN);
        $result = self::renderVotes($yes, $no, $unknown);
        return $result;
    }

    private static function renderVotes(int $yes, int $no, int $unknown) : string
    {
        $textYes = DefenceAttendance::getVoteLabel(DefenceAttendance::VOTE_YES);
        $textNo = DefenceAttendance::getVoteLabel(DefenceAttendance::VOTE_NO);
        $textUnknown = DefenceAttendance::getVoteLabel(DefenceAttendance::VOTE_UNKNOWN);

        $result = '<span class="badge" style="background-color: #5cb85c">' . $yes . ' ' . $textYes . '</span> ';
        $result .= '<span class="badge" style="background-color: #d9534f">' . $no . ' ' . $textNo . '</span> ';
        $result .= '<span class="badge">' . $unknown . ' ' . $textUnknown . '</span>';
        return $result;
    }
}