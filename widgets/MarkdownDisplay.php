<?php
namespace app\widgets;

use app\assets\MarkdownDisplayAsset;
use app\exceptions\BugError;
use app\utils\MarkdownRenderer;
use Yii;
use yii\helpers\Html;
use yii\jui\Widget;

/**
 * Renders markdown content to HTML.
 *
 * Class MarkdownDisplay
 * @package app\widgets
 */
class MarkdownDisplay extends Widget
{
    const
        CONTENT = 'markdownContent', CAN_EDIT = 'canEdit', FILE_PATH = 'filePath', FOOTER = 'footer', TITLE = 'title';

    /** @var bool True if user can edit displayed markdown text */
    public $canEdit;
    /** @var string Path to the file containing content */
    public $filePath;
    /** @var string */
    public $footer;
    /** @var string HTML generated content */
    public $markdownContent;
    /** @var string Title of the widget */
    public $title;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->title) {
            throw new BugError('title required');
        }
//        if (!($this->markdownContent xor $this->filePath)) {
//            throw new BugError('markdownContent or filePath should be used but not both');
//        }
        if ($this->markdownContent) {
            $this->markdownContent = MarkdownRenderer::render(Html::encode($this->markdownContent));
        }
        MarkdownDisplayAsset::register($this->getView());
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $result = "
        <div class=\"panel panel-default text-widget-show\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\" style=\"display: inline\">" . Html::encode($this->title) . "</h3>";
        if ($this->canEdit) {
            $result .= "<a href=\"javascript:void(0)\" title=\"" . Yii::t('app', 'Edit') . "\"><span
                class=\"markdown-edit glyphicon glyphicon-edit\" style=\"margin-left: 0.7em\"></span></a>";
        }
        $result .= "<span class='text-toggle pull-right' style='cursor: pointer; font-size: 10px'>
                    <span class=\"glyphicon glyphicon-triangle-left\"></span>
                    <span class=\"glyphicon glyphicon-triangle-bottom\" style='display: none'></span>
                </span>
            </div>
        <div class=\"text-show panel-body rendered-markdown\">";

        if ($this->filePath) {
            $result .= Html::a(Yii::t('app', 'Download'), $this->filePath);
        } else {
            $result .= $this->markdownContent;
        }

        $result .= "<div style='text-align: right; font-style: italic; color: darkgrey; position: relative; top: 7px;'>"
            . $this->footer
            . "</div>
            </div>
        </div>";

        return $result;
    }
}