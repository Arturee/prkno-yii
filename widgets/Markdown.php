<?php
namespace app\widgets;

use app\assets\MarkdownAsset;
use app\models\forms\MarkdownForm;
use app\models\records\Document;
use app\utils\DateTime;
use Yii;
use yii\jui\Widget;
use yii\widgets\ActiveForm;

/**
 * Must be wrapped in a Markdown form due to ActiveForm begin::
 *
 * Class Markdown
 * @package app\widgets
 */
class Markdown extends Widget
{
    const
        ACTIVE_FORM = 'activeForm', AUTHOR = 'author', MESSAGE = 'message', CAN_EDIT = 'canEdit', DOCUMENT = 'document', TITLE = 'title', TYPE = 'type', SUCCESS_BUTTON_FUNCTION = 'successFunction', TEMPLATE = 'template', CUSTOM_FORM_NAME = 'customFormName', FILE_UPLOAD_ENABLED = 'isFileUploadEnabled';

    /** @var  ActiveForm */
    public $activeForm;
    /** @var  \app\models\records\User */
    public $author;
    /** @var  string */
    public $message;
    /** @var  bool */
    public $canEdit;
    /** @var Document */
    public $document;
    /** @var  string */
    public $title;
    /** @var  string */
    public $type;
    /** @var  string */
    public $successFunction;
    /** @var  string */
    public $template = '';
    /** @var  string */
    public $customFormName;
    /** @var  bool */
    public $isFileUploadEnabled = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        MarkdownAsset::register($this->getView());
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $user = Yii::$app->user;
        $title = Yii::t('app', $this->title);
        $modelForm = new MarkdownForm();
        $modelForm->author_id = $user->id;

        if ($this->customFormName) {
            $modelForm->customFormName = $this->customFormName;
        }

        $result = '';

        if ($this->document) {
            // display
            $authorName = ($this->author ? $this->author->getFullNameWithDegrees() : 'author missing');

            $datetime = DateTime::from($this->document->updated_at)->format(DateTime::DATETIME_MINS);
            $footer = $authorName . ', ' . $datetime;

            $result = "<div class='markdown-view-edit'>";
            if ($this->document->content_md) {
                $result .= MarkdownDisplay::widget([
                    MarkdownDisplay::TITLE => $title,
                    MarkdownDisplay::FOOTER => $footer,
                    MarkdownDisplay::CAN_EDIT => $this->canEdit,
                    MarkdownDisplay::CONTENT => $this->document->content_md

                ]);
            } else {
                $result .= MarkdownDisplay::widget([
                    MarkdownDisplay::TITLE => $title,
                    MarkdownDisplay::FOOTER => $footer,
                    MarkdownDisplay::CAN_EDIT => $this->canEdit,
                    MarkdownDisplay::FILE_PATH => $this->document->fallback_path
                ]);
            }

            $modelForm->markdown = $this->document->content_md;
            $result .= MarkdownInput::widget([
                MarkdownInput::HIDDEN => true,
                MarkdownInput::TITLE => $title,
                MarkdownInput::MARKDOWN_FORM => $modelForm,
                MarkdownInput::ACTIVE_FORM => $this->activeForm,
                MarkdownInput::TYPE => $this->type,
                MarkdownInput::SUCCESS_BUTTON_FUNCTION => $this->successFunction,
                MarkdownInput::FILE_UPLOAD_ENABLED => $this->isFileUploadEnabled
            ]);
            $result .= "</div>";
        } else if ($this->canEdit) {
            // edit
            if ($this->message == null) {
                $modelForm->markdown = $this->template;
                $result .= MarkdownInput::widget([
                    MarkdownInput::TITLE => $title,
                    MarkdownInput::MARKDOWN_FORM => $modelForm,
                    MarkdownInput::ACTIVE_FORM => $this->activeForm,
                    MarkdownInput::TYPE => $this->type,
                    MarkdownInput::SUCCESS_BUTTON_FUNCTION => $this->successFunction,
                    MarkdownInput::FILE_UPLOAD_ENABLED => $this->isFileUploadEnabled
                ]);
            } else {
                $result .= MarkdownDisplay::widget([
                    MarkdownDisplay::TITLE => $title,
                    MarkdownDisplay::CAN_EDIT => false,
                    MarkdownDisplay::CONTENT => $this->message
                ]);
            }
        }

        return $result;
    }
}