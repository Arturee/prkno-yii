<?php
namespace app\widgets;

use app\assets\MarkdownInputAsset;
use app\assets\MarkdownPreviewAsset;
use app\exceptions\BugError;
use app\models\forms\MarkdownForm;
use Yii;
use yii\jui\Widget;
use yii\widgets\ActiveForm;

/**
 * Must be wrapped in a Markdown form due to ActiveForm begin::
 *
 * Class MarkdownInput
 * @package app\widgets
 */
class MarkdownInput extends Widget
{
    const
        HIDDEN = 'hidden',
        TITLE = 'title',
        MARKDOWN_FORM = 'form',
        TYPE = 'type',
        ACTIVE_FORM = 'activeForm',
        SUCCESS_BUTTON_FUNCTION = 'successFunction',
        FILE_UPLOAD_ENABLED = 'isFileUploadEnabled';

    /** @var bool True whether the input is hidden or not */
    public $hidden;
    /** @var string Label */
    public $title;
    /** @var MarkdownForm Markdown form*/
    public $form;
    /** @var ActiveForm Active form*/
    public $activeForm;
    /** @var string Input type */
    public $type;
    /** @var string */
    public $successFunction;
    /** @var bool */
    public $isFileUploadEnabled = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->title) {
            throw new BugError('title required');
        }
        if (!($this->form instanceof MarkdownForm)) {
            throw new BugError('form must be MarkdownForm');
        }
        if (!($this->activeForm instanceof ActiveForm)) {
            throw new BugError('activeForm must be ActiveForm');
        }

        MarkdownPreviewAsset::register($this->getView());
        MarkdownInputAsset::register($this->getView());
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $baseUrl = Yii::$app->homeUrl;
        $cancel = Yii::t('app', 'Clear');
        $upload = Yii::t('app', 'Upload file');

        $style = '';
        if ($this->hidden) {
            $style = "display: none";
        }

        return "
        <div class=\"panel panel-default text-widget\" style=\"" . $style . "\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\" style=\"display: inline\">" . $this->title . "</h3>"
            . $this->createUploadButton($upload) .
            "</div>
            <div class=\"panel-body\">
                <div class=\"md\">
                    <div class=\"form-group\" style=\"position: relative;\">"
            . $this->activeForm->field($this->form, 'markdown')->textarea([
            'class' => 'form-control markdown-content',
            'rows' => 5
        ])->label(false)
            . "<img src=\"" . $baseUrl . "img/md.png\"
                             style=\"width: 20px; position: absolute; left: -5px; top: -3px;\"
                             title=\"markdown\"
                        />
                    </div>
                </div>
                <div class=\"filename\"></div>"
            . $this->createSuccessButtons() .
            "<button type=\"button\" class=\"btn btn-danger pull-right cancel\">" . $cancel . "</button>
                <div class=\"upload\" style=\"display: none\">"
            . $this->createFileFields()
            . $this->activeForm->field($this->form, 'author_id')->hiddenInput()
            . "</div>
            </div>
        </div>";

        // type=button prevents submit
    }

    private function createSuccessButtons() : string
    {
        if ($this->successFunction === MarkdownForm::SUCCESS_BUTTON_SUBMIT) {
            $label = Yii::t('app', 'Save');
            return "<input type='submit' class=\"btn btn-success\" value=\" $label \"/>" .
                "&nbsp; &nbsp;" .
                $this->createMarkdownPreviewButton();
        } else {
            return $this->createMarkdownPreviewButton();
        }
    }

    private function createMarkdownPreviewButton() : string
    {
        return "<button type=\"button\" class=\"btn btn-info preview-button\">" . Yii::t('app', 'Markdown preview') . "</button>";
    }

    private function createUploadButton($upload) : string
    {
        if ($this->isFileUploadEnabled) {
            return "
                <span class=\"glyphicon glyphicon-folder-open pull-right open\"
                      style=\"color: #ffad33;  cursor: pointer\"
                      title=\"" . $upload . "\"
                ></span>
            ";
        } else {
            return "";
        }
    }

    private function createFileFields()
    {
        if ($this->isFileUploadEnabled) {
            return $this->activeForm->field($this->form, 'file')->fileInput(['multiple' => false, 'accept' => '*'])
                . $this->activeForm->field($this->form, 'type')->hiddenInput(['value' => $this->type]);
        } else {
            return "";
        }
    }
}


