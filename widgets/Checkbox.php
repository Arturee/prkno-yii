<?php
namespace app\widgets;

use app\exceptions\BugError;
use yii\helpers\Html;
use yii\jui\Widget;

/**
 * Widget implementing checkbox
 */
class Checkbox extends Widget
{
    const
        URL = 'url',
        IS_CHECKED = 'isChecked',
        TITLE = 'title',
        CONFIRMATION_MESSAGE = 'confirmationMessage',
        RELOAD_SELECTOR = 'reloadComponentSelector';

    /** @var string Url used for AJAX request in case of check/uncheck operations */
    public $url;

    /** @var bool True/false whether the checkbox is checked or not */
    public $isChecked;

    /** @var string Component selector for reloading */
    public $reloadComponentSelector;

    /** @var string Label */
    public $title = null;

    /** @var string Message in case of ajax request */
    public $confirmationMessage = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (is_null($this->isChecked)) {
            throw new BugError('$url and $isChecked are required');
        }
        $this->title = Html::encode($this->title);
        $this->confirmationMessage = Html::encode($this->confirmationMessage);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $class = "glyphicon glyphicon-" . ($this->isChecked ? 'ok' : 'unchecked');
        $html = '<span class="' . $class .'"></span>';
        $js = "$.ajax('" . $this->url . "', {
                    type: 'POST'
                }).done(function(data) {"
                . ($this->reloadComponentSelector ?
                    "$.pjax.reload({container: '" . $this->reloadComponentSelector . "'});"
                    : ""
                    )
                . "});";
        if ($this->confirmationMessage) {
            $js = "if (confirm('". $this->confirmationMessage ."')) {"
                . $js
                . "}";
        }
        $js .= " return false;"; // prevents default behavior of link (redirection)
        $result = Html::a($html, 'javascript:void(0)', [
            'title' => $this->title,
            'onclick' => $js
        ]);
        return $result;
    }
}