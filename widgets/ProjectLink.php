<?php
namespace app\widgets;

use app\exceptions\BugError;
use app\models\records\Project;
use yii\helpers\Html;
use yii\jui\Widget;

/**
 * Widget implementing link to the project
 */
class ProjectLink extends Widget
{
    const
        DISABLED = 'disabled',
        PROJECT = 'project',
        NAME = 'name',

        NAME_SHORT = 'short',
        NAME_LONG = 'long',
        NAME_BOTH = 'both';

    /** @var bool Whether the link is disabled or not */
    public $disabled;
    /** @var Project Project record */
    public $project;
    /** @var string Project name */
    public $name;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (!in_array($this->name, [self::NAME_SHORT, self::NAME_LONG, self::NAME_BOTH])) {
            throw new BugError('name must be in [\'short\', \'long\', \'both\']');
        }
        if (!($this->project instanceof Project)) {
            throw new BugError('project must be of type Project');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $name = Html::encode($this->project->name);
        $fullName = Html::encode($name . ' (' . $this->project->short_name . ')');
        if ($this->name === self::NAME_SHORT) {
            $name = $this->project->short_name;
        } else if ($this->name === self::NAME_BOTH) {
            $name = $fullName;
        }
        if ($this->disabled) {
            return "<span title='" . $fullName ."' style='color: lightgrey'>" . $name ."</span>";
        } else {
            return Html::a(
                $name,
                [ 'projects/view', 'id' => $this->project->id ],
                [
                    'title' => $fullName
                ]
            );
        }
    }
}