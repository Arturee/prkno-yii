<?php
namespace app\widgets;

use app\consts\Param;
use app\consts\Permission;
use app\models\records\User;
use Yii;
use yii\helpers\Html;
use yii\jui\Widget;

/**
 * Widget for link to the users
 */
class UserLinks extends Widget
{
    const
        USERS = 'users',
        DISABLE_LINKS = 'disableLinks';

    /** @var User[] array of users */
    public $users;
    /** @var boolean Whether link to the user has to be disabled or not */
    public $disableLinks;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (is_null($this->users)) {
            $this->users = [];
        } else if (!is_array($this->users)) {
            $this->users = [$this->users];
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (!Yii::$app->user->can(Permission::USERS_VIEW_NAME)) {
            return '';
        }

        $result = '';
        $lastIdx = sizeof($this->users) - 1;
        if ($lastIdx < 0) {
            return '';
        }
        for ($i = 0; $i < $lastIdx; $i++) {
            $result .= $this->link($this->users[$i]) . ', ';
        }
        $result .= $this->link($this->users[$lastIdx]);
        return $result;
    }

    private function link(User $user) : string
    {
        $name = $user->getNameWithEmail();
        $email = $user->email;

        if ($user->deleted) {
            return Html::tag(
                'span',
                htmlspecialchars(Param::USER_DELETED_USER)
            );
        }

        if ($this->disableLinks) {
            return Html::tag(
                'span',
                Html::encode($name),
                ['title' => $email]
            );
        } else {
            return Html::a(
                Html::encode($name),
                ['users/profile', 'id' => $user->id],
                ['title' => $email]
            );
        }
    }
}