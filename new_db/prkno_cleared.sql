-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2018 at 08:43 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prkno`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisement`
--

CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `type` enum('project','person') DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `subscribed_via_email` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advertisement`
--

INSERT INTO `advertisement` (`id`, `created_at`, `updated_at`, `deleted`, `user_id`, `title`, `description`, `type`, `keywords`, `subscribed_via_email`, `published`) VALUES
(1, '2018-06-11 08:37:36', '2018-07-11 08:38:18', 0, 5, 'Simulátor celulárního automatu', '# Cílem je vytvořit prostředí pro popis a simulaci celulárních automatů.\r\n\r\nPožadované vlastnosti:\r\n\r\n* zadávání stavů a přechodů mezi nimi pomocí \"typů\" buněk\r\n* tisk a export pravidel i konfigurace do různých formátů\r\n* schopnost efektivně provádět i velice rozsáhlé simulace (cca 10.000 x 10.000)\r\n* shopnost popsat a simulovat 2D i 3D automaty\r\n* representace závislá na rozměrech úlohy s cílem co nejvyšší rychlosti simulace\r\n* různé způsoby zobrazování (od textového režimu přes ikonky stavů až po mapu buňka=pixel)\r\n* bohaté způsoby sledování, krokování, breakpointů na změnu stavu, výskyt určitého stavu někde v zadané oblasti atd.\r\n* modulární řešení, důsledné oddělení Simulace od Zobrazování a Ladění\r\n* možnost popisovat komentáře k oblastem\r\n* vytvoření rozsáhlých modelů (\"obvody\" realizující logické funkce, (von Neumannův samoreplikující se stroj)\r\n\r\nPředpokládné prostředí: Windows  \r\nPředpokládaný počet lidí: 4-6 učitelů  \r\nPředpokládaná metodologie vývoje: Extrémní programování', 'project', 'simulation,cellular,automaton', 1, 1),
(2, '2018-04-10 05:18:57', '2018-06-16 09:59:29', 0, 10, 'Sprava dokumentu (DIS)', 'Abstrakt: Vetsina organizaci ma hiearchickou strukturu oddeleni. Tato oddeleni nabizeji sve dokumenty prostrednictvim svych informacnich systemu, pricemz zajistuji i odpovidajici vyhledavaci sluzby.  Projekt by mel realizovat reseni takoveho vyhledavajiciho systemu, ktery zajisti centralni vyhledavani s vyuzitim vyhledavajich sluzeb jednotlivych oddeleni.\r\n\r\nJazyk: Java\r\nOcekavany pocet resitelu: 4-5\r\n\r\nReference: [SM/II JAVA system](http://com-os2.ms.mff.cuni.cz/Perestroika/) - dokumentograficky informacni system v jazyce JAVA. ', 'project', 'dis,dokument,java', 1, 1),
(3, '2018-08-11 10:16:47', '2018-08-11 10:37:27', 0, 23, 'Looking for a team', 'Hi, I am a Chinese exchange student looking to join a team. I would communicated with the team members in English and the code and documentation would have to be written in English as well.\r\n\r\nI prefer the following programming languages:\r\n\r\n* C++\r\n* C#\r\n* Java\r\n\r\n*C++ is my favorite though.*\r\n\r\nI can work on the project from January to November 2019.\r\n\r\nPlease get in touch ASAP, I\'m looking forward to working with you!', 'person', 'java,c++,c#', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_comment`
--

CREATE TABLE `advertisement_comment` (
  `id` int(11) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advertisement_comment`
--

INSERT INTO `advertisement_comment` (`id`, `ad_id`, `user_id`, `content`, `created_at`, `updated_at`, `deleted`) VALUES
(8, 2, 1, 'Zajímavý návrh', '2018-09-11 19:43:14', '2018-09-11 19:43:14', 0),
(9, 3, 34, 'we may have something for you. write me an email.', '2018-08-28 19:43:59', '2018-08-28 19:43:59', 0),
(10, 3, 23, 'next please', '2018-10-30 19:45:05', '2018-10-30 19:45:05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

CREATE TABLE `audit` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `function_name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `comment` varchar(2000) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Table structure for table `defence_attendance`
--

CREATE TABLE `defence_attendance` (
  `id` int(11) NOT NULL,
  `defence_date_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vote` enum('yes','no','unknown') NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `must_come` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `defence_attendance`
--

INSERT INTO `defence_attendance` (`id`, `defence_date_id`, `user_id`, `vote`, `comment`, `must_come`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 1, 28, 'yes', 'Od 12:00', 0, '2018-11-07 22:34:39', '2018-11-07 22:36:57', 0),
(2, 1, 29, 'no', '', 0, '2018-11-07 22:34:39', '2018-11-07 22:36:10', 0),
(3, 1, 30, 'yes', '', 0, '2018-11-07 22:34:39', '2018-11-07 22:35:34', 0),
(4, 1, 1, 'yes', '', 0, '2018-11-07 22:34:39', '2018-11-07 22:34:52', 0),
(5, 1, 8, 'unknown', NULL, 0, '2018-11-07 22:34:39', '2018-11-07 22:34:39', 0),
(6, 2, 28, 'unknown', NULL, 1, '2018-11-07 22:51:08', '2018-11-10 17:59:37', 0),
(7, 2, 29, 'unknown', NULL, 1, '2018-11-07 22:51:08', '2018-11-10 17:59:38', 0),
(8, 2, 30, 'yes', '', 0, '2018-11-07 22:51:08', '2018-11-08 14:29:30', 0),
(9, 2, 1, 'yes', '', 1, '2018-11-07 22:51:09', '2018-11-10 17:59:39', 0),
(10, 2, 8, 'unknown', NULL, 0, '2018-11-07 22:51:09', '2018-11-07 22:51:09', 0),
(11, 3, 28, 'unknown', NULL, 0, '2018-11-08 13:55:40', '2018-11-08 13:55:40', 0),
(12, 3, 29, 'unknown', NULL, 0, '2018-11-08 13:55:40', '2018-11-08 13:55:40', 0),
(13, 3, 30, 'unknown', NULL, 0, '2018-11-08 13:55:40', '2018-11-08 13:55:40', 0),
(14, 3, 1, 'yes', '', 0, '2018-11-08 13:55:40', '2018-11-08 13:55:46', 0),
(15, 3, 8, 'unknown', NULL, 0, '2018-11-08 13:55:40', '2018-11-08 13:55:40', 0),
(16, 4, 28, 'no', 'zubař', 0, '2018-11-12 20:33:00', '2018-11-12 20:34:35', 0),
(17, 4, 29, 'unknown', NULL, 0, '2018-11-12 20:33:00', '2018-11-12 20:33:00', 0),
(18, 4, 30, 'yes', '', 0, '2018-11-12 20:33:00', '2018-11-12 20:34:57', 0),
(19, 4, 1, 'yes', 'já musím', 1, '2018-11-12 20:33:00', '2018-11-12 20:33:46', 0),
(20, 4, 8, 'unknown', NULL, 0, '2018-11-12 20:33:00', '2018-11-12 20:33:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `defence_date`
--

CREATE TABLE `defence_date` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  `room` varchar(200) DEFAULT '',
  `signup_deadline` date NOT NULL,
  `active_voting` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `defence_date`
--

INSERT INTO `defence_date` (`id`, `created_at`, `updated_at`, `deleted`, `date`, `room`, `signup_deadline`, `active_voting`) VALUES
(1, '2018-11-07 22:34:39', '2018-11-07 22:38:36', 0, '2018-12-16', '310', '2018-12-10', 0),
(2, '2018-11-07 22:51:08', '2018-11-08 14:30:16', 0, '2018-11-14', 'S5', '2018-11-13', 0),
(3, '2018-11-08 13:55:40', '2018-11-08 13:56:14', 0, '2018-10-23', '312', '2018-11-14', 0),
(4, '2018-11-12 20:33:00', '2018-11-12 20:33:00', 0, '2018-11-23', '', '2018-11-22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `content_md` text,
  `fallback_path` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `created_at`, `updated_at`, `deleted`, `content_md`, `fallback_path`) VALUES
(117, '2018-10-30 19:49:31', '2018-10-30 19:49:31', 0, '# Pravidla pro Projekty — předmět NPRG023\r\nTato pravidla vymezují obecná pravidla pro vypisování, vypracování a obhajoby projektů. Komise může v duchu těchto pravidel vydat jejich upřesnění.\r\n\r\n# 1 Cíle předmětu\r\nHlavním cílem předmětu Projekt NPRG023 je osvojení si kolektivní práce nad větším softwarovým dílem. Práce na projektu by se měla podle možností co nejvíce přiblížit reálným požadavkům na vývoj software a zahrnovat základní životní cyklus softwarového díla - sběr požadavků, specifikaci, implementaci, testování, dokumentaci a odevzdání.\r\n\r\n# 2 Komise pro obhajoby projektů\r\nKomise koordinuje veškeré záležitosti týkající se tohoto předmětu, zejména:\r\n\r\nSchvaluje projekty před jejich vypsáním\r\n\r\nOrganizuje vypisování projektů a schvaluje zahájení práce na projektu\r\n\r\nOrganizuje obhajoby\r\n\r\nUrčuje oponenta projektu\r\n\r\nNa základě odevzdaného díla, posudku vedoucího a průběhu obhajoby hlasováním rozhoduje o obhájení či neobhájení projektu a případné možnosti udělení bodů navíc za mimořádně kvalitní projekty\r\n\r\nSchvaluje případný návrh vedoucího na vyloučení studenta z řešitelského kolektivu\r\n\r\nVydává dokumenty upřesňující realizaci těchto pravidel.\r\n\r\nVe sporných případech má komise právo rozhodnout.\r\n\r\nKomise je usnášeníschopná, pokud je přítomna nadpoloviční většina jejich členů.\r\n\r\n# 3 Vedoucí projektu\r\nKaždý Projekt je veden vedoucím. Obvykle jde o pracovníka MFF UK, případně pracovníky jiných fakult, univerzit či ústavů, je však vítána i účast pracovníků softwarových firem. Pokud vede externí vedoucí projekt poprvé, určí komise zpravidla konzultanta z řad pracovníků MFF, který práci na projektu sleduje. Jeho úkolem je zajistit, aby projekt splňoval kritéria na projekty kladená.\r\n\r\nVedoucí má povinnost určit role a úkoly jednotlivých členů řešitelského týmu. Pokud některý z členů týmu soustavně neplní své povinnosti, může navrhnout komisi vyloučení tohoto člena z týmu. Vedoucí doporučuje či nedoporučuje projekt k obhajobě.\r\n\r\n# 4 Řešitelský kolektiv\r\nPočet řešitelů Projektu musí být úměrný rozsahu a předpokládané náročnosti Projektu, typický počet řešitelů jednoho projektu je 5. Minimální počet členů kolektivu je 4, maximální počet není stanoven, více než 8 členů by však řešitelský kolektiv měl mít jen výjimečně.\r\n\r\n# 5 Doba pro vypracování projektu\r\nSoučástí návrhu projektu předkládaného Komisi ke schválení je termín dokončení. Standardní doba pro vypracování Projektu je sedm až osm měsíců. Projekt musí být odevzdán nejpozději následující pracovní den po uplynutí schválené doby trvání projektu (pokud není včas odevzdán, je automaticky považován za neobhajený). Pokud na obhajobě po odevzdání není projekt obhájen, Komise určí nový termín pro odevzdání přepracovaného projektu. Standardní doba prodloužení je tři měsíce. Pokud ani po prodloužení není projekt obhájen, další prodloužení není možné a projekt je definitivně neobhájen.\r\n\r\nVedoucí projektu může ve výjimečných případech (dlouhodobá nemoc, odchod člena týmu apod.) komisi požádat o posunutí termínu dokončení.\r\n\r\n# 6 Obhajoby projektů\r\nProjekt končí obhajobou. Studenti splní studijní povinnost a získají příslušné body pouze za úspěšně obhájený projekt. Termíny obhajob vypisuje Komise s ohledem na projekty, kterým se blíží termín odevzdání. Obvykle se obhajoba koná cca 14 dnů po odevzdání projektu.\r\n\r\n# 6.1 Bodové hodnocení studentů odborného studia\r\nStandardní bodové ohodnocení projektů odborného studia je 10 bodů resp. 15 kreditů.\r\n\r\nPokud probíhají práce na projektu alespoň jeden semestr, může vedoucí projektu udělit členům řešitelského kolektivu zálohové body ve výši 4 bodů resp. 6 kreditů. O udělené zálohové body se snižuje bodové ohodnocení po úspěšném obhájení projektu. Pokud projekt není obhájen, snižuje se o tyto body ohodnocení nového projektu, který student řeší.\r\n\r\nU zvláště dobrých projektů může komise povolit vedoucímu projektu udělit těm členům řešitelského kolektivu, kteří se o úspěch nejvíce zasloužili, 2 body resp. 3 kredity navíc.\r\n\r\n# 7 Výjimky\r\nV odůvodněných případech může komise schválit i projekt, který nesplňuje některá ustanovení těchto pravidel; v takovém případě však musí být v rozhodnutí komise výslovně uvedeno, která ustanovení pravidel se projektu netýkají, případně která další pravidla pro takový projekt stanoví navíc.\r\n\r\n# 8 Užití vytvořeného softwarového díla\r\nUžití vytvořeného softwarového díla se řídí platnými ustanoveními Autorského zákona v posledním platném znění. Na vytvořené softwarové dílo se přitom hledí jako na školní dílo ve smyslu § 60 zákona 121/2000 Sb. (Autorský zákon).\r\nAutoři projektu nemají povinnost dílo po obhajobě doplňovat či jinak udržovat.', NULL),
(118, '2018-10-30 19:49:31', '2018-10-30 19:49:31', 0, 'Rules for the Project — NPRG023 course\r\nThis document defines a common rules for preparing, implementing and defending the projects. The committee can issue additional clarifications of thee rules.\r\n\r\n# 1 Goals of the course\r\nThe main goal of the NPRG023 course is to acquire principles of team-work on a bigger software project. Work on the project should as much as possible follow the real software development and cover the basic lifecycle of a software project, i.e., requirements collection, preparing a specification, implementation, testing, documentation creation and final delivery.\r\n\r\n# 2 Projects committee\r\nThe committee controls all arrangements of the course. Namely:\r\n\r\nApproves the projects topics.\r\n\r\nCommences the projects.\r\n\r\nArranges the projects defences.\r\n\r\nAssigns a reviewer for the project.\r\n\r\nBased on the delivered projects and reviews of the reviewer and supervisor, votes about the project acceptance or rejection. Plus, the committee can award excellent project teams with additional credits.\r\n\r\nApproves a project supervisor\'s proposal for a student exclusion from the project team\r\n\r\nIssues documents that refine these rules\r\n\r\nHas rights to decide all the issues\r\n\r\nThe committee is quorate when its majority is present.\r\n\r\n# 3 Project supervisor\r\nEach project has its supervisor. Typically, a supervisor is an employee of the faculty/university or other universities/institutes. Nevertheless, external supervisors from software companies are strongly welcomed. In the case, an external supervisor supervises a project for the first time, the committee may select a consultant (which is the faculty employee) that helps with supervising the project and ensures the project satisfies all necessary criteria.\r\n\r\nThe supervisor assigns roles and tasks of individual team members. In the case, a team member systematically does not fulfill his/her duties, the supervisor can propose his/her exclusion from the team. The supervisor recommends (or does not recommend) the project fo the review.\r\n\r\n# 4 Project team\r\nThe number of teh project team members has to correspond with size and expected complexity of the project. A common number of the team members is 5. The minimum number of the team members is 4. The maximum number of team members is not set, however it should not be over 8 (only in exceptional cases).\r\n\r\n# 5 Working on the project\r\nA part of the project proposal is an expected time of the project finalization. A common duration of the project is from 7 till 8 months. The project has to be submitted for the review at latest on the following working day after the approved project duration time expires (if it is not submitted on time, it is considered as not successfully reviewed). If the project is not successfully reviewed, the project committee specifies a new deadline to resubmit an updated version of the project. A common extension is 3 months. In a cases, the project is not successfully reviewed even after the extension, another extension is not allowed and the project is left as unsuccessful.\r\n\r\nIn exceptional cases (e.g., long-term disease fo a team member, withdrawal of a team members), the project supervisor can ask the committee for deadline extension.\r\n\r\n# 6 Project review\r\nThe project is concluded by the review. The team members obtains the credits only after the project is successfully reviewed. Dates of the project reviews are set by the committee (with respect to ongoing projects). Typically, the review take place 14 days after the project submission.\r\n\r\n# 6.1 Credits\r\nFor successfully reviewed projects, a common number of awarded credits is 15.\r\n\r\nIf the project actively runs for at least one semester, the supervisor can award 6 credits in advance. In such a case, the final amount of awarded credits is reduced by these in advance awarded credits. In a case the project is not successfully reviewed, the final amount of awarded credits of a new project is educed by these in advance awarded credits.\r\n\r\nFor extra good projects, the committee can allow the supervisor to award additional 3 credits to certain members of the project team.\r\n\r\n# 7 Exceptions\r\nIn reasonable cases, the committee can approve a project, which does not conform with some of these rules. In such a case, the committee has to explicitly state, which rules are omitted and/or which new ones are added.\r\n\r\n# 8 Usage of the resulting software\r\nUsage of the resulting software conforms with all the regulation of the Author\'s law in its latest version. The resulting software is a school work as define in § 60 of the law 121/2000 Sb. (the Author\'s law).\r\nAfter the project end, the team members do not have any obligation to extend the project or to maintain it.', NULL),
(119, '2018-10-30 19:50:11', '2018-10-30 19:50:11', 0, '# Pravidla pro Projekty — předmět NPRG023\r\nTato pravidla vymezují obecná pravidla pro vypisování, vypracování a obhajoby projektů. Komise může v duchu těchto pravidel vydat jejich upřesnění.\r\n\r\n# 1 Cíle předmětu\r\nHlavním cílem předmětu Projekt NPRG023 je osvojení si kolektivní práce nad větším softwarovým dílem. Práce na projektu by se měla podle možností co nejvíce přiblížit reálným požadavkům na vývoj software a zahrnovat základní životní cyklus softwarového díla - sběr požadavků, specifikaci, implementaci, testování, dokumentaci a odevzdání.\r\n\r\n# 2 Komise pro obhajoby projektů\r\nKomise koordinuje veškeré záležitosti týkající se tohoto předmětu, zejména:\r\n\r\nSchvaluje projekty před jejich vypsáním\r\n\r\nOrganizuje vypisování projektů a schvaluje zahájení práce na projektu\r\n\r\nOrganizuje obhajoby\r\n\r\nUrčuje oponenta projektu\r\n\r\nNa základě odevzdaného díla, posudku vedoucího a průběhu obhajoby hlasováním rozhoduje o obhájení či neobhájení projektu a případné možnosti udělení bodů navíc za mimořádně kvalitní projekty\r\n\r\nSchvaluje případný návrh vedoucího na vyloučení studenta z řešitelského kolektivu\r\n\r\nVydává dokumenty upřesňující realizaci těchto pravidel.\r\n\r\nVe sporných případech má komise právo rozhodnout.\r\n\r\nKomise je usnášeníschopná, pokud je přítomna nadpoloviční většina jejich členů.\r\n\r\n# 3 Vedoucí projektu\r\nKaždý Projekt je veden vedoucím. Obvykle jde o pracovníka MFF UK, případně pracovníky jiných fakult, univerzit či ústavů, je však vítána i účast pracovníků softwarových firem. Pokud vede externí vedoucí projekt poprvé, určí komise zpravidla konzultanta z řad pracovníků MFF, který práci na projektu sleduje. Jeho úkolem je zajistit, aby projekt splňoval kritéria na projekty kladená.\r\n\r\nVedoucí má povinnost určit role a úkoly jednotlivých členů řešitelského týmu. Pokud některý z členů týmu soustavně neplní své povinnosti, může navrhnout komisi vyloučení tohoto člena z týmu. Vedoucí doporučuje či nedoporučuje projekt k obhajobě.\r\n\r\n# 4 Řešitelský kolektiv\r\nPočet řešitelů Projektu musí být úměrný rozsahu a předpokládané náročnosti Projektu, typický počet řešitelů jednoho projektu je 5. Minimální počet členů kolektivu je 4, maximální počet není stanoven, více než 8 členů by však řešitelský kolektiv měl mít jen výjimečně.\r\n\r\n# 5 Doba pro vypracování projektu\r\nSoučástí návrhu projektu předkládaného Komisi ke schválení je termín dokončení. Standardní doba pro vypracování Projektu je sedm až osm měsíců. Projekt musí být odevzdán nejpozději následující pracovní den po uplynutí schválené doby trvání projektu (pokud není včas odevzdán, je automaticky považován za neobhajený). Pokud na obhajobě po odevzdání není projekt obhájen, Komise určí nový termín pro odevzdání přepracovaného projektu. Standardní doba prodloužení je tři měsíce. Pokud ani po prodloužení není projekt obhájen, další prodloužení není možné a projekt je definitivně neobhájen.\r\n\r\nVedoucí projektu může ve výjimečných případech (dlouhodobá nemoc, odchod člena týmu apod.) komisi požádat o posunutí termínu dokončení.\r\n\r\n# 6 Obhajoby projektů\r\nProjekt končí obhajobou. Studenti splní studijní povinnost a získají příslušné body pouze za úspěšně obhájený projekt. Termíny obhajob vypisuje Komise s ohledem na projekty, kterým se blíží termín odevzdání. Obvykle se obhajoba koná cca 14 dnů po odevzdání projektu.\r\n\r\n# 6.1 Bodové hodnocení studentů odborného studia\r\nStandardní bodové ohodnocení projektů odborného studia je 10 bodů resp. 15 kreditů.\r\n\r\nPokud probíhají práce na projektu alespoň jeden semestr, může vedoucí projektu udělit členům řešitelského kolektivu zálohové body ve výši 4 bodů resp. 6 kreditů. O udělené zálohové body se snižuje bodové ohodnocení po úspěšném obhájení projektu. Pokud projekt není obhájen, snižuje se o tyto body ohodnocení nového projektu, který student řeší.\r\n\r\nU zvláště dobrých projektů může komise povolit vedoucímu projektu udělit těm členům řešitelského kolektivu, kteří se o úspěch nejvíce zasloužili, 2 body resp. 3 kredity navíc.\r\n\r\n# 7 Výjimky\r\nV odůvodněných případech může komise schválit i projekt, který nesplňuje některá ustanovení těchto pravidel; v takovém případě však musí být v rozhodnutí komise výslovně uvedeno, která ustanovení pravidel se projektu netýkají, případně která další pravidla pro takový projekt stanoví navíc.\r\n', NULL),
(120, '2018-10-30 19:50:11', '2018-10-30 19:50:11', 0, 'Rules for the Project — NPRG023 course\r\nThis document defines a common rules for preparing, implementing and defending the projects. The committee can issue additional clarifications of thee rules.\r\n\r\n# 1 Goals of the course\r\nThe main goal of the NPRG023 course is to acquire principles of team-work on a bigger software project. Work on the project should as much as possible follow the real software development and cover the basic lifecycle of a software project, i.e., requirements collection, preparing a specification, implementation, testing, documentation creation and final delivery.\r\n\r\n# 2 Projects committee\r\nThe committee controls all arrangements of the course. Namely:\r\n\r\nApproves the projects topics.\r\n\r\nCommences the projects.\r\n\r\nArranges the projects defences.\r\n\r\nAssigns a reviewer for the project.\r\n\r\nBased on the delivered projects and reviews of the reviewer and supervisor, votes about the project acceptance or rejection. Plus, the committee can award excellent project teams with additional credits.\r\n\r\nApproves a project supervisor\'s proposal for a student exclusion from the project team\r\n\r\nIssues documents that refine these rules\r\n\r\nHas rights to decide all the issues\r\n\r\nThe committee is quorate when its majority is present.\r\n\r\n# 3 Project supervisor\r\nEach project has its supervisor. Typically, a supervisor is an employee of the faculty/university or other universities/institutes. Nevertheless, external supervisors from software companies are strongly welcomed. In the case, an external supervisor supervises a project for the first time, the committee may select a consultant (which is the faculty employee) that helps with supervising the project and ensures the project satisfies all necessary criteria.\r\n\r\nThe supervisor assigns roles and tasks of individual team members. In the case, a team member systematically does not fulfill his/her duties, the supervisor can propose his/her exclusion from the team. The supervisor recommends (or does not recommend) the project fo the review.\r\n\r\n# 4 Project team\r\nThe number of teh project team members has to correspond with size and expected complexity of the project. A common number of the team members is 5. The minimum number of the team members is 4. The maximum number of team members is not set, however it should not be over 8 (only in exceptional cases).\r\n\r\n# 5 Working on the project\r\nA part of the project proposal is an expected time of the project finalization. A common duration of the project is from 7 till 8 months. The project has to be submitted for the review at latest on the following working day after the approved project duration time expires (if it is not submitted on time, it is considered as not successfully reviewed). If the project is not successfully reviewed, the project committee specifies a new deadline to resubmit an updated version of the project. A common extension is 3 months. In a cases, the project is not successfully reviewed even after the extension, another extension is not allowed and the project is left as unsuccessful.\r\n\r\nIn exceptional cases (e.g., long-term disease fo a team member, withdrawal of a team members), the project supervisor can ask the committee for deadline extension.\r\n\r\n# 6 Project review\r\nThe project is concluded by the review. The team members obtains the credits only after the project is successfully reviewed. Dates of the project reviews are set by the committee (with respect to ongoing projects). Typically, the review take place 14 days after the project submission.\r\n\r\n# 6.1 Credits\r\nFor successfully reviewed projects, a common number of awarded credits is 15.\r\n\r\nIf the project actively runs for at least one semester, the supervisor can award 6 credits in advance. In such a case, the final amount of awarded credits is reduced by these in advance awarded credits. In a case the project is not successfully reviewed, the final amount of awarded credits of a new project is educed by these in advance awarded credits.\r\n\r\nFor extra good projects, the committee can allow the supervisor to award additional 3 credits to certain members of the project team.\r\n\r\n# 7 Exceptions\r\nIn reasonable cases, the committee can approve a project, which does not conform with some of these rules. In such a case, the committee has to explicitly state, which rules are omitted and/or which new ones are added.\r\n', NULL),
(121, '2018-10-30 19:50:36', '2018-10-30 19:50:36', 0, '# Pravidla pro Projekty — předmět NPRG023\r\nTato pravidla vymezují obecná pravidla pro vypisování, vypracování a obhajoby projektů. Komise může v duchu těchto pravidel vydat jejich upřesnění.\r\n\r\n# 1 Cíle předmětu\r\nHlavním cílem předmětu Projekt NPRG023 je osvojení si kolektivní práce nad větším softwarovým dílem. Práce na projektu by se měla podle možností co nejvíce přiblížit reálným požadavkům na vývoj software a zahrnovat základní životní cyklus softwarového díla - sběr požadavků, specifikaci, implementaci, testování, dokumentaci a odevzdání.\r\n\r\n# 2 Komise pro obhajoby projektů\r\nKomise koordinuje veškeré záležitosti týkající se tohoto předmětu, zejména:\r\n\r\nSchvaluje projekty před jejich vypsáním\r\n\r\nOrganizuje vypisování projektů a schvaluje zahájení práce na projektu\r\n\r\nOrganizuje obhajoby\r\n\r\nUrčuje oponenta projektu\r\n\r\nNa základě odevzdaného díla, posudku vedoucího a průběhu obhajoby hlasováním rozhoduje o obhájení či neobhájení projektu a případné možnosti udělení bodů navíc za mimořádně kvalitní projekty\r\n\r\nSchvaluje případný návrh vedoucího na vyloučení studenta z řešitelského kolektivu\r\n\r\nVydává dokumenty upřesňující realizaci těchto pravidel.\r\n\r\nVe sporných případech má komise právo rozhodnout.\r\n\r\nKomise je usnášeníschopná, pokud je přítomna nadpoloviční většina jejich členů.\r\n\r\n# 3 Vedoucí projektu\r\nKaždý Projekt je veden vedoucím. Obvykle jde o pracovníka MFF UK, případně pracovníky jiných fakult, univerzit či ústavů, je však vítána i účast pracovníků softwarových firem. Pokud vede externí vedoucí projekt poprvé, určí komise zpravidla konzultanta z řad pracovníků MFF, který práci na projektu sleduje. Jeho úkolem je zajistit, aby projekt splňoval kritéria na projekty kladená.\r\n\r\nVedoucí má povinnost určit role a úkoly jednotlivých členů řešitelského týmu. Pokud některý z členů týmu soustavně neplní své povinnosti, může navrhnout komisi vyloučení tohoto člena z týmu. Vedoucí doporučuje či nedoporučuje projekt k obhajobě.\r\n\r\n# 4 Řešitelský kolektiv\r\nPočet řešitelů Projektu musí být úměrný rozsahu a předpokládané náročnosti Projektu, typický počet řešitelů jednoho projektu je 5. Minimální počet členů kolektivu je 4, maximální počet není stanoven, více než 8 členů by však řešitelský kolektiv měl mít jen výjimečně.\r\n\r\n# 5 Doba pro vypracování projektu\r\nSoučástí návrhu projektu předkládaného Komisi ke schválení je termín dokončení. Standardní doba pro vypracování Projektu je sedm až osm měsíců. Projekt musí být odevzdán nejpozději následující pracovní den po uplynutí schválené doby trvání projektu (pokud není včas odevzdán, je automaticky považován za neobhajený). Pokud na obhajobě po odevzdání není projekt obhájen, Komise určí nový termín pro odevzdání přepracovaného projektu. Standardní doba prodloužení je tři měsíce. Pokud ani po prodloužení není projekt obhájen, další prodloužení není možné a projekt je definitivně neobhájen.\r\n\r\nVedoucí projektu může ve výjimečných případech (dlouhodobá nemoc, odchod člena týmu apod.) komisi požádat o posunutí termínu dokončení.\r\n\r\n# 6 Obhajoby projektů\r\nProjekt končí obhajobou. Studenti splní studijní povinnost a získají příslušné body pouze za úspěšně obhájený projekt. Termíny obhajob vypisuje Komise s ohledem na projekty, kterým se blíží termín odevzdání. Obvykle se obhajoba koná cca 14 dnů po odevzdání projektu.\r\n\r\n# 6.1 Bodové hodnocení studentů odborného studia\r\nStandardní bodové ohodnocení projektů odborného studia je 10 bodů resp. 15 kreditů.\r\n\r\nPokud probíhají práce na projektu alespoň jeden semestr, může vedoucí projektu udělit členům řešitelského kolektivu zálohové body ve výši 4 bodů resp. 6 kreditů. O udělené zálohové body se snižuje bodové ohodnocení po úspěšném obhájení projektu. Pokud projekt není obhájen, snižuje se o tyto body ohodnocení nového projektu, který student řeší.\r\n\r\nU zvláště dobrých projektů může komise povolit vedoucímu projektu udělit těm členům řešitelského kolektivu, kteří se o úspěch nejvíce zasloužili, 2 body resp. 3 kredity navíc.\r\n\r\n', NULL),
(122, '2018-10-30 19:50:36', '2018-10-30 19:50:36', 0, 'Rules for the Project — NPRG023 course\r\nThis document defines a common rules for preparing, implementing and defending the projects. The committee can issue additional clarifications of thee rules.\r\n\r\n# 1 Goals of the course\r\nThe main goal of the NPRG023 course is to acquire principles of team-work on a bigger software project. Work on the project should as much as possible follow the real software development and cover the basic lifecycle of a software project, i.e., requirements collection, preparing a specification, implementation, testing, documentation creation and final delivery.\r\n\r\n# 2 Projects committee\r\nThe committee controls all arrangements of the course. Namely:\r\n\r\nApproves the projects topics.\r\n\r\nCommences the projects.\r\n\r\nArranges the projects defences.\r\n\r\nAssigns a reviewer for the project.\r\n\r\nBased on the delivered projects and reviews of the reviewer and supervisor, votes about the project acceptance or rejection. Plus, the committee can award excellent project teams with additional credits.\r\n\r\nApproves a project supervisor\'s proposal for a student exclusion from the project team\r\n\r\nIssues documents that refine these rules\r\n\r\nHas rights to decide all the issues\r\n\r\nThe committee is quorate when its majority is present.\r\n\r\n# 3 Project supervisor\r\nEach project has its supervisor. Typically, a supervisor is an employee of the faculty/university or other universities/institutes. Nevertheless, external supervisors from software companies are strongly welcomed. In the case, an external supervisor supervises a project for the first time, the committee may select a consultant (which is the faculty employee) that helps with supervising the project and ensures the project satisfies all necessary criteria.\r\n\r\nThe supervisor assigns roles and tasks of individual team members. In the case, a team member systematically does not fulfill his/her duties, the supervisor can propose his/her exclusion from the team. The supervisor recommends (or does not recommend) the project fo the review.\r\n\r\n# 4 Project team\r\nThe number of teh project team members has to correspond with size and expected complexity of the project. A common number of the team members is 5. The minimum number of the team members is 4. The maximum number of team members is not set, however it should not be over 8 (only in exceptional cases).\r\n\r\n# 5 Working on the project\r\nA part of the project proposal is an expected time of the project finalization. A common duration of the project is from 7 till 8 months. The project has to be submitted for the review at latest on the following working day after the approved project duration time expires (if it is not submitted on time, it is considered as not successfully reviewed). If the project is not successfully reviewed, the project committee specifies a new deadline to resubmit an updated version of the project. A common extension is 3 months. In a cases, the project is not successfully reviewed even after the extension, another extension is not allowed and the project is left as unsuccessful.\r\n\r\nIn exceptional cases (e.g., long-term disease fo a team member, withdrawal of a team members), the project supervisor can ask the committee for deadline extension.\r\n\r\n# 6 Project review\r\nThe project is concluded by the review. The team members obtains the credits only after the project is successfully reviewed. Dates of the project reviews are set by the committee (with respect to ongoing projects). Typically, the review take place 14 days after the project submission.\r\n\r\n# 6.1 Credits\r\nFor successfully reviewed projects, a common number of awarded credits is 15.\r\n\r\nIf the project actively runs for at least one semester, the supervisor can award 6 credits in advance. In such a case, the final amount of awarded credits is reduced by these in advance awarded credits. In a case the project is not successfully reviewed, the final amount of awarded credits of a new project is educed by these in advance awarded credits.\r\n\r\nFor extra good projects, the committee can allow the supervisor to award additional 3 credits to certain members of the project team.\r\n\r\n', NULL),
(123, '2018-10-30 19:51:50', '2018-10-30 19:51:50', 0, 'David Bednárek\r\nTomáš Bureš\r\nJakub Gemrot\r\nPetr Hnětynka - předseda komise\r\nTomáš Holan - tajemník komise \r\nPavel Ježek\r\nMartin Kruliš\r\nVladislav Kuboň\r\nDan Lukeš\r\nFrantišek Mráz\r\nDavid Obdržálek\r\nFilip Zavoral', NULL),
(124, '2018-10-30 19:51:50', '2018-10-30 19:51:50', 0, 'David Bednárek\r\nTomáš Bureš\r\nJakub Gemrot\r\nPetr Hnětynka - chair\r\nTomáš Holan - secretary of the commission\r\nPavel Ježek\r\nMartin Kruliš\r\nVladislav Kuboň\r\nDan Lukeš\r\nFrantišek Mráz\r\nDavid Obdržálek\r\nFilip Zavoral', NULL),
(125, '2018-10-30 19:52:33', '2018-10-30 19:52:33', 0, 'David Bednárek\r\n\r\nTomáš Bureš\r\n\r\nJakub Gemrot\r\n\r\nPetr Hnětynka - předseda komise\r\n\r\nTomáš Holan - tajemník komise \r\n\r\nPavel Ježek\r\n\r\nMartin Kruliš\r\n\r\nVladislav Kuboň\r\n\r\nDan Lukeš\r\n\r\nFrantišek Mráz\r\n\r\nDavid Obdržálek\r\n\r\nFilip Zavoral', NULL),
(126, '2018-10-30 19:52:33', '2018-10-30 19:52:33', 0, 'David Bednárek\r\n\r\nTomáš Bureš\r\n\r\nJakub Gemrot\r\n\r\nPetr Hnětynka - chair\r\n\r\nTomáš Holan - secretary of the commission\r\n\r\nPavel Ježek\r\n\r\nMartin Kruliš\r\n\r\nVladislav Kuboň\r\n\r\nDan Lukeš\r\n\r\nFrantišek Mráz\r\n\r\nDavid Obdržálek\r\n\r\nFilip Zavoral', NULL),
(127, '2018-11-07 14:17:58', '2018-11-10 17:30:31', 0, 'Študentská wiki na zbieranie nápadov, tímovú prácu atď. \r\nPoužijeme **GIT** na verzovací systém. ', NULL),
(128, '2018-11-07 14:17:58', '2018-11-10 17:30:31', 0, '# Motivace\r\n*...krátký popis, proč by měl projekt vzniknout...*\r\n\r\n# Popis projektu\r\n*...popis funkčnosti, UI, atd....*\r\n\r\n# Platforma, technologie\r\n*...cílová platforma projektu; využité technologie, framework, apod. (pokud jsou již známy)...*\r\n\r\n# Odhad náročnosti\r\n*… počet řešitelů, případně i s odůvodněním (přibližné přidělení řešitelů jednotlivým částem projektu),\r\n termín dokončení, přibližný plán prací...*\r\n\r\n# Vymezení projektu\r\n\r\n**Diskrétní modely a algoritmy**\r\n\r\n- [x ] diskrétní matematika a algoritmy\r\n- [ ] geometrie a matematické struktury v informatice\r\n- [ ] optimalizace\r\n\r\n**Teoretická informatika**\r\n\r\n- [ ] Teoretická informatika\r\n\r\n**Softwarové a datové inženýrství**\r\n\r\n- [ ] softwarové inženýrství\r\n- [ ] vývoj software\r\n- [ ] webové inženýrství\r\n- [ ] databázové systémy\r\n- [ ] analýza a zpracování rozsáhlých dat\r\n\r\n**Softwarové systémy**\r\n\r\n- [ ] systémové programování\r\n- [ ] spolehlivé systémy\r\n- [ ] výkonné systémy\r\n\r\n**Matematická lingvistika**\r\n\r\n- [ ] počítačová a formální lingvistika\r\n- [ ] statistické metody a strojové učení v počítačové lingvistice\r\n\r\n**Umělá inteligence**\r\n\r\n- [ ] inteligentní agenti\r\n- [ ] strojové učení\r\n- [ ] robotika\r\n\r\n**Počítačová grafika a vývoj počítačových her**\r\n\r\n- [ ] počítačová grafika\r\n- [ ] vývoj počítačových her\r\n\r\n# Poznámky\r\n*... další informace nezapadající do sekcí výše...*\r\n', NULL),
(129, '2018-11-07 22:00:23', '2018-11-07 22:00:23', 0, 'zbierame zkušenosti', NULL),
(130, '2018-11-07 22:07:44', '2018-11-07 22:07:44', 0, NULL, '/files/download?bucket=projectUploads&filename=22%2Fuploads%2Fdoc_project_private.txt'),
(131, '2018-11-07 22:08:38', '2018-11-07 22:08:38', 0, NULL, '/files/download?bucket=projectUploads&filename=22%2Fuploads%2FUser_manual.txt'),
(132, '2018-11-07 22:09:21', '2018-11-07 22:09:21', 0, NULL, '/files/download?bucket=projectUploads&filename=22%2Fuploads%2FDEFENCE_PDF_1.PDF'),
(133, '2018-11-07 22:42:38', '2018-11-07 22:42:38', 0, '<small>\r\nPosudok Oponenta\r\nLegenda:\r\n\r\n* **lepší** *označuje splnění nad rámec obvyklého softwarového projektu*\r\n* **OK** *označuje práci, která kritérium vhodným způsobem splňuje*\r\n* **horší** *označuje splnění pod rámec obvyklého softwarového projektu*\r\n* **nevyhovuje** *označuje práci, která by neměla být obhájena*\r\n\r\n*Hodnocení zdůvodněte v komentáři (zejména u jiných hodnocení než OK).*\r\n</small>\r\n\r\n#K celému projektu:\r\n\r\nObtížnost zadání: lepší | OK | horší | nevyhovující\r\n\r\nSplnění zadání: lepší | OK | horší | nevyhovující\r\n\r\n**Komentář:**\r\n\r\n#Implementace:\r\n\r\nKvalita návrhu: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(architektura, struktury a algoritmy, použité technologie)*</small>\r\n  \r\nKvalita zpracování: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(jmenné konvence, formátování, komentáře, testování)*</small>\r\n  \r\nStabilita: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**\r\n\r\n#Dokumentace:\r\n\r\nVývojová dokumentace: lepší | OK | horší | nevyhovující\r\n  \r\nUživatelská dokumentace: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**', NULL),
(134, '2018-11-07 22:42:56', '2018-11-07 22:42:56', 0, '<small>\r\nPosudok Oponenta\r\nLegenda:\r\n\r\n* **lepší** *označuje splnění nad rámec obvyklého softwarového projektu*\r\n* **OK** *označuje práci, která kritérium vhodným způsobem splňuje*\r\n* **horší** *označuje splnění pod rámec obvyklého softwarového projektu*\r\n* **nevyhovuje** *označuje práci, která by neměla být obhájena*\r\n\r\n*Hodnocení zdůvodněte v komentáři (zejména u jiných hodnocení než OK).*\r\n</small>\r\n\r\n#K celému projektu:\r\n\r\nObtížnost zadání: lepší | OK | horší | nevyhovující\r\n\r\nSplnění zadání: lepší | OK | horší | nevyhovující\r\n\r\n**Komentář:**\r\n\r\n#Implementace:\r\n\r\nKvalita návrhu: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(architektura, struktury a algoritmy, použité technologie)*</small>\r\n  \r\nKvalita zpracování: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(jmenné konvence, formátování, komentáře, testování)*</small>\r\n  \r\nStabilita: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**\r\n\r\n#Dokumentace:\r\n\r\nVývojová dokumentace: lepší | OK | horší | nevyhovující\r\n  \r\nUživatelská dokumentace: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**', NULL),
(135, '2018-11-07 22:44:16', '2018-11-07 22:44:16', 0, '<small>\r\nPosudok veduceho prace\r\nLegenda:\r\n\r\n* **lepší** *označuje splnění nad rámec obvyklého softwarového projektu*\r\n* **OK** *označuje práci, která kritérium vhodným způsobem splňuje*\r\n* **horší** *označuje splnění pod rámec obvyklého softwarového projektu*\r\n* **nevyhovuje** *označuje práci, která by neměla být obhájena*\r\n\r\n*Hodnocení zdůvodněte v komentáři (zejména u jiných hodnocení než OK).*\r\n</small>\r\n\r\n#K celému projektu:\r\n\r\nObtížnost zadání: lepší | OK | horší | nevyhovující\r\n\r\nSplnění zadání: lepší | OK | horší | nevyhovující\r\n\r\n**Komentář:**\r\n\r\n#Implementace:\r\n\r\nKvalita návrhu: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(architektura, struktury a algoritmy, použité technologie)*</small>\r\n  \r\nKvalita zpracování: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(jmenné konvence, formátování, komentáře, testování)*</small>\r\n  \r\nStabilita: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**\r\n\r\n#Dokumentace:\r\n\r\nVývojová dokumentace: lepší | OK | horší | nevyhovující\r\n  \r\nUživatelská dokumentace: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**', NULL),
(136, '2018-11-07 22:49:37', '2018-11-07 22:49:37', 0, '**OBHAJEN**', NULL),
(137, '2018-11-07 22:53:28', '2018-11-08 14:54:12', 0, 'Informácie určené študentom o projekte.', NULL),
(138, '2018-11-07 22:53:28', '2018-11-08 14:12:28', 0, '# Motivace\r\n*...krátký popis, proč by měl projekt vzniknout...*\r\n\r\n# Popis projektu\r\n*...popis funkčnosti, UI, atd....*\r\n\r\n# Platforma, technologie\r\n*...cílová platforma projektu; využité technologie, framework, apod. (pokud jsou již známy)...*\r\n\r\n# Odhad náročnosti\r\n*… počet řešitelů, případně i s odůvodněním (přibližné přidělení řešitelů jednotlivým částem projektu),\r\n termín dokončení, přibližný plán prací...*\r\n\r\n# Vymezení projektu\r\n\r\n**Diskrétní modely a algoritmy**\r\n\r\n- [x ] diskrétní matematika a algoritmy\r\n- [ ] geometrie a matematické struktury v informatice\r\n- [ ] optimalizace\r\n\r\n**Teoretická informatika**\r\n\r\n- [ ] Teoretická informatika\r\n\r\n**Softwarové a datové inženýrství**\r\n\r\n- [ ] softwarové inženýrství\r\n- [ ] vývoj software\r\n- [ ] webové inženýrství\r\n- [ ] databázové systémy\r\n- [ ] analýza a zpracování rozsáhlých dat\r\n\r\n**Softwarové systémy**\r\n\r\n- [ ] systémové programování\r\n- [ ] spolehlivé systémy\r\n- [ ] výkonné systémy\r\n\r\n**Matematická lingvistika**\r\n\r\n- [ ] počítačová a formální lingvistika\r\n- [ ] statistické metody a strojové učení v počítačové lingvistice\r\n\r\n**Umělá inteligence**\r\n\r\n- [ ] inteligentní agenti\r\n- [ ] strojové učení\r\n- [ ] robotika\r\n\r\n**Počítačová grafika a vývoj počítačových her**\r\n\r\n- [ ] počítačová grafika\r\n- [ ] vývoj počítačových her\r\n\r\n# Poznámky\r\n*... další informace nezapadající do sekcí výše...*\r\n', NULL),
(139, '2018-11-07 23:08:38', '2018-11-08 18:56:18', 0, '', NULL),
(140, '2018-11-07 23:08:38', '2018-11-08 18:56:18', 0, '# Motivace\r\n*...krátký popis, proč by měl projekt vzniknout...*\r\n\r\n# Popis projektu\r\n*...popis funkčnosti, UI, atd....*\r\n\r\n# Platforma, technologie\r\n*...cílová platforma projektu; využité technologie, framework, apod. (pokud jsou již známy)...*\r\n\r\n# Odhad náročnosti\r\n*… počet řešitelů, případně i s odůvodněním (přibližné přidělení řešitelů jednotlivým částem projektu),\r\n termín dokončení, přibližný plán prací...*\r\n\r\n# Vymezení projektu\r\n\r\n**Diskrétní modely a algoritmy**\r\n\r\n- [x ] diskrétní matematika a algoritmy\r\n- [ ] geometrie a matematické struktury v informatice\r\n- [ ] optimalizace\r\n\r\n**Teoretická informatika**\r\n\r\n- [ ] Teoretická informatika\r\n\r\n**Softwarové a datové inženýrství**\r\n\r\n- [ ] softwarové inženýrství\r\n- [ ] vývoj software\r\n- [ ] webové inženýrství\r\n- [ ] databázové systémy\r\n- [ ] analýza a zpracování rozsáhlých dat\r\n\r\n**Softwarové systémy**\r\n\r\n- [ ] systémové programování\r\n- [ ] spolehlivé systémy\r\n- [ ] výkonné systémy\r\n\r\n**Matematická lingvistika**\r\n\r\n- [ ] počítačová a formální lingvistika\r\n- [ ] statistické metody a strojové učení v počítačové lingvistice\r\n\r\n**Umělá inteligence**\r\n\r\n- [ ] inteligentní agenti\r\n- [ ] strojové učení\r\n- [ ] robotika\r\n\r\n**Počítačová grafika a vývoj počítačových her**\r\n\r\n- [ ] počítačová grafika\r\n- [ ] vývoj počítačových her\r\n\r\n# Poznámky\r\n*... další informace nezapadající do sekcí výše...*\r\n', NULL),
(141, '2018-11-08 13:54:37', '2018-11-08 13:54:37', 0, '', NULL),
(142, '2018-11-08 13:54:37', '2018-11-08 13:54:37', 0, '# Motivace\r\n*...krátký popis, proč by měl projekt vzniknout...*\r\n\r\n# Popis projektu\r\n*...popis funkčnosti, UI, atd....*\r\n\r\n# Platforma, technologie\r\n*...cílová platforma projektu; využité technologie, framework, apod. (pokud jsou již známy)...*\r\n\r\n# Odhad náročnosti\r\n*… počet řešitelů, případně i s odůvodněním (přibližné přidělení řešitelů jednotlivým částem projektu),\r\n termín dokončení, přibližný plán prací...*\r\n\r\n# Vymezení projektu\r\n\r\n**Diskrétní modely a algoritmy**\r\n\r\n- [x ] diskrétní matematika a algoritmy\r\n- [ ] geometrie a matematické struktury v informatice\r\n- [ ] optimalizace\r\n\r\n**Teoretická informatika**\r\n\r\n- [ ] Teoretická informatika\r\n\r\n**Softwarové a datové inženýrství**\r\n\r\n- [ ] softwarové inženýrství\r\n- [ ] vývoj software\r\n- [ ] webové inženýrství\r\n- [ ] databázové systémy\r\n- [ ] analýza a zpracování rozsáhlých dat\r\n\r\n**Softwarové systémy**\r\n\r\n- [ ] systémové programování\r\n- [ ] spolehlivé systémy\r\n- [ ] výkonné systémy\r\n\r\n**Matematická lingvistika**\r\n\r\n- [ ] počítačová a formální lingvistika\r\n- [ ] statistické metody a strojové učení v počítačové lingvistice\r\n\r\n**Umělá inteligence**\r\n\r\n- [ ] inteligentní agenti\r\n- [ ] strojové učení\r\n- [ ] robotika\r\n\r\n**Počítačová grafika a vývoj počítačových her**\r\n\r\n- [ ] počítačová grafika\r\n- [ ] vývoj počítačových her\r\n\r\n# Poznámky\r\n*... další informace nezapadající do sekcí výše...*\r\n', NULL),
(143, '2018-11-08 13:59:33', '2018-11-08 13:59:42', 0, NULL, '/files/download?bucket=projectUploads&filename=22%2Fuploads%2FDEFENCE_ZIP_2.ZIP'),
(144, '2018-11-08 14:00:10', '2018-11-08 14:00:10', 0, '<small>\r\nLegenda:\r\n\r\n* **lepší** *označuje splnění nad rámec obvyklého softwarového projektu*\r\n* **OK** *označuje práci, která kritérium vhodným způsobem splňuje*\r\n* **horší** *označuje splnění pod rámec obvyklého softwarového projektu*\r\n* **nevyhovuje** *označuje práci, která by neměla být obhájena*\r\n\r\n*Hodnocení zdůvodněte v komentáři (zejména u jiných hodnocení než OK).*\r\n</small>\r\n\r\n#K celému projektu:\r\n\r\nObtížnost zadání: lepší | OK | horší | nevyhovující\r\n\r\nSplnění zadání: lepší | OK | horší | nevyhovující\r\n\r\n**Komentář:**\r\n\r\n#Implementace:\r\n\r\nKvalita návrhu: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(architektura, struktury a algoritmy, použité technologie)*</small>\r\n  \r\nKvalita zpracování: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(jmenné konvence, formátování, komentáře, testování)*</small>\r\n  \r\nStabilita: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**\r\n\r\n#Dokumentace:\r\n\r\nVývojová dokumentace: lepší | OK | horší | nevyhovující\r\n  \r\nUživatelská dokumentace: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**', NULL),
(145, '2018-11-08 14:00:15', '2018-11-08 14:00:15', 0, '<small>\r\nLegenda:\r\n\r\n* **lepší** *označuje splnění nad rámec obvyklého softwarového projektu*\r\n* **OK** *označuje práci, která kritérium vhodným způsobem splňuje*\r\n* **horší** *označuje splnění pod rámec obvyklého softwarového projektu*\r\n* **nevyhovuje** *označuje práci, která by neměla být obhájena*\r\n\r\n*Hodnocení zdůvodněte v komentáři (zejména u jiných hodnocení než OK).*\r\n</small>\r\n\r\n#K celému projektu:\r\n\r\nObtížnost zadání: lepší | OK | horší | nevyhovující\r\n\r\nSplnění zadání: lepší | OK | horší | nevyhovující\r\n\r\n**Komentář:**\r\n\r\n#Implementace:\r\n\r\nKvalita návrhu: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(architektura, struktury a algoritmy, použité technologie)*</small>\r\n  \r\nKvalita zpracování: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(jmenné konvence, formátování, komentáře, testování)*</small>\r\n  \r\nStabilita: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**\r\n\r\n#Dokumentace:\r\n\r\nVývojová dokumentace: lepší | OK | horší | nevyhovující\r\n  \r\nUživatelská dokumentace: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**', NULL),
(146, '2018-11-08 14:00:28', '2018-11-08 14:00:28', 0, 'Obhájen.', NULL),
(147, '2018-11-08 14:09:03', '2018-11-08 14:09:03', 0, NULL, NULL),
(148, '2018-11-08 14:11:39', '2018-11-08 14:11:39', 0, NULL, NULL),
(149, '2018-11-08 14:12:27', '2018-11-08 14:54:13', 0, '# Motivace\r\n*...krátký popis, proč by měl projekt vzniknout...*\r\n\r\n# Popis projektu\r\n*...popis funkčnosti, UI, atd....*\r\n\r\n# Platforma, technologie\r\n*...cílová platforma projektu; využité technologie, framework, apod. (pokud jsou již známy)...*\r\n\r\n# Odhad náročnosti\r\n*… počet řešitelů, případně i s odůvodněním (přibližné přidělení řešitelů jednotlivým částem projektu),\r\n termín dokončení, přibližný plán prací...*\r\n\r\n# Vymezení projektu\r\n\r\n**Diskrétní modely a algoritmy**\r\n\r\n- [x ] diskrétní matematika a algoritmy\r\n- [ ] geometrie a matematické struktury v informatice\r\n- [ ] optimalizace\r\n\r\n**Teoretická informatika**\r\n\r\n- [ ] Teoretická informatika\r\n\r\n**Softwarové a datové inženýrství**\r\n\r\n- [ ] softwarové inženýrství\r\n- [ ] vývoj software\r\n- [ ] webové inženýrství\r\n- [ ] databázové systémy\r\n- [ ] analýza a zpracování rozsáhlých dat\r\n\r\n**Softwarové systémy**\r\n\r\n- [ ] systémové programování\r\n- [ ] spolehlivé systémy\r\n- [ ] výkonné systémy\r\n\r\n**Matematická lingvistika**\r\n\r\n- [ ] počítačová a formální lingvistika\r\n- [ ] statistické metody a strojové učení v počítačové lingvistice\r\n\r\n**Umělá inteligence**\r\n\r\n- [ ] inteligentní agenti\r\n- [ ] strojové učení\r\n- [ ] robotika\r\n\r\n**Počítačová grafika a vývoj počítačových her**\r\n\r\n- [ ] počítačová grafika\r\n- [ ] vývoj počítačových her\r\n\r\n# Poznámky\r\n*... další informace nezapadající do sekcí výše...*\r\n', '/files/download?bucket=otherFiles&filename=Navrh.pdf'),
(150, '2018-11-08 14:53:50', '2018-11-08 14:53:50', 0, NULL, '/files/download?bucket=projectUploads&filename=23%2Fuploads%2FDEFENCE_ZIP_1.ZIP'),
(151, '2018-11-08 14:53:50', '2018-11-08 14:53:50', 0, NULL, '/files/download?bucket=projectUploads&filename=23%2Fuploads%2FDEFENCE_PDF_1.PDF'),
(152, '2018-11-08 14:54:12', '2018-11-08 14:54:12', 0, 'Naše zkušenosti s projektem.', NULL),
(153, '2018-11-08 14:54:41', '2018-11-08 14:54:41', 0, NULL, '/files/download?bucket=projectUploads&filename=23%2Fuploads%2Fdoc_project_private.txt'),
(154, '2018-11-08 15:01:36', '2018-11-08 15:01:36', 0, '<small>\r\nLegenda:\r\n\r\n* **lepší** *označuje splnění nad rámec obvyklého softwarového projektu*\r\n* **OK** *označuje práci, která kritérium vhodným způsobem splňuje*\r\n* **horší** *označuje splnění pod rámec obvyklého softwarového projektu*\r\n* **nevyhovuje** *označuje práci, která by neměla být obhájena*\r\n\r\n*Hodnocení zdůvodněte v komentáři (zejména u jiných hodnocení než OK).*\r\n</small>\r\n\r\n#K celému projektu:\r\n\r\nObtížnost zadání: lepší | OK | horší | nevyhovující\r\n\r\nSplnění zadání: lepší | OK | horší | nevyhovující\r\n\r\n**Komentář:**\r\n\r\n#Implementace:\r\n\r\nKvalita návrhu: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(architektura, struktury a algoritmy, použité technologie)*</small>\r\n  \r\nKvalita zpracování: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(jmenné konvence, formátování, komentáře, testování)*</small>\r\n  \r\nStabilita: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**\r\n\r\n#Dokumentace:\r\n\r\nVývojová dokumentace: lepší | OK | horší | nevyhovující\r\n  \r\nUživatelská dokumentace: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**', NULL),
(155, '2018-11-08 15:01:40', '2018-11-08 15:01:40', 0, '<small>\r\nLegenda:\r\n\r\n* **lepší** *označuje splnění nad rámec obvyklého softwarového projektu*\r\n* **OK** *označuje práci, která kritérium vhodným způsobem splňuje*\r\n* **horší** *označuje splnění pod rámec obvyklého softwarového projektu*\r\n* **nevyhovuje** *označuje práci, která by neměla být obhájena*\r\n\r\n*Hodnocení zdůvodněte v komentáři (zejména u jiných hodnocení než OK).*\r\n</small>\r\n\r\n#K celému projektu:\r\n\r\nObtížnost zadání: lepší | OK | horší | nevyhovující\r\n\r\nSplnění zadání: lepší | OK | horší | nevyhovující\r\n\r\n**Komentář:**\r\n\r\n#Implementace:\r\n\r\nKvalita návrhu: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(architektura, struktury a algoritmy, použité technologie)*</small>\r\n  \r\nKvalita zpracování: lepší | OK | horší | nevyhovující\r\n\r\n<small>*(jmenné konvence, formátování, komentáře, testování)*</small>\r\n  \r\nStabilita: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**\r\n\r\n#Dokumentace:\r\n\r\nVývojová dokumentace: lepší | OK | horší | nevyhovující\r\n  \r\nUživatelská dokumentace: lepší | OK | horší | nevyhovující  \r\n\r\n**Komentář:**', NULL),
(156, '2018-11-08 15:06:48', '2018-11-08 15:06:48', 0, 'Obhajeno.', NULL),
(157, '2018-11-09 01:20:59', '2018-11-09 01:43:54', 0, '', NULL),
(158, '2018-11-09 01:20:59', '2018-11-09 01:43:54', 0, '# Motivace\r\nMotivace projektu Theory of Everything', NULL),
(159, '2018-11-09 01:34:09', '2018-11-09 02:47:31', 0, NULL, '/files/download?bucket=projectUploads&filename=26%2Fuploads%2FDEFENCE_ZIP_1.ZIP'),
(160, '2018-11-09 01:34:09', '2018-11-09 01:34:09', 0, NULL, '/files/download?bucket=projectUploads&filename=26%2Fuploads%2FDEFENCE_PDF_1.PDF'),
(161, '2018-11-09 01:47:13', '2018-11-09 01:54:34', 0, '', NULL),
(162, '2018-11-09 01:47:13', '2018-11-09 01:54:34', 0, '# Motivation\r\nThe P versus NP problem is a major unsolved problem in computer science. It asks whether every problem whose solution can be quickly verified (technically, verified in polynomial time) can also be solved quickly (again, in polynomial time).\r\n\r\n# Project Description\r\nProject is revealing whether N equals NP or not.', NULL),
(163, '2018-11-09 03:02:13', '2018-11-09 03:02:29', 0, '', NULL),
(164, '2018-11-09 03:02:13', '2018-11-09 03:02:29', 0, '# Motivace\r\n*...krátký popis, proč by měl projekt vzniknout...*\r\n\r\n# Popis projektu\r\n*...popis funkčnosti, UI, atd....*\r\n\r\n# Platforma, technologie\r\n*...cílová platforma projektu; využité technologie, framework, apod. (pokud jsou již známy)...*\r\n\r\n# Odhad náročnosti\r\n*… počet řešitelů, případně i s odůvodněním (přibližné přidělení řešitelů jednotlivým částem projektu),\r\n termín dokončení, přibližný plán prací...*\r\n\r\n# Vymezení projektu\r\n\r\n**Diskrétní modely a algoritmy**\r\n\r\n- [x ] diskrétní matematika a algoritmy\r\n- [ ] geometrie a matematické struktury v informatice\r\n- [ ] optimalizace\r\n\r\n**Teoretická informatika**\r\n\r\n- [ ] Teoretická informatika\r\n\r\n**Softwarové a datové inženýrství**\r\n\r\n- [ ] softwarové inženýrství\r\n- [ ] vývoj software\r\n- [ ] webové inženýrství\r\n- [ ] databázové systémy\r\n- [ ] analýza a zpracování rozsáhlých dat\r\n\r\n**Softwarové systémy**\r\n\r\n- [ ] systémové programování\r\n- [ ] spolehlivé systémy\r\n- [ ] výkonné systémy\r\n\r\n**Matematická lingvistika**\r\n\r\n- [ ] počítačová a formální lingvistika\r\n- [ ] statistické metody a strojové učení v počítačové lingvistice\r\n\r\n**Umělá inteligence**\r\n\r\n- [ ] inteligentní agenti\r\n- [ ] strojové učení\r\n- [ ] robotika\r\n\r\n**Počítačová grafika a vývoj počítačových her**\r\n\r\n- [ ] počítačová grafika\r\n- [ ] vývoj počítačových her\r\n\r\n# Poznámky\r\n*... další informace nezapadající do sekcí výše...*\r\n', NULL),
(165, '2018-11-10 16:45:22', '2018-11-10 16:52:27', 0, '', NULL);
INSERT INTO `document` (`id`, `created_at`, `updated_at`, `deleted`, `content_md`, `fallback_path`) VALUES
(166, '2018-11-10 16:45:22', '2018-11-10 16:52:27', 0, '# Motivace\r\n\r\nMobile games are getting more and more popular, since they bring games to more people, mostly\r\ncasual players. Mobile gaming also brings new possibilities, since mobile phones have many\r\nsensors that can be used for game controls, especially GPS, accelerometer, gyroscope, camera.\r\nThere have already been some successful games using these innovative ideas, like Pokemon Go,\r\nMaguss or Underverse, which use GPS data to move around the real world map and AR for some\r\nspecial game mechanics. But there is still a lot of unexplored potential.\r\nAnother successful concept is location based games such as geocaching or geofun. In these\r\ngames, there are location bound points, which the player must visit. In geofun, there is usually\r\neven some historical facts or interesting facts about the places the player visits. But it is still mostly\r\ntouristic application that shows interesting places.\r\nThe project aspires to combine location bound story based quests made by community and mobile\r\nRPG games. \r\n\r\n# Popis projektu\r\n\r\nThe aim of the project is to implement a platform for a location-based augmented reality (AR) roleplaying\r\ngame (RPG) for Android mobile devices.\r\nThe game will contain separate quests. Quests will be short bound stories/tasks consisting of\r\ndialogues with NPCs, combat and minigames (woodcutting, crafting, finding hidden objects and\r\nplant growing). The player will be collecting items around the world, craft them and trade with other\r\nplayers. By completing quests and using his/her gathering and crafting skill the player will unlock\r\nnew abilities and recipes for the crafting system hence the inclusion of RPG elements. For some\r\nitems, the player will also need to gather resources at appropriate locations (e.g. a wood from\r\nforest). In this way the player will be rewarded by the time spent by seeing his/her statistics\r\nincreasing and unlocking new powerful skills and items. The game will also allow the players\r\n(users) to make and upload new quests via standardized packages. Uploading these quests to the\r\ngame will not need an update of the application itself.\r\nThe game will be divided in 10 screens:\r\nS1. one for the map, from which the player will be able to see his/her position,traders,enemies\r\nand people that will give them quests;\r\nS2. one screen for the inventory from which the player will be able to equip/unequip items or\r\ndelete them;\r\nS3. another screen will be used for tracking quests;\r\nS4. another one will be used to resume the statistics of the player\r\n(health/stamina/mana/strength etc.);\r\nS5-8. four screens will be used for four mini-games that will reward the player if completed. Two\r\nof them will use AR features to be solved.\r\nS9. last screen will be used for the augmented reality combat.\r\nTo put it into the list, the game will feature:\r\n1.1. Location-based map integration\r\n1.2. RPG elements\r\n1.3. Augmented reality combat\r\n1.4. Augmented reality mini games (x2)\r\n1.5. Other minigames (x2)\r\n1.6. Quests, Quest packages and Package Editor\r\n1.7. Items: crafting and trading (in-game currency)\r\nMore detailed description now follows\r\n\r\n# Platforma, technologie\r\n\r\n## 1.1. Location-based Map Integration\r\nDuring the gameplay, the player sees his character on real world map. The character will be\r\ncontrolled via GPS, so a player must move physically in order to move his character. On the map,\r\nthere will be intractable objects visible, which the player can interact with, e.g. non-player\r\ncharacters that a player can speak with, enemies that player can attack, other objects (trees,\r\nbuildings, etc.). A player can interact with them when he/she is close enough by basically tapping\r\nthe object on the map.\r\n\r\n## 1.2. RPG Elements\r\nThe game will contain traditional RPG elements. Firstly, health and combat system will be\r\nintroduced (See Section 1.4.). The inventory system will be implemented to allow player to collect\r\nitems, keep them in his/her inventory, convert them into in-game currency or trade them for other\r\nitems. Items will be divided in three category: equippable, consumable and reagents (See Section\r\n1.8). The player will have some gathering skills (woodcutting, fishing), by using them the player will\r\ngain experience and after gaining certain amount of experience the skill will level up. Levelling up\r\nwith the skills will allow the player to gather new reagents using them, which will be used to craft\r\nmore powerful items.\r\nKilling enemies and completing quests will grant experience to the player. After some\r\nthreshold the player will level-up, increasing statistics and unlocking new combat skills.\r\n\r\n## 1.3. Augmented reality combat\r\nAugmented reality games are becoming a front-runners of gaming industry. Nowadays, this\r\nis not just the fantastical concept. AR games allow you to fight aliens, capture fantastical creatures,\r\ndefend kingdoms in the real world. With our game, we want to provide an augmented reality role\r\nplaying world to the player. Our platform implements a real-time one versus one combat system.\r\nThe opponent character and the battle itself will be animated in AR. The player is not allowed to\r\nmove and is only able to attack and/or use special abilities by basically clicking on them. A\r\ngameplay will be cooldown based. The player will have the choice to toggle between the\r\naugmented reality mode and the regular mode.\r\n\r\n## 1.4. Augmented reality mini games\r\nThe game features 2 augmented reality supported mini games, which can be enabled\r\nduring quests. One of the mini games will be using the phone’s camera to find reagents or herbs\r\nfor the crafting skills. Those objects will be randomly distributed around the streets of the map. The\r\nplayer is supposed to tap on them in order to collect. Second one will be taking care of a plant, the\r\nplayer is going to see the pot in augmented reality and need to plant the seeds, water the\r\nseedlings, trim dead leaves and balance the sunlight. After the plant grows, the player will gain\r\nexperience points. If the player does not take care of the plant and it dies, he/she needs to start the\r\nmini-game over, remove the dead plant and put new seeds.\r\n\r\n## 1.5. RPG mini games\r\nTwo more minigames, which will use sensors the mobile phones can offer, as gyroscope and\r\naccelerometer, will be woodcutting and crafting. In woodcutting the player will have to make a\r\nmovement similar to swinging an axe with his phone. In crafting, the player will have to balance two\r\ningredients on scales in order to make a potion.\r\n\r\n## 1.6. Quest lines, quest packages and Package Editor\r\nThe quest is a series of short tasks; task examples are dialogues with NPCs, discovering new\r\nlocations, combat with NPCs and performing a mini-game. As a part of the development, we\r\nprovide a few basic quest lines with several quests.\r\nPlayers will be able to create new quests using Package Editor. Package Editor will be a plugin for\r\nUnity, which will allow the user to define his quest line by creating several quests, which can be\r\ncustomized by defining his own NPCs and its properties (i.e. locations, custom meshes, and\r\ntextures), choosing quest mini games, setting quest rewards, etc.\r\nPackage Editor will be generating a standardized data package in format of Unity AssetBundles,\r\nwhich can be loaded by the game during runtime. For our purposes, Unity AssetBundles format\r\ncontains a definition of the quest line data packed into a JSON file and the game objects needed in\r\nthe quests. Users are able to load quest packages into the game anytime during the gameplay.\r\n\r\n## 1.7. Items: crafting and trading (in-game currency)\r\n\r\nThe player will be able to obtain items either from killing monsters, as a reward for quests, or via\r\ngathering and crafting. Items will be of three types - equipment, consumables and raw materials.\r\nEquipment can be equipped to provide some bonuses to player attributes. Consumables can be\r\nconsumed for some bonuses, such as health restoration. Raw materials can be crafted to items\r\nfrom one of the two preceding categories. All items will have a value and the player will be able to\r\nsell them for ingame currency. With that ingame currency, player will be able to buy other items.\r\n\r\n# Platforma, technologie\r\n\r\n## Target Platform:\r\nAndroid\r\n\r\n## Technologies and Frameworks:\r\nUnity3D, Blender, ARCore, MapBox API with OpenStreetMaps, Bitbucket\r\nWe will be using Unity3D game engine, which is well suited for mobile games development. It also\r\noffers support by ARCore, which is needed to support augmented reality.\r\nWe will use Bitbucket version control service to synchronize the work.\r\nFor augmented reality, we will utilize ARCore, a Google’s platform which provides SDK for Unity for\r\nbuilding augmented reality applications.\r\nIntegration of Unity3D and OpenStreetMap will be done by using Mapbox, which is an open source\r\nlocation data platform for mobile and web applications that is optimized for Unity3D game engine.\r\nWe already combined ARCore and Mapbox in a small proof-of-concept application in Unity3D and\r\nwe already tested it on an Android mobile phone confirm that both are working together as\r\nintended.\r\n\r\n# Odhad náročnosti\r\nThe project is meant for 5 developers.\r\n\r\n* Architecture specification: 2 month\r\n* Core gameplay: 6 months\r\n* Location-based map integration: 2 months\r\n* Augmented reality combat: 3 months\r\n* Augmented reality mini games: 3 months\r\n* Serialization for quest packages and the packaging editor: 3 months\r\n* Game balance corrections: 1 month\r\n* User interface: 2 months\r\n* User experience testing: 1 month\r\n* Unit testing will be done during the development process; every developer is expected to\r\nwrite unit tests and isolate each part of the program.\r\n\r\n# Vymezení projektu\r\n\r\n**Diskrétní modely a algoritmy**\r\n\r\n- [x ] diskrétní matematika a algoritmy\r\n- [ ] geometrie a matematické struktury v informatice\r\n- [ ] optimalizace\r\n\r\n**Teoretická informatika**\r\n\r\n- [ ] Teoretická informatika\r\n\r\n**Softwarové a datové inženýrství**\r\n\r\n- [x] softwarové inženýrství\r\n- [x] vývoj software\r\n- [ ] webové inženýrství\r\n- [ ] databázové systémy\r\n- [ ] analýza a zpracování rozsáhlých dat\r\n\r\n**Softwarové systémy**\r\n\r\n- [ ] systémové programování\r\n- [ ] spolehlivé systémy\r\n- [ ] výkonné systémy\r\n\r\n**Matematická lingvistika**\r\n\r\n- [ ] počítačová a formální lingvistika\r\n- [ ] statistické metody a strojové učení v počítačové lingvistice\r\n\r\n**Umělá inteligence**\r\n\r\n- [ ] inteligentní agenti\r\n- [ ] strojové učení\r\n- [ ] robotika\r\n\r\n**Počítačová grafika a vývoj počítačových her**\r\n\r\n- [ ] počítačová grafika\r\n- [x] vývoj počítačových her\r\n\r\n', NULL),
(167, '2018-11-10 17:41:30', '2018-11-10 17:51:40', 0, '', NULL),
(168, '2018-11-10 17:41:30', '2018-11-10 17:51:40', 0, '', '/files/download?bucket=otherFiles&filename=rrally.pdf'),
(169, '2018-11-10 17:55:40', '2018-11-10 17:55:40', 0, NULL, '/files/download?bucket=projectUploads&filename=30%2Fuploads%2FDEFENCE_ZIP_1.ZIP'),
(170, '2018-11-10 17:55:40', '2018-11-10 17:55:40', 0, NULL, '/files/download?bucket=projectUploads&filename=30%2Fuploads%2FDEFENCE_PDF_1.PDF'),
(171, '2018-11-10 18:03:27', '2018-11-10 18:03:27', 0, '<small>\r\nLegenda:\r\n\r\n* **lepší** *označuje splnění nad rámec obvyklého softwarového projektu*\r\n* **OK** *označuje práci, která kritérium vhodným způsobem splňuje*\r\n* **horší** *označuje splnění pod rámec obvyklého softwarového projektu*\r\n* **nevyhovuje** *označuje práci, která by neměla být obhájena*\r\n\r\n*Hodnocení zdůvodněte v komentáři (zejména u jiných hodnocení než OK).*\r\n</small>\r\n\r\n#K celému projektu:\r\n\r\nObtížnost zadání: lepší\r\nSplnění zadání: lepší\r\n\r\n#Implementace:\r\n\r\nKvalita návrhu: OK\r\nKvalita zpracování: lepší\r\nStabilita: OK\r\n\r\n#Dokumentace:\r\n\r\nVývojová dokumentace: nevyhovující\r\nUživatelská dokumentace: OK\r\n\r\n**Komentář:** Není popsaná architektura.', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title_en` varchar(100) NOT NULL,
  `title_cs` varchar(100) NOT NULL,
  `content_en` text NOT NULL,
  `content_cs` text NOT NULL,
  `public` tinyint(1) NOT NULL,
  `type` enum('generated','normal','important') NOT NULL,
  `hide_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `user_id`, `title_en`, `title_cs`, `content_en`, `content_cs`, `public`, `type`, `hide_at`, `created_at`, `updated_at`, `deleted`) VALUES
(2, 1, 'New rules', 'Nová verze pravidel', 'New version of rules has been published for projects started after 1.10.2017.', 'Nová verze pravidel pro projekty zahájené po 1.10.2017.', 1, 'important', NULL, '2018-08-10 19:55:55', '2018-08-10 19:55:55', 0),
(3, 1, 'Course ID change', 'Změna kódu předmětu', 'The ID of this course in SIS has been changed from NPRG023 to NPRG024.', 'Kód předmětu v SIS se mění z NPRG023 na NPRG024.', 1, 'normal', NULL, '2018-08-10 19:59:50', '2018-08-10 19:59:50', 0),
(4, 1, 'Advertisements module', 'Modul \"témata\"', 'The \"Advertisements\" which has been available since 1.8. is a place where students and members of faculty can post ideas for future projects. A team of students will then be able to implement that idea as a software project.\r\n\r\nA secondary function of the Advertisements modules is to offer students who are looking for a team the ability to make themselves visible to other students.', 'Od 1.8. je funkční nový modul \"Témata\" kde studenti a pracovnící fakutly mohou přidávat náměty na budoucí projekty. Tým studentů se zájmem o dané téma jej pak může zpracovat jako náplň svého projektu.\r\n\r\nSekundárním účelem modulu Témata je nabídnout studentům, kteří hledají tým, dát o sobě vědět ostatním studentům.', 1, 'normal', '2019-03-16 00:00:00', '2018-08-10 20:12:37', '2018-08-10 20:12:37', 0),
(9, NULL, 'Page updated', 'Stránka aktualizovaná', 'The page [Rules of software projects](/pages/rules) was updated.', 'Stránka [Pravidlá softwarových projektů](/pages/rules) byla aktualizovaná.', 1, 'generated', '2018-11-02 19:50:49', '2018-10-30 19:50:49', '2018-10-30 19:50:49', 0),
(11, NULL, 'New defence date', 'Nové datum obhajoby', 'A new defence was just opened for **2018-12-16**.\r\n\r\nProjects can now be assigned to this date.', 'Nová obhajoba byla zahájena dne **2018-12-16**.\r\n\r\nProjekty už můžou být přihlášené k tomuto datu.', 1, 'normal', '2018-11-11 00:00:00', '2018-11-08 13:55:59', '2018-11-09 02:56:31', 0),
(12, NULL, 'New defence date', 'Nové datum obhajoby', 'A new defence was just opened for **2018-11-14**.\n\nProjects can now be assigned to this date.', 'Nová obhajoba byla zahájena dne **2018-11-14**.\n\nProjekty už můžou být přihlášené k tomuto datu.', 1, 'generated', '2018-11-11 14:29:56', '2018-11-08 14:29:56', '2018-11-08 14:29:56', 0);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `title_en` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `title_cs` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `path_pdf_download` varchar(300) COLLATE utf8_czech_ci DEFAULT NULL,
  `url` varchar(300) COLLATE utf8_czech_ci NOT NULL,
  `order_in_menu` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `created_at`, `updated_at`, `deleted`, `title_en`, `title_cs`, `path_pdf_download`, `url`, `order_in_menu`, `published`) VALUES
(5, '2018-10-30 19:49:31', '2018-10-30 19:50:49', 0, 'Rules of software projects', 'Pravidlá softwarových projektů', NULL, 'rules', 1, 1),
(6, '2018-10-30 19:51:50', '2018-10-30 19:51:50', 0, 'Members of Project Commission', 'Složení projektové komise', NULL, 'committee', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `page_version`
--

CREATE TABLE `page_version` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_en` int(11) DEFAULT NULL,
  `content_cs` int(11) DEFAULT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_version`
--

INSERT INTO `page_version` (`id`, `page_id`, `user_id`, `content_en`, `content_cs`, `published`, `created_at`, `updated_at`, `deleted`) VALUES
(10, 5, 1, 118, 117, 0, '2018-10-30 19:49:31', '2018-10-30 19:50:12', 0),
(11, 5, 1, 120, 119, 0, '2018-10-30 19:50:11', '2018-10-30 19:50:37', 0),
(12, 5, 1, 122, 121, 1, '2018-10-30 19:50:36', '2018-10-30 19:50:48', 0),
(13, 6, 1, 126, 125, 1, '2018-10-30 19:51:50', '2018-10-30 19:52:33', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `state` enum('proposal','accepted','analysis','analysis_handed_in','implementation','implementation_handed_in','conditionally_defended','conditional_documents_handed_in','defended','failed','canceled') NOT NULL,
  `run_start_date` datetime DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `short_name` varchar(45) NOT NULL,
  `description` varchar(2000) NOT NULL DEFAULT '',
  `content` int(11) DEFAULT NULL,
  `waiting_for_proposal_evaluation` tinyint(1) NOT NULL DEFAULT '0',
  `requested_to_run` tinyint(1) NOT NULL DEFAULT '0',
  `extra_credits` int(11) NOT NULL DEFAULT '0',
  `team_members_visible` tinyint(1) NOT NULL DEFAULT '0',
  `keywords` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `updated_at`, `created_at`, `deleted`, `state`, `run_start_date`, `deadline`, `name`, `short_name`, `description`, `content`, `waiting_for_proposal_evaluation`, `requested_to_run`, `extra_credits`, `team_members_visible`, `keywords`) VALUES
(22, '2018-11-10 17:30:31', '2018-11-07 14:17:58', 0, 'defended', '2018-11-07 00:00:00', '2019-08-07 00:00:00', 'C++ compiler', 'CPC', 'A new, more optimized implementation of a C++ compiler. The project is expected to cover only a part of the syntax.', 127, 1, 0, 0, 0, NULL),
(23, '2018-11-08 14:38:18', '2018-11-07 22:53:28', 0, 'analysis', '2018-11-08 00:00:00', '2019-08-08 00:00:00', 'General theory', 'GENTHE', 'General theory of programming language', 137, 1, 0, 0, 0, NULL),
(24, '2018-11-08 18:56:18', '2018-11-07 23:08:38', 0, 'accepted', NULL, NULL, 'Christmas in North Dakota', 'CHRISIN', 'Popis', 139, 1, 0, 0, 0, NULL),
(25, '2018-11-08 13:54:37', '2018-11-08 13:54:37', 0, 'proposal', NULL, NULL, 'Memsource Mobile Cloud', 'MMC', 'Memsource Mobile Cloud', 141, 0, 0, 0, 0, NULL),
(26, '2018-11-09 01:43:54', '2018-11-09 01:20:59', 0, 'analysis', '2018-11-09 00:00:00', '2019-08-09 00:00:00', 'Theory of Everything', 'THEOR', 'Description for the project Theory of Everything', 157, 1, 0, 0, 0, NULL),
(27, '2018-11-09 01:54:34', '2018-11-09 01:47:13', 0, 'accepted', NULL, NULL, 'P=NP relevation', 'PNP', 'Description for N=NP project', 161, 1, 0, 0, 0, NULL),
(28, '2018-11-09 03:02:29', '2018-11-09 03:02:13', 0, 'proposal', NULL, NULL, 'Artificial general intelligence', 'AGI', 'Intelligence of a machine that could successfully perform any intellectual task that a human being can.', 163, 1, 0, 0, 0, NULL),
(29, '2018-11-10 16:52:27', '2018-11-10 16:45:22', 0, 'proposal', NULL, NULL, 'Mobile Augmented Role-playing Game', 'MARPG', 'MARPG is a location-based augmented reality role-playing mobile game which\r\noffers player options such as combating, crafting and discovering.', 165, 1, 0, 0, 0, NULL),
(30, '2018-11-10 17:51:40', '2018-11-10 17:41:30', 0, 'analysis', '2018-11-10 00:00:00', '2019-08-10 00:00:00', 'Robo-RoboRally', 'RRally ', 'Cílem projektu je implementovat simulátor deskové hry RoboRally, ve kterém\r\nbude fyzicky zachován centrální prvek hry (deska s figurkami robotů), avšak\r\npohyb robotů po desce bude zajištěn mechanicky pomocí 2D plotteru a\r\npřipevněných magnetů. Součástí bude mobilní aplikace, která bude simulovat\r\nherní prvky patřící jednotlivým hráčům.', 167, 1, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_defence`
--

CREATE TABLE `project_defence` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL,
  `project_state` varchar(50) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `defence_date_id` int(11) NOT NULL,
  `opponent_id` int(11) DEFAULT NULL,
  `statement_author_id` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `defence_statement` int(11) DEFAULT NULL,
  `opponent_review` int(11) DEFAULT NULL,
  `supervisor_review` int(11) DEFAULT NULL,
  `result` enum('defended','conditionally_defended','failed') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_defence`
--

INSERT INTO `project_defence` (`id`, `created_at`, `updated_at`, `deleted`, `project_id`, `project_state`, `number`, `defence_date_id`, `opponent_id`, `statement_author_id`, `time`, `defence_statement`, `opponent_review`, `supervisor_review`, `result`) VALUES
(1, '2018-11-07 22:38:09', '2018-11-09 01:38:03', 1, 22, NULL, 1, 1, 28, 1, '2018-11-16 15:00:00', 136, 134, 135, 'defended'),
(2, '2018-11-08 13:56:05', '2018-11-08 14:00:28', 0, 22, NULL, 2, 3, NULL, 1, '2018-11-23 14:00:00', 146, 145, 144, 'defended'),
(3, '2018-11-08 14:38:33', '2018-11-10 17:56:59', 1, 23, NULL, 1, 2, NULL, 1, '2018-11-14 10:00:00', 156, 155, 154, 'defended'),
(4, '2018-11-09 01:30:33', '2018-11-09 01:39:20', 1, 26, NULL, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '2018-11-09 01:44:07', '2018-11-09 01:44:07', 0, 26, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '2018-11-10 17:56:47', '2018-11-10 18:03:27', 0, 30, NULL, 1, 2, 30, NULL, '2018-11-14 14:00:00', NULL, NULL, 171, NULL),
(7, '2018-11-10 17:57:10', '2018-11-10 17:57:37', 0, 23, NULL, 1, 2, NULL, NULL, '2018-11-14 14:30:00', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_document`
--

CREATE TABLE `project_document` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `team_private` tinyint(1) NOT NULL,
  `defence_number` int(11) DEFAULT NULL,
  `type` enum('for_defense_pdf','for_defense_zip','read_only','advice') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_document`
--

INSERT INTO `project_document` (`id`, `created_at`, `updated_at`, `deleted`, `project_id`, `document_id`, `comment`, `team_private`, `defence_number`, `type`) VALUES
(1, '2018-11-07 22:00:23', '2018-11-07 22:00:23', 0, 22, 129, '', 0, NULL, 'advice'),
(2, '2018-11-07 22:07:44', '2018-11-07 22:07:44', 0, 22, 130, 'Privatni informace', 1, NULL, 'read_only'),
(3, '2018-11-07 22:08:38', '2018-11-07 22:08:38', 0, 22, 131, 'Uživatelská příručka', 0, NULL, 'read_only'),
(4, '2018-11-07 22:09:21', '2018-11-08 13:59:48', 0, 22, 132, NULL, 0, 2, 'for_defense_pdf'),
(5, '2018-11-08 13:59:33', '2018-11-08 13:59:48', 0, 22, 143, NULL, 0, 2, 'for_defense_zip'),
(6, '2018-11-08 14:53:50', '2018-11-08 14:53:50', 0, 23, 150, NULL, 1, 1, 'for_defense_zip'),
(7, '2018-11-08 14:53:50', '2018-11-08 14:53:50', 0, 23, 151, NULL, 1, 1, 'for_defense_pdf'),
(8, '2018-11-08 14:54:12', '2018-11-08 14:54:12', 0, 23, 152, '', 0, NULL, 'advice'),
(9, '2018-11-08 14:54:41', '2018-11-08 14:54:41', 0, 23, 153, 'Privatni dokument', 1, NULL, 'read_only'),
(10, '2018-11-09 01:34:09', '2018-11-09 01:34:28', 0, 26, 159, NULL, 0, 1, 'for_defense_zip'),
(11, '2018-11-09 01:34:09', '2018-11-09 01:34:28', 0, 26, 160, NULL, 0, 1, 'for_defense_pdf'),
(12, '2018-11-10 17:55:40', '2018-11-10 17:55:40', 0, 30, 169, NULL, 1, 1, 'for_defense_zip'),
(13, '2018-11-10 17:55:40', '2018-11-10 17:55:40', 0, 30, 170, NULL, 1, 1, 'for_defense_pdf');

-- --------------------------------------------------------

--
-- Table structure for table `project_state_history`
--

CREATE TABLE `project_state_history` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL,
  `state` enum('proposal','accepted','analysis','analysis_handed_in','implementation','implementation_handed_in','conditionally_defended','conditional_documents_handed_in','defended','failed','canceled') NOT NULL,
  `comment` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_state_history`
--

INSERT INTO `project_state_history` (`id`, `created_at`, `updated_at`, `deleted`, `project_id`, `state`, `comment`) VALUES
(68, '2018-11-07 14:17:58', '2018-11-07 14:17:58', 0, 22, 'proposal', NULL),
(69, '2018-11-07 21:27:18', '2018-11-07 21:27:18', 0, 22, 'accepted', NULL),
(70, '2018-11-07 21:57:04', '2018-11-07 21:57:04', 0, 22, 'analysis', NULL),
(71, '2018-11-07 22:53:28', '2018-11-07 22:53:28', 0, 23, 'proposal', NULL),
(72, '2018-11-07 23:08:38', '2018-11-07 23:08:38', 0, 24, 'proposal', NULL),
(73, '2018-11-08 13:54:37', '2018-11-08 13:54:37', 0, 25, 'proposal', NULL),
(74, '2018-11-08 13:58:11', '2018-11-08 13:58:11', 0, 22, 'implementation', ''),
(75, '2018-11-08 13:58:15', '2018-11-08 13:58:15', 0, 22, 'implementation_handed_in', ''),
(76, '2018-11-08 14:01:16', '2018-11-08 14:01:16', 0, 22, 'defended', ''),
(77, '2018-11-08 14:37:24', '2018-11-08 14:37:24', 0, 23, 'accepted', NULL),
(78, '2018-11-08 14:38:18', '2018-11-08 14:38:18', 0, 23, 'analysis', NULL),
(79, '2018-11-08 18:56:18', '2018-11-08 18:56:18', 0, 24, 'accepted', NULL),
(80, '2018-11-09 01:20:59', '2018-11-09 01:20:59', 0, 26, 'proposal', NULL),
(81, '2018-11-09 01:27:39', '2018-11-09 01:27:39', 0, 26, 'accepted', NULL),
(82, '2018-11-09 01:29:58', '2018-11-09 01:29:58', 0, 26, 'analysis', NULL),
(83, '2018-11-09 01:35:01', '2018-11-09 01:35:01', 0, 26, 'analysis_handed_in', ''),
(84, '2018-11-09 01:43:54', '2018-11-09 01:43:54', 0, 26, 'analysis', ''),
(85, '2018-11-09 01:47:13', '2018-11-09 01:47:13', 0, 27, 'proposal', NULL),
(86, '2018-11-09 01:54:34', '2018-11-09 01:54:34', 0, 27, 'accepted', NULL),
(87, '2018-11-09 03:02:13', '2018-11-09 03:02:13', 0, 28, 'proposal', NULL),
(88, '2018-11-10 16:45:22', '2018-11-10 16:45:22', 0, 29, 'proposal', NULL),
(89, '2018-11-10 17:41:30', '2018-11-10 17:41:30', 0, 30, 'proposal', NULL),
(90, '2018-11-10 17:44:24', '2018-11-10 17:44:24', 0, 30, 'accepted', NULL),
(91, '2018-11-10 17:51:40', '2018-11-10 17:51:40', 0, 30, 'analysis', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_user`
--

CREATE TABLE `project_user` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_role` enum('team_member','supervisor','opponent','consultant') NOT NULL,
  `comment` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_user`
--

INSERT INTO `project_user` (`id`, `created_at`, `updated_at`, `deleted`, `user_id`, `project_id`, `project_role`, `comment`) VALUES
(54, '2018-11-07 14:17:58', '2018-11-07 14:17:58', 0, 1, 22, 'supervisor', NULL),
(55, '2018-11-07 14:17:58', '2018-11-07 14:17:58', 0, 2, 22, 'team_member', NULL),
(56, '2018-11-07 14:17:58', '2018-11-07 14:17:58', 0, 3, 22, 'team_member', NULL),
(57, '2018-11-07 14:17:58', '2018-11-07 14:17:58', 0, 12, 22, 'team_member', NULL),
(58, '2018-11-07 14:17:58', '2018-11-07 14:17:58', 0, 13, 22, 'team_member', NULL),
(59, '2018-11-07 22:53:28', '2018-11-07 22:53:28', 0, 1, 23, 'supervisor', NULL),
(60, '2018-11-07 22:53:28', '2018-11-07 22:53:28', 0, 12, 23, 'team_member', NULL),
(61, '2018-11-07 22:53:28', '2018-11-07 22:53:28', 0, 13, 23, 'team_member', NULL),
(62, '2018-11-07 22:53:28', '2018-11-07 22:53:28', 0, 23, 23, 'team_member', NULL),
(63, '2018-11-07 23:08:38', '2018-11-07 23:08:38', 0, 1, 24, 'supervisor', NULL),
(64, '2018-11-08 13:54:37', '2018-11-08 13:54:37', 0, 1, 25, 'supervisor', NULL),
(65, '2018-11-08 14:38:10', '2018-11-08 14:38:10', 0, 2, 23, 'team_member', NULL),
(66, '2018-11-09 01:20:59', '2018-11-09 01:20:59', 0, 5, 26, 'consultant', NULL),
(67, '2018-11-09 01:20:59', '2018-11-09 01:20:59', 0, 1, 26, 'supervisor', NULL),
(68, '2018-11-09 01:20:59', '2018-11-09 01:20:59', 0, 3, 26, 'team_member', NULL),
(69, '2018-11-09 01:20:59', '2018-11-09 01:20:59', 0, 12, 26, 'team_member', NULL),
(70, '2018-11-09 01:20:59', '2018-11-09 01:20:59', 0, 31, 26, 'team_member', NULL),
(71, '2018-11-09 01:20:59', '2018-11-09 01:20:59', 0, 34, 26, 'team_member', NULL),
(72, '2018-11-09 01:47:13', '2018-11-09 01:47:13', 0, 5, 27, 'consultant', NULL),
(73, '2018-11-09 01:47:13', '2018-11-09 01:47:13', 0, 30, 27, 'supervisor', NULL),
(74, '2018-11-09 03:02:13', '2018-11-09 03:02:13', 0, 29, 28, 'consultant', NULL),
(75, '2018-11-09 03:02:13', '2018-11-09 03:02:13', 0, 1, 28, 'supervisor', NULL),
(76, '2018-11-10 16:45:22', '2018-11-10 16:45:22', 0, 36, 29, 'consultant', NULL),
(77, '2018-11-10 16:45:22', '2018-11-10 16:45:22', 0, 29, 29, 'supervisor', NULL),
(78, '2018-11-10 16:45:22', '2018-11-10 16:45:22', 0, 2, 29, 'team_member', NULL),
(79, '2018-11-10 16:45:22', '2018-11-10 16:45:22', 0, 13, 29, 'team_member', NULL),
(80, '2018-11-10 17:41:30', '2018-11-10 17:41:30', 0, 37, 30, 'supervisor', NULL),
(81, '2018-11-10 17:41:30', '2018-11-10 17:41:30', 0, 2, 30, 'team_member', NULL),
(82, '2018-11-10 17:41:30', '2018-11-10 17:41:30', 0, 12, 30, 'team_member', NULL),
(83, '2018-11-10 17:41:30', '2018-11-10 17:41:30', 0, 13, 30, 'team_member', NULL),
(84, '2018-11-10 17:41:30', '2018-11-10 17:41:30', 0, 23, 30, 'team_member', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `proposal`
--

CREATE TABLE `proposal` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `state` enum('draft','sent','accepted','declined') NOT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `proposal`
--

INSERT INTO `proposal` (`id`, `created_at`, `updated_at`, `deleted`, `project_id`, `document_id`, `state`, `comment`) VALUES
(27, '2018-11-07 14:17:58', '2018-11-07 21:52:27', 0, 22, 128, 'accepted', 'Konečné rozhodnuti je **PŘIJMOUT**.'),
(28, '2018-11-07 22:53:28', '2018-11-08 14:12:28', 0, 23, 138, 'draft', 'Zamitnout pre nedostatok informácií.'),
(29, '2018-11-07 23:08:38', '2018-11-08 18:56:18', 0, 24, 140, 'accepted', 'Rozhodli sme sa prijať návrh.'),
(30, '2018-11-08 13:54:37', '2018-11-08 13:54:37', 0, 25, 142, 'draft', NULL),
(31, '2018-11-08 14:12:28', '2018-11-08 14:37:24', 0, 23, 149, 'accepted', 'Schvalen.'),
(32, '2018-11-09 01:20:59', '2018-11-09 01:27:39', 0, 26, 158, 'accepted', 'Comment of the final decision of this proposal.'),
(33, '2018-11-09 01:47:13', '2018-11-09 01:54:34', 0, 27, 162, 'accepted', ''),
(34, '2018-11-09 03:02:13', '2018-11-09 03:02:29', 0, 28, 164, 'sent', NULL),
(35, '2018-11-10 16:45:22', '2018-11-10 16:52:27', 0, 29, 166, 'sent', NULL),
(36, '2018-11-10 17:41:30', '2018-11-10 17:44:24', 0, 30, 168, 'accepted', '');

-- --------------------------------------------------------

--
-- Table structure for table `proposal_comment`
--

CREATE TABLE `proposal_comment` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `content` varchar(2000) NOT NULL,
  `committee_only` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `proposal_comment`
--

INSERT INTO `proposal_comment` (`id`, `created_at`, `updated_at`, `deleted`, `creator_id`, `proposal_id`, `content`, `committee_only`) VALUES
(18, '2018-11-07 14:29:02', '2018-11-07 14:29:02', 0, 8, 27, 'Pekny navrh projektu.', 1),
(19, '2018-11-07 21:01:28', '2018-11-07 21:17:49', 0, 28, 27, 'Navrh projektu splna vsetky body dobreho projektu.', 0),
(20, '2018-11-08 14:21:15', '2018-11-08 14:21:15', 0, 29, 31, 'Pekny projekt. Tesim se na vysledek.', 1),
(21, '2018-11-08 14:23:02', '2018-11-08 14:23:08', 0, 28, 31, 'Rozsah projektu splna poziadavky.', 0),
(22, '2018-11-08 18:47:44', '2018-11-08 18:47:44', 0, 1, 29, 'Návrh je v poriadku.', 1),
(23, '2018-11-09 01:26:33', '2018-11-09 01:26:40', 0, 1, 32, 'This proposal looks very promising.', 0),
(24, '2018-11-09 01:26:55', '2018-11-09 01:26:55', 0, 1, 32, 'Example of private comment', 1),
(25, '2018-11-09 01:54:18', '2018-11-09 01:54:22', 0, 1, 33, 'I think this is too ambitious but I am curious about the result.', 0),
(26, '2018-11-09 03:06:24', '2018-11-09 03:06:29', 0, 28, 34, 'Our faculty needs a project like this.', 0),
(27, '2018-11-09 03:07:20', '2018-11-09 03:07:20', 0, 29, 34, 'I agree', 1),
(28, '2018-11-10 16:58:56', '2018-11-10 17:09:30', 0, 28, 35, 'Odhad náročnosti nepočítá s časem na nastudování Unity a dalších frameworků a knihoven, které mají být použité pro vývoj této aplikace. Doporučil bych týmu řešitelů přehodnotit časový odhad.', 0),
(29, '2018-11-10 17:06:31', '2018-11-10 17:09:23', 0, 30, 35, 'Souhlasím a to samé platí pro sepsání specifikace a dokumentace. To také zabere dost času.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `proposal_vote`
--

CREATE TABLE `proposal_vote` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `vote` enum('yes','no') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `proposal_vote`
--

INSERT INTO `proposal_vote` (`id`, `created_at`, `updated_at`, `deleted`, `user_id`, `proposal_id`, `vote`) VALUES
(20, '2018-11-07 14:24:09', '2018-11-07 14:24:09', 0, 1, 27, 'yes'),
(21, '2018-11-07 14:27:45', '2018-11-07 14:27:45', 0, 8, 27, 'yes'),
(22, '2018-11-07 20:43:48', '2018-11-07 20:43:48', 0, 28, 27, 'yes'),
(23, '2018-11-08 14:19:18', '2018-11-08 14:19:18', 0, 1, 31, 'yes'),
(24, '2018-11-08 14:20:27', '2018-11-08 14:20:27', 0, 29, 31, 'yes'),
(25, '2018-11-08 14:20:36', '2018-11-08 14:20:36', 0, 29, 29, 'yes'),
(26, '2018-11-08 14:21:59', '2018-11-08 14:21:59', 0, 28, 31, 'yes'),
(27, '2018-11-08 14:29:16', '2018-11-08 14:29:16', 0, 30, 31, 'yes'),
(28, '2018-11-08 18:47:09', '2018-11-08 18:47:09', 0, 1, 29, 'yes'),
(29, '2018-11-09 01:26:10', '2018-11-09 01:26:10', 0, 1, 32, 'yes'),
(30, '2018-11-09 01:52:09', '2018-11-09 01:52:09', 0, 30, 33, 'yes'),
(31, '2018-11-09 03:02:43', '2018-11-09 03:02:46', 0, 1, 34, 'no'),
(32, '2018-11-09 03:05:56', '2018-11-09 03:05:56', 0, 28, 34, 'yes'),
(33, '2018-11-09 03:07:24', '2018-11-09 03:07:24', 0, 29, 34, 'yes'),
(34, '2018-11-10 17:06:42', '2018-11-10 17:06:42', 0, 30, 35, 'yes'),
(35, '2018-11-10 17:07:52', '2018-11-10 17:07:52', 0, 28, 35, 'yes'),
(36, '2018-11-10 17:24:09', '2018-11-10 17:24:09', 0, 8, 35, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `role_history`
--

CREATE TABLE `role_history` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `role` enum('student','academic','committee_member','committee_chair') DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `degree_prefix` varchar(45) DEFAULT NULL,
  `degree_suffix` varchar(45) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varbinary(200) DEFAULT NULL,
  `news_subscription` tinyint(1) NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `custom_url` varchar(500) DEFAULT NULL,
  `last_CAS_refresh` datetime DEFAULT NULL,
  `role` enum('student','academic','committee_member','committee_chair') DEFAULT NULL,
  `role_expiration` datetime DEFAULT NULL,
  `role_is_from_CAS` tinyint(1) NOT NULL,
  `login_type` enum('cas','manual') NOT NULL,
  `password_recovery_token` varchar(100) DEFAULT NULL,
  `password_recovery_token_expiration` datetime DEFAULT NULL,
  `banned` tinyint(1) NOT NULL,
  `auth_key` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `created_at`, `updated_at`, `deleted`, `name`, `degree_prefix`, `degree_suffix`, `email`, `password`, `news_subscription`, `last_login`, `custom_url`, `last_CAS_refresh`, `role`, `role_expiration`, `role_is_from_CAS`, `login_type`, `password_recovery_token`, `password_recovery_token_expiration`, `banned`, `auth_key`) VALUES
(1, '2018-08-10 19:45:22', '2018-11-12 20:43:23', 0, 'Petr Hnětynka', 'Doc. Mgr.', 'PhD.', 'hnetynka@gmail.com', 0x243279243133246f496e596c324d667048413752667a4b6c4f7871497575555270436231456a6144616c31573173506f712e3335756a544a3257486d, 1, '2018-11-12 20:43:23', 'https://d3s.mff.cuni.cz/~hnetynka/', '2017-08-10 19:45:22', 'committee_chair', '2019-08-10 19:45:22', 1, 'manual', 'JIFPOSDifjsdfjjapjOIJP', '2018-08-14 19:45:22', 0, NULL),
(2, '2018-08-10 19:45:23', '2018-11-10 17:54:33', 0, 'Jiří Novák', NULL, NULL, 'jiri.novak@gmail.com', 0x24327924313324412f4d3637764b77674e77654c6b5a327a7379366a2e714a442e634a30744463636c384c387a324e69683553596832504a4c504371, 1, '2018-11-10 17:54:33', NULL, '2018-01-30 00:00:00', 'student', '2019-01-30 00:00:00', 1, 'cas', 'GwTqWKlcjtsmObUDkLXP', '2018-07-26 00:00:00', 0, NULL),
(3, '2018-08-10 19:45:24', '2018-08-11 15:45:23', 0, 'Jan Svoboda', 'Bc.', NULL, 'jan.svoboda@gmail.com', 0x243279243133243277622e5879497974654b6877317352554e46783965622f4c2f48354d4b766377323073427a6b2e68324a31586b72374447513079, 1, '2018-08-11 15:45:23', 'www.jansvob.czhost.cz', '2018-01-15 10:01:07', 'student', '2019-04-30 00:13:00', 1, 'cas', 'corajenny', '2018-07-26 00:00:00', 0, NULL),
(5, '2018-08-10 19:45:25', '2018-08-11 21:21:08', 0, 'Jana Novotná', NULL, NULL, 'jana.novotna65@seznam.cz', 0x24327924313324476961334f33754c576732733172594966412e76774f594553326b4557394d4b2f34617a506f64313542723837626a497552533636, 1, '2018-08-11 21:21:08', 'www.jannov.cz', '2018-08-10 19:45:24', 'academic', '2019-01-30 00:00:00', 0, 'manual', 'bobeštristan', '2018-07-26 00:23:00', 0, NULL),
(8, '2018-08-10 19:45:27', '2018-11-10 17:56:29', 0, 'Tomáš Holan', 'RNDr.', 'Phd.', 'holan@mff.cz', 0x243279243133246b55456d2e6e4b61786f4f4f6f576a6e47372e516b2e57534471526b346c652e6c346e66444c356956343850543547306972337175, 1, '2018-11-10 17:56:29', 'http://ksvi.mff.cuni.cz/~holan/', '2017-04-11 00:00:00', 'committee_chair', '2019-01-28 00:00:00', 0, 'manual', 'babette45', '2018-08-10 19:45:26', 0, NULL),
(10, '2018-08-10 19:45:28', '2018-08-16 16:32:55', 0, 'Miroslav Veselý', 'Ing.', '', 'miro_vesel@gmail.com', 0x24327924313324304635445366335a5468526b5864622f39614b2e4f754a4a764f586e2e6b7376477769515a2e6e74756535617168795742416c6432, 1, '2018-08-16 16:32:55', 'http://www.mff.cuni.cz/~mirves', '2018-01-28 00:00:00', 'academic', '2019-01-28 00:00:00', 1, 'cas', 'columbo', '2018-08-14 07:00:00', 0, NULL),
(11, '2018-08-10 19:45:29', '2018-08-11 22:41:17', 0, 'John Doe', 'Ing.', '', 'jonh_doe@gmail.com', 0x24327924313324495a72514c6b6f4b4d6e4b6a3246304e456452634165454543684d3544536d504c74774d4a4b554a33574f706d4c4a506565344c47, 0, '2018-08-11 22:41:17', 'http://www.mff.cuni.cz/~johnd', '2018-01-28 00:00:00', 'academic', '2019-01-28 00:00:00', 1, 'cas', NULL, '2018-08-10 19:45:28', 0, NULL),
(12, '2018-08-10 19:45:30', '2018-08-10 19:45:30', 0, 'Sam Williams', NULL, 'Phd.', 'samwilliams@gmail.com', 0x243279243133246b614634373676684630376d654c795557586c75584f6a49332e344e424d4a75776e65544b424b464d574f6b534164443537786579, 0, '2018-01-29 15:15:12', 'http://www.mff.cuni.cz/~samwil', '2018-01-28 00:00:00', 'student', '2019-01-28 00:00:00', 1, 'cas', NULL, '2018-08-10 19:45:29', 0, NULL),
(13, '2018-08-10 19:45:30', '2018-08-10 19:45:30', 0, 'Jessica Twain', NULL, NULL, 'jestwain@gmail.com', 0x2432792431332468494634547a315a34303638714659707450726f354f4174495679742e4456694c64546a6551495371356a63357a334833754a5075, 0, '2018-01-29 15:15:12', 'http://www.mff.cuni.cz/~jestwa', '2018-01-28 00:00:00', 'student', '2019-01-28 00:00:00', 1, 'cas', NULL, '2018-08-10 19:45:30', 0, NULL),
(23, '2018-08-09 00:00:00', '2018-10-30 19:44:49', 0, 'Li Hao', NULL, NULL, 'li.hao@gmail.com', 0x24327924313324412f4d3637764b77674e77654c6b5a327a7379366a2e714a442e634a30744463636c384c387a324e69683553596832504a4c504371, 0, '2018-10-30 19:44:49', NULL, NULL, 'student', NULL, 1, 'cas', NULL, NULL, 0, NULL),
(28, '2018-08-11 12:59:03', '2018-11-12 20:34:09', 0, 'David Bednárek', 'RNDr.', 'PhD.', 'bednarek@mff.cz', 0x243279243133244f5a6b69425a516d6b5635324962324a4b757057784f3273377246684e745a464b3374736a58327645687749356f4b53383265394f, 0, '2018-11-12 20:34:09', NULL, NULL, 'committee_member', '2019-04-20 00:00:00', 0, 'cas', NULL, NULL, 0, NULL),
(29, '2018-08-11 13:09:28', '2018-11-10 16:43:37', 0, 'Jakub Gemrot', 'Mgr.', 'PhD.', 'gemrot@mff.cz', 0x2432792431332458562e364c675166764b66486d5171476e4b70722e6566385267444f48397967344a4d52455643773843386f484b58536d65394161, 0, '2018-11-10 16:43:37', NULL, NULL, 'committee_member', '2019-02-15 00:00:00', 0, 'cas', NULL, NULL, 0, NULL),
(30, '2018-08-11 13:23:58', '2018-11-12 20:34:51', 0, 'Martin Kruliš', 'RNDr.', 'Ph.D.', 'krulis@mff.cz', 0x24327924313324316144626b4e48446842436e6a42755931373552774f424357364574584945337062354d594858703377632f6e385267493566736d, 0, '2018-11-12 20:34:51', NULL, NULL, 'committee_member', '2019-04-07 00:00:00', 0, 'cas', NULL, NULL, 0, NULL),
(31, '2018-08-11 21:15:57', '2018-08-16 16:23:36', 0, 'Artur Finger', '', '', 'finger@gmail.com', 0x243279243133244d5968554e334e6634704b65644a544b4d74543142655a494e634966456e6757323231516a66736d7a59766d435a2e70536b307153, 0, '2018-08-16 16:23:36', NULL, NULL, 'student', '2019-04-20 00:00:00', 0, 'cas', NULL, NULL, 0, NULL),
(33, '2018-08-11 21:17:54', '2018-08-11 21:18:10', 0, 'Slávka Ivaničová', '', '', 'slavka@gmail.com', 0x243279243133246d4671686353666a4265652e3870736b687a4f39784f3849727248612e617551446d6c6e5465555531702e5474794e39744d794c47, 0, '2018-08-11 21:18:10', NULL, NULL, 'student', '2019-04-21 00:00:00', 0, 'cas', NULL, NULL, 0, NULL),
(34, '2018-08-11 21:19:24', '2018-10-30 19:43:40', 0, 'Gergely Tóth', '', '', 'gergely@gmail.com', 0x2432792431332467676551743259775932706975356e615271612e6d2e585a4f4a75423244746f6c4f4d694d5a3466436c3430792e55774b74784d79, 0, '2018-10-30 19:43:40', NULL, NULL, 'student', '2019-04-20 00:00:00', 0, 'cas', NULL, NULL, 0, NULL),
(35, '2018-08-11 21:20:36', '2018-08-11 21:20:52', 0, 'Marek Beňovič', '', '', 'marek@gmail.com', 0x243279243133247645482f422f4a332e654f38727a453638644876612e5a4b31426d584e337545785873576d6d3978675242546447653777792e3675, 0, '2018-08-11 21:20:52', NULL, NULL, 'student', '2019-08-24 00:00:00', 0, 'cas', NULL, NULL, 0, NULL),
(36, '2018-08-16 10:40:42', '2018-08-16 16:41:56', 0, 'Jiří Benc', '', '', 'benc@mff.cz', 0x243279243133242f507a476161464e61736a6468386f686f793749354f6d304b4c62795246367956627255304c347279515a4f334b6e6c335750632e, 0, '2018-08-16 16:41:56', NULL, NULL, 'academic', '2019-02-23 00:00:00', 0, 'cas', NULL, NULL, 0, NULL),
(37, '2018-08-16 10:43:29', '2018-11-10 17:44:58', 0, 'Martin Děcký', 'Mgr.', 'PhD.', 'decky@mff.cz', 0x24327924313324412f4d3637764b77674e77654c6b5a327a7379366a2e714a442e634a30744463636c384c387a324e69683553596832504a4c504371, 0, '2018-11-10 17:44:58', NULL, NULL, 'academic', '2019-12-28 00:00:00', 0, 'cas', NULL, NULL, 0, NULL),
(38, '2018-06-11 10:43:29', '2018-10-30 19:28:08', 0, 'Libor Forst', 'RNDr.', '', 'forst@mff.cz', 0x24327924313324412f4d3637764b77674e77654c6b5a327a7379366a2e714a442e634a30744463636c384c387a324e69683553596832504a4c504371, 0, '2018-10-30 19:28:08', NULL, NULL, 'academic', '2019-12-28 00:00:00', 0, 'cas', NULL, NULL, 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisement`
--
ALTER TABLE `advertisement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advertisement_user_id_idx` (`user_id`);

--
-- Indexes for table `advertisement_comment`
--
ALTER TABLE `advertisement_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advertisement_comment_ad_id_idx` (`ad_id`),
  ADD KEY `advertisement_comment_user_id_idx` (`user_id`);

--
-- Indexes for table `audit`
--
ALTER TABLE `audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `defence_attendance`
--
ALTER TABLE `defence_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `defence_attendance_user_id_idx` (`user_id`),
  ADD KEY `defence_attendance_defence_date_id_idx` (`defence_date_id`) USING BTREE;

--
-- Indexes for table `defence_date`
--
ALTER TABLE `defence_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_user_id_idx` (`user_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_version`
--
ALTER TABLE `page_version`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_page_id_idx` (`page_id`),
  ADD KEY `pages_user_id_idx` (`user_id`),
  ADD KEY `pages_content_cs_idx` (`content_cs`) USING BTREE,
  ADD KEY `pages_content_en_idx` (`content_en`) USING BTREE;

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `projects_short_name_uq` (`short_name`),
  ADD KEY `projects_content_idx` (`content`);

--
-- Indexes for table `project_defence`
--
ALTER TABLE `project_defence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_defence_project_id_idx` (`project_id`),
  ADD KEY `project_defence_defence_date_id_idx` (`defence_date_id`),
  ADD KEY `project_defence_statement_author_id_idx` (`statement_author_id`),
  ADD KEY `project_defence_defence_statement_idx` (`defence_statement`),
  ADD KEY `project_defence_opponent_review_idx` (`opponent_review`),
  ADD KEY `project_defence_supervisor_review_idx` (`supervisor_review`),
  ADD KEY `project_defence_opponent_id_fk` (`opponent_id`);

--
-- Indexes for table `project_document`
--
ALTER TABLE `project_document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_document_project_id_idx` (`project_id`),
  ADD KEY `project_document_document_id_idx` (`document_id`);

--
-- Indexes for table `project_state_history`
--
ALTER TABLE `project_state_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_state_history_project_id_idx` (`project_id`);

--
-- Indexes for table `project_user`
--
ALTER TABLE `project_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_user_user_id_idx` (`user_id`),
  ADD KEY `project_user_project_id_idx` (`project_id`);

--
-- Indexes for table `proposal`
--
ALTER TABLE `proposal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proposal_project_id_idx` (`project_id`),
  ADD KEY `proposal_document_id_idx` (`document_id`);

--
-- Indexes for table `proposal_comment`
--
ALTER TABLE `proposal_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator_id_idx` (`creator_id`),
  ADD KEY `proposal_id_idx` (`proposal_id`);

--
-- Indexes for table `proposal_vote`
--
ALTER TABLE `proposal_vote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proposal_vote_user_id_idx` (`user_id`),
  ADD KEY `proposal_vote_proposal_id_idx` (`proposal_id`);

--
-- Indexes for table `role_history`
--
ALTER TABLE `role_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_uq` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisement`
--
ALTER TABLE `advertisement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `advertisement_comment`
--
ALTER TABLE `advertisement_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `audit`
--
ALTER TABLE `audit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `defence_attendance`
--
ALTER TABLE `defence_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `defence_date`
--
ALTER TABLE `defence_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `page_version`
--
ALTER TABLE `page_version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `project_defence`
--
ALTER TABLE `project_defence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_document`
--
ALTER TABLE `project_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `project_state_history`
--
ALTER TABLE `project_state_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `project_user`
--
ALTER TABLE `project_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `proposal`
--
ALTER TABLE `proposal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `proposal_comment`
--
ALTER TABLE `proposal_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `proposal_vote`
--
ALTER TABLE `proposal_vote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `role_history`
--
ALTER TABLE `role_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `advertisement`
--
ALTER TABLE `advertisement`
  ADD CONSTRAINT `advertisement_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `advertisement_comment`
--
ALTER TABLE `advertisement_comment`
  ADD CONSTRAINT `advertisement_comment_ad_id_fk` FOREIGN KEY (`ad_id`) REFERENCES `advertisement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `advertisement_comment_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `defence_attendance`
--
ALTER TABLE `defence_attendance`
  ADD CONSTRAINT `defence_attendance_defence_date_id_fk` FOREIGN KEY (`defence_date_id`) REFERENCES `defence_date` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `defence_attendance_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `page_version`
--
ALTER TABLE `page_version`
  ADD CONSTRAINT `pages_content_cs_idx` FOREIGN KEY (`content_cs`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pages_content_en_idx` FOREIGN KEY (`content_en`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pages_page_id_fk` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pages_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `projects_content_fk` FOREIGN KEY (`content`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `project_defence`
--
ALTER TABLE `project_defence`
  ADD CONSTRAINT `project_defence_defence_date_id_fk` FOREIGN KEY (`defence_date_id`) REFERENCES `defence_date` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_defence_statement_fk` FOREIGN KEY (`defence_statement`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_opponent_id_fk` FOREIGN KEY (`opponent_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `project_defence_opponent_review_fk` FOREIGN KEY (`opponent_review`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_statement_author_id_fk` FOREIGN KEY (`statement_author_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_supervisor_review_fk` FOREIGN KEY (`supervisor_review`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `project_document`
--
ALTER TABLE `project_document`
  ADD CONSTRAINT `project_document_document_id_fk` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_document_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `project_state_history`
--
ALTER TABLE `project_state_history`
  ADD CONSTRAINT `project_state_history_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `project_user`
--
ALTER TABLE `project_user`
  ADD CONSTRAINT `project_user_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `proposal`
--
ALTER TABLE `proposal`
  ADD CONSTRAINT `proposal_document_id_fk` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `proposal_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `proposal_comment`
--
ALTER TABLE `proposal_comment`
  ADD CONSTRAINT `creator_id` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `proposal_comment_creator_id_idx` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `proposal_comment_proposal_id_idx` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `proposal_id` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `proposal_vote`
--
ALTER TABLE `proposal_vote`
  ADD CONSTRAINT `proposal_vote_proposal_id_fk` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `proposal_vote_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `role_history`
--
ALTER TABLE `role_history`
  ADD CONSTRAINT `role_history_user_id_fk` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
