<?php
namespace app\controllers;

use app\cas\CasClient;
use Yii;
use yii\web\Controller;
use app\consts\Param;

/**
 * Class BaseController
 * @package app\controllers
 */
class BaseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (Yii::$app->session->has('lang')) {
            Yii::$app->language = Yii::$app->session->get('lang');
        }
        return parent::beforeAction($action);
    }

    /**
     * Create new CAS client
     * @return CasClient    New cas client
     */
    public function createCasClient() : CasClient
    {
        $params = Yii::$app->params[Param::CAS_PARAMETERS];
        return new CasClient(
            $params['host'],
            $params['port'],
            $params['url'],
            preg_replace('/@runtime/', Yii::$app->runtimePath, $params['log']),
            false
        );
    }
}