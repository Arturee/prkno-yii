<?php
namespace app\controllers;

use app\consts\Permission;
use app\exceptions\CasConnectionException;
use app\exceptions\OrmException;
use app\mail\Emails;
use app\models\records\User;
use app\models\search\UserSearch;
use app\utils\Anonymizer;
use app\utils\DateTime;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'reload-cas' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => [Permission::USERS_DELETE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'view', 'ban', 'anonymize', 'inactive-anonymize'],
                        'roles' => [Permission::USERS_MANAGE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['profile'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-profile', 'reload-cas'],
                        'roles' => [Permission::USERS_UPDATE_OWN_PROFILE],
                        'roleParams' => function () {
                            return [Permission::PARAM_USER => User::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception(Yii::t('app', 'You are not allowed to access this page'));
                },
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return string the rendering result of the index page
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param int $id   User id
     * @return string The rendering result of the view page
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(int $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single User profile.
     * @param int $id   User id
     * @return string Rendering result of the profile page
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionProfile(int $id)
    {
        return $this->render('profile', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the view page.
     * @return Response|string Response object from redirection to view page, if action was successfull
     *                         or rendering result of the 'create' view if not.
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->loadFromPost()) {
            if ($model->role == '') {
                $model->role = null;
            }
            $model->role_expiration = new DateTime($model->role_expiration);
            $model->last_login = null;
            $model->last_CAS_refresh = null;
            $model->login_type = "manual";
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the view page.
     * @param int $id   User id
     * @return Response|string Response object from redirection to view page, if action was successfull
     *                         or rendering result of the update view if not.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);
        $role = $model->role;
        $loginType = $model->login_type;

        if ($model->loadFromPost()) {
            if ($model->role == '') {
                $model->role = null;
            }

            $expiration = $model->role_expiration;
            if (strlen(trim($expiration)) > 0) {
                $model->role_expiration = new DateTime($expiration);
            } else {
                $model->role_expiration = null;
            }

            if ($model->save()) {
                if ($role != $model->role) {
                    Emails::userRoleChanged($model);
                }
                if ($loginType != $model->login_type && $model->login_type == User::LOGIN_TYPE_MANUAL) {

                    if (!User::isPasswordRecoveryTokenValid($model->password_recovery_token)) {
                        $model->generatePasswordRecoveryToken();
                        if ($model->save()) {
                            Emails::userLoginTypeChanged($model);
                        }
                    }
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Users model Profile.
     * If update is successful, the browser will be redirected to the view page.
     * @param int $id   User id
     * @return Response|string  Response object from redirection to profile page, if action was successfull
     *                          or rendering result of the update-profile view if not.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateProfile(int $id)
    {
        $model = $this->findModel($id);

        if ($model->loadFromPost() && $model->save()) {
            return $this->redirect(['profile', 'id' => $model->id]);
        }

        return $this->render('update-profile', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the index page.
     * @param int $id   User id
     * @return Response     Response object from redirection to index page
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete(int $id)
    {
        if (Yii::$app->user->id != $id) {
            $model = $this->findModel($id);
            $model->deleted = true;
            $model->save();
        }
        return $this->redirect(['index']);
    }

    /**
     * Ban user with given id
     * @param int $id   User id
     * @return Response     Response object from redirection to index page
     */
    public function actionBan(int $id)
    {
        if (Yii::$app->user->id != $id) {
            $model = $this->findModel($id);
            $model->banned = !$model->banned;
            $model->save();
        }

        return $this->redirect(['index']);
    }

    /**
     * Anonymize user with given id
     * @param int $id   User id
     * @return Response Response object from redirection to index page
     */
    public function actionAnonymize(int $id)
    {
        if (Yii::$app->user->id != $id) {
            $model = $this->findModel($id);
            $model->anonymize();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Download user info from cas and update user data in database
     * @param int $id   User id
     * @return Response Response object from redirection to referrer
     */
    public function actionReloadCas(int $id)
    {
        $model = $this->findModel($id);
        $original = clone $model;
        try {
            $info = $this->createCasClient()->downloadCurrentUserInfo();
            $model->loadCasInfo($info);
            $model->save();
        } catch (OrmException $e) {
            $model->email = $original->email;
            $model->save();
        } catch (CasConnectionException $e) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Failed. A problem with CAS occured.'));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Anonymize inactive and deleted users
     * @return Response     Redirection object to index page
     */
    public function actionInactiveAnonymize()
    {
        Anonymizer::anonymizeInactiveAndDeletedUsers();
        $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id   User id
     * @return User The loaded user model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : User
    {
        $model = User::findIdentity($id);
        if ($model == null || $model->deleted) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }
}
