<?php
namespace app\controllers;

use app\consts\Param;
use app\consts\Permission;
use app\mail\Emails;
use app\models\forms\UploadForm;
use app\models\records\Document;
use app\models\records\Project;
use app\models\records\ProjectDocument;
use app\models\records\User;
use app\models\search\ProjectSearch;
use app\utils\storage\ProjectStorage;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ProjectsController implements the CRUD actions for Project model.
 */
class ProjectsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'status-update' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['my'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [Permission::PROJECT_CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => [Permission::PROJECT_DELETE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'send', 'request-to-run'],
                        'roles' => [Permission::PROJECT_UPDATE_OWN_PROJECT],
                        'roleParams' => function () {
                            return ['project' => Project::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['send-documents'],
                        'roles' => [Permission::PROJECT_SEND_DOCUMENTS],
                        'roleParams' => function () {
                            return ['project' => Project::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'update-content', 'advice-document', 'send', 'send-documents', 'request-to-run'],
                        'roles' => [Permission::PROJECT_UPDATE],
                        'roleParams' => function () {
                            return ['project' => Project::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-content'],
                        'roles' => [Permission::PROJECT_UPDATE_CONTENT],
                        'roleParams' => function () {
                            return ['project' => Project::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['advice-document'],
                        'roles' => [Permission::PROJECT_UPDATE_ADVICE],
                        'roleParams' => function () {
                            return ['project' => Project::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['status-update', 'run'],
                        'roles' => [Permission::PROJECT_UPDATE_STATUS],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['upload', 'upload-defence', 'delete-document'],
                        'roles' => [Permission::PROJECT_UPLOAD_FILE],
                        'roleParams' => function () {
                            return ['project' => Project::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception(Yii::t('app', 'You are not allowed to access this page'));
                },
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return string the rendering result.
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all user Project models.
     * @return string the rendering result.
     */
    public function actionMy()
    {
        $searchModelMyProjects = new ProjectSearch();
        $dataProviderMyProjects = $searchModelMyProjects->search([
            $searchModelMyProjects->formName() => ['supervisorIds' => Yii::$app->user->id]
        ]);

        return $this->render('my', [
            'searchModelMyProjects' => $searchModelMyProjects,
            'dataProviderMyProjects' => $dataProviderMyProjects,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param int $id   Project id
     * @return string the rendering result.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(int $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string the rendering result.
     */
    public function actionCreate()
    {
        $model = new Project();
        $model->scenario = Project::SCENARIO_CREATE;
        $model->supervisorIds = Yii::$app->user->id;

        if ($model->loadFromPost() && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Saved'));
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id   Project id
     * @return Response|string  Response object from redirection if action was successfull
     *                          or rendering result if not.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);
        $model->scenario = Project::SCENARIO_DRAFT;

        if (Yii::$app->request->post()) {
            $model->consultantIds = [];
            $model->memberIds = [];

            if ($model->loadFromPost() && $model->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Updated'));
                return $this->redirect(['view', 'id' => $model->id]);

            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Update project status by AJAX request.
     * @param int $id   Project id
     * @return Response|string  Response object from redirection to view if no data from post were loaded
     *                          or response object for the request
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionStatusUpdate(int $id)
    {
        $model = $this->findModel($id);
        $model->scenario = Project::SCENARIO_STATUS_UPDATE;

        $previousState = $model->state;

        if ($model->loadFromPost()) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->requested_to_run) $model->requested_to_run = false;

            if ($model->save()) {
                /** @var \app\models\records\ProjectStateHistory $lastState */
                $lastState = $model->getLastState();
                $lastState->comment = Yii::$app->request->post('statusComment');
                $lastState->save();

                if ($previousState != $model->state) {
                    foreach ($model->getActiveUsers()->all() as $user) {
                        Emails::projectStateChanged($model, $previousState, $user);
                    }
                }

                return ['output' => Yii::t('app', $model->state), 'message' => ''];
            }

            return ['output' => '', 'message' => Yii::t('app', 'Validation error')];
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Updates content of an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id   Project id
     * @return Response|string  Response object from redirection if action was successfull
     *                          or the rendering result of the update-content view
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateContent(int $id)
    {
        $model = $this->findModel($id);

        if ($model->loadFromPost() && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Updated'));
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update-content', [
            'model' => $model,
        ]);
    }

    /**
     * Creates or updates Advice document.
     * @param int $id       Project id
     * @return Response Response object from redirection to the view page
     * @throws NotFoundHttpException if the model cannot be found 
     */
    public function actionAdviceDocument(int $id)
    {
        $model = $this->findModel($id);

        if ($model->loadFromPost()) {
            if (isset($model->adviceDocument)) {
                $document = Document::findOne($model->adviceDocument->document_id);
                $document->content_md = $model->adviceMarkdown;
                $document->save();
            } else {
                $document = new Document(['content_md' => $model->adviceMarkdown]);
                $document->save();
                $projectDocument = new ProjectDocument([
                    'project_id' => $id, 'document_id' => $document->id,
                    'team_private' => 0, 'type' => ProjectDocument::TYPE_ADVICE, 'comment' => ''
                ]);
                $projectDocument->save();
            }
            if ($model->save()) Yii::$app->session->addFlash('success', Yii::t('app', 'Updated'));
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Set proposal to wait for confirmation by committee members
     * @param int $id   Project id
     * @return Response Response object from redirection to the view page
     * @throws NotFoundHttpException if the model cannot be found 
     */
    public function actionSend(int $id)
    {
        $model = $this->findModel($id);
        $model->scenario = Project::SCENARIO_SEND;
        $model->waiting_for_proposal_evaluation = true;

        if ($model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Sent for approval'));

            foreach (User::findCommitteeMembers() as $committeeMember) {
                Emails::proposalSent($model, $committeeMember);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    /**
     * Send documents for defence.
     * @param int $id   Project id
     * @return Response Response object from redirection to the view page
     * @throws NotFoundHttpException if the model cannot be found 
     */
    public function actionSendDocuments(int $id)
    {
        $model = $this->findModel($id);

        if ($model->save() && $model->sendDefenceDocuments()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Defence documents sent'));
            foreach ($model->getActiveUsers()->all() as $user) {
                Emails::projectDocumentsUploaded($model, $user);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            Yii::$app->session->addFlash('danger', Yii::t('app', 'Defence documents can not be send'));
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id   Project id
     * @return Response Response object from redirection to the index page
     * @throws NotFoundHttpException if the model cannot be found 
     */
    public function actionDelete(int $id)
    {
        $model = $this->findModel($id);
        $model->deleted = true;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Request project for runnning. Project is running after approval
     * for running.
     * @param int $id   Project id
     * @return Response Response object from redirection to the view page
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRequestToRun(int $id)
    {
        $model = $this->findModel($id);

        $minUserCount = Yii::$app->params[Param::MIN_USERS_ON_PROJECT];
        if (sizeof($model->members) < $minUserCount) {
            Yii::$app->session->addFlash(
                'error',
                Yii::t('app', 'At least {0} students must be assigned to this project.', [$minUserCount])
            );
            return $this->redirect(['view', 'id' => $id]);
        }

        if ($model->loadFromPost() && $model->validate()) {
            $model->requested_to_run = true;
            $model->save();
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Sorry, we are unable to send request.'));
        }
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Run project which has been requested for running.
     * @param int $id   Project id
     * @return Response Response object from redirection to the view page
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRun(int $id)
    {
        $model = $this->findModel($id);

        $previousState = $model->state;
        $model->requested_to_run = false;
        $model->state = Project::STATE_ANALYSIS;
        $model->save();

        foreach ($model->getActiveUsers()->all() as $user) {
            Emails::projectStateChanged($model, $previousState, $user);
        }
        Yii::$app->session->addFlash('success', Yii::t('app', 'Emails sent, project state changed.'));
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Delete document uploaded for the given project
     * @param int $id   Project id
     * @return Response Response object from redirection to the view page
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteDocument($doc, $id)
    {
        $model = ProjectDocument::findOne(['id' => $doc]);
        $model->deleted = true;
        $model->document->deleted = true;
        $model->document->save();
        $model->save();

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Upload document for the project.
     * @param int $id   Project id
     * @return Response Response object from redirection to the view page
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpload(int $id)
    {
        $model = $this->findModel($id);
        $uploadModel = new UploadForm();
        $comment = Yii::$app->request->post('ProjectDocument')['comment'];
        $teamPrivate = Yii::$app->request->post('ProjectDocument')['team_private'];

        if (Yii::$app->request->isPost) {
            $uploadModel->file = UploadedFile::getInstances($uploadModel, 'file');

            if ($uploadModel->file) {
                $this->saveProjectDocument(
                    $uploadModel->file[0],
                    $model->id,
                    $teamPrivate,
                    ProjectDocument::TYPE_READ_ONLY,
                    $comment
                );
            }
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Upload documents for defence. Overwrite existing defence documents.
     * @param int $id   Project id
     * @return Response Response object from redirection to the view page
     * @throws NotFoundHttpException
     */
    public function actionUploadDefence(int $id)
    {
        $model = $this->findModel($id);
        $uploadModel = new UploadForm();
        $nextDefenceNumber = $model->getUpcomingDefenceNumber();

        if (Yii::$app->request->isPost) {
            $uploadModel->zip = UploadedFile::getInstances($uploadModel, 'zip');
            $uploadModel->pdf = UploadedFile::getInstances($uploadModel, 'pdf');

            $defenceDocuments = $model->getDefenceDocuments();
            if ($uploadModel->zip) {
                $uploadFile = $uploadModel->zip[0];
                if ($defenceDocuments->zip != null) {
                    $defenceDocuments->zip->fallback_path = ProjectStorage::store($uploadFile->tempName, $model->id, ProjectDocument::TYPE_FOR_DEFENSE_ZIP, $uploadFile->name, $nextDefenceNumber);
                    $defenceDocuments->zip->save();
                } else {
                    $this->saveProjectDocument($uploadFile, $model->id, true, ProjectDocument::TYPE_FOR_DEFENSE_ZIP, null, $nextDefenceNumber);
                }
            }
            if ($uploadModel->pdf) {
                $uploadFile = $uploadModel->pdf[0];
                if ($defenceDocuments->pdf != null) {
                    $defenceDocuments->pdf->fallback_path = ProjectStorage::store($uploadFile->tempName, $model->id, ProjectDocument::TYPE_FOR_DEFENSE_PDF, $uploadFile->name, $nextDefenceNumber);
                    $defenceDocuments->zip->save();
                } else {
                    $this->saveProjectDocument($uploadFile, $model->id, true, ProjectDocument::TYPE_FOR_DEFENSE_PDF, null, $nextDefenceNumber);
                }
            }
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id   Project id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : Project
    {
        $model = Project::findOne($id);
        if ($model == null || $model->deleted) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    private function saveProjectDocument(UploadedFile $uploadFile, $projectId, $teamPrivate, $documentType, $comment, ? int $defenceNumber = null)
    {
        $document = new Document();
        $document->fallback_path = ProjectStorage::store($uploadFile->tempName, $projectId, $documentType, $uploadFile->name, $defenceNumber);
        $document->save();
        $projectDocument = new ProjectDocument([
            'project_id' => $projectId,
            'document_id' => $document->id,
            'team_private' => $teamPrivate,
            'type' => $documentType,
            'comment' => $comment,
            'defence_number' => $defenceNumber
        ]);
        $projectDocument->save();
    }
}
