<?php
namespace app\controllers;

use app\exceptions\BugError;
use app\models\records\Advertisement;
use app\models\records\News;
use app\models\records\Project;
use app\models\records\ProjectDefence;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * 
 * iCal: https://github.com/markuspoerschke/iCal
 * composer require eluceo/ical
 * 
 * inspiration:
 * http://www.yiiframework.com/doc-2.0/guide-runtime-responses.html
 *
 * example endpoint:
 * http://localhost/prkno-yii/web/api/projects?format=xml
 */
class ApiController extends BaseController
{
    /**
     * Set response format for the action
     * @param string $requestedTable    Name of the database table
     * @param int $id                   Id of the record
     * @param string $format            Value format
     * @return mixed All models from the requested table, or one model based on given id
     */
    public function actionHandler(string $requestedTable, int $id = null, string $format = 'json')
    {
        switch ($format) {
            case 'json':
                Yii::$app->response->format = Response::FORMAT_JSON;
                header('Content-type: application/json');
                break;

            case 'xml':
                Yii::$app->response->format = Response::FORMAT_XML;
                header('Content-type: application/xml');
                break;

            case 'ical':
                header('Content-Type: text/calendar');
                return $this->createICal($requestedTable, $id);

            default:
                $data = [
                    "error" => "Invalid format '" . $format . "'. Use ?format=json|xml|ical."
                ];
                $this->sendJsonError($data);
                break;
        }

        if (!$id)
            return $this->returnAllEntries($requestedTable);
        else
            return $this->returnSingleEntry($requestedTable, $id);
    }

    private function sendJsonError(array $data) : void
    {
        Yii::$app->response->statusCode = 400;
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->data = $data;
        Yii::$app->response->send();
        exit();
    }

    private function returnAllEntries(string $requestedTable)
    {
        switch ($requestedTable) {
            case 'projects':
                return Project::find()->all();

            case 'defences':
                return ProjectDefence::find()->all();

            case 'news':
                return News::find()->all();

            case 'ads':
                return Advertisement::find()->all();
        }

        throw new BugError('url rewriting failed');
    }

    private function returnSingleEntry(string $requestedTable, int $id)
    {
        if ($requestedTable !== 'defences' && $requestedTable !== 'projects') {
            $data = [
                "error" => "Parameter '?id=' not supported for this URL."
            ];
            $this->sendJsonError($data);
        }

        switch ($requestedTable) {
            case 'projects':
                return Project::find()->where(['id' => $id])->one();

            case 'defences':
                return ProjectDefence::find()->where(['id' => $id])->one();
        }

        throw new BugError('url rewriting failed');
    }

    private function createICal(string $requestedTable, int $id = null) : string
    {
        if ($requestedTable !== 'defences') {
            $data = [
                "error" => "ical format not supported for this URL."
            ];
            $this->sendJsonError($data);
        }

        $vCalendar = new Calendar('prkno/defences');
        $events = $this->createEvents($id);
        foreach ($events as $vEvent) {
            $vCalendar->addComponent($vEvent);
        }
        return $vCalendar->render();
    }

    private function createEvents(int $id = null) : array
    {
        if ($id) {
            return [$this->createSingleEventById($id)];
        }

        $allDefenceRecords = ProjectDefence::find()->all();
        $defenceIdArray = \yii\helpers\ArrayHelper::getColumn($allDefenceRecords, 'id');
        $events = [];
        foreach ($defenceIdArray as $defenceId) {
            $defenceEvent = $this->createSingleEventById($defenceId);
            array_push($events, $defenceEvent);
        }

        return $events;
    }

    private function createSingleEventById(int $id) : Event
    {
        $defence = ProjectDefence::find()->where(['id' => $id])->one()->toArray();
        $defenceDescription = $this->createDefenceDescription($defence);

        $iCalEvent = new Event();
        $iCalEvent->setDtStart(date_create_from_format(DATE_ISO8601, $defence["time"]))
            ->setLocation($defence["room"])
            ->setDescription($defenceDescription)
            ->setSummary("defenceSuccessful: " . $defence["defenceSuccessful"]);
        return $iCalEvent;
    }

    private function createDefenceDescription(array $defence) : string
    {
        $result = "projectID: " . $defence["projectID"] . ", ";
        $result .= "projectName: " . $defence["projectName"] . ", ";
        $result .= "projectDescription: " . $defence["projectDescription"];
        return $result;
    }
}
