<?php
namespace app\controllers;

use app\consts\Permission;
use app\consts\Sql;
use app\mail\Emails;
use app\models\records\News;
use app\models\records\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'create', 'delete'],
                        'roles' => [Permission::NEWS_MANAGE],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception(Yii::t('app', 'You are not allowed to access this page'));
                },
            ]
        ];
    }

    /**
     * Lists all News models.
     * @return string the rendering result.
     */
    public function actionIndex()
    {
        $news = News::find()
            ->where(['>', 'hide_at', Sql::now()])
            ->orWhere(['hide_at' => null])
            ->andWhere(Sql::CONDITION_NOT_DELETED)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->render('index', [
            'news' => $news
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return Response|string  Response object from redirection if action was successfull
     *                          or rendering result if not.
     */
    public function actionCreate()
    {
        $model = new News();
        $model->user_id = Yii::$app->user->getId();
        if ($model->loadFromPost() && $model->save()) {
            $subscribedUsers = User::findNewsSubscribedUsers();

            foreach ($subscribedUsers as $user) {
                Emails::newsCreated($user, $model);
            }

            return $this->redirect(['/site/index']);
        }

        return $this->render('/news/create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param int $id   News id
     * @return Response|string  Response object from redirection if action was successfull
     *                          or rendering result if not.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);

        if ($model->loadFromPost() && $model->save()) {
            return $this->redirect(['/site/index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id   News id
     * @return Response Response object from redirection
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete(int $id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/site/index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id   News id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : News
    {
        $model = News::findOne($id);
        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        if ($model->deleted) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

}
