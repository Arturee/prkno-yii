<?php
namespace app\controllers;

use app\consts\Permission;
use app\models\records\Advertisement;
use app\models\search\AdvertisementSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * AdvertisementsController implements the CRUD actions for Advertisement model.
 */
class AdvertisementsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'my'],
                        'roles' => [Permission::ADS_MANAGE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'my'],
                        'roles' => [Permission::ADS_CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'delete'],
                        'roles' => [Permission::ADS_MANAGE_OWN_ADS],
                        'roleParams' => function () {
                            return ['advertisement' => Advertisement::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception(Yii::t('app', 'You are not allowed to access this page'));
                },
            ],
        ];
    }

    /**
     * Lists all Advertisement models.
     * @return string the rendering result.
     */
    public function actionIndex()
    {
        $searchModel = new AdvertisementSearch();
        $searchModel->published = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all My Advertisement models.
     * @return string the rendering result.
     */
    public function actionMy()
    {
        $searchModel = new AdvertisementSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('my', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Advertisement model.
     * @param int $id   Advertisement id
     * @return string the rendering result of the 'view' view
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(int $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Advertisement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return Response|string Response object from redirection if creation was successfull
     *                         or rendering result of the 'create' view if not.
     */
    public function actionCreate()
    {
        $model = new Advertisement();
        $model->user_id = Yii::$app->user->id;
        $model->published = true;

        if ($model->loadFromPost() && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Advertisement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id   Advertisement id
     * @return Response|string Response object from redirection if update was successfull
     *                         or rendering result if not.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);

        if ($model->loadFromPost() && $model->validate()) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes or restores an existing Advertisement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id       Advertisement id
     * @param string $view  View string identifier
     * @return Response Response object from redirection
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete(int $id, string $view)
    {
        $model = $this->findModel($id);
        $model->deleted = !$model->deleted;
        $model->save();
        return $this->redirect([$view]);
    }

    /**
     * Finds the Advertisement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id       Advertisement id
     * @return Advertisement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : Advertisement
    {
        $model = Advertisement::findOne($id);
        if ($model == null || $model->deleted) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        return $model;
    }
}
