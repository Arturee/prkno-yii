<?php
namespace app\controllers;

use app\consts\Permission;
use app\mail\Emails;
use app\models\forms\UploadForm;
use app\models\records\Document;
use app\models\records\Project;
use app\models\records\Proposal;
use app\models\records\ProposalComment;
use app\models\records\ProposalVote;
use app\models\records\User;
use app\models\search\ProposalSearch;
use app\utils\storage\FileStorage;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ProposalsController implements the CRUD actions for Proposal model.
 */
class ProposalsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $id = Yii::$app->request->get('id');
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [Permission::PROPOSAL_LIST],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view', 'add-comment'],
                        'roles' => [Permission::PROPOSAL_VIEW],
                        'roleParams' => function () use ($id) {
                            $proposal = $this->findModel($id);
                            return ['proposal' => $proposal];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view', 'update'],
                        'roles' => [Permission::PROPOSAL_UPDATE],
                        'roleParams' => function () use ($id) {
                            $proposal = $this->findModel($id);
                            return ['project' => $proposal->project];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [Permission::PROPOSAL_UPDATE],
                        'roleParams' => function () use ($id) {
                            $project = Project::findById($id);
                            return ['project' => $project];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-comment', 'delete-comment', 'publish-comment'],
                        'roles' => [Permission::PROPOSAL_UPDATE_OWN_COMMENT],
                        'roleParams' => function () use ($id) {
                            return ['comment' => ProposalComment::findOne($id)];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view', 'update-state', 'publish-comment', 'update', 'create'],
                        'roles' => [Permission::PROPOSALS_MANAGE],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception(Yii::t('app', 'You are not allowed to access this page'));
                },
            ],
        ];
    }

    /**
     * Lists all Proposal models.
     * @return string the rendering result.
     */
    public function actionIndex()
    {
        $searchModel = new ProposalSearch();
        $dataProvider = $searchModel->search([$searchModel->formName() => ['state' => Proposal::STATE_SENT]]);

        $searchModelResolved = new ProposalSearch();
        $dataProviderResolved = $searchModelResolved->search(Yii::$app->request->queryParams);
        $dataProviderResolved->query->andFilterWhere(['in', 'state', [Proposal::STATE_ACCEPTED, Proposal::STATE_DECLINED]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelResolved' => $searchModelResolved,
            'dataProviderResolved' => $dataProviderResolved,
        ]);
    }

    /**
     * Displays a single Proposal model.
     * @param int $id   Proposal id
     * @return Response|string  Response object from redirection if update was successfull
     *                          or object response for ajax request.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(int $id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post('hasEditable') &&
            Yii::$app->request->post('proposalUserVote') &&
            Yii::$app->user->can(Permission::PROPOSAL_VOTE, ['proposal' => $model])) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $voteModel = $model->getProposalVoteByUserId(Yii::$app->user->identity->id);

            if (empty($voteModel)) {
                $voteModel = new ProposalVote();
                $voteModel->user_id = Yii::$app->user->identity->id;
                $voteModel->proposal_id = $id;
            }

            $voteModel->vote = Yii::$app->request->post('proposalUserVote');

            if ($voteModel->save()) {
                return ['output' => $voteModel->vote, 'message' => ''];
            }

            return ['output' => '', 'message' => Yii::t('app', 'Validation error')];
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Proposal model.
     * If update is successful, the browser will be redirected to the project 'view' page.
     * @param int $id   Proposal id
     * @return Response|string  Response object from redirection if update was successfull
     *                          or rendering result if not.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);
        $uploadModel = new UploadForm();

        if ($model->loadFromPost() && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Updated'));
            $uploadModel->files = UploadedFile::getInstances($uploadModel, 'files');

            if (sizeof($uploadModel->files)) {
                $uploadFile = $uploadModel->files[0];
                if ($model->document != null) {
                    $model->document->fallback_path = FileStorage::store($uploadFile->tempName, $uploadFile->name, true);
                    $model->document->save();
                } else {
                    $document = new Document();
                    $document->fallback_path = FileStorage::store($uploadFile->tempName, $uploadFile->name);
                    $document->save();

                    $model->document = $document;
                    $model->save();
                }
            }

            return $this->redirect(['projects/view', 'id' => $model->project_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new proposal with a Project model recognized by id.
     * If creation is successful, the browser will be redirected to the project 'view' page.
     * @param int $id   Proposal id
     * @return Response|string  Response object from redirection if update was successfull
     *                          or rendering result if not.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCreate(int $id)
    {
        // find project
        $project = Project::findOne(['id' => $id]);

        // create new document
        $document = new Document();
        $document->save();

        // create new proposal
        $proposal = new Proposal();
        $proposal->project_id = $project->id;
        $proposal->state = Proposal::STATE_DRAFT;
        $proposal->document_id = $document->id;
        $proposal->documentMarkdown = $project->lastProposal->document->content_md;

        if ($proposal->loadFromPost() && $proposal->save() && $project->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'New Proposal Created'));
            return $this->redirect(['projects/view', 'id' => $project->id]);
        }

        return $this->render('create', [
            'model' => $proposal,
        ]);
    }

    /**
     * Creates a new Proposal model.
     * If creation is successful, the browser will be redirected to the view page
     * of the proposal.
     * @return Response Response object from redirection to view, if comment
     *                  was successfully added
     * @throws NotFoundHttpException Throw if post variable has expected data
     */
    public function actionAddComment(int $id)
    {
        $model = new ProposalComment();

        if ($model->loadFromPost('ProposalComment')) {
            $model->proposal_id = $id;
            $model->creator_id = Yii::$app->user->identity->id;
            if ($model->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Saved'));

                foreach (User::findCommitteeMembers() as $committeeMember) {
                    Emails::proposalCommented(Proposal::findOne(['id' => $id]), $committeeMember);
                }
            }
            return $this->redirect(['view', 'id' => $id]);
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Update proposal comment
     * @param int $id   Proposal id
     * @return array Object response to ajax request
     * @throws NotFoundHttpException if the proposal comment model with given id cannot be found
     */
    public function actionUpdateComment(int $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findProposalCommentModel($id);
        $model->setAttribute('content', Yii::$app->request->post('comment'));

        if ($model->save()) {
            return ['output' => $model->content, 'message' => ''];
        } else {
            return ['output' => '', 'message' => Yii::t('app', 'Validation error')];
        }
    }

    /**
     * Publish / unpublish an existing ProposalComment model and redirect
     * to the proposal view.
     * @param int $id   Proposal id
     * @return Response Response object from redirection
     * @throws NotFoundHttpException if the proposal comment model with given id cannot be found
     */
    public function actionPublishComment(int $id)
    {
        $model = $this->findProposalCommentModel($id);
        
        if (Yii::$app->request->get('lock')) {
            $model->committee_only = true;
        } else {
            $model->committee_only = false;
        }

        if ($model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Updated'));
        } else {
            Yii::t('app', 'Validation error');
        }

        return $this->redirect(['view', 'id' => $model->proposal_id]);
    }

    /**
     * Deletes an existing ProposalComment model.
     * If deletion is successful, the browser will be redirected to the 'view' page.
     * @param int $id   Proposal id
     * @return Response Response object from redirection to the view
     * @throws NotFoundHttpException if the proposal comment model with given id cannot be found
     */
    public function actionDeleteComment(int $id)
    {
        $model = $this->findProposalCommentModel($id);
        $model->deleted = true;

        if ($model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Deleted'));
        } else {
            Yii::t('app', 'Validation error');
        }

        return $this->redirect(['view', 'id' => $model->proposal_id]);
    }

    /**
     * Update proposal state to accepted / declined and send emails about the decision.
     * Save statement comment.
     * @param int $id   Proposal id
     * @return Response Response object from redirection to the view
     * @throws NotFoundHttpException if the proposal comment model with given id cannot be found
     */
    public function actionUpdateState(int $id)
    {
        $model = $this->findModel($id);

        if ((Yii::$app->request->post('accept') || Yii::$app->request->post('decline'))) {
            if (Yii::$app->request->post('accept')) {
                $model->state = Proposal::STATE_ACCEPTED;
                $model->project->state = Project::STATE_ACCEPTED;
                $model->project->save();
            } else {
                $model->state = Proposal::STATE_DECLINED;
            }
            $model->comment = Yii::$app->request->post('proposal-comment');

            if ($model->save() && $model->project->lastState->save()) {
                $commentIds = json_decode(Yii::$app->request->post('comment-ids'));

                if (!empty($commentIds)) {
                    $comments = ProposalComment::findAll(['id' => $commentIds, 'proposal_id' => $id]);
                    foreach ($comments as $comment) {
                        $comment->committee_only = true;
                        $comment->save();
                    }
                }

                $committee = User::findCommitteeMembers();
                foreach ($committee as $committeeMember) {
                    Emails::proposalDecision($model, $committeeMember);
                }
                foreach ($model->project->supervisors as $supervisor) {
                    if (!in_array($supervisor->role, User::COMMITTEE_ROLES)) {
                        Emails::proposalDecision($model, $supervisor);
                    }
                }
                Yii::$app->session->addFlash('success', Yii::t('app', 'Emails sent.'));
            }
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the Proposal model based on its id.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id   Proposal id
     * @return Proposal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : Proposal
    {
        $model = Proposal::findOne($id);
        if ($model == null || $model->deleted) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    /**
     * Finds the Proposal comment model based on its id.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id   Proposal id
     * @return ProposalComment the loaded proposal comment model
     * @throws NotFoundHttpException if the model with given id does not exist
     */
    protected function findProposalCommentModel(int $id)
    {
        $model = ProposalComment::findOne($id);
        if ($model == null || $model->deleted) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }
}
