<?php
namespace app\controllers;

use app\consts\FileType;
use app\consts\Http;
use app\consts\Permission;
use app\mail\Emails;
use app\models\forms\ChooseOpponentForm;
use app\models\forms\DefenceAcceptForm;
use app\models\forms\MarkdownForm;
use app\models\records\Document;
use app\models\records\Project;
use app\models\records\ProjectDefence;
use app\models\records\User;
use app\utils\storage\DefenceStorage;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Controller for Defences
 */
class DefencesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $model = $this->findModel(Yii::$app->request->get('id'));
        $projectParamsFunction = function () use ($model) {
            return [Permission::PARAM_PROJECT => $model->project];
        };

        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'update' => [Http::GET],
                    'set-time' => [Http::POST],
                    'upload' => [Http::POST],
                    'accept' => [Http::POST],
                    'view' => [Http::GET],
                    'choose-opponent' => [Http::POST],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => [Permission::DEFENCES_MANAGE],
                        'roleParams' => $projectParamsFunction,
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'accept', 'choose-opponent', 'upload-final-statement', 'upload-opponent-review',
                            'upload-supervisor-review'
                        ],
                        'roles' => [Permission::DEFENCES_MANAGE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['upload-opponent-review', 'update'],
                        'roles' => [Permission::DEFENCES_UPLOAD_OPPONENT_REVIEW],
                        'roleParams' => function () use ($model) {
                            return [Permission::PARAM_DEFENCE => $model];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['upload-supervisor-review', 'update'],
                        'roles' => [Permission::DEFENCES_UPLOAD_SUPERVISOR_REVIEW],
                        'roleParams' => function () use ($model) {
                            return [Permission::PARAM_DEFENCE => $model];
                        }
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception(Yii::t('app', 'You are not allowed to access this page'));
                },
            ],
        ];
    }

    /**
     * Action for view of defence project
     * @param int $id   Defence project id
     * @return string the rendering result.
     */
    public function actionView(int $id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Action for rendering update page of defence project
     * @param int $id   Defence project id
     * @return string the rendering result.
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Action for uploading supervisor review
     * @param int $id   Defence project id
     * @return Response Response object from redirection
     */
    public function actionUploadSupervisorReview(int $id)
    {
        $this->upload($id, DefenceStorage::FILETYPE_SUPERVISOR_REVIEW);
    }

    /**
     * Action for uploading opponentReview
     * @param int $id   Defence project id
     * @return Response Response object from redirection
     */
    public function actionUploadOpponentReview(int $id)
    {
        $this->upload($id, DefenceStorage::FILETYPE_OPPONENT_REVIEW);
    }

    /**
     * Action for uploading final statement document
     * @param int $id   Defence project id
     * @return Response Response object from redirection
     */
    public function actionUploadFinalStatement(int $id)
    {
        $this->upload($id, DefenceStorage::FILETYPE_DEFENCE_STATEMENT);
    }

    /**
     * Action for request for choosing oponent
     * @param int $id   Defence project id
     * @return Response Response object from redirection
     */
    public function actionChooseOpponent(int $id)
    {
        $model = $this->findModel($id);
        $form = new ChooseOpponentForm();
        $form->loadFromPost();
        $opponentId = $form->opponent_id;
        $opponent = User::findIdentity($opponentId);
        if ($opponent) {
            $model->opponent_id = $opponentId;
            $model->save();
            Emails::defenceOpponentChosen($model, $opponent);
            Yii::$app->session->addFlash('success', Yii::t('app', 'Emails sent.'));
        }
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Action for accepting project
     * @param int $id   Defence project id
     * @return Response Response object from redirection
     */
    public function actionAccept(int $id)
    {
        $model = self::findModel($id);
        $acceptForm = new DefenceAcceptForm();
        $acceptForm->loadFromPost();
        self::accept($acceptForm, $model);
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Retrieve project defence with given id
     * @param int $id   Project defence id
     * @return ProjectDefence     Project defence
     * @throws NotFoundHttpException    Exception is thrown if page with given id does not exists, or is deleted
     */
    protected function findModel(int $id) : ProjectDefence
    {
        $model = ProjectDefence::findOne($id);
        if ($model == null || $model->deleted) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    private function upload(int $id, string $fileType)
    {
        $model = $this->findModel($id);
        $form = new MarkdownForm();
        if (Yii::$app->request->isPost) {
            $form->loadFromPost();
            $files = UploadedFile::getInstances($form, 'file');
            $document = new Document();
            if ($files) {
                $file = $files[0];
                if ($file->type !== FileType::PDF) {
                    Yii::error("Extension of uploaded file is wrong: $file->name, $file->tempName, $file->type", __METHOD__);
                    return;
                }
                $document->fallback_path = DefenceStorage::store($file->tempName, $model, $fileType);
            } else if ($form->markdown) {
                $document->content_md = $form->markdown;
            }
            $document->save();
            if ($document) {
                switch ($form->type) {
                    case MarkdownForm::TYPE_SUPERVISOR_REVIEW:
                        $model->supervisor_review = $document->id;
                        break;
                    case MarkdownForm::TYPE_OPPONENT_REVIEW:
                        $model->opponent_review = $document->id;
                        break;
                    case MarkdownForm::TYPE_FINAL_STATEMENT:
                        $model->defence_statement = $document->id;
                        $model->statement_author_id = (int)$form->author_id;
                        break;
                }
                $model->save();
            }
        }
        if ($form->type !== MarkdownForm::TYPE_FINAL_STATEMENT) {
            Emails::defenceReviewUploaded($model, $form->type);
            Yii::$app->session->addFlash('success', Yii::t('app', 'Emails sent.'));
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    private static function accept(DefenceAcceptForm $acceptForm, ProjectDefence $model) : void
    {
        $model->scenario = ProjectDefence::SCENARIO_ACCEPT;
        $model->result = $acceptForm->result;
        self::changeProjectState($model->project, $acceptForm);
        $model->save();
    }

    private static function changeProjectState(Project $project, DefenceAcceptForm $acceptForm) : void
    {
        $sendEmails = true;
        $previousState = $project->state;
        if ($project->state === Project::STATE_ANALYSIS_HANDED_IN || $project->state === Project::STATE_ANALYSIS) {
            if ($acceptForm->result !== ProjectDefence::RESULT_FAILED) {
                $project->state = Project::STATE_IMPLEMENTATION;
            } else {
                $project->state = Project::STATE_ANALYSIS;
            }
        } else if ($project->state === Project::STATE_IMPLEMENTATION_HANDED_IN || $project->state === Project::STATE_IMPLEMENTATION) {
            if ($acceptForm->result === ProjectDefence::RESULT_DEFENDED) {
                $project->state = Project::STATE_DEFENDED;
            } else if ($acceptForm->result === ProjectDefence::RESULT_CONDITIONALLY_DEFENDED) {
                $project->state = Project::STATE_CONDITIONALLY_DEFENDED;
            } else if ($acceptForm->result === ProjectDefence::RESULT_FAILED) {
                $project->state = Project::STATE_FAILED;
            } else {
                Yii::$app->session->addFlash('fail', Yii::t('app', 'Automatic state change failed, please change project state manually.'));
                $sendEmails = false;
            }
        }
        $extraCredits = $acceptForm->extraCredits;
        if ($extraCredits) {
            $project->extra_credits = $extraCredits;
        }
        $project->save();

        if ($sendEmails) {
            foreach ($project->getActiveUsers()->all() as $user) {
                Emails::projectStateChanged($project, $previousState, $user);
            }
            Yii::$app->session->addFlash('success', Yii::t('app', 'Emails sent.'));
        }
    }
}
