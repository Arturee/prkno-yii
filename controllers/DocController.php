<?php
namespace app\controllers;

/**
 * Controller for rendering doc section
 */
class DocController extends BaseController
{
    /**
     * Render main doc page
     * @return string Rendering results of the index page
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }
}
