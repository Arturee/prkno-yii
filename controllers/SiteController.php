<?php
namespace app\controllers;

use app\cas\CasClient;
use app\consts\Param;
use app\consts\Sql;
use app\exceptions\BannedUserException;
use app\exceptions\CasConnectionException;
use app\exceptions\CasRegistrationUnsuccessfulException;
use app\exceptions\ConflictAccountException;
use app\exceptions\GuestAttemptsToLoginException;
use app\mail\Emails;
use app\models\forms\LoginForm;
use app\models\forms\PasswordResetRequestForm;
use app\models\forms\ResetPasswordForm;
use app\models\records\DefenceDate;
use app\models\records\News;
use app\models\records\Project;
use app\models\records\Proposal;
use app\models\records\User;
use app\models\search\AdvertisementSearch;
use app\models\search\ProjectSearch;
use app\utils\DateTime;
use app\utils\MarkdownRenderer;
use Yii;
use yii\base\InvalidArgumentException;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/**
 * Controller for main page
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     * @return string the rendering result from the index view
     */
    public function actionIndex()
    {
        // News
        $news = News::find()
            ->where(['>', 'hide_at', Sql::now()])
            ->orWhere(['hide_at' => null])
            ->andWhere(Sql::CONDITION_NOT_DELETED)
            ->orderBy(['updated_at' => SORT_DESC])
            ->all();

        // Advertisement
        $SearchModelAds = new AdvertisementSearch();
        $advertisements = $SearchModelAds->search([]);
        $advertisements->query
            ->where(Sql::CONDITION_NOT_DELETED)
            ->andWhere(['published' => true])
            ->limit(4);

        // My Project
        $searchModelMyProjects = new ProjectSearch();
        $myProjects = $searchModelMyProjects->search([
            $searchModelMyProjects->formName() => ['supervisorIds' => Yii::$app->user->id]
        ]);

        // Inbox
        // projects: requested, waiting and after deadline
        $now = new DateTime();
        $searchInboxProjects = new ProjectSearch();
        $inboxProjects = $searchInboxProjects->search([]);
        $inboxProjects->query
            ->where([
                'or',
                ['requested_to_run' => true, 'state' => Project::STATE_ACCEPTED],
                ['waiting_for_proposal_evaluation' => true, 'state' => Project::STATE_PROPOSAL],
                [
                    'and',
                    ['<', 'deadline', $now],
                    ['state' => Project::getProjectRunningStates()]
                ]
            ])
            ->all();

        // proposals: waiting for a vote from the user
        $projectTableName = Project::tableName();
        $proposalTableName = Proposal::tableName();
        $inboxProposals = new ActiveDataProvider([
            'query' => Proposal::find()->where(Sql::CONDITION_NOT_DELETED)
                ->joinWith($projectTableName)->where(
                // only projects before deadline, if there is deadline
                    [   'or',
                        [ $projectTableName . '.deadline' => NULL ],
                        [
                            'and',
                            ['>=', $projectTableName . '.deadline', $now],
                            [$projectTableName . '.state' => Project::STATE_PROPOSAL]
                        ]
                    ]
                )
                ->andWhere( [$projectTableName.'.deleted'  => false])
                ->andWhere( [$proposalTableName.'.deleted'  => false])
                ->andwhere([$proposalTableName.'.state' => Proposal::STATE_SENT])
                ->orderBy([$proposalTableName.'.updated_at' => SORT_DESC]),
            'pagination' => false,
        ]);

        // defence dates: attendance voting
        $inboxDefenceDates = new ActiveDataProvider([
            'query' => DefenceDate::getVotingDates(),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'news' => $news,
            'advertisements' => $advertisements,
            'myProjects' => $myProjects,
            'inboxProjects' => $inboxProjects,
            'inboxProposals' => $inboxProposals,
            'inboxDefenceDates' => $inboxDefenceDates,
        ]);
    }

    /**
     * Redirects to CAS. After CAS auth is successful, CAS redirects
     * back here. User can be logged only if there is no conflict with
     * another account with the same email address with different login type,
     * user is not banned or users role is not expired.
     * @return Response|null    Response object from home page redirection if the user
     *                          was successfully logged in, or was logged in previously,
     *                          or null otherwise
     */
    public function actionLoginViaCas()
    {
        if (!Yii::$app->user->isGuest) { // already logged in (hack attempt)
            return $this->goHome();
        }

        $error = null;
        try {
            $casClient = $this->createCasClient();

            if ($casClient->isCurrentUserAuthenticated()) {
                // redirected here from CAS after a successful CAS auth

                $email = $casClient->getCurrentUserEmail();
                $user = User::findByEmail($email);

                if ($user && $user->login_type !== User::LOGIN_TYPE_CAS) {
                    throw new ConflictAccountException();
                }

                if (!$user) {
                    $user = $this->registerUserFromCas($email, $casClient);
                }

                if ($user->banned) {
                    throw new BannedUserException();
                }

                if ($user->role_expiration && DateTime::from($user->role_expiration)->isInPast()) {
                    $this->reloadRoleFromCas($user, $casClient);
                }

                $expiration = Yii::$app->params[Param::LOGIN_EXPIRATION];
                Yii::$app->user->login($user, $expiration);
                return $this->goHome();

            } else {
                // accessed by user after he clicked "login via CAS"

                $casClient->redirectToCasForAuthentication();
            }
        } catch (CasConnectionException $e) {
            $error = Yii::t('app', 'An error occurred with CAS, please retry after a while. (code 4)');
        } catch (GuestAttemptsToLoginException $e) {
            $error = Yii::t('app', 'You do not have sufficient privileges to log in. (code 3)');
        } catch (BannedUserException $e) {
            $error = Yii::t('app', 'You are banned.');
        } catch (CasRegistrationUnsuccessfulException $e) {
            $error = Yii::t('app', 'You cannot log in via CAS, because your email in CAS is invalid. (code 2)');
        } catch (ConflictAccountException $e) {
            $error = Yii::t('app', 'You cannot log in via CAS, you must use your email and password. (code 1)');
        }
        finally {
            if ($error) {
                $urlLogin = Yii::$app->urlManager->createAbsoluteUrl(['site/login', 'error' => $error]);
                $casClient->logout($urlLogin);
                // CAS logout destroys session, that is why I send the error message via HTTP GET parameters
                // (cannot use session or Yii flashes directly, because session is destroyed)
            }
        }
    }

    /**
     * Login action. If user was successfully logged in, redirect to previous page,
     * otherwise render login page again.
     * @return Response|string  Response object from redirection to previous page
     *                          if login was successfull, or rendering result
     *                          of the login page if not.
     */
    public function actionLogin(string $error = null)
    {
        if ($error) {
            Yii::$app->session->addFlash('error', $error);
        }

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $ok = $model->loadFromPost();
        if ($ok) {
            $ok = $model->login();
        }
        if ($ok) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     * @return Response     Response object from home page redirection
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $language = Yii::$app->language;
        $casClient = $this->createCasClient();
        if ($casClient->isCurrentUserAuthenticated()) {
            $this->casClient->logout(Yii::$app->getHomeUrl());
        }
        // keep language in session
        Yii::$app->session->set('lang', $language);
        Yii::$app->language = Yii::$app->session->get('lang');
        return $this->goHome();
    }

    /**
     * Requests password reset.
     * @return Response|string  Response object from redirection to homepage if request
     *                          was successfull, or rendering result if no data was post yet.
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->loadFromPost() && $model->validate()) {
            /* @var $user \app\models\records\User */
            $user = User::findOne([
                'deleted' => false,
                'banned' => false,
                'email' => $model->email,
            ]);
            if (!$user) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'User with given email address was not found.'));
            } else if ($user->login_type == "cas") {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Please, use login via CAS.'));
            } else if (!$this->sendEmail($user)) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Sorry, we are unable to reset password for the provided email address.'));
            } else {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Check your email for further instructions.'));
                return $this->goHome();
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password with new one from the form.
     * @param string $token     Reset password token
     * @return Response|string  Response object from redirection if password was
     *                          successfully resetted, or rendering result of the
     *                          resetPassword view if not.
     * @throws BadRequestHttpException  If the token is invalid
     */
    public function actionResetPassword(string $token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->loadFromPost() && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'New password saved.'));
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Change language action
     * @param string $language  Language code 'cs-CZ' or 'en-US'
     * @return string the rendering result of the referrer
     */
    public function actionChangeLanguage(string $language)
    {
        Yii::$app->session->set('lang', $language);
        Yii::$app->language = Yii::$app->session->get('lang');
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Return HTML generated content from the markdown text
     * loaded from form input 'inputText'
     * @return string   HTML generated content of the markdown text
     */
    public function actionRenderMarkdown() : string
    {
        $inputText = Yii::$app->request->post()['inputText'];
        return MarkdownRenderer::render($inputText);
    }

    private function registerUserFromCas(string $email, CasClient $casClient) : User
    {
        $info = $casClient->downloadCurrentUserInfo();
        $user = new User();
        $user->loadCasInfo($info);

        $savedOK = $user->save();

        if (!$savedOK) {
            Yii::error("Bad email from CAS: $user->email, $user->role, $info->getUkco()", __METHOD__);
            throw new CasRegistrationUnsuccessfulException($user->email);
        }
        $user = User::findByEmail($email);
        return $user;
    }

    private function reloadRoleFromCas(User $user, CasClient $casClient) : void
    {
        $info = $casClient->downloadCurrentUserInfo();
        $user->loadRole($info);
        $user->save();
    }

    private function sendEmail(User $user) : bool
    {
        if (!User::isPasswordRecoveryTokenValid($user->password_recovery_token)) {
            $user->generatePasswordRecoveryToken();
            if (!$user->save()) {
                return false;
            }
        }
        Emails::userResetPassword($user);
        return true;
    }
}
