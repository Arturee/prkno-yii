<?php

namespace app\controllers;

/**
 * Controller for authorized file downloading
 */
class FilesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'download' => [
                'class' => 'app\utils\storage\authorization\AuthorizedDownloadAction',
            ],
        ];
    }
}