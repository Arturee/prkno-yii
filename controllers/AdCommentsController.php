<?php
namespace app\controllers;

use app\consts\Permission;
use app\mail\Emails;
use app\models\records\AdvertisementComment;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * AdCommentsController implements the CRUD actions for AdComment model.
 */
class AdCommentsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'delete'],
                        'roles' => [Permission::ADS_MANAGE_COMMENTS],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [Permission::ADS_CREATE_COMMENT],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => [Permission::ADS_MANAGE_OWN_COMMENTS],
                        'roleParams' => function () {
                            return ['ad-comment' => AdvertisementComment::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception(Yii::t('app', 'You are not allowed to access this page'));
                },
            ],
        ];
    }

    /**
     * Lists all AdComment models.
     * @return string the rendering result.
     */
    public function actionIndex()
    {
        $query = AdvertisementComment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'deleted' => false,
        ]);

        return $this->render('index', [
            'dataProviderComment' => $dataProvider,
        ]);
    }

    /**
     * Creates a new AdComment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return Response Response object from redirection
     */
    public function actionCreate()
    {
        $model = new AdvertisementComment();
        $model->user_id = Yii::$app->user->id;

        if ($model->loadFromPost() && $model->save()) {
            $advertisement = $model->ad;
            if ($advertisement != null && $advertisement->deleted == false && $advertisement->subscribed_via_email) {
                Emails::advertisementAddComment($model);
            }
        }

        return $this->redirect(['advertisements/index']);
    }

    /**
     * Deletes or restores an existing AdComment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id   AdComment id
     * @return Response Response object from redirection
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete(int $id)
    {
        $model = $this->findModel($id);
        if ($model != null) {
            $model->deleted = !$model->deleted;
            $model->save();
        }
        return $this->redirect(['advertisements/index']);
    }

    /**
     * Finds the AdComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id   AdComments id
     * @return AdvertisementComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : AdvertisementComment
    {
        if (($model = AdvertisementComment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}