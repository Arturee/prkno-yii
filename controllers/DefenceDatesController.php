<?php
namespace app\controllers;

use app\consts\Permission;
use app\mail\Emails;
use app\models\forms\DefenceDateVoteForm;
use app\models\forms\DefenceProjectTimeForm;
use app\models\forms\IdsForm;
use app\models\forms\RoomForm;
use app\models\records\DefenceAttendance;
use app\models\records\DefenceDate;
use app\models\records\Project;
use app\models\records\ProjectDefence;
use app\utils\DateTime;
use app\utils\NewsGenerator;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Controller for Defence dates
 */
class DefenceDatesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'vote' => ['POST'],
                    'add-project' => ['POST'],
                    'add-projects' => ['POST'],
                    'delete-project' => ['POST'],
                    'set-room' => ['POST'],
                    'set-time' => ['POST'],
                    'confirm-date' => ['POST'],
                    'must-come' => ['POST'],
                    'email-must-come' => ['POST'],
                    'delete' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::class,
                'only' => ['must-come'],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['vote', 'update'],
                        'roles' => [Permission::DEFENCES_VOTE_ABOUT_DATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'add-projects', 'delete-project', 'set-room', 'set-time', 'confirm-date', 'must-come', 'email-must-come', 'delete'],
                        'roles' => [Permission::DEFENCES_MANAGE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['add-project'],
                        'roles' => [Permission::DEFENCE_ADD_PROJECT],
                        'roleParams' => function () {
                            return ['project' => Project::findOne(Yii::$app->request->get('projectId'))];
                        },
                    ]

                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception(Yii::t('app', 'You are not allowed to access this page'));
                },
            ],
        ];
    }

    /**
     * Render tables with defences dates and project defences
     */
    public function actionIndex()
    {
        $votingDateProvider = new ActiveDataProvider([
            'query' => DefenceDate::getVotingDates(),
            'pagination' => false,
        ]);
        $futureDateProvider = new ActiveDataProvider([
            'query' => DefenceDate::getFutureDates(),
            'pagination' => false,
        ]);
        $pastDateProvider = new ActiveDataProvider([
            'query' => DefenceDate::getPastDates(),
            'pagination' => ['pageSize' => 10],
        ]);
        return $this->render('index', [
            'votingDataProvider' => $votingDateProvider,
            'futureDataProvider' => $futureDateProvider,
            'pastDataProvider' => $pastDateProvider
        ]);
    }

    /**
     * Set room for defence date
     * @param int $id   Defence date id
     * @return Response Response object from redirection
     */
    public function actionSetRoom(int $id)
    {
        $form = new RoomForm();
        if ($form->loadFromPost()) {
            $defenceDate = $this->findModel($id);
            $defenceDate->room = $form->room;
            $defenceDate->save();
        }
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Delete project from defence date
     * @param int $id   Defence id
     * @return Response Response object from redirection
     */
    public function actionDeleteProject(int $id)
    {
        $defence = ProjectDefence::findById($id);
        $defenceDate = $defence->defenceDate;
        $defence->cancel();
        return $this->redirect(['update', 'id' => $defenceDate->id]);
    }

    /**
     * Approve defence date
     * @param int $id   Defence id
     * @return Response Response object from redirection
     */
    public function actionConfirmDate(int $id)
    {
        $defenceDate = $this->findModel($id);
        $defenceDate->active_voting = 0;
        $defenceDate->save();

        Emails::defenceDateCreated($defenceDate);
        NewsGenerator::defenceDateOpened($defenceDate);

        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Ajax voting for defence date attendance
     * @param int $id   ProjectDefence vote
     * @return array    AJAX response
     */
    public function actionVote(int $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $form = new DefenceDateVoteForm();
        if ($form->loadFromPost()) {
            $defenceDate = $this->findModel($id);
            $userId = Yii::$app->getUser()->getId();
            $voteModel = $defenceDate->getDefenceAttendanceByUserId($userId);
            if (empty($voteModel)) {
                $voteModel = new DefenceAttendance();
                $voteModel->user_id = $userId;
                $voteModel->defence_date_id = $defenceDate->id;
                $voteModel->must_come = false;
            }
            $voteModel->vote = $form->vote;
            $voteModel->comment = $form->comment;
            if ($voteModel->save()) {
                return ['output' => $voteModel->vote, 'message' => ''];
            }
        }
        return ['output' => '', 'message' => Yii::t('app', 'Validation error')];
    }

    /**
     * Set defence time to particular project
     * Project id is retrieved from POST variables
     * @param int $id   ProjectDefence date id
     * @return Response Response object from redirection
     */
    public function actionSetTime(int $id)
    {
        $form = new DefenceProjectTimeForm();
        if ($form->loadFromPost()) {
            $defenceDate = $this->findModel($id);
            foreach ($form->ids as $i => $defenceId) {
                $time = $form->times[$i];
                $defence = ProjectDefence::findOne($defenceId);
                $defenceTime = $defence->time ? DateTime::from($defence->time) : null;
                $newDefenceTime = $time ? DateTime::from($defenceDate->date)->setHoursAndMinutes($time) : null;
                if ($defenceTime != $newDefenceTime) {
                    $defence->time = $newDefenceTime;
                    $defence->scenario = ProjectDefence::SCENARIO_SET_TIME;
                    $defence->save();
                    if ($newDefenceTime != null) {
                        $users = $defence->project->getTeamAndSupervisor();
                        Emails::defenceTimeScheduled($defence, $users, $newDefenceTime);
                        Yii::$app->session->addFlash('success', Yii::t('app', 'Emails sent.'));
                    }
                }
            }
        }
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Ajax action for adding single project to defence dates
     * @param int $id           ProjectDefence date id
     * @param int $projectId    Project id
     * @return Response Response object from redirection
     */
    public function actionAddProject(int $id, int $projectId)
    {
        $project = Project::findOne($projectId);
        if ($project->getUpcomingDefence() === null) {
            $this->addProject($id, $projectId);
            Yii::$app->session->addFlash('success', Yii::t('app', 'Project assigned to a defence.'));
        }
        return $this->redirect(['projects/view', 'id' => $projectId]);
    }

    /**
     * Add project to the defence date. Project id is
     * retrieved from POST variable
     * @param int $id   ProjectDefence date id
     * @return Response Response object from redirection
     */
    public function actionAddProjects(int $id)
    {
        $form = new IdsForm();
        if ($form->loadFromPost()) {
            foreach ($form->ids as $projectId) {
                $this->addProject($id, $projectId);
            }
            if (!empty($form->ids)) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Emails sent to the team and supervisor.'));
                Yii::$app->session->addFlash('success', Yii::t('app', 'Supervisor asked to supply a review.'));
            }
        }
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Display update form for defence date
     * @param $id   ProjectDefence date id
     * @return Response|string The rendering result.
     */
    public function actionUpdate(int $id)
    {
        $defenceDate = $this->findModel($id);
        $attendances = $defenceDate->defenceAttendances;
        $attendancesProvider = new ArrayDataProvider([
            'allModels' => $attendances,
            'pagination' => false,
        ]);
        return $this->render('update', [
            'model' => $defenceDate,
            'attendancesProvider' => $attendancesProvider
        ]);
    }

    /**
     * Render form with create defence date
     * @return Response|string  Response object from redirection if update was successfull
     *                          or rendering result if not.
     */
    public function actionCreate()
    {
        $defenceDate = new DefenceDate();
        $defenceDate->scenario = DefenceDate::SCENARIO_CREATE;
        if ($defenceDate->loadFromPost() && $defenceDate->save()) {
            DefenceAttendance::generateForCommitteMembers($defenceDate->id);
            Yii::$app->session->addFlash('success', Yii::t('app', 'Emails sent.'));
            return $this->redirect(['update', 'id' => $defenceDate->id]);
        } else {
            return $this->render('create', ['model' => $defenceDate]);
        }
    }

    /**
     * Delete defence date and send email about it
     * @param int $id   ProjectDefence date id
     * @return Response Response object from redirection
     */
    public function actionDelete(int $id)
    {
        $defenceDate = $this->findModel($id);
        $defenceDate->cancel();
        if (!$defenceDate->active_voting) {
            NewsGenerator::defenceDateCancelled($defenceDate);

            $cancelledDefences = $defenceDate->projectDefences;
            foreach ($cancelledDefences as $defence) {
                if ($defence->time) {
                    Emails::defenceCancelled($defence);
                }
            }
            $count = sizeof($cancelledDefences);
            if ($count > 0) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Emails about {0} cancelled defences sent.', [$count]));
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Send email to all committee members who must come
     * to the defence date about it.
     * @param int $id   ProjectDefence date id
     * @return Response Response object from redirection
     */
    public function actionEmailMustCome(int $id)
    {
        $defenceDate = $this->findModel($id);
        foreach ($defenceDate->defenceAttendances as $attendance) {
            if ($attendance->must_come) {
                Emails::informACommitteeMemberThatTheyMustComeToADefence($defenceDate, $attendance->user);
            }
        }
        Yii::$app->session->addFlash('success', Yii::t('app', 'Emails sent.'));
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Ajax request about setting committee member for defence date
     * that he must come
     * @param int $id       Id of the defence date
     * @param int $userid   User id
     * @param bool $mustCome True/false whether the user must come
     */
    public function actionMustCome(int $id, int $userId, bool $mustCome) : void
    {
        $defenceDate = $this->findModel($id);
        $attendance = $defenceDate->getDefenceAttendanceByUserId($userId);
        if (empty($attendance)) {
            $attendance = new DefenceAttendance();
            $attendance->user_id = $userId;
            $attendance->defence_date_id = $defenceDate->id;
            $attendance->vote = DefenceAttendance::VOTE_UNKNOWN;
        }
        $attendance->must_come = $mustCome;
        $attendance->save();
    }

    /**
     * Get model instance with given Id
     * @param int $id       Id of the Defence date
     * @return DefenceDate  Defence date with given id
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : DefenceDate
    {
        $model = DefenceDate::findOne($id);
        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        if ($model->deleted) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        return $model;
    }

    private function addProject(int $id, int $projectId) : void
    {
        $project = Project::findOne($projectId);
        $defence = new ProjectDefence();
        $defence->scenario = ProjectDefence::SCENARIO_CREATE;
        $defence->project_id = $project->id;
        $defence->defence_date_id = $id;
        $defence->number = $project->getNextDefenceNumber();
        $defence->save();
        $users = $project->getTeamAndSupervisor();
        Emails::projectAssignedToDefence($defence, $users);
        if ($project->supervisors) {
            Emails::requestReviewFromASupervisor($defence, $project->supervisors[0]);
        }
    }
}
