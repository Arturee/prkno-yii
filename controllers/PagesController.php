<?php
namespace app\controllers;

use app\consts\Permission;
use app\models\records\Document;
use app\models\records\Page;
use app\models\records\PageVersion;
use app\utils\NewsGenerator;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Controller for static pages
 */
class PagesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view', 'download'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'update', 'create', 'delete', 'move-up', 'move-down', 'publish', 'view-page-version'],
                        'roles' => [Permission::PAGES_MANAGE],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception(Yii::t('app', 'You are not allowed to access this page'));
                },
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return string the rendering result.
     */
    public function actionIndex()
    {
        $query = Page::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'deleted' => false,
        ]);

        if (!Yii::$app->user->can(Permission::PAGES_MANAGE)) {
            $query->andFilterWhere([
                'published' => true,
            ]);
        }

        $query->orderBy('order_in_menu');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page.
     * @param string $url   Page url
     * @return string the rendering result.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(string $url)
    {
        $model = Page::findOne(['url' => $url]);
        return $this->renderViewPage($model);
    }

    /**
     * View page version
     * @param int $id   Pageversion id
     * @return string the rendering result.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionViewPageVersion(int $id)
    {
        $pageVersion = PageVersion::findOne($id);
        if (!$pageVersion) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page version does not exist.'));
        }

        $pageModel = Page::findOne($pageVersion->page_id);
        return $this->renderViewPage($pageModel, $pageVersion);
    }

    /**
     * Creates a new page.
     * If creation is successful, the browser will be redirected to the page 'view' page.
     * @return string the rendering result.
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCreate()
    {
        $page = new Page();
        $page->published = false;
        $page->order_in_menu = $this->getNextOrderInMenu();

        $newPageVersion = new PageVersion();
        $newPageVersion->published = true;

        if ($this->updatePageAndVersion($page, $newPageVersion)) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'New Page Created'));
            return $this->redirect(['pages/view', 'url' => $page->url]);
        }

        return $this->render('create-and-update', [
            'model' => $page,
            'updatePage' => null
        ]);
    }

    /**
     * Update existing page version
     * @param int $id           Page id
     * @param int $versionId    Page version id
     * @return Response|string  Response object from redirection if update was successfull
     *                          or rendering result of the create-and-update page if not
     */
    public function actionUpdate(int $id, int $versionId = null)
    {
        $page = $this->findPage($id);

        $pageVersion = null;
        if ($versionId) {
            $pageVersion = PageVersion::findOne($versionId);
            if (!$pageVersion) {
                throw new NotFoundHttpException(Yii::t('app', 'The requested page version does not exist.'));
            }
        }

        $pageVersion = $pageVersion ? $pageVersion : $page->getCurrentlyShowingPageVersion();
        $pageVersion = $pageVersion ? $pageVersion : $page->getLastPageVersion();

        if (!$pageVersion) {
            $pageVersion = new PageVersion();
        }

        if ($this->updatePageAndVersion($page, $pageVersion)) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Page updated'));
            return $this->redirect(['pages/view', 'url' => $page->url]);
        }

        return $this->render('create-and-update', [
            'model' => $page,
            'updatePage' => $pageVersion
        ]);
    }

    /**
     * Delete existin page version
     * @param int $id           Page id
     * @param int $versionId    Version id
     * @return Response Response index object from redirection
     */
    public function actionDelete(int $id, int $versionId = null)
    {
        $model = $this->findPage($id);

        if ($versionId) {
            return $this->deletePageVersion($model, $versionId);
        }

        $model->deleted = !$model->deleted;
        if ($model->published) {
            $this->concealPage($model);
        }
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Move page version up among other page versions. Page is after that redirected to index page
     * @param int $id   Page id
     * @return Response Response object from redirection
     */
    public function actionMoveUp(int $id)
    {
        $model = $this->findPage($id);
        $pageAbove = $this->findPageAbovePage($model->order_in_menu);

        if ($pageAbove) {
            $model->order_in_menu = $pageAbove->order_in_menu;
            $pageAbove->order_in_menu = $pageAbove->order_in_menu + 1;

            $model->save();
            $pageAbove->save();
        }

        return $this->redirect(['index']);
    }

    /**
     * Move page version down among other page versions. Page is after that redirected to index page
     * @param int $id   Page id
     * @return Response Response object from redirection
     */
    public function actionMoveDown(int $id)
    {
        $model = $this->findPage($id);
        $pageBelow = $this->findPageBelowPage($model->order_in_menu);

        if ($pageBelow) {
            //we dont simply exchange them because if we delete a page between the two, we assign sequential numbers to them
            $pageBelow->order_in_menu = $model->order_in_menu;
            $model->order_in_menu = $model->order_in_menu + 1;

            $model->save();
            $pageBelow->save();
        }

        return $this->redirect(['index']);
    }

    /**
     * Publish page. After that page will be visible to other users
     * @param int $id           Page id
     * @param int $versionId    Page version id
     * @return Response Response object from redirection
     */
    public function actionPublish(int $id, int $versionId = null)
    {
        $model = $this->findPage($id);

        if ($versionId) {
            return $this->selectSpecificVersion($model, $versionId);
        }

        if ($model->published) {
            $this->concealPage($model);
        } else {
            /** @var PageVersion $selectedOrLastPageVersion */
            $selectedOrLastPageVersion = $model->getSelectedOrLastPageVersion();
            if ($selectedOrLastPageVersion) {
                $selectedOrLastPageVersion->published = true;
                $selectedOrLastPageVersion->save();
            }

            $model->published = true;
            NewsGenerator::pageUpdated($model);
        }

        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Generate pdf of the page version and return to the user.
     * @param int $id           Page id
     * @param int $versionId    Page version id
     * @return Response|string  Response object from redirection if creation was successfull
     *                          or rendering result if not.
     */
    public function actionDownload(int $id, int $versionId = null)
    {
        $page = $this->findPage($id);

        $pageVersion = null;
        if ($versionId) {
            $pageVersion = PageVersion::findOne($versionId);
            if (!$pageVersion) {
                throw new NotFoundHttpException(Yii::t('app', 'The requested page version does not exist.'));
            }
        }

        $pageVersion = $pageVersion ? $pageVersion : $page->getCurrentlyShowingPageVersion();
        if (!$pageVersion) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'There is no file to download!'));
            return $this->redirect(Yii::$app->request->referrer);
        }

        $fileTitle = $page->getTitleBasedOnSiteLanguage();
        $documentToDownload = $pageVersion->getDocumentBasedOnSiteLanguage();

        if ($documentToDownload->content_md) {
            $renderedMarkdown = $documentToDownload->getContentMarkdownProcessed();
            $pdf = Yii::$app->pdf;
            $pdf->filename = $fileTitle . '.pdf';
            $pdf->methods = ['SetHeader' => $fileTitle];
            $pdf->content = $renderedMarkdown;
            return $pdf->render();
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'The content of this page is null, download failed.'));
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /** 
     * Return page below given order number
     * @param int $pageOrderInMenu  Order number
     * @return Page Page below given page with given order number
     */
    protected function findPageBelowPage(int $pageOrderInMenu) : Page
    {
        return Page::find()
            ->where(['>', 'order_in_menu', $pageOrderInMenu])
            ->orderBy(['order_in_menu' => SORT_ASC])
            ->one();
    }

    /** 
     * Return page below given order number
     * @param int $pageOrderInMenu  Order number
     * @return Page Page below given page with given order number
     */
    protected function findPageAbovePage(int $pageOrderInMenu) : Page
    {
        return Page::find()
            ->where(['<', 'order_in_menu', $pageOrderInMenu])
            ->orderBy(['order_in_menu' => SORT_DESC])
            ->one();
    }

    /**
     * Retrieve page with given id
     * @param int $id   Page id
     * @return Page     Page
     * @throws NotFoundHttpException    Exception is thrown if page with given id does not exists, or is deleted
     */
    protected function findPage(int $id) : Page
    {
        $model = Page::findOne($id);
        if ($model == null || $model->deleted) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    private function concealPage(Page $model)
    {
        $this->concealAllPublishedVersions($model);
        $model->published = false;
    }

    private function updatePageAndVersion(Page $page, PageVersion $pageVersion)
    {
        $data = Yii::$app->request->post();
        if ($page->load($data) && $page->save()) {
            if (Yii::$app->request->post('save') === 'newVersion') {
                $pageVersion = new PageVersion();
                $pageVersion->published = true;
            }

            if ($this->updatePageVersionAttributes($data, $page, $pageVersion)) {
                if ($pageVersion->published) {
                    $this->selectSpecificVersion($page, $pageVersion->id);
                }
                return true;
            }
        }

        return false;
    }

    private function updatePageVersionAttributes($data, Page $page, PageVersion $pageVersion)
    {
        $pageVersion->page_id = $page->id;

        $pageVersion->user_id = $data['CSContent']['author_id'];

        $documentCS = new Document();
        $documentCS->content_md = $data['CSContent']['markdown'];
        if (!$documentCS->save()) {
            return false;
        }

        $pageVersion->content_cs = $documentCS->id;

        $documentEN = new Document();
        $documentEN->content_md = $data['ENContent']['markdown'];
        if (!$documentEN->save()) {
            return false;
        }

        $pageVersion->content_en = $documentEN->id;

        return $pageVersion->save();
    }

    private function concealAllPublishedVersions(Page $model)
    {
        foreach ($model->pageVersions as $pageVersion) {
            if ($pageVersion->published) {
                $pageVersion->published = false;
                $pageVersion->save();
            }
        }
    }

    private function selectSpecificVersion(Page $pageModel, int $versionId)
    {
        foreach ($pageModel->pageVersions as $pageVersion) {
            if ($pageVersion->published && $pageVersion->id != $versionId) {
                $pageVersion->published = false;
                $pageVersion->save();
            } else if (!$pageVersion->published && $pageVersion->id == $versionId) {
                $pageVersion->published = true;
                $pageVersion->save();
            }
        }

        if ($pageModel->published)
        {
            NewsGenerator::pageUpdated($pageModel);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    private function deletePageVersion($model, $versionId)
    {
        $pageVersion = PageVersion::findOne($versionId);
        $pageVersion->deleted = true;

        if ($pageVersion->published) {
            $this->concealPage($model);
        }

        $pageVersion->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

    private function renderViewPage(? Page $model, ? PageVersion $pageVersion = null)
    {
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        } else if (!$model->published && !Yii::$app->user->can(Permission::PAGES_MANAGE))
        {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $query = PageVersion::find();
        $pageVersionDataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'page_id' => $model->id,
            'deleted' => false
        ]);

        $pageVersion = $pageVersion ? $pageVersion : $model->getCurrentlyShowingPageVersion();

        return $this->render('view', [
            'model' => $model,
            'pageVersionDataProvider' => $pageVersionDataProvider,
            'pageVersion' => $pageVersion
        ]);
    }

    private function getNextOrderInMenu() : int
    {
        $maxOrderInMenu = Page::find()->max('order_in_menu');
        return $maxOrderInMenu + 1;
    }
}