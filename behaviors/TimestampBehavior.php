<?php
namespace app\behaviors;

use app\consts\Sql;

/**
 * From Yii doc: created_at and updated_at should not appear in the rules() method of the model.
 *
 * Class TimestampBehavior
 * @package app\behaviors
 */
class TimestampBehavior extends \yii\behaviors\TimestampBehavior
{
    /**
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->value = Sql::now();
    }
}